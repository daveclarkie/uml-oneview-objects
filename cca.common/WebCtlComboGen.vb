Public Class WebCtlComboGen

    Dim dr As System.Data.DataRow
    Dim _CodeBehind As String = ""
    Dim _WebControl As String = ""

    Dim _PersonsCodeBehind As String = ""
    Dim _PersonsWebControl As String = ""
    Dim _PersonsFunction As String = ""


    Dim CSI As Integer = 0
    Public ReadOnly Property CodeBehind() As String
        Get
            Return _CodeBehind
        End Get
    End Property

    Public ReadOnly Property WebControl() As String
        Get
            Return _WebControl
        End Get
    End Property

    Public ReadOnly Property PersonsCodeBehind() As String
        Get
            Return _PersonsCodeBehind
        End Get
    End Property

    Public ReadOnly Property PersonsWebControl() As String
        Get
            Return _PersonsWebControl
        End Get
    End Property

    Public ReadOnly Property PersonsFunction() As String
        Get
            Return _PersonsFunction
        End Get
    End Property


    Public Sub New(ByVal MT As String, ByVal ST As String, Optional ByVal includeManage As Boolean = False, Optional ByRef DataClass As DataCommon = Nothing, Optional ByVal iCSI As Integer = 0)
        CSI = iCSI
        Dim Table As DataTable
        Dim LT As String = MT & ST

        Dim CDC As DataCommon = DataClass
        If CDC Is Nothing Then CDC = New DataCommon()
        Dim CO As New SqlClient.SqlCommand("rkg_Combo_All")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@mt", MT)
        CO.Parameters.AddWithValue("@st", ST)

        Table = CDC.ReadDataTable(CO, CSI)

        Dim _wc As New Text.StringBuilder(5000)
        Dim _cb As New Text.StringBuilder(5000)
        Dim _pwc As New Text.StringBuilder(5000)
        Dim _pcb As New Text.StringBuilder(5000)
        Dim _pf As New Text.StringBuilder(5000)

        Dim cp As ColumnStructure
        Dim _pk As String = ""
        Dim _STpk As String = ""
        Dim _tbl As String = ""
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.IsPrimaryKey Then
                _tbl = cp.TableName
                Select Case _tbl
                    Case LT & "s"
                        _pk = cp.ColumnName
                    Case ST & "s"
                        _STpk = cp.ColumnName
                End Select


            End If
        Next

        _wc.AppendLine("<%@ Control Language=""VB"" AutoEventWireup=""false"" EnableViewState=""false"" CodeFile=""" & LT & "_combo_ctl.ascx.vb"" Inherits=""" & LT & "_combo_ctl"" %>")
        Dim ctlAdded As Boolean = False
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.IsForeignKeyControl And Not ctlAdded Then
                ctlAdded = True
            End If
        Next

        _wc.AppendLine("<h5>Detail</h5>")
        _wc.AppendLine("<ul class='formcontrol'>")
        _cb.AppendLine("Partial Class " & LT & "_combo_ctl")
        _cb.AppendLine("   Inherits DaveControl")
        _cb.AppendLine("   Public PSM As MySM")
        _cb.AppendLine("")
        _cb.AppendLine("   Public WithEvents o" & LT & " As Objects." & LT & "s")
        _cb.AppendLine("   Public WithEvents o" & ST & " As Objects." & ST & "s")
        _cb.AppendLine("   Dim CO As SqlClient.SqlCommand")
        _cb.AppendLine("   Dim _CurrentPk as Integer=-1")
        _cb.AppendLine("   Dim _Status as Integer=0")
        _cb.AppendLine("   Dim _STCurrentPk as Integer=-1")
        _cb.AppendLine("   Dim _STStatus as Integer=0")
        _cb.AppendLine("")
        _cb.AppendLine("   Public Sub InitControl() Handles MyBase.InitSpecificControl")
        _cb.AppendLine("       ' Place any page specific initialisation code here")
        _cb.AppendLine("       PSM = SM")
        _cb.AppendLine("       ControlInit")
        _cb.AppendLine("   End Sub")
        _cb.AppendLine("   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl")
        _cb.AppendLine("      ' Place any page specific initialisation code here")
        _cb.AppendLine("   End Sub")
        _cb.AppendLine("")
        _cb.AppendLine("   Public LastError As Exception")
        _cb.AppendLine("   Public ReadOnly Property CurrentPk() as Integer")
        _cb.AppendLine("        Get")
        _cb.AppendLine("            Return _CurrentPk")
        _cb.AppendLine("        End Get")
        _cb.AppendLine("   End Property")
        _cb.AppendLine("")
        _cb.AppendLine("   Public ReadOnly Property STCurrentPk() as Integer")
        _cb.AppendLine("        Get")
        _cb.AppendLine("            Return _STCurrentPk")
        _cb.AppendLine("        End Get")
        _cb.AppendLine("   End Property")
        _cb.AppendLine("")
        _cb.AppendLine("   Public Property Status() as Integer")
        _cb.AppendLine("        Get")
        _cb.AppendLine("            Return _Status")
        _cb.AppendLine("        End Get")
        _cb.AppendLine("        Set (ByVal value As integer)")
        _cb.AppendLine("            _Status=value")
        _cb.AppendLine("            SetControlStatus")
        _cb.AppendLine("        End Set")
        _cb.AppendLine("   End Property")
        _cb.AppendLine("")

        _cb.AppendLine("  Public Sub ControlInit()")
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If Not cp.IsPrimaryKey Then
                Select Case cp.ColumnName
                    Case "creator_fk", "editor_fk", "created", "modified", "rowstatus", MT & "_fk", ST & "_fk"
                    Case Else

                        Select Case cp.DataType
                            Case "Integer"
                                If cp.IsForeignKey Then
                                    If cp.ColumnValidationType = ValidationType.FixedLookUp Then
                                        _cb.AppendLine("CO=new SqlClient.SqlCommand(""rsp_lkp_" & CommonFN.TableFromKey(cp.ColumnName, CDC) & "_" & cp.ColumnValidationExpression & """)")
                                    Else
                                        _cb.AppendLine("CO=new SqlClient.SqlCommand(""rkg_" & CommonFN.TableFromKey(cp.ColumnName, CDC) & "_lookup"")")
                                    End If

                                    _cb.AppendLine("CO.CommandType = CommandType.StoredProcedure")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".DataSource=CDC.ReadDataTable(CO)")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".DataTextField = ""value""")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".DataValueField = ""pk""")
                                    _cb.AppendLine("Try")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".DataBind")
                                    _cb.AppendLine("Catch Ex as Exception")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".SelectedValue=-1")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".DataBind")
                                    _cb.AppendLine("End Try")
                                ElseIf cp.IsForeignKeyControl Then

                                End If
                        End Select
                End Select
            End If
        Next
        _cb.AppendLine("  End Sub")
        _cb.AppendLine("")

        _cb.AppendLine("Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If Not cp.IsPrimaryKey Then
                Select Case cp.ColumnName
                    Case "creator_fk", "editor_fk", "created", "modified", "rowstatus", MT & "_fk", ST & "_fk"
                    Case Else
                        _wc.AppendLine("<li runat=""server"" id=""blk" & cp.ColumnName & """>")
                        _wc.AppendLine("<span class='label'>" & cp.ColumnLabelFull & "</span>")
                        If cp.IsForeignKeyControl Then
                        Else
                            Select Case cp.DataType
                                Case "Integer"
                                    If cp.IsForeignKey Then
                                        _wc.AppendLine("<asp:DropDownList ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_ddl"" />")
                                    Else
                                        _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_num""  />")
                                    End If
                                Case "String"
                                    If cp.MaxLength > 150 Then
                                        _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Multiline"" runat=""server"" cssClass=""input_box"" />")
                                    Else
                                        _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Singleline"" runat=""server"" cssClass=""input_str"" />")
                                    End If
                                Case "Double"
                                    _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_dbl"" />")
                                Case "Char()"
                                    _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Multiline"" runat=""server"" cssClass=""input_box"" />")
                                Case "Byte()"
                                    _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Multiline"" runat=""server"" cssClass=""input_box"" />")
                                Case "Boolean"
                                    _wc.AppendLine("<asp:CheckBox ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_chk"" />")
                                Case "Datetime"
                                    _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_dtm"" />")
                                Case "Object"
                                    _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Multiline"" runat=""server"" cssClass=""input_box"" />")
                            End Select
                        End If
                        _wc.AppendLine("</li>")
                End Select
            End If
        Next
        _wc.AppendLine("</ul>")

        _cb.AppendLine("End Sub")

        _cb.AppendLine("Public Sub SetObjects(ByVal CurrentUser as Integer,ByVal ActualUser as Integer,Optional ByVal pk as Integer=-1)")
        _cb.AppendLine("    If o" & LT & " is Nothing then")
        _cb.AppendLine("        o" & LT & " = new Objects." & LT & "s(currentuser,actualuser,pk,CDC)")
        _cb.AppendLine("        If o" & LT & "." & ST & "_fk>0 then")
        _cb.AppendLine("            o" & ST & " = new Objects." & ST & "s(currentuser,actualuser,o" & LT & "." & ST & "_fk,CDC)")
        _cb.AppendLine("        Else")
        _cb.AppendLine("            o" & ST & " = new Objects." & ST & "s(currentuser,actualuser,-1,CDC)")
        _cb.AppendLine("        End If")
        _cb.AppendLine("    End If")
        _cb.AppendLine("    If o" & ST & " is Nothing then")
        _cb.AppendLine("        If o" & LT & "." & ST & "_fk>0 then")
        _cb.AppendLine("            o" & ST & " = new Objects." & ST & "s(currentuser,actualuser,o" & LT & "." & ST & "_fk,CDC)")
        _cb.AppendLine("        Else")
        _cb.AppendLine("            o" & ST & " = new Objects." & ST & "s(currentuser,actualuser,-1,CDC)")
        _cb.AppendLine("        End If")
        _cb.AppendLine("    End If")
        _cb.AppendLine("End Sub")

        _cb.AppendLine("Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1)")

        _cb.AppendLine("    SetObjects(currentuser,actualuser,pk)")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            _tbl = cp.TableName.TrimEnd("s".ToCharArray)
            If Not cp.IsPrimaryKey Then
                Select Case cp.ColumnName
                    Case "creator_fk", "editor_fk", "created", "modified", "rowstatus" ', (MT & "_fk"), (ST & "_fk")
                    Case MT & "_fk"
                        '_cb.AppendLine("SM.mainId=o" & LT & "." & cp.ColumnName)
                    Case ST & "_fk"
                        _cb.AppendLine("SM.subId=o" & LT & "." & cp.ColumnName)
                    Case Else
                        Select Case cp.DataType
                            Case "Integer"
                                If cp.IsForeignKey Then
                                    _cb.AppendLine("Try")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".SelectedValue=o" & _tbl & "." & cp.ColumnName)
                                    _cb.AppendLine("Catch ex" & cp.ColumnName & " as Exception")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".SelectedValue=-1")
                                    _cb.AppendLine("End Try")
                                ElseIf cp.IsForeignKeyControl Then
                                Else
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onFocus"", ""setNotice('Format:(n)n eg 4736')"")")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onBlur"", ""clearNotice()"")")
                                End If
                            Case "String"
                                _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                            Case "Double"
                                _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                                _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onFocus"", ""setNotice('Format: (n)n(.nn) eg 1234.56')"")")
                                _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onBlur"", ""clearNotice()"")")
                            Case "Char()"
                                _cb.AppendLine("'Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                            Case "Byte()"
                                _cb.AppendLine("'Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                            Case "Boolean"
                                _cb.AppendLine("Me." & cp.ColumnName & ".Checked=o" & _tbl & "." & cp.ColumnName)
                            Case "Datetime"
                                _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName & ".ToString(""dd MMM yyyy"")")
                                _cb.AppendLine("If Me." & cp.ColumnName & ".Text=""01 Jan 0001"" Then Me." & cp.ColumnName & ".Text=""""")
                                _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onFocus"", ""setNotice('Format: D MMM YYYY eg 1 Jan 1980')"")")
                                _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onBlur"", ""clearNotice()"")")
                            Case "Object"
                                _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                        End Select
                End Select
            End If
        Next


        _cb.AppendLine("_CurrentPk=o" & LT & "." & _pk)
        _cb.AppendLine("_STCurrentPk=o" & ST & "." & _STpk)
        _cb.AppendLine("Status=o" & LT & ".rowstatus OR o" & ST & ".rowstatus")
        _cb.AppendLine("End Sub")

        _cb.AppendLine("Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1) as boolean")

        _cb.AppendLine("    SetObjects(currentuser,actualuser,pk)")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            _tbl = cp.TableName.TrimEnd("s".ToCharArray)
            If Not cp.IsPrimaryKey Then
                Select Case cp.ColumnName
                    Case "creator_fk", "editor_fk", "created", "modified", "rowstatus"
                    Case MT & "_fk"
                        _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=SM.targetuser")
                    Case ST & "_fk"
                        _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=SM.subId")
                    Case Else
                        Select Case cp.DataType
                            Case "Integer"
                                If cp.IsForeignKey Then
                                    _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".SelectedValue")
                                ElseIf cp.IsForeignKeyControl Then
                                Else
                                    _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                                End If
                            Case "String"
                                _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                            Case "Double"
                                _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                            Case "Char()"
                                _cb.AppendLine("'o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                            Case "Byte()"
                                _cb.AppendLine("'o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                            Case "Boolean"
                                _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Checked")
                            Case "Datetime"
                                _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=CommonFN.CheckEmptyDate(Me." & cp.ColumnName & ".Text)")
                            Case "Object"
                                _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                        End Select

                End Select
            Else
                'Save sub table

                If _tbl.StartsWith(MT) Then
                    '_cb.AppendLine("Catch MainEx as Exception")
                    '_cb.AppendLine("LastError=MainEx")
                    '_cb.AppendLine("Return False")
                    '_cb.AppendLine("End Try")
                    _cb.AppendLine("Dim STResult as boolean=False")
                    _cb.AppendLine("Try")
                    _cb.AppendLine("STResult=o" & ST & ".Save()")
                    _cb.AppendLine("if not STResult then throw o" & ST & ".LastError")
                    _cb.AppendLine("_STCurrentPk=o" & ST & "." & _STpk)
                    _cb.AppendLine("SM.subId=o" & ST & "." & _STpk)
                    _cb.AppendLine("Catch Ex as Exception")
                    _cb.AppendLine("STResult=False")
                    _cb.AppendLine("LastError=Ex")
                    _cb.AppendLine("SM.subId=-1")
                    _cb.AppendLine("End Try")

                    _cb.AppendLine("Try")
                Else

                    '_cb.AppendLine("Try")

                End If
            End If
        Next

        _cb.AppendLine("Catch MainEx as Exception")
        _cb.AppendLine("LastError=MainEx")
        _cb.AppendLine("End Try")


        _cb.AppendLine("Dim result as boolean=false")
        _cb.AppendLine("Try")
        _cb.AppendLine("Result=o" & LT & ".Save()")
        _cb.AppendLine("if not Result then throw o" & LT & ".LastError")
        _cb.AppendLine("_CurrentPk=o" & LT & "." & _pk)
        _cb.AppendLine("Catch Ex as Exception")
        _cb.AppendLine("Result=False")
        _cb.AppendLine("LastError=Ex")
        _cb.AppendLine("End Try")
        _cb.AppendLine("SM.linkId=_CurrentPk")

        _cb.AppendLine("Status=o" & LT & ".rowstatus OR o" & ST & ".rowstatus")
        _cb.AppendLine("Return result AND STResult")
        _cb.AppendLine("End Function")

        _cb.AppendLine("Public Function Enable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean")
        _cb.AppendLine("If o" & LT & " is Nothing then")
        _cb.AppendLine("o" & LT & " = new Objects." & LT & "s(currentuser,actualuser,pk,CDC)")
        _cb.AppendLine("End If")
        _cb.AppendLine("o" & LT & ".rowstatus=0")
        _cb.AppendLine("Return Save(currentuser,actualuser,pk)")
        _cb.AppendLine("End Function")

        _cb.AppendLine("Public Function Disable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean")
        _cb.AppendLine("If o" & LT & " is Nothing then")
        _cb.AppendLine("o" & LT & " = new Objects." & LT & "s(currentuser,actualuser,pk,CDC)")
        _cb.AppendLine("End If")
        _cb.AppendLine("o" & LT & ".rowstatus=1")
        _cb.AppendLine("Return Save(currentuser,actualuser,pk)")
        _cb.AppendLine("End Function")


        _cb.AppendLine("Public Function Delete(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean")
        _cb.AppendLine("If o" & LT & " is Nothing then")
        _cb.AppendLine("o" & LT & " = new Objects." & LT & "s(currentuser,actualuser,pk,CDC)")
        _cb.AppendLine("End If")
        _cb.AppendLine("o" & LT & ".rowstatus=2")
        _cb.AppendLine("Return Save(currentuser,actualuser,pk)")
        _cb.AppendLine("End Function")



        _cb.AppendLine("Public Sub SetControlStatus()")
        _cb.AppendLine("  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)")
        _cb.AppendLine("  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,Me.GetType().ToString,CDC)")
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            _tbl = cp.TableName.TrimEnd("s".ToCharArray)
            If Not cp.IsPrimaryKey Then
                Select Case cp.ColumnName
                    Case "creator_fk", "editor_fk", "created", "modified", "rowstatus", MT & "_fk", ST & "_fk"
                    Case Else
                        _cb.AppendLine("Me." & cp.ColumnName & ".Enabled=IIF(WL>=" & cp.ColumnSecurityLevel & ",True,False)")
                        _cb.AppendLine("Me.blk" & cp.ColumnName & ".Visible=IIF(RL>=" & cp.ColumnSecurityLevel & ",True,False)")
                End Select
            End If
        Next
        _cb.AppendLine("End Sub")

        _cb.AppendLine("End Class")

        _WebControl = _wc.ToString
        _CodeBehind = _cb.ToString


        If MT.ToLower = "user" Then


            Dim SecTable As String = ST

            _pcb.AppendLine("Imports System.Data.SqlClient")
            _pcb.AppendLine("")
            _pcb.AppendLine("Partial Class controls_combo_person" & SecTable)
            _pcb.AppendLine("   Inherits DaveControl")
            _pcb.AppendLine("   Public PSM As MySM")
            _pcb.AppendLine("")
            _pcb.AppendLine("   Public Sub InitControl() Handles MyBase.InitSpecificControl")
            _pcb.AppendLine("       ' Place any page specific initialisation code here")
            _pcb.AppendLine("       PSM = SM")
            _pcb.AppendLine("   End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl")
            _pcb.AppendLine("      ' Place any page specific initialisation code here")
            _pcb.AppendLine("   End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Dim dt As DataTable")
            _pcb.AppendLine("    Dim CO As SqlCommand")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Dim _PageMode As Local.Action = Local.Action.None")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Dim saved As Boolean = False")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Property PageMode() As Local.Action")
            _pcb.AppendLine("        Get")
            _pcb.AppendLine("            Return _PageMode")
            _pcb.AppendLine("        End Get")
            _pcb.AppendLine("        Set(ByVal value As Local.Action)")
            _pcb.AppendLine("            _PageMode = value")
            _pcb.AppendLine("            Reload()")
            _pcb.AppendLine("        End Set")
            _pcb.AppendLine("    End Property")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Property DetailVisible() As Boolean")
            _pcb.AppendLine("        Get")
            _pcb.AppendLine("            Return " & LT & "_combo_ctl.Visible")
            _pcb.AppendLine("        End Get")
            _pcb.AppendLine("        Set(ByVal value As Boolean)")
            If includeManage Then
                _pcb.AppendLine("            If Not (saved And value) Then " & LT & "_combo_ctl.Visible = value : " & LT & "_manage_ctl.Visible = value")
                _pcb.AppendLine("            If Not (value) Then " & LT & "_combo_ctl.Visible = value : " & LT & "_manage_ctl.Visible = value")
            Else
                _pcb.AppendLine("            If Not (saved And value) Then " & LT & "_combo_ctl.Visible = value")
                _pcb.AppendLine("            If Not (value) Then " & LT & "_combo_ctl.Visible = value")
            End If
            _pcb.AppendLine("        End Set")
            _pcb.AppendLine("    End Property")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub Reload()")
            _pcb.AppendLine("        Dim pk As Integer = -1")
            _pcb.AppendLine("")
            _pcb.AppendLine("         If Integer.TryParse(Request.QueryString(""spk""), pk) Then")
            _pcb.AppendLine("             SM.linkId = pk")
            _pcb.AppendLine("             If SM.linkId = -1 then SM.subId=-1")
            _pcb.AppendLine("         End If")
            _pcb.AppendLine("")
            _pcb.AppendLine("        DetailVisible=True")
            _pcb.AppendLine("        Select Case PageMode")
            _pcb.AppendLine("            Case Local.Action.Save")
            _pcb.AppendLine("                SaveItem()")
            _pcb.AppendLine("                saved=True")
            _pcb.AppendLine("                DetailVisible=False")
            _pcb.AppendLine("            Case Local.Action.Read")
            _pcb.AppendLine("                LoadItem()")
            _pcb.AppendLine("                LoadList()")
            _pcb.AppendLine("            Case Local.Action.None")
            _pcb.AppendLine("                LoadList()")
            _pcb.AppendLine("                DetailVisible=False")
            _pcb.AppendLine("        End Select")
            '_pcb.AppendLine("        SetSubPageMode()")
            _pcb.AppendLine("")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Private Sub SetSubPageMode()")
            If includeManage Then _pcb.AppendLine("        user" & SecTable & "_manage_ctl.PageMode = PageLoadMode.LoadList")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub NewItem()")
            _pcb.AppendLine("        SM.linkId = -1")
            _pcb.AppendLine("        SM.subId = -1")
            _pcb.AppendLine("        user" & SecTable & "_combo_ctl.Read(SM.CurrentUser, SM.ActualUser,SM.linkId)")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub LoadList()")
            _pcb.AppendLine("        CO = New SqlCommand()")
            _pcb.AppendLine("        CO.CommandType = CommandType.StoredProcedure")
            _pcb.AppendLine("        CO.CommandText = ""rsp_user" & SecTable & "s""")
            _pcb.AppendLine("        CO.Parameters.AddWithValue(""@user_pk"", SM.TargetUser)")
            _pcb.AppendLine("        dt = CDC.ReadDataTable(CO)")
            _pcb.AppendLine("        rpt.DataSource = dt")
            _pcb.AppendLine("        rpt.DataBind()")
            _pcb.AppendLine("        dt.Dispose()")
            _pcb.AppendLine("        CO.Dispose()")
            If includeManage Then _pcb.AppendLine("        user" & SecTable & "_manage_ctl.Reload()")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub LoadItem()")
            _pcb.AppendLine("        If SM.TargetUser > 0 Then")
            _pcb.AppendLine("            user" & SecTable & "_combo_ctl.Read(SM.CurrentUser, SM.ActualUser, SM.linkId)")
            _pcb.AppendLine("        End If")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub SaveItem()")
            _pcb.AppendLine("        Dim errMsg As String()")
            _pcb.AppendLine("        Dim msg As String")
            _pcb.AppendLine("        Dim mk As Boolean = False")
            _pcb.AppendLine("        Try")
            _pcb.AppendLine("            mk =user" & SecTable & "_combo_ctl.Save(SM.CurrentUser, SM.ActualUser, SM.linkId)")
            _pcb.AppendLine("        Catch ex As Exception")
            _pcb.AppendLine("")
            _pcb.AppendLine("        End Try")
            _pcb.AppendLine("")
            _pcb.AppendLine("        If Not mk Then")
            _pcb.AppendLine("            errMsg = user" & SecTable & "_combo_ctl.LastError.Message.Split((vbCr & "":"").ToCharArray())")
            _pcb.AppendLine("            If errMsg.Length > 1 Then")
            _pcb.AppendLine("                msg = ""This record has not been saved because the "" & errMsg(0).ToLower() & "".<br />"" & ""Please check the "" & errMsg(3) & "" field; "" & errMsg(1)")
            _pcb.AppendLine("            Else")
            _pcb.AppendLine("                msg = ""This record has not been saved. <br /> System reports the following message:-<br />"" & errMsg(0).Replace("""""""",""`"").Replace(""'"", ""`"") & "".""")
            _pcb.AppendLine("            End If")
            _pcb.AppendLine("            AddNotice(""setError('"" & msg & ""');"")")
            _pcb.AppendLine("        Else")
            _pcb.AppendLine("            AddNotice(""setNotice('Record Saved');"")")
            _pcb.AppendLine("        End If")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("End Class")



            _pwc.AppendLine("<%@ Control Language=""VB"" AutoEventWireup=""false"" CodeFile=""person" & SecTable & "s.ascx.vb"" Inherits=""controls_combo_person" & SecTable & """ %>")
            _pwc.AppendLine("<%@ Register Src=""~/controls/combo/user" & SecTable & "_combo_ctl.ascx"" TagName=""combo"" TagPrefix=""uc1"" %>")
            If includeManage Then _pwc.AppendLine("<%@ Register Src=""~/controls/combo/user" & SecTable & "_manage_ctl.ascx"" TagName=""manage"" TagPrefix=""uc2"" %>")
            _pwc.AppendLine("<h5>Stored " & SecTable & "s</h5>    ")
            _pwc.AppendLine("<asp:Repeater EnableViewState=""false"" ID=""rpt"" runat=""server"">")
            _pwc.AppendLine("    <HeaderTemplate><ul class=""combolist""></HeaderTemplate> ")
            _pwc.AppendLine("    <FooterTemplate></ul></FooterTemplate> ")
            _pwc.AppendLine("    <ItemTemplate>")
            _pwc.AppendLine("        <li id='node_<%# Container.DataItem(""user" & SecTable & "_pk"") %>'>")
            _pwc.AppendLine("            <a class=""remove"" title=""Remove""  href='javascript:removeNode(<%# Container.DataItem(""user" & SecTable & "_pk"") %>)'>X</a>")
            Dim sc As String = ""
            Select Case SecTable
                Case "location" : sc = "lc"
                Case "phone" : sc = "ph"
                Case "email" : sc = "ml"
            End Select
            _pwc.AppendLine("            <a href='persons.aspx?pk=<%=PSM.TargetUser%>&md=" & sc & "&spk=<%# Container.DataItem(""user" & SecTable & "_pk"") %><%=Local.noCache %>'><%#Container.DataItem(""linktext"")%></a><br />")
            _pwc.AppendLine("            <span class=""item""><%#Container.DataItem(""subtext"")%></span></li>")
            _pwc.AppendLine("    </ItemTemplate>")
            _pwc.AppendLine("</asp:Repeater>")
            _pwc.AppendLine("")
            If includeManage Then _pwc.AppendLine("<uc2:manage ID=""user" & SecTable & "_manage_ctl"" runat=""server"" />")
            _pwc.AppendLine("")
            _pwc.AppendLine("<uc1:combo ID=""user" & SecTable & "_combo_ctl"" runat=""server"" />")
            _pwc.AppendLine("")
            _pwc.AppendLine("")
            _pwc.AppendLine("")
            _pwc.AppendLine("")
            _pwc.AppendLine("<script type=""text/javascript"" language=""javascript"">")
            _pwc.AppendLine("")
            _pwc.AppendLine("function removeNode(pk){")
            _pwc.AppendLine("    var selItem=""node_""+pk;")
            _pwc.AppendLine("    setReqHttp();")
            _pwc.AppendLine("    if (hasXmlhttp==true){")
            _pwc.AppendLine("        xmlhttp.open(""GET"",""/functions/remove" & SecTable & ".ashx?pk=""+pk+noCache(),true);")
            _pwc.AppendLine("        xmlhttp.send(null);   ")
            _pwc.AppendLine("        var nd=document.getElementById(selItem);")
            _pwc.AppendLine("        nd.parentNode.removeChild(nd);   ")
            _pwc.AppendLine("    }")
            _pwc.AppendLine("   else")
            _pwc.AppendLine("   {")
            _pwc.AppendLine("    alert(""Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection."");")
            _pwc.AppendLine("   } ")
            _pwc.AppendLine("}")
            _pwc.AppendLine("")
            _pwc.AppendLine("</script>")


            _pf.AppendLine("<%@ WebHandler Language=""VB"" Class=""functions_remove" & SecTable & """ %>")
            _pf.AppendLine("Public Class functions_remove" & SecTable)
            _pf.AppendLine("    Implements IHttpHandler, IRequiresSessionState")
            _pf.AppendLine("    Dim CDC As DataCommon")
            _pf.AppendLine("    Dim SM As MySM")
            _pf.AppendLine("    Dim Request As HttpRequest")
            _pf.AppendLine("    Dim Response As HttpResponse")
            _pf.AppendLine("")
            _pf.AppendLine("    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest")
            _pf.AppendLine("        Request = context.Request")
            _pf.AppendLine("        Response = context.Response")
            _pf.AppendLine("        CDC = New DataCommon(Local.AuthURL)")
            _pf.AppendLine("        SM = New MySM(context.Session.SessionID, Request, CDC)")
            _pf.AppendLine("        Page_Load()")
            _pf.AppendLine("    End Sub")
            _pf.AppendLine("")
            _pf.AppendLine("    Public Sub VerifyRenderingInServerForm(ByRef control As Control)")
            _pf.AppendLine("    End Sub")
            _pf.AppendLine("")
            _pf.AppendLine("    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable")
            _pf.AppendLine("        Get")
            _pf.AppendLine("            Return False")
            _pf.AppendLine("        End Get")
            _pf.AppendLine("    End Property")
            _pf.AppendLine("")
            _pf.AppendLine("    Public Sub Page_Load()")
            _pf.AppendLine("        Dim CRQ As Object = Request.QueryString")
            _pf.AppendLine("        Dim Obj As Objects.user" & SecTable & "s")
            _pf.AppendLine("        Dim Msg as string=""OK""")
            _pf.AppendLine("        Try")
            _pf.AppendLine("            Obj = New Objects.user" & SecTable & "s(SM.CurrentUser, SM.ActualUser, Integer.Parse(CRQ(""pk"")), CDC)")
            _pf.AppendLine("            If Not Obj.Disable() then")
            _pf.AppendLine("                Msg=""FAIL:"" & Obj.LastError.Message")
            _pf.AppendLine("            End If")
            _pf.AppendLine("        Catch LE as Exception")
            _pf.AppendLine("            Msg=""FAIL:"" & LE.Message")
            _pf.AppendLine("        End Try")
            _pf.AppendLine("        Response.Write(Msg)")
            _pf.AppendLine("    End Sub")
            _pf.AppendLine("")
            _pf.AppendLine("End Class")

            _PersonsWebControl = _pwc.ToString
            _PersonsCodeBehind = _pcb.ToString
            _PersonsFunction = _pf.ToString

        Else


            
            Dim SecTable As String = ST
            Dim MstTable As String = MT

            _pcb.AppendLine("Imports System.Data.SqlClient")
            _pcb.AppendLine("")
            _pcb.AppendLine("Partial Class controls_combo_" & MstTable & SecTable)
            _pcb.AppendLine("   Inherits DaveControl")
            _pcb.AppendLine("   Public PSM As MySM")
            _pcb.AppendLine("")
            _pcb.AppendLine("   Public Sub InitControl() Handles MyBase.InitSpecificControl")
            _pcb.AppendLine("       ' Place any page specific initialisation code here")
            _pcb.AppendLine("       PSM = SM")
            _pcb.AppendLine("   End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl")
            _pcb.AppendLine("      ' Place any page specific initialisation code here")
            _pcb.AppendLine("   End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Dim dt As DataTable")
            _pcb.AppendLine("    Dim CO As SqlCommand")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Dim _PageMode As Local.Action = Local.Action.None")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Dim saved As Boolean = False")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Property PageMode() As Local.Action")
            _pcb.AppendLine("        Get")
            _pcb.AppendLine("            Return _PageMode")
            _pcb.AppendLine("        End Get")
            _pcb.AppendLine("        Set(ByVal value As Local.Action)")
            _pcb.AppendLine("            _PageMode = value")
            _pcb.AppendLine("            Reload()")
            _pcb.AppendLine("        End Set")
            _pcb.AppendLine("    End Property")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Property DetailVisible() As Boolean")
            _pcb.AppendLine("        Get")
            _pcb.AppendLine("            Return " & LT & "_combo_ctl.Visible")
            _pcb.AppendLine("        End Get")
            _pcb.AppendLine("        Set(ByVal value As Boolean)")
            If includeManage Then
                _pcb.AppendLine("            If Not (saved And value) Then " & LT & "_combo_ctl.Visible = value : " & LT & "_manage_ctl.Visible = value")
                _pcb.AppendLine("            If Not (value) Then " & LT & "_combo_ctl.Visible = value : " & LT & "_manage_ctl.Visible = value")
            Else
                _pcb.AppendLine("            If Not (saved And value) Then " & LT & "_combo_ctl.Visible = value")
                _pcb.AppendLine("            If Not (value) Then " & LT & "_combo_ctl.Visible = value")
            End If
            _pcb.AppendLine("        End Set")
            _pcb.AppendLine("    End Property")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub Reload()")
            _pcb.AppendLine("        Dim pk As Integer = -1")
            _pcb.AppendLine("")
            _pcb.AppendLine("         If Integer.TryParse(Request.QueryString(""spk""), pk) Then")
            _pcb.AppendLine("             SM.linkId = pk")
            _pcb.AppendLine("             If SM.linkId = -1 then SM.subId=-1")
            _pcb.AppendLine("         End If")
            _pcb.AppendLine("")
            _pcb.AppendLine("        DetailVisible=True")
            _pcb.AppendLine("        Select Case PageMode")
            _pcb.AppendLine("            Case Local.Action.Save")
            _pcb.AppendLine("                SaveItem()")
            _pcb.AppendLine("                saved=True")
            _pcb.AppendLine("                DetailVisible=False")
            _pcb.AppendLine("            Case Local.Action.Read")
            _pcb.AppendLine("                LoadItem()")
            _pcb.AppendLine("                LoadList()")
            _pcb.AppendLine("            Case Local.Action.None")
            _pcb.AppendLine("                LoadList()")
            _pcb.AppendLine("                DetailVisible=False")
            _pcb.AppendLine("        End Select")
            '_pcb.AppendLine("        SetSubPageMode()")
            _pcb.AppendLine("")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Private Sub SetSubPageMode()")
            If includeManage Then _pcb.AppendLine("        user" & SecTable & "_manage_ctl.PageMode = PageLoadMode.LoadList")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub NewItem()")
            _pcb.AppendLine("        SM.linkId = -1")
            _pcb.AppendLine("        SM.subId = -1")
            _pcb.AppendLine("        " & MstTable & SecTable & "_combo_ctl.Read(SM.CurrentUser, SM.ActualUser,SM.linkId)")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub LoadList()")
            _pcb.AppendLine("        CO = New SqlCommand()")
            _pcb.AppendLine("        CO.CommandType = CommandType.StoredProcedure")
            _pcb.AppendLine("        CO.CommandText = ""rsp_" & MstTable & SecTable & "s""")
            _pcb.AppendLine("        CO.Parameters.AddWithValue(""@" & MstTable & "_pk"", SM.Target" & MstTable & ")")
            _pcb.AppendLine("        dt = CDC.ReadDataTable(CO)")
            _pcb.AppendLine("        rpt.DataSource = dt")
            _pcb.AppendLine("        rpt.DataBind()")
            _pcb.AppendLine("        dt.Dispose()")
            _pcb.AppendLine("        CO.Dispose()")
            If includeManage Then _pcb.AppendLine("        " & MstTable & SecTable & "_manage_ctl.Reload()")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub LoadItem()")
            _pcb.AppendLine("        If SM.Target" & MstTable & " > 0 Then")
            _pcb.AppendLine("            " & MstTable & SecTable & "_combo_ctl.Read(SM.CurrentUser, SM.ActualUser, SM.Target" & MstTable & ")")
            _pcb.AppendLine("        End If")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("    Public Sub SaveItem()")
            _pcb.AppendLine("        Dim errMsg As String()")
            _pcb.AppendLine("        Dim msg As String")
            _pcb.AppendLine("        Dim mk As Boolean = False")
            _pcb.AppendLine("        Try")
            _pcb.AppendLine("            mk =" & MstTable & SecTable & "_combo_ctl.Save(SM.CurrentUser, SM.ActualUser, SM.Target" & MstTable & ")")
            _pcb.AppendLine("        Catch ex As Exception")
            _pcb.AppendLine("")
            _pcb.AppendLine("        End Try")
            _pcb.AppendLine("")
            _pcb.AppendLine("        If Not mk Then")
            _pcb.AppendLine("            errMsg = " & MstTable & SecTable & "_combo_ctl.LastError.Message.Split((vbCr & "":"").ToCharArray())")
            _pcb.AppendLine("            If errMsg.Length > 1 Then")
            _pcb.AppendLine("                msg = ""This record has not been saved because the "" & errMsg(0).ToLower() & "".<br />"" & ""Please check the "" & errMsg(3) & "" field; "" & errMsg(1)")
            _pcb.AppendLine("            Else")
            _pcb.AppendLine("                msg = ""This record has not been saved. <br /> System reports the following message:-<br />"" & errMsg(0).Replace("""""""",""`"").Replace(""'"", ""`"") & "".""")
            _pcb.AppendLine("            End If")
            _pcb.AppendLine("            AddNotice(""setError('"" & msg & ""');"")")
            _pcb.AppendLine("        Else")
            _pcb.AppendLine("            AddNotice(""setNotice('Record Saved');"")")
            _pcb.AppendLine("        End If")
            _pcb.AppendLine("    End Sub")
            _pcb.AppendLine("")
            _pcb.AppendLine("End Class")



            _pwc.AppendLine("<%@ Control Language=""VB"" AutoEventWireup=""false"" CodeFile=""" & MstTable & SecTable & "s.ascx.vb"" Inherits=""controls_combo_" & MstTable & SecTable & """ %>")
            _pwc.AppendLine("<%@ Register Src=""~/controls/combo/" & MstTable & SecTable & "_combo_ctl.ascx"" TagName=""combo"" TagPrefix=""uc1"" %>")
            If includeManage Then _pwc.AppendLine("<%@ Register Src=""~/controls/combo/" & MstTable & SecTable & "_manage_ctl.ascx"" TagName=""manage"" TagPrefix=""uc2"" %>")
            _pwc.AppendLine("<h5>Stored " & SecTable & "s</h5>    ")
            _pwc.AppendLine("<asp:Repeater EnableViewState=""false"" ID=""rpt"" runat=""server"">")
            _pwc.AppendLine("    <HeaderTemplate><ul class=""combolist""></HeaderTemplate> ")
            _pwc.AppendLine("    <FooterTemplate></ul></FooterTemplate> ")
            _pwc.AppendLine("    <ItemTemplate>")
            _pwc.AppendLine("        <li id='node_<%# Container.DataItem(""" & MstTable & SecTable & "_pk"") %>'>")
            _pwc.AppendLine("            <a class=""remove"" title=""Remove""  href='javascript:removeNode(<%# Container.DataItem(""" & MstTable & SecTable & "_pk"") %>)'>X</a>")
            Dim sc As String = ""
            Select Case SecTable
                Case "site" : sc = "st"
            End Select
            _pwc.AppendLine("            <a href='agreements.aspx?pk=<%=PSM.TargetAgreement%>&md=" & sc & "&spk=<%# Container.DataItem(""" & MstTable & SecTable & "_pk"") %><%=Local.noCache %>'><%#Container.DataItem(""linktext"")%></a><br />")
            _pwc.AppendLine("            <span class=""item""><%#Container.DataItem(""subtext"")%></span></li>")
            _pwc.AppendLine("    </ItemTemplate>")
            _pwc.AppendLine("</asp:Repeater>")
            _pwc.AppendLine("")
            If includeManage Then _pwc.AppendLine("<uc2:manage ID=""" & MstTable & SecTable & "_manage_ctl"" runat=""server"" />")
            _pwc.AppendLine("")
            _pwc.AppendLine("<uc1:combo ID=""" & MstTable & SecTable & "_combo_ctl"" runat=""server"" />")
            _pwc.AppendLine("")
            _pwc.AppendLine("")
            _pwc.AppendLine("")
            _pwc.AppendLine("")
            _pwc.AppendLine("<script type=""text/javascript"" language=""javascript"">")
            _pwc.AppendLine("")
            _pwc.AppendLine("function removeNode(pk){")
            _pwc.AppendLine("    var selItem=""node_""+pk;")
            _pwc.AppendLine("    setReqHttp();")
            _pwc.AppendLine("    if (hasXmlhttp==true){")
            _pwc.AppendLine("        xmlhttp.open(""GET"",""/functions/remove" & SecTable & ".ashx?pk=""+pk+noCache(),true);")
            _pwc.AppendLine("        xmlhttp.send(null);   ")
            _pwc.AppendLine("        var nd=document.getElementById(selItem);")
            _pwc.AppendLine("        nd.parentNode.removeChild(nd);   ")
            _pwc.AppendLine("    }")
            _pwc.AppendLine("   else")
            _pwc.AppendLine("   {")
            _pwc.AppendLine("    alert(""Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection."");")
            _pwc.AppendLine("   } ")
            _pwc.AppendLine("}")
            _pwc.AppendLine("")
            _pwc.AppendLine("</script>")


            _pf.AppendLine("<%@ WebHandler Language=""VB"" Class=""functions_remove" & SecTable & """ %>")
            _pf.AppendLine("Public Class functions_remove" & SecTable)
            _pf.AppendLine("    Implements IHttpHandler, IRequiresSessionState")
            _pf.AppendLine("    Dim CDC As DataCommon")
            _pf.AppendLine("    Dim SM As MySM")
            _pf.AppendLine("    Dim Request As HttpRequest")
            _pf.AppendLine("    Dim Response As HttpResponse")
            _pf.AppendLine("")
            _pf.AppendLine("    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest")
            _pf.AppendLine("        Request = context.Request")
            _pf.AppendLine("        Response = context.Response")
            _pf.AppendLine("        CDC = New DataCommon")
            _pf.AppendLine("        SM = New MySM(context.Session.SessionID, Request, CDC)")
            _pf.AppendLine("        Page_Load()")
            _pf.AppendLine("    End Sub")
            _pf.AppendLine("")
            _pf.AppendLine("    Public Sub VerifyRenderingInServerForm(ByRef control As Control)")
            _pf.AppendLine("    End Sub")
            _pf.AppendLine("")
            _pf.AppendLine("    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable")
            _pf.AppendLine("        Get")
            _pf.AppendLine("            Return False")
            _pf.AppendLine("        End Get")
            _pf.AppendLine("    End Property")
            _pf.AppendLine("")
            _pf.AppendLine("    Public Sub Page_Load()")
            _pf.AppendLine("        Dim CRQ As Object = Request.QueryString")
            _pf.AppendLine("        Dim Obj As Objects." & MstTable & SecTable & "s")
            _pf.AppendLine("        Dim Msg as string=""OK""")
            _pf.AppendLine("        Try")
            _pf.AppendLine("            Obj = New Objects." & MstTable & SecTable & "s(SM.CurrentUser, SM.ActualUser, Integer.Parse(CRQ(""pk"")), CDC)")
            _pf.AppendLine("            If Not Obj.Disable() then")
            _pf.AppendLine("                Msg=""FAIL:"" & Obj.LastError.Message")
            _pf.AppendLine("            End If")
            _pf.AppendLine("        Catch LE as Exception")
            _pf.AppendLine("            Msg=""FAIL:"" & LE.Message")
            _pf.AppendLine("        End Try")
            _pf.AppendLine("        Response.Write(Msg)")
            _pf.AppendLine("    End Sub")
            _pf.AppendLine("")
            _pf.AppendLine("End Class")

            _PersonsWebControl = _pwc.ToString
            _PersonsCodeBehind = _pcb.ToString
            _PersonsFunction = _pf.ToString

        End If




    End Sub
End Class
