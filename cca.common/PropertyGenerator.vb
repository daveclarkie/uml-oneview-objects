Public Class PropertyGenerator

    Dim dr As System.Data.DataRow
    Dim _Properties As String = ""

    Public ReadOnly Property Properties() As String
        Get
            Return _Properties
        End Get
    End Property

    Public Sub New(ByRef Table As DataTable)

        Dim _sb As New Text.StringBuilder(5000)
        Dim _sb_d As New Text.StringBuilder(500)
        Const cTab As String = ChrW(9)
        Dim cp As ColumnStructure

        _sb.AppendLine("Dim _ReadingData as Boolean=False")
        _sb.AppendLine("Dim _LastError as Exception=Nothing")
        _sb.AppendLine("")

        _sb.AppendLine("Public ReadOnly Property LastError() as Exception")
        _sb.AppendLine(cTab & "Get")
        _sb.AppendLine(cTab & cTab & "Return _LastError")
        _sb.AppendLine(cTab & "End Get")
        _sb.AppendLine("End Property")
        _sb.AppendLine("")

        _sb_d.AppendLine("Dim _IsDirty as Boolean=False")
        _sb_d.AppendLine("Public ReadOnly Property IsDirty() as Boolean")
        _sb_d.AppendLine(cTab & "Get")
        _sb_d.AppendLine(cTab & cTab & "_IsDirty=False")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.DefaultValue Is Nothing Or cp.ColumnPosition < 6 Then
                _sb.AppendLine("Dim _" & cp.ColumnName & " as " & cp.DataType)
            Else
                _sb.Append("Dim _" & cp.ColumnName & " as " & cp.DataType)
                Select Case cp.DataType
                    Case "Integer", "Double"
                        _sb.Append(" = " & cp.DefaultValue.ToString)
                    Case "String"
                        If Not cp.DefaultValue.contains("[dbo].[") Then _sb.Append(" = """ & cp.DefaultValue.ToString & """")
                    Case "Datetime"
                        If cp.DefaultValue.ToString.ToLower.StartsWith("getdate") = False Then
                            _sb.Append(" = """ & cp.DefaultValue.ToString & """")
                        End If
                    Case "Boolean"
                        If cp.DefaultValue = 1 Then
                            _sb.Append(" = True")
                        Else
                            _sb.Append(" = False")
                        End If
                End Select
            End If
            _sb.AppendLine("")

            _sb.AppendLine("Dim _" & cp.ColumnName & "_d as Boolean=False")
            _sb.AppendLine("Public Event " & cp.ColumnName & "Changed()")
            _sb.AppendLine("Public " & IIf(cp.IsPrimaryKey, "ReadOnly ", "") & "Property " & cp.ColumnName & "() as " & cp.DataType)
            _sb.AppendLine(cTab & "Get")
            _sb.AppendLine(cTab & cTab & "Return _" & cp.ColumnName)
            _sb.AppendLine(cTab & "End Get")
            If Not cp.IsPrimaryKey Then
                _sb.AppendLine(cTab & "Set (ByVal value As " & cp.DataType & ")")
                _sb.AppendLine(cTab & cTab & "Dim _d as boolean=false")
                If cp.IsForeignKey Then
                    _sb.AppendLine(cTab & cTab & "If value=0 or value=nothing Then value=-1")
                End If
                Select Case cp.DataType
                    Case "Char()", "Byte()", "Object"
                        _sb.AppendLine(cTab & cTab & "If not _" & cp.ColumnName & " is value Then _d=True")
                    Case Else
                        _sb.AppendLine(cTab & cTab & "If _" & cp.ColumnName & " <> value Then _d=True")
                End Select

                _sb.AppendLine(cTab & cTab & "If not _ReadingData=True Then")
                If cp.IsForeignKey Then
                    Select Case cp.ColumnValidationType
                        Case ValidationType.Required
                            _sb.AppendLine(cTab & cTab & "If Not CommonFN.IsValidPk(""" & cp.ColumnName & """,value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr(""" & cp.ColumnLabel & """), CStr(""Validation failed: Required Field""))")
                        Case Else
                            _sb.AppendLine(cTab & cTab & "If Not CommonFN.IsValidPk(""" & cp.ColumnName & """,value,CDC) Then Throw New ArgumentOutOfRangeException(CStr(""" & cp.ColumnLabel & """), CStr(""Validation failed: Invalid Foreign Key""))")
                    End Select
                Else
                    Select Case cp.ColumnValidationType
                        Case ValidationType.LookUp
                            _sb.AppendLine(cTab & cTab & "If Not CommonFN.IsValidPk(""" & cp.ColumnName & """,value,CDC) Then Throw New ArgumentOutOfRangeException(CStr(""" & cp.ColumnLabel & """), CStr(""Validation failed: Invalid Foreign Key""))")
                        Case ValidationType.Range
                            _sb.AppendLine(cTab & cTab & "Select Case value")
                            _sb.AppendLine(cTab & cTab & cTab & "Case " & cp.ColumnValidationExpression)
                            _sb.AppendLine(cTab & cTab & cTab & "Case Else : Throw New ArgumentOutOfRangeException(CStr(""" & cp.ColumnLabel & """), CStr(""Validation failed: "" & value & "" is not in the range of " & cp.ColumnValidationExpression & "))")
                            _sb.AppendLine(cTab & cTab & "End Select")
                        Case ValidationType.Required
                            If cp.DataType = "Datetime" Then
                                _sb.AppendLine(cTab & cTab & "If value = #12:00:00 AM# Then Throw New ArgumentOutOfRangeException(CStr(""" & cp.ColumnLabel & """), CStr(""Validation failed: Required Field""))")
                            Else
                                _sb.AppendLine(cTab & cTab & "If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr(""" & cp.ColumnLabel & """), CStr(""Validation failed: Required Field""))")
                            End If
                        Case ValidationType.Regex
                            _sb.AppendLine(cTab & cTab & "Dim _rx As New Text.RegularExpressions.Regex(""" & cp.ColumnValidationExpression & """)")
                            _sb.AppendLine(cTab & cTab & "If Not _rx.IsMatch(value) Then Throw New ArgumentOutOfRangeException(CStr(""" & cp.ColumnLabel & """), CStr(""Validation failed: "" & value))")
                        Case ValidationType.None
                        Case Else
                    End Select
                End If
                _sb.AppendLine(cTab & cTab & "If _" & cp.ColumnName & " <> value then RaiseEvent " & cp.ColumnName & "Changed")
                _sb.AppendLine(cTab & cTab & "End If")
                _sb.AppendLine(cTab & cTab & "_" & cp.ColumnName & " = value")
                _sb.AppendLine(cTab & cTab & "If _ReadingData=True Then _d=False")
                _sb.AppendLine(cTab & cTab & "_" & cp.ColumnName & "_d=_d")
                _sb.AppendLine(cTab & "End Set")
            End If
            _sb.AppendLine("End Property")
            _sb.AppendLine("")

            If cp.IsForeignKeyControl Then
                _sb.AppendLine("Public Property " & cp.ColumnName.Replace("_fx", "_fk") & "() as " & cp.DataType)
                _sb.AppendLine(cTab & "Get")
                _sb.AppendLine(cTab & cTab & "Return _" & cp.ColumnName)
                _sb.AppendLine(cTab & "End Get")
                _sb.AppendLine(cTab & "Set (ByVal value As " & cp.DataType & ")")
                _sb.AppendLine(cTab & cTab & cp.ColumnName & " = value")
                _sb.AppendLine(cTab & "End Set")
                _sb.AppendLine("End Property")
                _sb.AppendLine("")
            End If


            _sb_d.AppendLine(cTab & cTab & "_IsDirty = _IsDirty OR _" & cp.ColumnName & "_d")
        Next

        _sb_d.AppendLine(cTab & cTab & "Return _IsDirty")
        _sb_d.AppendLine(cTab & "End Get")
        _sb_d.AppendLine("End Property")
        _sb.AppendLine(_sb_d.ToString)
        _sb.AppendLine("")
        _Properties = _sb.ToString

    End Sub

End Class
