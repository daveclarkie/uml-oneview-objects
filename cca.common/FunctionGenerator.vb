Public Class FunctionGenerator

    Dim dr As System.Data.DataRow
    Dim _Functions As String = ""

    Public ReadOnly Property Functions() As String
        Get
            Return _Functions
        End Get
    End Property

    Public Sub New(ByRef Table As DataTable)
        Dim _sb As New Text.StringBuilder(15000)
        Dim _pkField As String = ""
        Const cTab As String = ChrW(9)
        Dim cp As ColumnStructure
        Dim tablename As String = ""
        _sb.AppendLine("Dim CDC as DataCommon")
        _sb.AppendLine("Dim Row as DataRow")

        cp = New ColumnStructure(Table.Rows(0))

        If cp.TableName = "sessions" Then
            _sb.AppendLine("Public Sub New()")
            _sb.AppendLine("End Sub")
        Else
            _sb.AppendLine("Dim _CurrentUser as Integer")
            _sb.AppendLine("Dim _ActualUser as Integer")
        End If
        _sb.AppendLine("")

        _sb.AppendLine("Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)")
        _sb.AppendLine(cTab & cTab & "CDC = DataClass")
        _sb.AppendLine(cTab & "_CurrentUser=CurrentUser")
        _sb.AppendLine(cTab & "_ActualUser=ActualUser")
        _sb.AppendLine(cTab & "_ReadingData=True")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            tablename = cp.TableName
            If cp.IsPrimaryKey Then _pkField = cp.ColumnName

            If Not cp.DefaultValue = Nothing Then
                _sb.AppendLine(cTab & "Dim _d" & cp.ColumnName & " as " & cp.DataType & " = Nothing: CDC.ReadScalarValue(_d" & cp.ColumnName & ", new sqlclient.sqlCommand(""select " & cp.DefaultValue & """))")
            End If

            If Not (cp.IsPrimaryKey Or cp.IsForeignKey) Then
                If Not cp.DefaultValue = Nothing Then
                    _sb.AppendLine(cTab & cp.ColumnName & "= _d" & cp.ColumnName)
                End If
            Else
                If cp.ColumnName = "creator_fk" Then
                    _sb.AppendLine(cTab & "_" & cp.ColumnName & "=CurrentUser")
                ElseIf cp.ColumnName = "editor_fk" Then
                    _sb.AppendLine(cTab & "_" & cp.ColumnName & "=ActualUser")
                Else
                    If Not cp.DefaultValue = Nothing Then
                        _sb.AppendLine(cTab & "_" & cp.ColumnName & "= _d" & cp.ColumnName)
                    Else
                        _sb.AppendLine(cTab & "_" & cp.ColumnName & "=-1")
                    End If
                End If
            End If
        Next
        _sb.AppendLine(cTab & "_ReadingData=False")
        _sb.AppendLine("End Sub")
        _sb.AppendLine("")

        _sb.AppendLine("Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)")
        _sb.AppendLine(cTab & cTab & "CDC = DataClass")
        _sb.AppendLine(cTab & "_CurrentUser=CurrentUser")
        _sb.AppendLine(cTab & "_ActualUser=ActualUser")
        _sb.AppendLine(cTab & "Read(PK)")
        _sb.AppendLine("End Sub")
        _sb.AppendLine("")

        _sb.AppendLine("Public Function Read(byval PK as Integer) as boolean")
        _sb.AppendLine(cTab & "Dim _ReturnStatus as boolean=True")
        _sb.AppendLine(cTab & "CDC.AccessLog(""" & tablename & """,PK,_ActualUser)")
        _sb.AppendLine(cTab & "_ReadingData=True")
        _sb.AppendLine(cTab & "Try")
        _sb.AppendLine(cTab & cTab & "Dim CO As New SqlClient.SqlCommand(""rkg_" & tablename & "_read"")")
        _sb.AppendLine(cTab & cTab & "CO.CommandType = CommandType.StoredProcedure")
        _sb.AppendLine(cTab & cTab & "CO.Parameters.Add(New SqlClient.SqlParameter(""@" & _pkField & """, PK))")
        _sb.AppendLine(cTab & cTab & "Row=CDC.ReadDataRow(CO)")
        _sb.AppendLine(cTab & cTab & "CO.Dispose")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If Not cp.IsPrimaryKey Then
                _sb.AppendLine(cTab & cTab & cp.ColumnName & "=CommonFN.SafeRead(Row(""" & cp.ColumnName & """))")
            Else
                _sb.AppendLine(cTab & cTab & "_" & cp.ColumnName & "=CommonFN.SafeRead(Row(""" & cp.ColumnName & """))")
            End If
        Next
        _sb.AppendLine(cTab & cTab & "Row=Nothing")
        _sb.AppendLine(cTab & "Catch Ex as Exception")
        _sb.AppendLine(cTab & "_ReturnStatus=False")
        _sb.AppendLine(cTab & "_LastError=Ex")
        _sb.AppendLine(cTab & "End Try")

        _sb.AppendLine(cTab & "_ReadingData=False")
        _sb.AppendLine(cTab & "Return _ReturnStatus")
        _sb.AppendLine("End Function")
        _sb.AppendLine("")

        Dim fkc As Integer = 0
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.ColumnPosition > 6 And (cp.IsForeignKey Or cp.IsForeignKeyControl) Then
                fkc += 1
            End If
        Next

        _sb.AppendLine("Dim _MatchedRecords as datatable")
        _sb.AppendLine("Public ReadOnly Property MatchedRecords() as datatable")
        _sb.AppendLine(cTab & "Get")
        _sb.AppendLine(cTab & cTab & "if _MatchedRecords is nothing then Exists")
        _sb.AppendLine(cTab & cTab & "return _MatchedRecords")
        _sb.AppendLine(cTab & "End Get")
        _sb.AppendLine("End Property")
        If fkc > 0 Then
            _sb.AppendLine("Public Function Exists() as boolean")
            _sb.AppendLine(cTab & "Dim PO As New SqlClient.SqlParameter()")
            _sb.AppendLine(cTab & "Dim CO As New SqlClient.SqlCommand()")
            _sb.AppendLine(cTab & "CO.CommandType = CommandType.StoredProcedure")
            _sb.AppendLine(cTab & "CO.CommandText = ""rkg_" & tablename & "_exists""")
            For Each Me.dr In Table.Rows
                cp = New ColumnStructure(dr)
                If cp.ColumnPosition > 6 And (cp.IsForeignKey Or cp.IsForeignKeyControl) Then
                    _sb.AppendLine(cTab & "PO = New SqlClient.SqlParameter(""@" & cp.ColumnName & """, " & cp.ColumnName & ")")
                    _sb.AppendLine(cTab & "CO.parameters.add(PO)")
                End If
            Next
            _sb.AppendLine(cTab & "Dim _returnPK As Integer = 0")
            _sb.AppendLine(cTab & "_MatchedRecords=CDC.ReadDataTable(CO)")
            _sb.AppendLine(cTab & "_returnPK=_MatchedRecords.rows.count")
            _sb.AppendLine(cTab & "If _returnPK > 0 Then")
            _sb.AppendLine(cTab & cTab & "Return True")
            _sb.AppendLine(cTab & "Else")
            _sb.AppendLine(cTab & cTab & "Return False")
            _sb.AppendLine(cTab & "End If")
            _sb.AppendLine("End Function")
        Else
            _sb.AppendLine("Public Function Exists() as boolean")
            _sb.AppendLine(cTab & "Return False")
            _sb.AppendLine("End Function")
        End If
        _sb.AppendLine("")


        Dim ufkc As Integer = 0
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.ColumnPosition > 6 And cp.ColumnName = "user_fk" Then
                ufkc += 1
            End If
        Next
        If ufkc > 0 Then
            _sb.AppendLine("Dim _UserRecords as datatable")
            _sb.AppendLine("Public ReadOnly Property UserRecords() as datatable")
            _sb.AppendLine(cTab & "Get")
            _sb.AppendLine(cTab & cTab & "if _UserRecords is nothing then ByUser()")
            _sb.AppendLine(cTab & cTab & "return _UserRecords")
            _sb.AppendLine(cTab & "End Get")
            _sb.AppendLine("End Property")
            _sb.AppendLine("Public Function ByUser(optional byval user_pk as integer=-1) as boolean")
            _sb.AppendLine(cTab & "If user_pk=-1 then user_pk=_user_fk")
            _sb.AppendLine(cTab & "Dim PO As New SqlClient.SqlParameter()")
            _sb.AppendLine(cTab & "Dim CO As New SqlClient.SqlCommand()")
            _sb.AppendLine(cTab & "CO.CommandType = CommandType.StoredProcedure")
            _sb.AppendLine(cTab & "CO.CommandText = ""rkg_" & tablename & "_byuser""")
            _sb.AppendLine(cTab & "PO = New SqlClient.SqlParameter(""@user_pk"", user_pk)")
            _sb.AppendLine(cTab & "CO.parameters.add(PO)")
            _sb.AppendLine(cTab & "Dim _returnPK As Integer = 0")
            _sb.AppendLine(cTab & "_UserRecords=CDC.ReadDataTable(CO)")
            _sb.AppendLine(cTab & "_returnPK=_UserRecords.rows.count")
            _sb.AppendLine(cTab & "If _returnPK > 0 Then")
            _sb.AppendLine(cTab & cTab & "Return True")
            _sb.AppendLine(cTab & "Else")
            _sb.AppendLine(cTab & cTab & "Return False")
            _sb.AppendLine(cTab & "End If")
            _sb.AppendLine("End Function")
        End If
        _sb.AppendLine("")

        Dim vc As Integer = 0
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.DataTypeRaw = "varchar" Then
                vc += 1
            End If
        Next

        _sb.AppendLine("Dim _SimilarRecords as datatable")
        _sb.AppendLine("Public ReadOnly Property SimilarRecords() as datatable")
        _sb.AppendLine(cTab & "Get")
        _sb.AppendLine(cTab & cTab & "if _SimilarRecords is nothing then isUnique")
        _sb.AppendLine(cTab & cTab & "return _SimilarRecords")
        _sb.AppendLine(cTab & "End Get")
        _sb.AppendLine("End Property")

        If vc > 0 Then

            _sb.AppendLine("Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean")
            _sb.AppendLine(cTab & "Dim PO As New SqlClient.SqlParameter()")
            _sb.AppendLine(cTab & "Dim CO As New SqlClient.SqlCommand()")
            _sb.AppendLine(cTab & "CO.CommandType = CommandType.StoredProcedure")
            _sb.AppendLine(cTab & "CO.CommandText = ""rkg_" & tablename & "_compare""")
            For Each Me.dr In Table.Rows
                cp = New ColumnStructure(dr)
                If cp.DataTypeRaw = "varchar" Or (cp.DataTypeRaw = "datetime" And cp.ColumnPosition > 5) Or cp.IsPrimaryKey Then
                    If cp.DataTypeRaw = "datetime" Then
                        _sb.AppendLine(cTab & "PO = New SqlClient.SqlParameter(""@" & cp.ColumnName & """, CommonFN.CheckEmptyDate(" & cp.ColumnName & ".ToString()))")
                    Else
                        _sb.AppendLine(cTab & "PO = New SqlClient.SqlParameter(""@" & cp.ColumnName & """, " & cp.ColumnName & ")")
                    End If
                    _sb.AppendLine(cTab & "CO.parameters.add(PO)")
                End If
            Next
            _sb.AppendLine(cTab & "If minMatchCount>0 then CO.Parameters.AddWithValue(""@minMatchCount"",minMatchCount)")
            _sb.AppendLine(cTab & "Dim _returnPK As Integer = 0")
            _sb.AppendLine(cTab & "_SimilarRecords=CDC.ReadDataTable(CO)")
            _sb.AppendLine(cTab & "_returnPK=_SimilarRecords.rows.count")
            _sb.AppendLine(cTab & "If _returnPK = 0 Then")
            _sb.AppendLine(cTab & cTab & "Return True")
            _sb.AppendLine(cTab & "Else")
            _sb.AppendLine(cTab & cTab & "Return False")
            _sb.AppendLine(cTab & "End If")
            _sb.AppendLine("End Function")
        Else
            _sb.AppendLine("Public Function IsUnique() as boolean")
            _sb.AppendLine(cTab & "Return True")
            _sb.AppendLine("End Function")
        End If
        _sb.AppendLine("")

        _sb.AppendLine("Public Function Save() as boolean")
        _sb.AppendLine(cTab & "Dim _ReturnStatus as boolean=True")
        _sb.AppendLine(cTab & "If IsDirty then")
        _sb.AppendLine(cTab & cTab & "If _CurrentUser<>creator_fk and " & _pkField & "<1 then creator_fk=_CurrentUser : created=now()")
        _sb.AppendLine(cTab & cTab & "If _ActualUser<>editor_fk then editor_fk=_ActualUser")
        _sb.AppendLine(cTab & cTab & "modified=now()")
        _sb.AppendLine(cTab & cTab & "Try")
        _sb.AppendLine(cTab & cTab & cTab & "CDC.BeginTransaction")

        _sb.AppendLine(cTab & cTab & "Dim PO As SqlClient.SqlParameter")
        _sb.AppendLine(cTab & cTab & "Dim CO As New SqlClient.SqlCommand()")
        _sb.AppendLine(cTab & cTab & "CO.CommandType = CommandType.StoredProcedure")
        _sb.AppendLine(cTab & cTab & "CO.CommandText = ""rkg_" & tablename & "_save""")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            _sb.AppendLine(cTab & cTab & "PO=Nothing")
            If Not cp.DefaultValue Is Nothing Then
                _sb.AppendLine(cTab & cTab & "Dim _t" & cp.ColumnName & " as " & cp.DataType & "  = Nothing")
                Select Case cp.DataType
                    Case "Integer", "Datetime", "Boolean", "Double"
                        _sb.AppendLine(cTab & cTab & "If " & cp.ColumnName & "=" & " nothing  then")
                    Case Else
                        _sb.AppendLine(cTab & cTab & "If " & cp.ColumnName & " is" & " nothing  then")
                End Select
                _sb.AppendLine(cTab & cTab & cTab & "CDC.ReadScalarValue(_t" & cp.ColumnName & ", new sqlclient.sqlCommand(""select " & cp.DefaultValue & """))")
                _sb.AppendLine(cTab & cTab & cTab & "PO = New SqlClient.SqlParameter(""@" & cp.ColumnName & """, _t" & cp.ColumnName & ")")
                _sb.AppendLine(cTab & cTab & "Else")
                _sb.AppendLine(cTab & cTab & cTab & "PO = New SqlClient.SqlParameter(""@" & cp.ColumnName & """, " & cp.ColumnName & ")")
                _sb.AppendLine(cTab & cTab & "End If")
            ElseIf cp.DataTypeRaw = "datetime" Then
                _sb.AppendLine(cTab & cTab & "PO = New SqlClient.SqlParameter(""@" & cp.ColumnName & """, CommonFN.CheckEmptyDate(" & cp.ColumnName & ".ToString()))")
            Else
                _sb.AppendLine(cTab & cTab & "PO = New SqlClient.SqlParameter(""@" & cp.ColumnName & """, " & cp.ColumnName & ")")
            End If
            _sb.AppendLine(cTab & cTab & "If not PO is nothing then CO.parameters.add(PO)")
        Next

        _sb.AppendLine(cTab & cTab & "Dim _returnPK As Integer = 0")
        _sb.AppendLine(cTab & cTab & "If CDC.ReadScalarValue(_returnPK, CO) Then")

        _sb.AppendLine(cTab & cTab & cTab & "Try")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)

            Select Case cp.DataType
                Case "Byte()"
                    _sb.AppendLine(cTab & cTab & cTab & cTab & "If _" & cp.ColumnName & "_d then CDC.AuditLog(""" & tablename & """,_returnPK,""" & cp.ColumnName & """,_CurrentUser,modified,(New System.Text.ASCIIEncoding()).GetString(" & cp.ColumnName & ")):_" & cp.ColumnName & "_d=False")
                Case "Char()"
                    _sb.AppendLine(cTab & cTab & cTab & cTab & "If _" & cp.ColumnName & "_d then CDC.AuditLog(""" & tablename & """,_returnPK,""" & cp.ColumnName & """,_CurrentUser,modified,CStr(" & cp.ColumnName & ").replace(Chr(0),"""")):_" & cp.ColumnName & "_d=False")
                Case Else
                    _sb.AppendLine(cTab & cTab & cTab & cTab & "If _" & cp.ColumnName & "_d then CDC.AuditLog(""" & tablename & """,_returnPK,""" & cp.ColumnName & """,_CurrentUser,modified,CStr(" & cp.ColumnName & ")):_" & cp.ColumnName & "_d=False")
            End Select
        Next

        _sb.AppendLine(cTab & cTab & cTab & "Catch Ex2 as Exception")
        _sb.AppendLine(cTab & cTab & cTab & cTab & "_ReturnStatus=False")
        _sb.AppendLine(cTab & cTab & cTab & cTab & "_LastError=Ex2")
        _sb.AppendLine(cTab & cTab & cTab & "End Try")
        _sb.AppendLine(cTab & cTab & "Else")
        _sb.AppendLine(cTab & cTab & cTab & "_ReturnStatus=False")
        _sb.AppendLine(cTab & cTab & "End If")
        _sb.AppendLine(cTab & cTab & "If _ReturnStatus Then")
        _sb.AppendLine(cTab & cTab & cTab & "CDC.CommitTransaction")
        _sb.AppendLine(cTab & cTab & cTab & "Read(_returnPK)")
        _sb.AppendLine(cTab & cTab & "Else")
        _sb.AppendLine(cTab & cTab & cTab & "CDC.RollBackTransaction")
        _sb.AppendLine(cTab & cTab & "End If")
        _sb.AppendLine(cTab & cTab & "Catch Ex as Exception")
        _sb.AppendLine(cTab & cTab & cTab & "_ReturnStatus=False")
        _sb.AppendLine(cTab & cTab & cTab & "_LastError=Ex")
        _sb.AppendLine(cTab & cTab & cTab & "CDC.RollBackTransaction")
        _sb.AppendLine(cTab & cTab & "End Try")
        _sb.AppendLine(cTab & "Else")
        _sb.AppendLine(cTab & cTab & "_ReturnStatus=True")
        _sb.AppendLine(cTab & "End If")
        _sb.AppendLine(cTab & "Return _ReturnStatus")
        _sb.AppendLine("End Function")
        _sb.AppendLine("")
        _sb.AppendLine("Public Function Enable() as boolean")
        _sb.AppendLine(cTab & "rowstatus = 0")
        _sb.AppendLine(cTab & "Return Save")
        _sb.AppendLine("End Function")
        _sb.AppendLine("")
        _sb.AppendLine("Public Function Disable() as boolean")
        _sb.AppendLine(cTab & "rowstatus = 1")
        _sb.AppendLine(cTab & "Return Save")
        _sb.AppendLine("End Function")
        _sb.AppendLine("")
        _sb.AppendLine("Public Function Delete() as boolean")
        _sb.AppendLine(cTab & "rowstatus = 2")
        _sb.AppendLine(cTab & "Return Save")
        _sb.AppendLine("End Function")
        _sb.AppendLine("")
        _Functions = _sb.ToString

    End Sub


End Class
