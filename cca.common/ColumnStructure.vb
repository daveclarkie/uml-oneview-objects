Public Class ColumnStructure

    Dim _defVal As Object
    Dim _colName As String
    Dim _tblName As String
    Dim _colPosition As Integer
    Dim _dataType As String
    Dim _dataTypeRaw As String
    Dim _isPK As Boolean
    Dim _isFK As Boolean
    Dim _isFX As Boolean
    Dim _fkTable As String
    Dim _colDescription(2) As String
    Dim _colLabel As String = ""
    Dim _colLabelFull As String = ""
    Dim _colValType As ValidationType
    Dim _colValExpr As String = ""
    Dim _maxLength As Integer
    Dim _allowNull As Boolean
    Dim _colSecLevel As Integer

    Public Property DefaultValue() As Object
        Get
            If Not _defVal Is Nothing Then
                If _defVal.ToString = "((0))" Then _defVal = 0
                If _defVal.ToString = "((-1))" Then _defVal = -1
            End If
            Return _defVal
        End Get
        Set(ByVal value As Object)
            _defVal = value
        End Set
    End Property

    Public Property ColumnName() As String
        Get
            Return _colName
        End Get
        Set(ByVal value As String)
            _colName = value
        End Set
    End Property

    Public Property TableName() As String
        Get
            Return _tblName
        End Get
        Set(ByVal value As String)
            _tblName = value
        End Set
    End Property

    Public Property ColumnPosition() As Integer
        Get
            Return _colPosition
        End Get
        Set(ByVal value As Integer)
            _colPosition = value
        End Set
    End Property

    Public Property DataType() As String
        Get
            Return _dataType
        End Get
        Set(ByVal value As String)
            _dataType = value
        End Set
    End Property

    Public Property DataTypeRaw() As String
        Get
            Return _dataTypeRaw
        End Get
        Set(ByVal value As String)
            _dataTypeRaw = value
        End Set
    End Property

    Public Property IsPrimaryKey() As Boolean
        Get
            Return _isPK
        End Get
        Set(ByVal value As Boolean)
            _isPK = value
        End Set
    End Property

    Public Property IsForeignKey() As Boolean
        Get
            Return _isFK
        End Get
        Set(ByVal value As Boolean)
            _isFK = value
        End Set
    End Property

    Public Property IsForeignKeyControl() As Boolean
        Get
            Return _isFX
        End Get
        Set(ByVal value As Boolean)
            _isFX = value
        End Set
    End Property

    Public Property ForeignKeyTable() As String
        Get
            Return _fkTable
        End Get
        Set(ByVal value As String)
            _fkTable = value
        End Set
    End Property

    Public Property ColumnSecurityLevel() As String
        Get
            Return _colSecLevel
        End Get
        Set(ByVal value As String)
            _colSecLevel = value
        End Set
    End Property

    Public Property ColumnLabel() As String
        Get
            Return _colLabel
        End Get
        Set(ByVal value As String)
            _colLabel = value
        End Set
    End Property

    Public Property ColumnLabelFull() As String
        Get
            Return _colLabelFull
        End Get
        Set(ByVal value As String)
            _colLabelFull = value
        End Set
    End Property

    Public Property ColumnValidationType() As ValidationType
        Get
            Return _colValType
        End Get
        Set(ByVal value As ValidationType)
            _colValType = value
        End Set
    End Property

    Public Property ColumnValidationExpression() As String
        Get
            Return _colValExpr
        End Get
        Set(ByVal value As String)
            _colValExpr = value
        End Set
    End Property

    Public Property AllowNull() As Boolean
        Get
            Return _allowNull
        End Get
        Set(ByVal value As Boolean)
            _allowNull = value
        End Set
    End Property

    Public Property MaxLength() As Integer
        Get
            Return _maxLength
        End Get
        Set(ByVal value As Integer)
            _maxLength = value
        End Set
    End Property

    Public Sub New(ByRef dr As DataRow)
        _defVal = CommonFN.SafeRead(dr("COLUMN_DEFAULT"))
        _colName = CommonFN.SafeRead(dr("COLUMN_NAME"))
        _tblName = CommonFN.SafeRead(dr("TABLE_NAME"))
        _colPosition = CommonFN.SafeRead(dr("ORDINAL_POSITION"))
        _dataType = CommonFN.ConvertSqlTypeToNetClrType(CommonFN.SafeRead(dr("DATA_TYPE")))
        _dataTypeRaw = CommonFN.SafeRead(dr("DATA_TYPE"))
        _maxLength = CommonFN.SafeRead(dr("CHARACTER_MAXIMUM_LENGTH"))
        _isPK = _colName.EndsWith("_pk")
        _isFK = _colName.EndsWith("_fk")
        _isFX = _colName.EndsWith("_fx")
        Select Case CommonFN.SafeRead(dr("IS_NULLABLE"))
            Case "NO" : _allowNull = False
            Case Else : _allowNull = True
        End Select
        _fkTable = ""
        If _isFK Then _fkTable = TableNameFromFK(CommonFN.SafeRead(dr("COLUMN_NAME")).ToString)
        If CommonFN.SafeRead(dr("COLUMNDESCRIPTION")) Is Nothing Then
            _colLabelFull = _colName
            _colValType = ValidationType.Parse(GetType(ValidationType), "0")
            _colValExpr = ""
        Else
            If _isPK Then
                _colDescription = CommonFN.SafeRead(dr("COLUMNDESCRIPTION")).ToString.Split("|", 4, System.StringSplitOptions.None)
                Try
                    ReDim Preserve _colDescription(3)
                    _colSecLevel = _colDescription(0)
                    _colLabelFull = _colDescription(1)
                    _colValExpr = _colDescription(2)
                Catch ex As Exception

                End Try
            Else
                _colDescription = CommonFN.SafeRead(dr("COLUMNDESCRIPTION")).ToString.Split("|", 4, System.StringSplitOptions.None)
                Try
                    ReDim Preserve _colDescription(3)
                    _colSecLevel = _colDescription(0)
                    _colLabelFull = _colDescription(1)
                    If _colLabelFull Is Nothing Then _colLabelFull = _colName
                    If _colDescription(2) Is Nothing Then _colDescription(2) = "0"
                    _colValType = ValidationType.Parse(GetType(ValidationType), _colDescription(2))
                    _colValExpr = _colDescription(3)
                Catch ex As Exception

                End Try
            End If
        End If

        If _colLabel = "" Then _colLabel = _colLabelFull

        If _colLabel.Contains("<") Then
            _colLabel = _colLabel.Split("<")(0)
        End If

    End Sub


    Private Function TableNameFromFK(ByRef colname As String) As String
        Dim tblname As String = ""
        tblname = CommonFN.TableFromKey(colname.Replace("_pk", "_fk"))
        Return tblname
    End Function


End Class