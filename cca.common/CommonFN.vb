Imports System.Data.SqlClient

Public Class CommonFN
    Shared Function SafeRead(ByRef value As Object) As Object
        If TypeOf (value) Is System.DBNull Then
            Return Nothing
        Else
            Return value
        End If
    End Function

    Shared Function ConvertSqlTypeToNetClrType(ByVal typeSql As String) As String
        Dim netClrType As String = "Object"
        Select Case typeSql.ToLower
            Case "int", "integer" : netClrType = "Integer"
            Case "float", "money", "numeric", "real" : netClrType = "Double"
            Case "varchar", "nvarchar", "text", "ntext" : netClrType = "String"
            Case "char", "nchar" : netClrType = "Char()"
            Case "varbinary" : netClrType = "Byte()"
            Case "datetime" : netClrType = "Datetime"
            Case "bit" : netClrType = "Boolean"
            Case "character varying" : netClrType = "String"
            Case "timestamp without time zone" : netClrType = "Datetime"
            Case Else : netClrType = "Not supported: " & typeSql
        End Select
        Return netClrType
    End Function

    Shared Function IsValidPk(ByVal field As String, ByVal pk As Integer, Optional ByRef DataClass As DataCommon = Nothing) As Boolean
        Select Case field
            Case "matrixdetail_fk", "agreement_fk", "meterpoint_fk", "matrix_fk", "agreementthroughput_fk", "surveydetail_fk", "appointmentstatus_fk"
                Return True
        End Select

        If DataClass Is Nothing Then DataClass = New DataCommon
        field = field.Replace("_pk", "_fk")
        field = field.Replace("parent", "")
        field = field.Replace("creator", "user")
        field = field.Replace("editor", "user")
        If field.StartsWith("start") Then field = field.Replace("start", "")
        If field.StartsWith("end") Then field = field.Replace("end", "")
        Dim IsValid As Boolean = False
        Dim TargetTablePkField As String = TableFromKey(field, DataClass)

        Dim CO As New SqlClient.SqlCommand("rkg_" & TargetTablePkField & "_IsValid")
        If TargetTablePkField = "surveystatuses" Or TargetTablePkField = "appointmentstatuses" Then
            TargetTablePkField = TargetTablePkField.TrimEnd("s".ToCharArray()).TrimEnd("e".ToCharArray()) & "_pk"
        Else
            TargetTablePkField = TargetTablePkField.TrimEnd("s".ToCharArray()) & "_pk"
        End If

        CO.Parameters.Add(New SqlClient.SqlParameter("@" & TargetTablePkField, pk))
        CO.CommandType = CommandType.StoredProcedure
        DataClass.ReadScalarValue(IsValid, CO)
        CO.Dispose()
        Return IsValid
    End Function

    Shared Function TableFromKey(ByVal Key As String, Optional ByRef DataClass As DataCommon = Nothing) As String
        Dim k As String = Key
        If k.StartsWith("matrixdetail") Or k.StartsWith("matrix_") Then
            k = "matrix0details"
        ElseIf k.StartsWith("surveystatus") Then
            k = "surveystatuses"
        Else
            If DataClass Is Nothing Then DataClass = New DataCommon
            Dim CO As New SqlClient.SqlCommand("rsp_tablefromfk")
            CO.Parameters.Add(New SqlClient.SqlParameter("@fk", Key))
            CO.CommandType = CommandType.StoredProcedure
            DataClass.ReadScalarValue(k, CO)
            CO.Dispose()
        End If

        Return k
    End Function

    Shared Function CheckEmptyDate(ByRef PossibleDate As String) As Object
        Try
            Dim dt As DateTime
            If PossibleDate = "00:00:00" Or PossibleDate = "01 Jan 0001" Or PossibleDate = "" Or PossibleDate = "01/01/0001 00:00:00" Then
                Try
                    PossibleDate = ""
                Catch ex As Exception
                End Try
                Return Nothing
            Else
                If Date.TryParse(PossibleDate, dt) Then
                    If dt.Ticks <> 0 Then
                        Try
                            PossibleDate = dt.ToString("d MMM yyyy")
                        Catch ex As Exception
                        End Try
                        Return dt
                    Else
                        Try
                            PossibleDate = ""
                        Catch ex As Exception
                        End Try
                        Return Nothing
                    End If
                Else
                    Try
                        PossibleDate = ""
                    Catch ex As Exception
                    End Try
                    Return Nothing
                End If
            End If
        Catch gex As Exception
            Try
                PossibleDate = ""
            Catch ex As Exception
            End Try
            Return Nothing
        End Try
    End Function


    'Shared Function findItem(ByRef child As Object) As Object
    '    If child.page.findcontrol("probleminfo") = Nothing Then
    '        Return findItem(child.parent)
    '    Else
    '        Return child.page.findcontrol("probleminfo")
    '    End If
    'End Function

    Public Shared Function ToLatLong(ByVal Postcode As String, ByRef latitude As Single, ByRef longitude As Single) As Boolean

        Dim wc As New Net.WebClient()
        Dim p As String
        Dim rv As Boolean = True
        Dim rvA As Boolean = False
        Dim rvO As Boolean = False
        Dim rxp As New Text.RegularExpressions.Regex("(E|W|N|S)([0-9]){1,2}:([0-9]){1,2}:([0-9]){1,2}\ \(\ (-){0,1}([0-9]){1,3}\.([0-9]){1,6}\ \)")
        Dim rxs As New Text.RegularExpressions.Regex("(-){0,1}([0-9]){1,3}\.([0-9]){1,6}")
        Dim mps As Text.RegularExpressions.MatchCollection
        Dim mp As Text.RegularExpressions.Match
        Dim mss As Text.RegularExpressions.MatchCollection
        Dim ms As Text.RegularExpressions.Match
        Try
            p = wc.DownloadString("http://www.streetmap.co.uk/streetmap.dll?GridConvert?type=Postcode&name=" & Postcode.Replace(" ", "+").ToUpper())
            mps = rxp.Matches(p)
            For Each mp In mps
                mss = rxs.Matches(mp.Value)
                For Each ms In mss
                    Select Case mp.Value.Substring(0, 1)
                        Case "N", "S"
                            latitude = Single.Parse(ms.Value)
                            rvA = True
                        Case "E", "W"
                            longitude = Single.Parse(ms.Value)
                            rvO = True
                        Case Else
                            'WTF??
                            rv = False
                    End Select
                Next
            Next
        Catch ex As Exception
            rv = False
        End Try
        Return (rv And rvA And rvO)
    End Function


    Shared Function businessDaysBetween(ByVal dStart As Date, ByVal isAmStart As Boolean, ByVal dEnd As Date, ByVal isAmEnd As Boolean, ByRef CDC As DataCommon) As Single

        Dim CO As New SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_holidaysbetween"
        CO.Parameters.AddWithValue("@start", dStart.Year)
        CO.Parameters.AddWithValue("@end", dEnd.Year)
        Dim dt As DataTable = CDC.ReadDataTable(CO)
        Dim dr As DataRow

        Dim oCount As Single = 0.0
        Dim mCount As Integer = dEnd.Subtract(dStart).TotalDays
        Dim i As Integer
        Dim skip As Boolean = False
        For i = 0 To mCount
            skip = False
            Select Case dStart.AddDays(i).DayOfWeek
                Case DayOfWeek.Saturday, DayOfWeek.Sunday
                    skip = True
                Case Else
                    For Each dr In dt.Rows
                        If dr("holiday") = dStart.AddDays(i) Then skip = True
                    Next
            End Select
            If Not skip Then oCount += 1
        Next

        If Not isAmStart Then oCount -= 0.5
        If isAmEnd Then oCount -= 0.5

        Return oCount
    End Function

    Shared Function businessDaysBetween(ByVal dStart As Date, ByVal dEnd As Date, ByRef CDC As DataCommon) As Single

        Dim CO As New SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_holidaysbetween"
        CO.Parameters.AddWithValue("@start", dStart.Year)
        CO.Parameters.AddWithValue("@end", dEnd.Year)
        Dim dt As DataTable = CDC.ReadDataTable(CO)
        Dim dr As DataRow

        Dim oCount As Single = 0.0
        Dim mCount As Integer = dEnd.Subtract(dStart).TotalDays
        Dim i As Integer
        Dim skip As Boolean = False
        For i = 0 To mCount
            skip = False
            Select Case dStart.AddDays(i).DayOfWeek
                Case DayOfWeek.Saturday, DayOfWeek.Sunday
                    skip = True
                Case Else
                    For Each dr In dt.Rows
                        If dr("holiday") = dStart.AddDays(i) Then skip = True
                    Next
            End Select
            If Not skip Then oCount += 1
        Next

        Return oCount
    End Function


End Class

