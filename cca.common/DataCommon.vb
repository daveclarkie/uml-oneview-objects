Imports System.Data.Common
Imports System.IO
Imports System.Web

Public Class DataCommon

    Dim _Connection As Object()
    Dim _Transactions As Object()
    'Dim _ConnectionString As String() = {"Data Source=UML-SQL\UMLSQL2005;Initial Catalog=se_cca;Persist Security Info=True;User ID=sa;Password=chicago", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;"}
    'Dim _ConnectionString As String() = {"Data Source=10.44.5.205\UMLSQL2005,49341;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=10.44.5.205\UMLSQL2005,49341;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;"}
    ' 2017.08.08 Dim _ConnectionString As String() = {"Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;"}
    ' 2018.01.12 Dim _ConnectionString As String() = {"Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;"}
    Dim _ConnectionString = {"Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;TransparentNetworkIPResolution=False"}

    Dim _DbType As UnderlyingDatabaseType() = {UnderlyingDatabaseType.MSSQL}
    Dim _UseTransaction As Boolean
    Dim _ConnectionReader As ConnectionReader = Nothing
    Dim _CSI As Integer = 0

    Dim _LastError As Exception = Nothing

    Public Shared Sub PrintCommand(command As DbCommand)
        System.Diagnostics.Debug.WriteLine("CommandType: " & command.CommandType)
        System.Diagnostics.Debug.WriteLine("CommandText: " & command.CommandText)

        For Each parameter In command.Parameters
            System.Diagnostics.Debug.WriteLine("    Parameter " & parameter.ParameterName & ", " & parameter.DbType & ": " & parameter.Value & " (" & parameter.Value.[GetType]().Name & ")")
        Next
    End Sub

    Public Property LastError() As Exception
        Get
            Return _LastError
        End Get
        Set(ByVal value As Exception)
            _LastError = value
        End Set
    End Property

    Public Property CSI() As Integer
        Get
            Return _CSI
        End Get
        Set(ByVal value As Integer)
            _CSI = value
        End Set
    End Property

    Public Property ConnectionString(ByVal ix As Integer) As String
        Get
            Return _ConnectionString(ix)
        End Get
        Set(ByVal value As String)
            _ConnectionString(ix) = value
        End Set
    End Property

    Private Property UseTransaction() As Boolean
        Get
            Return _UseTransaction
        End Get
        Set(ByVal value As Boolean)
            _UseTransaction = value
        End Set
    End Property

    Public ReadOnly Property CurrentConnectionReader() As ConnectionReader
        Get
            Return _ConnectionReader
        End Get
    End Property

    Public Sub New()
        '2017.01.01_ConnectionString = {"Data Source=UML-SQL\UMLSQL2005;Initial Catalog=se_cca;Persist Security Info=True;User ID=sa;Password=chicago;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=UML-SQL\UMLSQL2005;Initial Catalog=se_cca;Persist Security Info=True;User ID=sa;Password=chicago;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;"}
        '2017.08.08_ConnectionString = {"Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=Trusted_Connection=True;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;"}
        '2017.01.12_ConnectionString = {"Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;"}
        _ConnectionString = {"Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=se_cca;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=se_cca;Trusted_Connection=True;TransparentNetworkIPResolution=False", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;TransparentNetworkIPResolution=False"}

        SetDbTypes()
    End Sub

    Public Sub New(ByVal AuthUrl As String)
        Dim _ConnectionReader As ConnectionReader = Nothing
        If _ConnectionString.Length = 0 Then
            While _ConnectionReader Is Nothing
                Try
                    _ConnectionReader = New ConnectionReader(AuthUrl)
                    _ConnectionString = _ConnectionReader.ConnectionString
                    SetDbTypes()
                Catch ex As AccessDeniedException
                    Throw New AccessDeniedException(ex.ToString)
                Catch ex As NoConnectionsAvailableException
                    Console.WriteLine("No connection error - retrying")
                    _ConnectionReader = Nothing
                End Try
            End While
        Else
            SetDbTypes()
        End If
    End Sub

    Private Sub SetDbTypes()
        ReDim _DbType(_ConnectionString.Length - 1)
        ReDim _Transactions(_ConnectionString.Length - 1)
        ReDim _Connection(_ConnectionString.Length - 1)
        Dim x As Integer
        For x = 0 To _ConnectionString.Length - 1
            If _ConnectionString(x).Contains("5432") Then
                _DbType(x) = UnderlyingDatabaseType.PostgreSQL
            ElseIf _ConnectionString(x).ToLower().Contains("pwd") Then
                _DbType(x) = UnderlyingDatabaseType.MySQL
            Else
                _DbType(x) = UnderlyingDatabaseType.MSSQL
            End If
        Next
    End Sub

    Public Function CommitTransaction(Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _returnStatus As Boolean = True
        UseTransaction = False
        Select Case _DbType(ConnectionStringIndex)
            Case UnderlyingDatabaseType.MSSQL
                Try
                    CType(_Transactions(ConnectionStringIndex), SqlClient.SqlTransaction).Commit()
                    CType(_Transactions(ConnectionStringIndex), SqlClient.SqlTransaction).Dispose()
                    DisposeConnection(ConnectionStringIndex)
                Catch ex As Exception
                    _returnStatus = False
                End Try
            Case UnderlyingDatabaseType.MySQL
            Case UnderlyingDatabaseType.PostgreSQL
        End Select
        Return _returnStatus
    End Function

    Public Function RollbackTransaction(Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _returnStatus As Boolean = True
        UseTransaction = False
        Select Case _DbType(ConnectionStringIndex)
            Case UnderlyingDatabaseType.MSSQL
                Try
                    CType(_Transactions(ConnectionStringIndex), SqlClient.SqlTransaction).Rollback()
                    CType(_Transactions(ConnectionStringIndex), SqlClient.SqlTransaction).Dispose()
                    DisposeConnection(ConnectionStringIndex)
                Catch ex As Exception
                    _returnStatus = False
                End Try
            Case UnderlyingDatabaseType.MySQL
            Case UnderlyingDatabaseType.PostgreSQL
        End Select
        Return _returnStatus
    End Function

    Public Function BeginTransaction(Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _returnStatus As Boolean = True
        UseTransaction = True
        GetConnection(ConnectionStringIndex)
        Select Case _DbType(ConnectionStringIndex)
            Case UnderlyingDatabaseType.MSSQL
                Try
                    _Transactions(ConnectionStringIndex) = CType(_Connection(ConnectionStringIndex), SqlClient.SqlConnection).BeginTransaction()
                Catch ex As Exception
                    _returnStatus = False
                End Try
            Case UnderlyingDatabaseType.MySQL
            Case UnderlyingDatabaseType.PostgreSQL
        End Select
        Return _returnStatus
    End Function

    Public Function ReadDataSet(ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As DataSet
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim ds As DataSet
        Dim da As SqlClient.SqlDataAdapter
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        da = New SqlClient.SqlDataAdapter(CommandObject)
        ds = New DataSet()
        da.Fill(ds)
        da.Dispose()
        CommandObject.Connection = Nothing
        coOut(True)
        Return ds
    End Function

    Public Function ReadDataTable(ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As DataTable
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim dt As DataTable
        Dim ds As DataSet
        Dim da As SqlClient.SqlDataAdapter
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        da = New SqlClient.SqlDataAdapter(CommandObject)
        ds = New DataSet()
        da.Fill(ds)
        dt = ds.Tables(0)
        ds.Dispose()
        da.Dispose()
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return dt
    End Function


    Public Function ReadDataRow(ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As DataRow
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim dr As DataRow
        Dim dt As DataTable
        Dim ds As DataSet
        Dim da As SqlClient.SqlDataAdapter
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        da = New SqlClient.SqlDataAdapter(CommandObject)
        ds = New DataSet()
        da.Fill(ds)
        dt = ds.Tables(0)
        Try
            dr = dt.Rows(0)
        Catch ex As Exception
            LastError = New Exception("No row at position 0")
            dr = Nothing
        End Try
        dt.Dispose()
        ds.Dispose()
        da.Dispose()
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return dr
    End Function

    Public Function ReadScalarValue(ByRef Target As String, ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try
            Target = CommandObject.ExecuteScalar
        Catch ex As Exception
            _ReturnStatus = False
            _LastError = ex
        End Try
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function

    Public Function ReadScalarValue(ByRef Target As Integer, ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try
            Target = CommandObject.ExecuteScalar
        Catch ex As Exception
            _LastError = ex
            _ReturnStatus = False
        End Try
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function

    Public Function ReadScalarValue(ByRef Target As Double, ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try
            Target = CommandObject.ExecuteScalar
        Catch ex As Exception
            _ReturnStatus = False
            _LastError = ex
        End Try
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function

    Public Function ReadScalarValue(ByRef Target As DateTime, ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try
            Target = CommandObject.ExecuteScalar
        Catch ex As Exception
            _ReturnStatus = False
            _LastError = ex
        End Try
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function

    Public Function ReadScalarValue(ByRef Target As Byte(), ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try
            Target = CommandObject.ExecuteScalar
        Catch ex As Exception
            _ReturnStatus = False
            _LastError = ex
        End Try
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function

    Public Function ReadScalarValue(ByRef Target As Char(), ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try
            Target = CommandObject.ExecuteScalar
        Catch ex As Exception
            _ReturnStatus = False
            _LastError = ex
        End Try
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function

    Public Function ReadScalarValue(ByRef Target As Boolean, ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try
            Target = CommandObject.ExecuteScalar
        Catch ex As Exception
            _ReturnStatus = False
            _LastError = ex
        End Try
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function

    Public Function ExecuteBulkCopy(ByRef dt As DataTable, ByRef DestinationTableName As String, ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try
            Using bulkCopy As System.Data.SqlClient.SqlBulkCopy = New System.Data.SqlClient.SqlBulkCopy(CommandObject.Connection.ConnectionString)
                bulkCopy.DestinationTableName = DestinationTableName
                bulkCopy.WriteToServer(dt)
            End Using
        Catch ex As Exception
            _ReturnStatus = False
            _LastError = ex
        End Try
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function
    Public Function Execute(ByRef CommandObject As SqlClient.SqlCommand, Optional ByVal ConnectionStringIndex As Integer = -1) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        CommandObject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try
            CommandObject.ExecuteNonQuery()
        Catch ex As Exception
            _ReturnStatus = False
            _LastError = ex
        End Try
        CommandObject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function

    Public Sub AccessLog(ByVal Table As String, ByVal RowPK As Integer, ByVal User As Integer)
        If RowPK = 0 Then Exit Sub
        Dim PO As SqlClient.SqlParameter
        Dim CO As SqlClient.SqlCommand
        Dim _returnStatus As Boolean = True
        Dim x As Integer = 0
        PO = New SqlClient.SqlParameter()
        CO = New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_accesslogs_save"
        PO = New SqlClient.SqlParameter("@accesslog_pk", 0)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@creator_fk", User)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@objectname", Table)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@row_fk", RowPK)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@created", Now())
        CO.Parameters.Add(PO)
        Execute(CO)
    End Sub

    Public Function AuditLog(ByVal Table As String, ByVal RowPK As Integer, ByVal Field As String, ByVal User As Integer, ByVal ChangeDate As DateTime, ByVal Value As String) As Boolean
        Dim PO As SqlClient.SqlParameter
        Dim CO As SqlClient.SqlCommand
        Dim _returnStatus As Boolean = True
        Dim x As Integer = 0
        Dim _value As String
        Dim _pass As Integer = 0
        Dim _audit_pk As Integer
        Dim _vIn As Char() = Value.ToCharArray
        Dim _vOut(49) As Char

        Dim k As Integer

        For x = 0 To Value.Length - 1 Step 50
            k = Value.Length - x
            If k > 50 Then k = 50
            Array.Clear(_vOut, 0, 50)
            Array.ConstrainedCopy(_vIn, x, _vOut, 0, k)
            _value = CStr(_vOut).Replace(Chr(0), "")
            PO = New SqlClient.SqlParameter()
            CO = New SqlClient.SqlCommand()
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rkg_auditlogs_save"
            PO = New SqlClient.SqlParameter("@auditlog_pk", 0)
            CO.Parameters.Add(PO)
            PO = New SqlClient.SqlParameter("@creator_fk", User)
            CO.Parameters.Add(PO)
            PO = New SqlClient.SqlParameter("@created", ChangeDate)
            CO.Parameters.Add(PO)
            PO = New SqlClient.SqlParameter("@tablename", Table)
            CO.Parameters.Add(PO)
            PO = New SqlClient.SqlParameter("@row_fk", RowPK)
            CO.Parameters.Add(PO)
            PO = New SqlClient.SqlParameter("@fieldname", Field)
            CO.Parameters.Add(PO)
            PO = New SqlClient.SqlParameter("@fieldvalue", _value)
            CO.Parameters.Add(PO)
            PO = New SqlClient.SqlParameter("@valueinstance", _pass)
            CO.Parameters.Add(PO)
            If _pass = 0 Then
                PO = New SqlClient.SqlParameter("@auditlog_fk", 0)
                CO.Parameters.Add(PO)
                _returnStatus = ReadScalarValue(_audit_pk, CO)
            Else
                PO = New SqlClient.SqlParameter("@auditlog_fk", _audit_pk)
                CO.Parameters.Add(PO)
                _returnStatus = _returnStatus And Execute(CO)
            End If
            _pass += 1
        Next
        Return _returnStatus
    End Function

    Private Sub DisposeConnection(Optional ByVal ConnectionStringIndex As Integer = -1)
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        If Not UseTransaction Then
            Select Case _DbType(ConnectionStringIndex)
                Case UnderlyingDatabaseType.MSSQL
                    If Not _Connection(ConnectionStringIndex) Is Nothing Then
                        CType(_Connection(ConnectionStringIndex), SqlClient.SqlConnection).Close()
                        CType(_Connection(ConnectionStringIndex), SqlClient.SqlConnection).Dispose()
                        _Connection(ConnectionStringIndex) = Nothing
                    End If
                Case UnderlyingDatabaseType.MySQL
                Case UnderlyingDatabaseType.PostgreSQL
            End Select
        End If
    End Sub

    '
    Public Function GetConnection_debug(ByRef Target As String, ByRef CommandObject As SqlClient.SqlCommand, ByRef ConnectionStringIndex As Integer) As Boolean
        _lastCO = CommandObject.Clone
        coIn()
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _ReturnStatus As Boolean = True
        commandobject.Connection = GetConnection(ConnectionStringIndex)
        CommandObject.Transaction = _Transactions(ConnectionStringIndex)
        Try

            Target = ConnectionStringIndex & " | " & CommandObject.Connection.ConnectionString
        Catch ex As Exception
            _ReturnStatus = False
            _LastError = ex
        End Try
        commandobject.Connection = Nothing
        DisposeConnection(ConnectionStringIndex)
        coOut(True)
        Return _ReturnStatus
    End Function

    Public Shared Function hostpath() As String
        Dim vhostpath As String = ""
        Try
            vhostpath = System.Web.HttpContext.Current.Server.MapPath("DataCommon.dll")
        Catch ex As Exception

        End Try
        Return vhostpath
    End Function

    Private Function GetConnection(Optional ByVal ConnectionStringIndex As Integer = -1) As Object
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _rc As Object

        Dim vhostpath As String = hostpath() ' System.Web.HttpContext.Current.Server.MapPath("DataCommon.dll") 'AppDomain.CurrentDomain.BaseDirectory 'System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
        'msgbox(vhostpath)
        If vhostpath.Contains("seweb_dev") Then
            ' SE DEV
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 0 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 1 : End If
        ElseIf vhostpath.Contains("seweb_test") Then
            ' SE TEST
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 0 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 1 : End If
        ElseIf vhostpath.Contains("seweb_prod") Then
            ' SE LIVE
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 4 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 5 : End If
        ElseIf vhostpath.Contains("dave.clarke") Then
            ' DAVE DEV
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 4 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 5 : End If
        Else
            ' UML LIVE
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 4 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 5 : End If
        End If

        If _Connection(ConnectionStringIndex) Is Nothing Then
            _Connection(ConnectionStringIndex) = New SqlClient.SqlConnection(_ConnectionString(ConnectionStringIndex))
            _Connection(ConnectionStringIndex).Open()
        Else
            If CType(_Connection(ConnectionStringIndex), SqlClient.SqlConnection).State <> ConnectionState.Open Then
                CType(_Connection(ConnectionStringIndex), SqlClient.SqlConnection).Dispose()
                _Connection(ConnectionStringIndex) = New SqlClient.SqlConnection(_ConnectionString(ConnectionStringIndex))
                _Connection(ConnectionStringIndex).Open()
            End If
        End If
        _rc = _Connection(ConnectionStringIndex)

        If Not hostpath.Contains("inetpub") Then
            Dim dll_log As String = "c:\logs\dll\"
            dll_log = Replace(dll_log, "bin\Debug\", "")
            If Not Directory.Exists(dll_log) Then
                Directory.CreateDirectory(dll_log)
            End If
            dll_log &= "log_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"
            Dim fs_dll As FileStream = Nothing
            If (Not File.Exists(dll_log)) Then
                Try
                    fs_dll = File.Create(dll_log)
                    fs_dll.Close()
                Catch ex As Exception

                End Try
            End If
            Dim sw_dll As StreamWriter = File.AppendText(dll_log)
            sw_dll.Write(vbCrLf)
            sw_dll.Write("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString())
            sw_dll.Write(" | {0}", _rc.connectionstring)
            sw_dll.Write(" | {0}", hostpath)
            sw_dll.Close()
        End If

        Return _rc
    End Function
    Private Function GetConnectionString(Optional ByVal ConnectionStringIndex As Integer = -1) As String
        ConnectionStringIndex = IIf(ConnectionStringIndex = -1, CSI, ConnectionStringIndex)
        Dim _rc As String

        Dim vhostpath As String = hostpath() 'AppDomain.CurrentDomain.BaseDirectory
        If vhostpath.Contains("seweb_dev") Then
            ' SE DEV
            'msgbox("SE DEV")
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 0 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 1 : End If
        ElseIf vhostpath.Contains("seweb_test") Then
            ' SE TEST
            'msgbox("SE TEST")
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 2 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 3 : End If
        ElseIf vhostpath.Contains("seweb_prod") Then
            ' SE LIVE
            'msgbox("SE LIVE")
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 4 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 5 : End If
        ElseIf vhostpath.Contains("dave.clarke") Then
            ' UML DEV
            'MsgBox("UML DEV")
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 4 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 5 : End If
        Else
            ' UML LIVE
            'MsgBox("UML LIVE")
            If ConnectionStringIndex = 0 Then : ConnectionStringIndex = 4 : End If
            If ConnectionStringIndex = 1 Then : ConnectionStringIndex = 5 : End If
        End If

        If _Connection(ConnectionStringIndex) Is Nothing Then
            _Connection(ConnectionStringIndex) = New SqlClient.SqlConnection(_ConnectionString(ConnectionStringIndex))
            _Connection(ConnectionStringIndex).Open()
        Else
            If CType(_Connection(ConnectionStringIndex), SqlClient.SqlConnection).State <> ConnectionState.Open Then
                CType(_Connection(ConnectionStringIndex), SqlClient.SqlConnection).Dispose()
                _Connection(ConnectionStringIndex) = New SqlClient.SqlConnection(_ConnectionString(ConnectionStringIndex))
                _Connection(ConnectionStringIndex).Open()
            End If
        End If
        _rc = _Connection(ConnectionStringIndex)

        Return _rc
    End Function

    Public Sub Sanitize(ByRef value As String)
        value = value.Replace("'", "`")
        value = value.Replace("/*", "")
        value = value.Replace("*/", "")
        value = value.Replace("--", "")
        value = value.Replace(";", "")
        value = value.Replace("*", "")
        value = value.Replace("%", "")
        value = value.Replace("?", "")
        value = value.Trim(" ")
    End Sub

    Dim _lastCO As SqlClient.SqlCommand = Nothing

    Public Function LastParams() As String
        Dim retval As String = ""
        If _lastCO Is Nothing Then
            retval = "No CO"
        Else
            retval = _lastCO.CommandText
            For Each pm As SqlClient.SqlParameter In _lastCO.Parameters
                retval &= (pm.ParameterName & " :: " & pm.Value.ToString & vbCrLf)
            Next
        End If
        Return retval
    End Function

    Dim _coTimeIn As Long = 0
    Dim _coTimeOut As Long = 0
    Dim _coParams As String = ""
    Dim _coCmd As String = ""

    Private Sub coIn()
        '_coTimeIn = Now.Ticks
        '_coParams = LastParams()
        '_coCmd = _lastCO.CommandText
    End Sub

    Private Sub coOut(ByVal state As Boolean)
        '_coTimeOut = Now.Ticks
        'Dim t As New TimeSpan(_coTimeOut - _coTimeIn)
        'Dim f As IO.StreamWriter = IO.File.AppendText("c:\logs\sql.txt")
        'Dim ts As Double = 0.0001
        'If t.TotalMilliseconds > 0 Then ts = t.TotalMilliseconds

        'Select Case _coCmd
        '    Case "rkg_auditlogs_save", "rsp_getsession", "rkg_accesslogs_save", "rsp_tablefromfk", "rkg_activitylogs_save", "rkg_accesslogs_save"
        '        f.WriteLine("{0}    {1}    {2}", ts, _coCmd, "removed")
        '    Case Else
        '        f.WriteLine("{0}    {1}    {2}", ts, _coCmd, _coParams.Replace(vbCrLf, " | "))
        'End Select
        'f.Flush()
        'f.Close()
        'f.Dispose()
        '_coTimeIn = 0
        '_coTimeOut = 0
        '_coParams = ""
        '_coCmd = ""
    End Sub

End Class
