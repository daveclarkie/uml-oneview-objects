Public Class SqlGenerator

    Dim dr As System.Data.DataRow
    Dim _Sql As String = ""
    Dim CSI As Integer = 0

    Public ReadOnly Property Sql() As String
        Get
            Return _Sql
        End Get
    End Property

    Public Sub New(ByRef Table As DataTable, Optional ByRef DataClass As DataCommon = Nothing, Optional ByVal iCSI As Integer = 0)
        CSI = iCSI
        Dim CDC As DataCommon = DataClass
        If CDC Is Nothing Then CDC = New DataCommon()
        Dim _sb As Text.StringBuilder
        Dim cp As ColumnStructure
        Dim CO As SqlClient.SqlCommand
        Dim _tbl As String = ""
        Dim _pk As String = ""
        Dim _dc As String = ""  ' parameter list (DECLARE style)
        Dim _iv As String = ""  ' parameter list (INSERT VALUES style)
        Dim _if As String = ""  ' parameter list (INSERT FIELDS style)
        Dim _uf As String = ""  ' parameter list (UPDATE FIELDS style)
        Dim _lu As String = ""  ' parameter list (LOOKUP style)
        Dim _wc As String = ""  ' Where clause

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)

            _dc = _dc & "@" & cp.ColumnName & " [" & cp.DataTypeRaw & "]"
            If cp.MaxLength <> 0 And Not cp.DataTypeRaw.Contains("text") Then _dc = _dc & "(" & cp.MaxLength & ")"
            If cp.DataTypeRaw.Contains("date") Then
                _dc = _dc & "=null"
            End If


            _dc = _dc & ","

            If cp.IsPrimaryKey Then
                _pk = cp.ColumnName
                _tbl = cp.TableName
                _wc = "WHERE " & cp.ColumnName & "=@" & cp.ColumnName
                Try
                    If Not cp.ColumnValidationExpression = "" Then
                        _lu = "CREATE PROCEDURE [dbo].[rkg_" & _tbl & "_lookup] AS SELECT -1 as Pk,'Select...' as Value, 0 [odr] UNION SELECT " & _pk & " AS Pk," & cp.ColumnValidationExpression.Replace(",", "+' '+") & " AS Value, 1 [odr] FROM " & _tbl & " WHERE rowstatus=0 ORDER BY odr,value"
                    Else
                        _lu = ""
                    End If
                Catch ex As Exception
                    _lu = ""
                End Try
            Else
                _iv = _iv & "@" & cp.ColumnName & ","
                _if = _if & cp.ColumnName & ","
                _uf = _uf & cp.ColumnName & "=@" & cp.ColumnName & ","

                If _lu = "" And cp.DataType = "String" Then
                    _lu = "CREATE PROCEDURE [dbo].[rkg_" & _tbl & "_lookup] AS SELECT -1 as Pk,'Select...' as Value UNION SELECT " & _pk & " AS PK," & cp.ColumnName & " AS Value FROM " & _tbl & " WHERE rowstatus=0"
                End If


            End If

        Next




        _dc = _dc.TrimEnd(",".ToCharArray())
        _if = _if.TrimEnd(",".ToCharArray())
        _iv = _iv.TrimEnd(",".ToCharArray())
        _uf = _uf.TrimEnd(",".ToCharArray())


        If _lu = "" Then
            _lu = "CREATE PROCEDURE [dbo].[rkg_" & _tbl & "_lookup] AS SELECT -1 as Pk,'Select...' as Value UNION SELECT " & _pk & " AS PK, cast(" & _pk & " as varchar(4)) AS Value FROM " & _tbl & " WHERE rowstatus=0"
        End If

        CO = New SqlClient.SqlCommand
        CO.CommandText = "DROP PROCEDURE rkg_" & _tbl & "_lookup"
        _Sql &= CO.CommandText
        _Sql &= Chr(10)
        _Sql &= Chr(13)
        CDC.Execute(CO, CSI)

        CO = New SqlClient.SqlCommand
        CO.CommandText = _lu
        _Sql &= CO.CommandText
        _Sql &= Chr(10)
        _Sql &= Chr(13)
        CDC.Execute(CO, CSI)

        CO = New SqlClient.SqlCommand
        CO.CommandText = "DROP PROCEDURE rkg_" & _tbl & "_save"
        _Sql &= CO.CommandText
        _Sql &= Chr(10)
        _Sql &= Chr(13)
        CDC.Execute(CO, CSI)

        CO = New SqlClient.SqlCommand
        CO.CommandText = "DROP PROCEDURE rkg_" & _tbl & "_read"
        _Sql &= CO.CommandText
        _Sql &= Chr(10)
        _Sql &= Chr(13)
        CDC.Execute(CO, CSI)

        CO.CommandText = "DROP PROCEDURE rkg_" & _tbl & "_IsValid"
        _Sql &= CO.CommandText
        CDC.Execute(CO, CSI)

        Try
            CO.CommandText = "DROP PROCEDURE rkg_" & _tbl & "_compare"
            _Sql &= CO.CommandText
            CDC.Execute(CO, CSI)
        Catch ex As Exception
        End Try

        Try
            CO.CommandText = "DROP PROCEDURE rkg_" & _tbl & "_exists"
            _Sql &= CO.CommandText
            CDC.Execute(CO, CSI)
        Catch ex As Exception
        End Try


        _sb = New Text.StringBuilder(5000)

        _sb.AppendLine("")
        _sb.AppendLine("CREATE PROCEDURE [dbo].[rkg_" & _tbl & "_save]")
        _sb.AppendLine("  " & _dc)
        _sb.AppendLine("AS")
        _sb.AppendLine("SET NOCOUNT ON")
        _sb.AppendLine("IF @" & _pk & " IN (-1,0) ")
        _sb.AppendLine("	BEGIN")
        _sb.AppendLine("		INSERT INTO " & _tbl & " (")
        _sb.AppendLine("				" & _if)
        _sb.AppendLine("			) VALUES (")
        _sb.AppendLine("				" & _iv)
        _sb.AppendLine("			)")
        _sb.AppendLine("")
        _sb.AppendLine("		SELECT SCOPE_IDENTITY()")
        _sb.AppendLine("	END")
        _sb.AppendLine("ELSE")
        _sb.AppendLine("	BEGIN")
        _sb.AppendLine("		UPDATE " & _tbl & " SET")
        _sb.AppendLine("			" & _uf)
        _sb.AppendLine("		" & _wc)
        _sb.AppendLine("")
        _sb.AppendLine("		SELECT @" & _pk)
        _sb.AppendLine("	END")
        _sb.AppendLine("")


        CO.CommandText = _sb.ToString
        _Sql &= CO.CommandText
        CDC.Execute(CO, CSI)

        _sb = New Text.StringBuilder(5000)


        _sb.AppendLine("CREATE PROCEDURE [dbo].[rkg_" & _tbl & "_read]")
        _sb.AppendLine("  @" & _pk & " [int]")
        _sb.AppendLine("AS")
        _sb.AppendLine("SELECT * FROM " & _tbl & " WHERE rowstatus<2 and " & _pk & "=@" & _pk)
        _sb.AppendLine("")

        CO.CommandText = _sb.ToString
        _Sql &= CO.CommandText
        CDC.Execute(CO, CSI)

        _sb = New Text.StringBuilder(5000)

        _sb.AppendLine("CREATE PROCEDURE [dbo].[rkg_" & _tbl & "_IsValid]")
        _sb.AppendLine("  @" & _pk & " [int],@r [int]=0")
        _sb.AppendLine("AS")
        _sb.AppendLine("SELECT @r=1 WHERE @" & _pk & " IN ((SELECT " & _pk & " FROM " & _tbl & " WHERE rowstatus<2 )) OR @" & _pk & "=-1")
        _sb.AppendLine("SELECT @r")
        _sb.AppendLine("")

        CO.CommandText = _sb.ToString
        _Sql &= CO.CommandText
        CDC.Execute(CO, CSI)

        _sb = New Text.StringBuilder(5000)
        Dim _cp As String = "" ' params
        Dim _cs As String = "" ' scope
        Dim _cq As String = "" ' section
        Dim _cc As Integer = 0 ' number of params

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.IsPrimaryKey Then
                _cs &= cp.ColumnName
                _cs &= ","
                _cp &= "@"
                _cp &= cp.ColumnName
                _cp &= " [int],"
            ElseIf cp.DataTypeRaw = "varchar" Or (cp.DataTypeRaw = "datetime" And cp.ColumnPosition > 5) Then
                _cc += 1
                _cs &= cp.ColumnName
                _cs &= ","
                _cp &= "@"
                _cp &= cp.ColumnName
                Select Case cp.DataTypeRaw
                    Case "varchar"
                        _cp &= " [varchar] ("
                        _cp &= cp.MaxLength
                        _cp &= "),"
                    Case Else
                        _cp &= " [datetime],"
                End Select
            End If
        Next
        _cs = _cs.TrimEnd(", ".ToCharArray)
        _cp &= " @minMatchCount [int] = "
        _cp &= (IIf(_cc > 0, 0.75 * _cc, 1)).ToString

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.DataTypeRaw = "varchar" Or (cp.DataTypeRaw = "datetime" And cp.ColumnPosition > 5) Then
                If _cq.Length > 0 Then
                    _cq &= " union all "
                    _cq &= vbCrLf
                End If
                _cq &= "Select "
                _cq &= _cs
                _cq &= " From "
                _cq &= _tbl
                _cq &= " Where ("
                _cq &= cp.ColumnName
                Select Case cp.DataTypeRaw
                    Case "varchar"
                        _cq &= " like @"
                        _cq &= cp.ColumnName
                        _cq &= "+'%' and @"
                        _cq &= cp.ColumnName
                        _cq &= "<>'')"
                    Case Else
                        _cq &= " = @"
                        _cq &= cp.ColumnName
                        _cq &= " and Cast(@"
                        _cq &= cp.ColumnName
                        _cq &= " as varchar)<>'')"
                End Select
                _cq &= " and rowstatus=0 and not "
                _cq &= _pk
                _cq &= "=@"
                _cq &= _pk
                _cq &= vbCrLf
            End If
        Next

        _sb.Append("CREATE PROCEDURE [dbo].[rkg_")
        _sb.Append(_tbl)
        _sb.Append("_compare] ")
        _sb.Append(_cp)
        _sb.AppendLine(" AS ")
        _sb.Append("select count(")
        _sb.Append(_pk)
        _sb.Append(") cnt,")
        _sb.Append(_pk)
        _sb.Append(" from ( ")
        _sb.Append(_cq)
        _sb.Append(") tbl group by ")
        _sb.Append(_pk)
        _sb.Append(" having count(")
        _sb.Append(_pk)
        _sb.Append(")>=@minMatchCount")


        If _cc > 0 Then
            CO.CommandText = _sb.ToString
        Else
            CO.CommandText = "CREATE PROCEDURE [dbo].[rkg_" & _tbl & "_compare]  AS Select 1"
        End If

        _Sql &= CO.CommandText
        CDC.Execute(CO, CSI)

        _sb = New Text.StringBuilder(5000)

        _cp = "" ' params
        _cs = "" ' scope
        _cq = "" ' section
        _cc = 0 ' number of params

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If (cp.IsForeignKey Or cp.IsForeignKeyControl) And cp.ColumnPosition > 6 Then
                _cc += 1
                _cs &= cp.ColumnName
                _cs &= ","
                _cp &= "@"
                _cp &= cp.ColumnName
                _cp &= " [int],"
            End If
        Next


        _sb.Append("CREATE PROCEDURE [dbo].[rkg_")
        _sb.Append(_tbl)
        _sb.Append("_exists] ")


        _cc = 0
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If (cp.IsForeignKey Or cp.IsForeignKeyControl) And cp.ColumnPosition > 6 Then
                _cc += 1
                If _cc > 1 Then _sb.Append(", ")
                _sb.Append("@")
                _sb.Append(cp.ColumnName)
                _sb.Append(" [int]")
            End If
        Next

        _sb.AppendLine(" AS ")

        _sb.AppendLine("SELECT * FROM ")
        _sb.AppendLine(_tbl)
        _sb.AppendLine(" WHERE rowstatus=0 AND ")
        _cc = 0
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If (cp.IsForeignKey Or cp.IsForeignKeyControl) And cp.ColumnPosition > 6 Then
                _cc += 1
                If _cc > 1 Then _sb.Append(" AND ")
                _sb.Append(cp.ColumnName)
                _sb.Append("=")
                _sb.Append("@")
                _sb.Append(cp.ColumnName)
            End If
        Next

        If _cc > 0 Then
            CO.CommandText = _sb.ToString
        Else
            CO.CommandText = "CREATE PROCEDURE [dbo].[rkg_" & _tbl & "_exists]  AS Select 0"
        End If

        _Sql &= CO.CommandText
        CDC.Execute(CO, CSI)

        _sb = New Text.StringBuilder(5000)

        _cp = "" ' params
        _cs = "" ' scope
        _cq = "" ' section
        _cc = 0 ' number of params

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If (cp.ColumnName = "user_fk") And cp.ColumnPosition > 6 Then
                _cc += 1
            End If
        Next

        _sb.Append("CREATE PROCEDURE [dbo].[rkg_")
        _sb.Append(_tbl)
        _sb.Append("_byuser] ")
        _sb.Append("@user_pk [int]")
        _sb.AppendLine(" AS ")
        _sb.AppendLine("SELECT * FROM ")
        _sb.AppendLine(_tbl)
        _sb.AppendLine(" WHERE rowstatus=0 AND user_fk=@user_pk")

        If _cc > 0 Then
            CO.CommandText = _sb.ToString
            _Sql &= CO.CommandText
            CDC.Execute(CO, CSI)
        End If




    End Sub


End Class
