Imports System.Net

Public Class ConnectionReader
    Dim _versionInfo As String = ""
    Dim _appName As String = ""
    Dim _userName As String = ""
    Dim _ConnectionString As String() = {"Data Source=UML-SQL\UMLSQL2005;Initial Catalog=se_cca;Persist Security Info=True;User ID=sa;Password=chicago", "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=servicedesk;Trusted_Connection=True;", "Data Source=uk-test-sql-01.testdev.local;Initial Catalog=servicedesk;Trusted_Connection=True;"}
    Dim _useURL As String = ""
    Dim _error As String = "Unauthorised application"

    Public ReadOnly Property VersionInfo() As String
        Get
            Return _versionInfo
        End Get
    End Property

    Public ReadOnly Property ApplicationName() As String
        Get
            Return _appName
        End Get
    End Property

    Public ReadOnly Property UserName() As String
        Get
            Return _userName
        End Get
    End Property

    Public ReadOnly Property ConnectionString() As String()
        Get
            If _connectionString Is Nothing Then
                Throw New NoConnectionsAvailableException(_error)
            Else
                If _connectionString(0).StartsWith("Acc") Then
                    Throw New AccessDeniedException(_connectionString(0))
                Else
                    Return _connectionString
                End If
            End If
        End Get
    End Property

    Public Sub New(ByVal TargetURL As String)

        Dim appInfo() As String
        If System.Reflection.Assembly.GetEntryAssembly Is Nothing Then
            _versionInfo = "=0.0"
            _appName = AppDomain.CurrentDomain.BaseDirectory
        Else
            appInfo = System.Reflection.Assembly.GetCallingAssembly.FullName.Split(",")
            _versionInfo = appInfo(1).Trim
            _appName = appInfo(0).Trim
            appInfo = Nothing
        End If

        Dim winPrin As System.Security.Principal.WindowsPrincipal = New System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent())
        _userName = winPrin.Identity.Name
        winPrin = Nothing
        _useURL = TargetURL
        ReadConnectionStringInfo()
    End Sub

    Private Sub ReadConnectionStringInfo()
        Dim _client As New WebClient
        Dim _formValues As New Specialized.NameValueCollection()
        Dim _responseData As [Byte]() = Nothing
        Try
            _formValues.Add("app", ApplicationName)
            _formValues.Add("ver", VersionInfo.Split("=")(1))
            _formValues.Add("usr", UserName)
            '_responseData = _client.UploadValues(_useURL, _formValues)
            '_connectionString = System.Text.Encoding.ASCII.GetString(_responseData).Split("|")
        Catch webEx As System.Net.WebException
            _error = webEx.Status & vbCrLf & webEx.Message
        End Try
    End Sub

End Class

Public Class NoConnectionsAvailableException
    Inherits System.ApplicationException
    Dim _ErrorMessage As String = ""

    Public Overrides ReadOnly Property Message() As String
        Get
            Return _ErrorMessage
        End Get
    End Property

    Public Sub New(Optional ByVal ErrorMessage As String = "")
        _ErrorMessage = ErrorMessage
    End Sub
End Class

Public Class AccessDeniedException
    Inherits System.ApplicationException
    Dim _ErrorMessage As String = ""

    Public Overrides ReadOnly Property Message() As String
        Get
            Return _ErrorMessage
        End Get
    End Property

    Public Sub New(Optional ByVal ErrorMessage As String = "")
        _ErrorMessage = ErrorMessage
    End Sub
End Class
