Public Class Diff

    Public Shared Function ChangeValue(ByVal original As String, ByVal update As String) As Single
        If original Is Nothing Or update Is Nothing Then
            Return 2.0
        Else
            Dim P As String = ""
            Dim N As String = ""
            Dim OP As String = ""

            Dim NPc As Integer = 0
            Dim PPc As Single = 0
            Dim PPp As Single = 0
            Dim RLP As Single = 0.0

            Dim ICost As Integer = 0
            Dim RCost As Integer = 0

            P = original.Replace(" ", "")
            OP = original.Replace(" ", "")
            N = update.Replace(" ", "")
            NPc = 0
            PPc = 0
            PPp = 0
            ICost = 0
            RCost = 0
            RLP = (P.Length ^ 0.25) + 1
            For NPc = 0 To N.Length - 1
                PPc = P.IndexOf(N.Chars(NPc), CInt(PPp))
                If PPc = -1 Then
                    OP = OP.Insert(NPc, N.Chars(NPc))
                    ICost += 1
                Else
                    If (PPc - PPp) <= RLP Then
                        If PPp <> PPc Then
                            OP = OP.Remove(PPp + (ICost - RCost), (PPc - PPp))
                            RCost += (PPc - PPp)
                        End If
                        PPp = PPc + 1
                    Else
                        OP = OP.Insert(NPc, N.Chars(NPc))
                        ICost += 1
                    End If
                End If
            Next

            RCost += (OP.Length - N.Length)
            OP = OP.Substring(0, N.Length)
            If ((P.Length + N.Length) - 2) = 0 And original = update Then
                Return 0
            ElseIf ((P.Length + N.Length) - 2) = 0 And original <> update Then
                Return ((ICost + RCost))
            Else
                Return ((ICost + RCost) / ((P.Length + N.Length) - 2))
            End If
        End If

    End Function

End Class

