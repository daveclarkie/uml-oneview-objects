Imports System.IO
Imports Winnovative.WnvHtmlConvert

Public Class PdfConversion
    Shared dpi As Single = 72

    Shared Function SingleConvert(ByVal Url As String) As Byte()
        Dim pdfConverter As New PdfConverter
        pdfConverter.LicenseKey = "HDcuPC08LS4kPCkyLDwvLTItLjIlJSUl"
        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4
        'pdfConverter.PdfDocumentOptions.CustomPdfPageSize = New Drawing.SizeF(8.3 * dpi, 11.7 * dpi)
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait
        pdfConverter.PdfDocumentOptions.JpegCompressionLevel = 0
        pdfConverter.PdfDocumentOptions.ShowHeader = False
        pdfConverter.PdfDocumentOptions.ShowFooter = False
        pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = True
        pdfConverter.PdfDocumentOptions.FitWidth = False
        pdfConverter.PdfDocumentOptions.EmbedFonts = True
        pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = True
        pdfConverter.PdfDocumentOptions.AutoSizePdfPage = False
        pdfConverter.PdfStandardSubset = PdfStandardSubset.Full

        pdfConverter.ScriptsEnabled = False
        pdfConverter.ActiveXEnabled = False
        pdfConverter.PdfDocumentOptions.JpegCompressionEnabled = False
        pdfConverter.PdfSecurityOptions.CanPrint = True
        pdfConverter.PdfSecurityOptions.CanEditContent = False
        pdfConverter.PdfDocumentInfo.AuthorName = ""

        Dim pdfBytes As Byte() = pdfConverter.GetPdfBytesFromUrl(Url)

        pdfConverter = Nothing

        Return pdfBytes
    End Function

    Shared Function MultiConvert(ByVal UrlList As String()) As Byte()

        If UrlList.Length = 0 Then
            Return Nothing
        ElseIf UrlList.Length = 1 Then
            Return SingleConvert(UrlList(0))
        End If

        Dim Url As String
        Dim mUrl As String = UrlList(0)

        Dim pdfConverter As PdfConverter = New PdfConverter()

        pdfConverter.LicenseKey = "HDcuPC08LS4kPCkyLDwvLTItLjIlJSUl"

        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4
        'pdfConverter.PdfDocumentOptions.CustomPdfPageSize = New Drawing.SizeF(8.5 * dpi, 11.0 * dpi)
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait
        pdfConverter.PdfDocumentOptions.ShowHeader = False
        pdfConverter.PdfDocumentOptions.ShowFooter = False
        pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = True
        pdfConverter.PdfDocumentOptions.FitWidth = False
        pdfConverter.PdfDocumentOptions.EmbedFonts = True
        pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = True
        pdfConverter.PdfDocumentOptions.AutoSizePdfPage = False
        pdfConverter.PdfStandardSubset = PdfStandardSubset.Full

        pdfConverter.ScriptsEnabled = False
        pdfConverter.ActiveXEnabled = False
        pdfConverter.PdfDocumentOptions.JpegCompressionEnabled = True
        pdfConverter.PdfSecurityOptions.CanPrint = True
        pdfConverter.PdfSecurityOptions.CanEditContent = False
        pdfConverter.PdfDocumentInfo.AuthorName = ""

        'pdfConverter.ConversionDelay = 1


        'pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4
        'pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
        'pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait
        'pdfConverter.PdfDocumentOptions.ShowHeader = False
        'pdfConverter.PdfDocumentOptions.ShowFooter = False
        'pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = True
        'pdfConverter.PdfDocumentOptions.FitWidth = True
        'pdfConverter.PdfDocumentOptions.EmbedFonts = False
        'pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = False
        'pdfConverter.PdfDocumentOptions.AutoSizePdfPage = False
        'pdfConverter.PdfStandardSubset = PdfStandardSubset.Full

        'pdfConverter.ScriptsEnabled = False
        'pdfConverter.ActiveXEnabled = False
        'pdfConverter.PdfDocumentOptions.JpegCompressionEnabled = True
        'pdfConverter.PdfSecurityOptions.CanPrint = True
        'pdfConverter.PdfSecurityOptions.CanEditContent = False
        'pdfConverter.PdfDocumentInfo.AuthorName = ""



        Dim s(UrlList.Length - 2) As MemoryStream
        Dim m As MemoryStream
        Dim sc As Integer = -1
        For Each Url In UrlList
            Try
                If sc <> -1 Then
                    s(sc) = New MemoryStream(pdfConverter.GetPdfBytesFromUrl(Url))
                End If
                sc += 1
            Catch ex As Exception
            End Try
        Next

        pdfConverter.PdfDocumentOptions.AppendPDFStreamArray = s
        m = New MemoryStream(pdfConverter.GetPdfBytesFromUrl(mUrl))


        pdfConverter = Nothing
        Array.Clear(s, 0, s.Length)
        s = Nothing
        Return m.ToArray


    End Function

End Class
