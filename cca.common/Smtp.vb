Imports System
Imports System.Text
Imports System.Text.Encoding
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Net.Sockets
Imports System.Threading.Thread

Public Class SMTP
    Dim SendBuffer As Byte()
    Dim args() As String
    Dim r As String
    Dim Sock As TcpClient
    Dim encoding As New System.Text.ASCIIEncoding

    Public User As String = ""
    Public UserDomain As String = ""
    Public Pass As String = ""
    Public Server As String = ""
    Public Port As Integer = 25
    Public MsgTo() As String
    Public MsgCC() As String
    Public MsgBCC() As String
    Public Subject As String

    Public HTMLBodyContent As String = ""
    Public TextBodyContent As String = ""
    Public Log As String = ""

    Public Sub Dispose()
        If Not Sock Is Nothing Then
            If Sock.Connected Then Sock.Close()
            Sock = Nothing
        End If
    End Sub

    Public Sub Dispose(ByVal dispose As Boolean)
        If Sock.Connected Then Sock.Close()
        Sock = Nothing
    End Sub

    Private Function base64Encode(ByVal strPlain As String) As String
        If strPlain.Length = 0 Then
            Return ""
        Else
            Dim encoding As New System.Text.ASCIIEncoding
            Return System.Convert.ToBase64String(encoding.GetBytes(strPlain))
        End If
    End Function

    Private Function base64Decode(ByVal strEncoded As String) As String
        If strEncoded.Length = 0 Then
            Return ""
        Else
            Dim enc As New System.Text.ASCIIEncoding
            ' strEncoded.Replace(" ", "+") is required to work round the auto urldecoding
            Return enc.GetString(System.Convert.FromBase64String(strEncoded.Replace(" ", "+")))
        End If
    End Function

    Private Function SmtpConnect() As Boolean
        Sock = New TcpClient
        Try
            Sock.Connect(Server, Port)
            r = GetRspn("")
            r = GetRspn("EHLO " & UserDomain)
            If r = "" Then Throw New Exception("Welcome Msg didn't appear")
            Return True
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.ToString)
            Return False
        End Try
    End Function

    Private Function SmtpLogin() As Boolean
        Dim blnLoginOK As Boolean = False
        Try
            If Pass = "" Then
                blnLoginOK = True
            Else
                r = GetRspn("AUTH LOGIN")
                If r.StartsWith("334 VXNlcm5hbWU6") Then
                    r = GetRspn(base64Encode(User))
                    If r.StartsWith("334 UGFzc3dvcmQ6") Then
                        r = GetRspn(base64Encode(Pass))
                        If r.StartsWith("235 ") Then
                            blnLoginOK = True
                        End If
                    End If
                End If
            End If
            Return blnLoginOK
        Catch e As Exception
            Return False
        End Try
    End Function

    Private Sub SMTPClose()
        Try
            r = GetRspn("QUIT")
            Sock.Close()
        Catch e As Exception
        Finally
            Sock = Nothing
        End Try
    End Sub

    Private Function SetSMTPFrom() As Boolean
        Dim blnFromSet As Boolean = False
        Try
            r = GetRspn("MAIL FROM:<" & User & ">")
            If Not r.StartsWith("250 ") Then Throw New Exception("Problem with from address:" & User)
            blnFromSet = True
        Catch ex As Exception
            Log &= ex.ToString
        End Try
        Return blnFromSet
    End Function

    Private Function SetSMTPTo() As Boolean
        Dim blnToSet As Boolean = False
        Dim s As String
        If Not MsgTo Is Nothing Then
            For Each s In MsgTo
                Try
                    r = GetRspn("RCPT TO: <" & s & ">")
                    If Not r.StartsWith("250 ") Then Throw New Exception("Problem with to address:" & s)
                    blnToSet = True
                Catch ex As Exception
                    Log &= ex.ToString
                End Try
            Next
        End If

        If Not MsgCC Is Nothing Then
            For Each s In MsgCC
                Try
                    r = GetRspn("RCPT TO: <" & s & ">")
                    If Not r.StartsWith("250 ") Then Throw New Exception("Problem with cc address:" & s)
                    blnToSet = True
                Catch ex As Exception
                    Log &= ex.ToString
                End Try
            Next
        End If

        If Not MsgBCC Is Nothing Then
            For Each s In MsgBCC
                Try
                    r = GetRspn("RCPT TO: <" & s & ">")
                    If Not r.StartsWith("250 ") Then Throw New Exception("Problem with bcc address:" & s)
                    blnToSet = True
                Catch ex As Exception
                    Log &= ex.ToString
                End Try
            Next
        End If

        Return blnToSet
    End Function

    Private Function SetSMTPData() As Boolean
        Dim blnSent As Boolean = False
        r = GetRspn("DATA")
        If r.StartsWith("354") Then
            r = GetRspn(BuildMsg)
            If r.StartsWith("250 ") Then
                blnSent = True
            End If
        End If
        Return blnSent
    End Function

    Public Function SMTPSend() As Boolean
        Dim blnSent As Boolean = False
        Try
            If SmtpConnect() Then
                If SmtpLogin() Then
                    If SetSMTPFrom() Then
                        If SetSMTPTo() Then
                            If SetSMTPData() Then
                                blnSent = True
                            End If
                        End If
                    End If
                End If
            End If
            SMTPClose()
        Catch ex As Exception
            Log &= ex.ToString
            SMTPClose()
        End Try
        Return blnSent
    End Function

    Private Function BuildMsg() As String
        Dim msg As New StringBuilder
        Dim s As String
        msg.Append("From:")
        msg.Append(User)
        msg.Append(vbCrLf)
        msg.Append("To:")
        For Each s In MsgTo
            msg.Append(s)
            msg.Append(",")
        Next
        If MsgTo.Length <> 0 Then msg.Remove(msg.Length - 1, 1)
        msg.Append(vbCrLf)
        If Not MsgCC Is Nothing Then
            If MsgCC.Length <> 0 Then
                msg.Append("CC:")
                For Each s In MsgTo
                    msg.Append(s)
                    msg.Append(",")
                Next
                msg.Remove(msg.Length - 1, 1)
                msg.Append(vbCrLf)
            End If
        End If

        msg.Append("Subject:")
        msg.Append(Subject)
        msg.Append(vbCrLf)
        msg.Append("Content-Type:")

        If HTMLBodyContent <> "" Then
            msg.Append("text/html")
        Else
            msg.Append("text/plain")
        End If
        msg.Append(vbCrLf)
        msg.Append(vbCrLf)

        If HTMLBodyContent <> "" Then
            msg.Append(HTMLBodyContent)
        Else
            msg.Append(TextBodyContent)
        End If
        msg.Append(vbCrLf)
        msg.Append(".")

        Return msg.ToString
    End Function

    Private Function GetRspn(ByVal Cmd As String) As String
        Log &= Cmd
        Log &= vbCrLf

        Dim sRcv As String = ""
        Dim aResult As IAsyncResult
        Dim SockStream As NetworkStream
        Dim p As Long = 0
        Dim RecvBuffer(99) As Byte
        Array.Clear(RecvBuffer, 0, 100)
        If Cmd.Length <> 0 Then Cmd = Cmd & vbCrLf
        SendBuffer = UTF8.GetBytes(Cmd)
        Try
            SockStream = Sock.GetStream()
            SockStream.Write(SendBuffer, 0, SendBuffer.Length)
            Do Until SockStream.DataAvailable
                Sleep(5)
                p += 1
                If p > 500 Then
                    If Cmd <> "" Then
                        SockStream.Close()
                        Sock = Nothing
                    Else
                    End If
                    Return ""
                    Exit Function
                End If
            Loop
            sRcv = ""

            If Cmd.StartsWith("EHLO") Then
                Dim s() As String
                Dim t As String
                Dim blnDone As Boolean = False
                Do
                    Do While SockStream.DataAvailable
                        Array.Clear(RecvBuffer, 0, 100)
                        aResult = SockStream.BeginRead(RecvBuffer, 0, 100, Nothing, Nothing)
                        aResult.AsyncWaitHandle.WaitOne()
                        sRcv = sRcv & UTF8.GetString(RecvBuffer, 0, SockStream.EndRead(aResult))
                        Sleep(5)
                    Loop
                    s = sRcv.Split(vbLf.ToString)
                    For Each t In s
                        If t.StartsWith("250 ") Then blnDone = True
                    Next
                Loop Until blnDone
                Log &= sRcv
                Log &= vbCrLf
                Return sRcv
            Else
                Do While SockStream.DataAvailable
                    Array.Clear(RecvBuffer, 0, 100)
                    aResult = SockStream.BeginRead(RecvBuffer, 0, 100, Nothing, Nothing)
                    aResult.AsyncWaitHandle.WaitOne()
                    sRcv = sRcv & UTF8.GetString(RecvBuffer, 0, SockStream.EndRead(aResult))
                    Sleep(5)
                Loop
                Log &= sRcv
                Log &= vbCrLf
                Return sRcv
            End If
        Catch dumb As Exception
            Log &= dumb.ToString
            Log &= vbCrLf
            Return ""
        End Try
    End Function

End Class
