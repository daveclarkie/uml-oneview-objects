Imports System.Net
Imports System.Xml

Public Class HTMLResponse

    Public Shared Function PostTo(url As String, postData As String) As String
        Dim myWebRequest As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
        myWebRequest.Method = "POST"
        Dim byteArray As Byte() = System.Text.Encoding.[Default].GetBytes(postData)
        myWebRequest.ContentType = "application/x-www-form-urlencoded"
        myWebRequest.ContentLength = byteArray.Length
        Dim dataStream As System.IO.Stream = myWebRequest.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()

        Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
        'Response.Write((DirectCast(myWebResponse, HttpWebResponse)).StatusDescription)
        dataStream = myWebResponse.GetResponseStream()
        Dim reader As New System.IO.StreamReader(dataStream)
        Dim responseFromServer As String = "<?xml version='1.0'?>" & reader.ReadToEnd()
        'Response.Write(responseFromServer + ":")

        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(responseFromServer)

        Dim formResponse As String = "<table>"
        Dim outcome As String = xmlDoc.SelectSingleNode("/operation/operationstatus").InnerText
        formResponse &= "<tr><td width='100px'>Outcome</td> <td>" & outcome & "</td></tr>"
        If outcome = "Success" Then
            formResponse &= "<tr><td>Request ID</td> <td>" & xmlDoc.SelectSingleNode("/operation/workorderid").InnerText & "</td></tr>"
        ElseIf outcome = "Failure" Then
            Dim xmlReason As String = xmlDoc.SelectSingleNode("/operation/message").InnerText
            formResponse &= "<tr><td>Reason</td> <td>" & xmlReason & "</td></tr>"
            If xmlReason = "Invalid UserName or Password" Then
                formResponse &= "<tr><td></td> <td>Please contact <a style='color:blue;text-decoration:underline;' href='mailto:dave.clarke@ems.schneider-electric.com?Subject=UTILISYS Helpdesk User Account Error'>Dave Clarke</a></td></tr>"
            End If
        End If
        formResponse &= "</table>"

        reader.Close()
        dataStream.Close()
        myWebResponse.Close()

        Return formResponse

    End Function

End Class

