Public Class Security

    Shared Function ReferalCheck(ByRef rqst As Object) As Boolean
        Return rqst.servervariables("HTTP_REFERER").ToString.StartsWith("http://" & rqst.ServerVariables("HTTP_HOST"))
    End Function


    'Shared Sub ItemLevels(ByVal User As Integer, ByVal ItemType As Integer, ByRef ReadLevel As Integer, ByRef WriteLevel As Integer, ByRef CDC As DataCommon)

    '    Dim cmd As New SqlClient.SqlCommand()
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.CommandText = "[rsp_productpermissions]"
    '    cmd.Parameters.AddWithValue("@user", User)
    '    cmd.Parameters.AddWithValue("@itemtype", ItemType)

    '    Dim dr As DataRow = CDC.ReadDataRow(cmd)
    '    Try
    '        ReadLevel = dr("readlevel")
    '    Catch ex As Exception
    '        ReadLevel = 0
    '    End Try
    '    Try
    '        WriteLevel = dr("writelevel")
    '    Catch ex As Exception
    '        WriteLevel = 0
    '    End Try

    '    dr.Table.Dispose()
    '    dr = Nothing
    '    cmd.Dispose()

    'End Sub

    'Shared Function AccessCheck(ByVal User As Integer, ByVal Permission As SystemPermissions, ByRef CDC As DataCommon) As Boolean
    '    Dim retval As Boolean = False
    '    Dim cmd As New SqlClient.SqlCommand()
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.CommandText = "rsp_haspermission"
    '    cmd.Parameters.AddWithValue("@user", User)
    '    cmd.Parameters.AddWithValue("@permission", Permission)
    '    Dim allowed As Integer = 0
    '    CDC.ReadScalarValue(allowed, cmd)
    '    retval = IIf(allowed = 1, True, False)
    '    cmd.Dispose()
    '    Return retval
    'End Function

    Shared Function GroupMember(ByVal User As Integer, ByVal Group As Integer, ByRef CDC As DataCommon) As Boolean
        'If Group > 0 And User > 0 Then
        Dim retval As Boolean = False
        Dim cmd As New SqlClient.SqlCommand()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "rsp_isgroupmember"
        cmd.Parameters.AddWithValue("@user", User)
        cmd.Parameters.AddWithValue("@group", Group)
        Dim allowed As Integer = 0
        CDC.ReadScalarValue(allowed, cmd)
        retval = IIf(allowed = 1, True, False)
        cmd.Dispose()
        Return retval
        'Else
        'Return False
        'End If
    End Function

    Shared Function ReadLevel(ByVal User As Integer, ByVal Control As String, ByRef CDC As DataCommon) As Integer
        Dim RL As Integer = 0
        Dim cmd As New SqlClient.SqlCommand()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "rsp_readlevel"
        cmd.Parameters.AddWithValue("@user", User)
        cmd.Parameters.AddWithValue("@control", Control)
        CDC.ReadScalarValue(RL, cmd)
        Return RL
    End Function

    Shared Function WriteLevel(ByVal User As Integer, ByVal Control As String, ByRef CDC As DataCommon) As Integer
        Dim WL As Integer = 0
        Dim cmd As New SqlClient.SqlCommand()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "rsp_writelevel"
        cmd.Parameters.AddWithValue("@user", User)
        cmd.Parameters.AddWithValue("@control", Control)
        CDC.ReadScalarValue(WL, cmd)
        Return WL
    End Function


End Class
