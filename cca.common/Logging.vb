'Imports System.Diagnostics
'Imports System.Reflection

'Public Class Logging

'    Public Shared Function CallingFunction() As String
'        Dim st As New StackTrace
'        Dim sf As StackFrame
'        sf = st.GetFrame(2)
'        Dim mb As MethodBase = sf.GetMethod()
'        Return mb.Name
'    End Function

'    Public Shared Function CallingClass() As String
'        Dim st As New StackTrace
'        Dim sf As StackFrame
'        sf = st.GetFrame(2)
'        Dim mb As MethodBase = sf.GetMethod()
'        Return mb.Module.Name
'    End Function

'    Public Shared Sub LogAction(ByVal user_pk As Integer, ByVal sessionid As String, ByVal ip As String, Optional ByVal url As String = "", Optional ByVal server As String = "")

'        Dim l As New logs()
'        Try
'            l.created = Now
'            l.user_fk = user_pk
'            l.sessionid = sessionid
'            l.ip = ip
'            l.url = url
'            l.classname = CallingClass()
'            l.methodname = CallingFunction()
'            l.server = server
'        Catch oex As Exception
'        End Try
'        If Not l.Save() Then
'            Throw l.LastError
'        End If

'    End Sub

'    Public Shared Sub LogAction(ByVal user_pk As Integer, ByVal sessionid As String, ByVal ip As String, ByRef ex As Exception, Optional ByVal url As String = "", Optional ByVal server As String = "")
'        Dim l As New logs()
'        Try
'            l.created = Now
'            l.user_fk = user_pk
'            l.sessionid = sessionid
'            l.ip = ip
'            l.url = url
'            l.ex = ex
'            l.classname = CallingClass()
'            l.methodname = CallingFunction()
'            l.server = server
'        Catch oex As Exception
'        End Try
'        If Not l.Save() Then
'            Throw l.LastError
'        End If

'    End Sub


'End Class

'Public Class logs

'    Dim _LastError As Exception = Nothing
'    Dim _log_pk As Integer
'    Dim _created As DateTime
'    Dim CDC As New DataCommon

'    Public ReadOnly Property LastError() As Exception
'        Get
'            If _LastError Is Nothing Then
'                If CDC.LastError Is Nothing Then
'                    _LastError = New Exception()
'                Else
'                    _LastError = CDC.LastError
'                End If
'            Else
'                _LastError = New Exception()
'            End If
'            Return _LastError
'        End Get
'    End Property

'    Public WriteOnly Property created() As DateTime
'        Set(ByVal value As DateTime)
'            _created = value
'        End Set
'    End Property

'    Dim _user_fk As Integer
'    Public WriteOnly Property user_fk() As Integer
'        Set(ByVal value As Integer)
'            _user_fk = value
'        End Set
'    End Property

'    Dim _sessionid As String
'    Public WriteOnly Property sessionid() As String
'        Set(ByVal value As String)
'            _sessionid = value
'        End Set
'    End Property

'    Dim _ip As String
'    Public WriteOnly Property ip() As String
'        Set(ByVal value As String)
'            _ip = value
'        End Set
'    End Property

'    Dim _url As String
'    Public WriteOnly Property url() As String
'        Set(ByVal value As String)
'            _url = value
'        End Set
'    End Property

'    Dim _ex As String = ""
'    Public WriteOnly Property ex() As Exception
'        Set(ByVal value As Exception)
'            Try
'                _ex = value.Message
'            Catch innerex As Exception
'                _ex = "No error message"
'            End Try
'        End Set
'    End Property

'    Dim _classname As String
'    Public WriteOnly Property classname() As String
'        Set(ByVal value As String)
'            _classname = value
'        End Set
'    End Property

'    Dim _methodname As String
'    Public WriteOnly Property methodname() As String
'        Set(ByVal value As String)
'            _methodname = value
'        End Set
'    End Property

'    Dim _server As String = ""
'    Public WriteOnly Property server() As String
'        Set(ByVal value As String)
'            _server = value
'        End Set
'    End Property


'    Public Function Save() As Boolean
'        Dim _ReturnStatus As Boolean = True
'        Try
'            Dim PO As New SqlClient.SqlParameter()
'            Dim CO As New SqlClient.SqlCommand()
'            CO.CommandType = CommandType.StoredProcedure
'            CO.CommandText = "log_logs_save"
'            PO = New SqlClient.SqlParameter("@log_pk", -1) : CO.Parameters.Add(PO)
'            PO = New SqlClient.SqlParameter("@created", CommonFN.CheckEmptyDate(_created.ToString())) : CO.Parameters.Add(PO)
'            PO = New SqlClient.SqlParameter("@user_fk", _user_fk) : CO.Parameters.Add(PO)
'            PO = New SqlClient.SqlParameter("@sessionid", _sessionid) : CO.Parameters.Add(PO)
'            PO = New SqlClient.SqlParameter("@ip", _ip) : CO.Parameters.Add(PO)
'            PO = New SqlClient.SqlParameter("@url", _url) : CO.Parameters.Add(PO)
'            PO = New SqlClient.SqlParameter("@ex", _ex) : CO.Parameters.Add(PO)
'            PO = New SqlClient.SqlParameter("@classname", _classname) : CO.Parameters.Add(PO)
'            PO = New SqlClient.SqlParameter("@methodname", _methodname) : CO.Parameters.Add(PO)
'            PO = New SqlClient.SqlParameter("@server", _server) : CO.Parameters.Add(PO)
'            _ReturnStatus = CDC.Execute(CO)
'        Catch fEx As Exception
'            _ReturnStatus = False
'            _LastError = fEx
'        End Try
'        Return _ReturnStatus
'    End Function



'End Class
