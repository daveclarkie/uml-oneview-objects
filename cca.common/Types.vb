Public Enum SystemGroups As Integer
    AccessToBackendSystems = 1
    ManageUsers = 2
    ManageGroups = 3
    Staff = 4
    Managers = 5
    Admin = 6
    CreateAgreementSnapshots = 7
    CCASystem = 8
    MatrixSystem = 9
    ContractManagerSystem = 10
    demandSystem = 18
End Enum

Public Enum RowStatusType As Integer
    Active = 0
    Disabled = 1
    Deleted = 2
End Enum

Public Enum UnderlyingDatabaseType As Integer
    None = -1
    MSSQL = 0
    PostgreSQL = 1
    MySQL = 2
End Enum

Public Enum ValidationType As Integer
    None = 0
    Required = 1
    Range = 2
    Regex = 3
    LookUp = 4
    FixedLookUp = 5
End Enum

Public Enum TaskCountType As Integer
    Today = 0
    Overdue = 1
End Enum

Public Enum UserListType As Integer
    Recent = 0
    Popular = 1
End Enum

Public Enum PageLoadMode As Integer
    NotReady = 0
    LoadList = 1
    LoadItem = 2
    SaveItem = 3
    NewItem = 4
End Enum

Public Enum InvoicePageMode As Integer
    Search = 0
    Pack = 1
    Product = 2
    Tasks = 3
    Docs = 4
    Downloads = 5
    Notes = 6
End Enum

Public Enum AgreementPageMode As Integer
    Search = 0
    Agreement = 1
    AgreementMeterPoints = 2
    AgreementThroughputs = 3
    AgreementTargets = 4
    AgreementFuels = 5
    DataEntryConsumption = 6
    DataEntryThroughputs = 7
    Reports = 8
    ActivityLogs = 9
    DataAnalysis = 10
    AgreementRequests = 11
    AgreementNotes = 12
    Overview = 13
    Appointments = 14
    Contacts = 15
    Submission = 16
    Summary = 17
    Ranking = 18
End Enum

Public Enum BubblePageMode As Integer
    Search = 0
    Details = 1
    Targets = 2
    Agreements = 3
    Reports = 4
End Enum
Public Enum CustomerPageMode As Integer
    Search = 0
    Customer = 1
    SiteInformation = 2
    MeterInformation = 3
    ServiceAgreements = 4
    ContractInformation = 5
    Appointments = 6
    Notes = 7
    CCAAgreements = 8
End Enum

Public Enum MatrixPageMode As Integer
    Search = 0
    Matrix = 1
    Price = 2
    Detail = 3
    Report = 4
End Enum

Public Enum DataEntryPageMode As Integer
    Search = 0
    Consumption = 1
    Throughput = 2
End Enum

Public Enum PersonPageMode As Integer
    Search = 0
    Person = 1
    Location = 2
    Phone = 3
    Email = 4
    Helpdesk = 5
    Hierarchy = 6
End Enum

Public Enum SystemItemType As Integer
    FailedLogins = 0
    MultipleLogins = 1
    ActiveUsers = 2
    DormantAccounts = 3
    ActiveSessions = 4
    ActivateOnLive = 5
End Enum

Public Enum InformationItemType As Integer
    CountOrganisations = 0
    CountAgreements = 1
    CountMeterPoints = 2
    CountThroughputs = 3
    CountSites = 4
    CountMeterPointsType1 = 5
    CountMeterPointsType2 = 6
    CountMeterPointsType3 = 7

    CountUKElecPrices = 8
    CountUKGasPrices = 9
    CountMatrixToday = 10

    CountCCAReports = 11

    helpdesk_openrequests = 12

    countmyopensurveys = 13

    organisation_supplypoints = 14
    organisation_contracts = 15
    organisation_ccaagreements = 16
    organisation_appointments = 17

End Enum



'Public Enum SystemPermissions As Integer  ' MUST match values in dbo.permissions
'    Access_To_Backend_Systems = 14
'    Activate_User_Account = 15
'End Enum

Public Enum SystemOffices As Integer
    CancellationController = 1
End Enum
