Public Class WebControlGenerator

    Dim dr As System.Data.DataRow
    Dim _CodeBehind As String = ""
    Dim _WebControl As String = ""

    Public ReadOnly Property CodeBehind() As String
        Get
            Return _CodeBehind
        End Get
    End Property

    Public ReadOnly Property WebControl() As String
        Get
            Return _WebControl
        End Get
    End Property


    Public Sub New(ByRef Table As DataTable, Optional ByRef DataClass As DataCommon = Nothing)
        Dim CDC As DataCommon = DataClass
        If CDC Is Nothing Then CDC = New DataCommon()
        Dim _wc As New Text.StringBuilder(5000)
        Dim _cb As New Text.StringBuilder(5000)
        Dim cp As ColumnStructure
        Dim _pk As String = ""
        Dim _tbl As String = ""
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.IsPrimaryKey Then
                _pk = cp.ColumnName
                _tbl = cp.TableName
            End If
        Next


        Dim _SectionTitle As String = _tbl
        If _tbl.StartsWith("item") And _tbl.EndsWith("details") Then

            Dim pid As Integer = CInt(_tbl.Replace("item", "").Replace("details", ""))

            Dim cmd As New SqlClient.SqlCommand("select itemname from itemtypes where itemtype_pk=" & pid)


            If Not CDC.ReadScalarValue(_SectionTitle, cmd) Then
                _SectionTitle = _tbl
            End If

        End If

        _wc.AppendLine("<%@ Control Language=""VB"" AutoEventWireup=""false"" CodeFile=""" & _tbl & "_ctl.ascx.vb"" Inherits=""" & _tbl & "_ctl"" %>")

        Dim ctlAdded As Boolean = False
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If cp.IsForeignKeyControl And Not ctlAdded Then
                ctlAdded = True
            End If
        Next

        _wc.AppendLine("<h5>" & _SectionTitle & "</h5>")
        _wc.AppendLine("<ul class='formcontrol'>")

        _cb.AppendLine("Partial Class " & _tbl & "_ctl")
        _cb.AppendLine("   Inherits DaveControl")

        _cb.AppendLine("   Public PSM As MySM")

        _cb.AppendLine("   Public Sub InitControl() Handles MyBase.InitSpecificControl")
        _cb.AppendLine("       ' Place any page specific initialisation code here")
        _cb.AppendLine("       PSM = SM")
        _cb.AppendLine("       CtlInit()")
        _cb.AppendLine("   End Sub")
        _cb.AppendLine("   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl")
        _cb.AppendLine("      ' Place any page specific initialisation code here")
        _cb.AppendLine("   End Sub")

        _cb.AppendLine("   Public WithEvents o" & _tbl & " As Objects." & _tbl)
        _cb.AppendLine("   Dim CO As SqlClient.SqlCommand")
        _cb.AppendLine("   Dim _CurrentPk as Integer=0")
        _cb.AppendLine("   Dim _Status as Integer=0")
        _cb.AppendLine("   Public LastError As Exception")
        _cb.AppendLine("   Public ReadOnly Property CurrentPk() as Integer")
        _cb.AppendLine("        Get")
        _cb.AppendLine("            Return _CurrentPk")
        _cb.AppendLine("        End Get")
        _cb.AppendLine("   End Property")
        _cb.AppendLine("   Public Property Status() as Integer")
        _cb.AppendLine("        Get")
        _cb.AppendLine("            Return _Status")
        _cb.AppendLine("        End Get")
        _cb.AppendLine("        Set (ByVal value As integer)")
        _cb.AppendLine("            _Status=value")
        _cb.AppendLine("            SetControlStatus")
        _cb.AppendLine("        End Set")
        _cb.AppendLine("   End Property")
        _cb.AppendLine("   Dim _read as boolean=false")
        '_cb.AppendLine("   Public Sub SetCDC(byref ExtCDC as DataCommon)")
        '_cb.AppendLine("       CDC=ExtCDC")
        '_cb.AppendLine("   End Sub")

        _cb.AppendLine("   Public Sub ctlInit()")
        _cb.AppendLine("        If _read Then Exit Sub")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If Not cp.IsPrimaryKey Then
                If Not (cp.ColumnName = "user_fk" And cp.TableName = "persons") Then
                    Select Case cp.ColumnName
                        Case "creator_fk", "editor_fk", "created", "modified", "rowstatus", "invoice_fk"
                        Case Else
                            Select Case cp.DataType
                                Case "Integer"
                                    If cp.IsForeignKey Then
                                        If cp.ColumnValidationType = ValidationType.LookUp Then
                                            Dim params() As String
                                            Dim param As String
                                            Dim lookup As String = ""
                                            Dim prms As String = ""
                                            params = cp.ColumnValidationExpression.Split(",".ToCharArray())
                                            For Each param In params
                                                lookup &= "_"
                                                lookup &= param
                                                prms &= "   CO.Parameters.Add(New SqlClient.SqlParameter(""@" & param & """, SM." & IIf(param = "user", "TargetUser", param) & "))"
                                            Next
                                            _cb.AppendLine("    CO=new SqlClient.SqlCommand(""rsp_lkp_" & cp.ForeignKeyTable & lookup & """)")
                                            _cb.AppendLine("    CO.CommandType = CommandType.StoredProcedure")
                                            _cb.AppendLine(prms)
                                        ElseIf cp.ColumnValidationType = ValidationType.FixedLookUp Then
                                            Dim params() As String
                                            Dim param As String
                                            Dim lookup As String = ""
                                            params = cp.ColumnValidationExpression.Split(",".ToCharArray())
                                            For Each param In params
                                                lookup &= "_"
                                                lookup &= param
                                            Next
                                            _cb.AppendLine("    CO=new SqlClient.SqlCommand(""rsp_lkp_" & cp.ForeignKeyTable & lookup & """)")
                                            _cb.AppendLine("    CO.CommandType = CommandType.StoredProcedure")
                                        Else
                                            _cb.AppendLine("    CO=new SqlClient.SqlCommand(""rkg_" & cp.ForeignKeyTable & "_lookup"")")
                                            _cb.AppendLine("    CO.CommandType = CommandType.StoredProcedure")
                                        End If
                                        _cb.AppendLine("    If Me." & cp.ColumnName & ".Items.Count=0 Then")
                                        _cb.AppendLine("        Me." & cp.ColumnName & ".DataSource=CDC.ReadDataTable(CO)")
                                        _cb.AppendLine("        Me." & cp.ColumnName & ".DataTextField = ""value""")
                                        _cb.AppendLine("        Me." & cp.ColumnName & ".DataValueField = ""pk""")
                                        _cb.AppendLine("        Try")
                                        _cb.AppendLine("            Me." & cp.ColumnName & ".DataBind")
                                        _cb.AppendLine("        Catch Ex as Exception")
                                        _cb.AppendLine("            Me." & cp.ColumnName & ".SelectedValue=-1")
                                        _cb.AppendLine("            Me." & cp.ColumnName & ".DataBind")
                                        _cb.AppendLine("        End Try")
                                        _cb.AppendLine("    End If")
                                    ElseIf cp.IsForeignKeyControl Then



                                    Else
                                        _cb.AppendLine("            Me." & cp.ColumnName & ".Text=""0""")
                                    End If
                                Case "Double"
                                    _cb.AppendLine("            Me." & cp.ColumnName & ".Text=""0.00""")
                            End Select
                    End Select
                End If
            End If
        Next
        _cb.AppendLine("        _read=True")
        _cb.AppendLine("   End Sub")

        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If Not cp.IsPrimaryKey Then
                If Not (cp.ColumnName = "user_fk" And cp.TableName = "persons") Then
                    Select Case cp.ColumnName
                        Case "creator_fk", "editor_fk", "created", "modified", "rowstatus"
                        Case Else
                            _wc.AppendLine("<li runat=""server"" id=""blk" & cp.ColumnName & """>")
                            _wc.AppendLine("<span class='label'>" & cp.ColumnLabelFull & "</span>")

                            If cp.IsForeignKeyControl Then
                            Else
                                Select Case cp.DataType
                                    Case "Integer"
                                        If cp.IsForeignKey Then
                                            _wc.AppendLine("<asp:DropDownList ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_ddl"" />")
                                        Else
                                            _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_num""  />")
                                        End If
                                    Case "String"
                                        If cp.MaxLength > 150 Then
                                            _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Multiline"" runat=""server"" cssClass=""input_box"" />")
                                        Else
                                            _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Singleline"" runat=""server"" cssClass=""input_str"" />")
                                        End If
                                    Case "Double"
                                        _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_dbl"" />")
                                    Case "Char()"
                                        _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Multiline"" runat=""server"" cssClass=""input_box"" />")
                                    Case "Byte()"
                                        _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Multiline"" runat=""server"" cssClass=""input_box"" />")
                                    Case "Boolean"
                                        _wc.AppendLine("<asp:CheckBox ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_chk"" />")
                                    Case "Datetime"
                                        _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ runat=""server"" cssClass=""input_dtm"" />")
                                    Case "Object"
                                        _wc.AppendLine("<asp:TextBox EnableViewState=""false"" ID=""" & cp.ColumnName & """ TextMode=""Multiline"" runat=""server"" cssClass=""input_box"" />")
                                End Select
                            End If
                            _wc.AppendLine("</li>")
                    End Select
                End If
            End If
        Next
        _wc.AppendLine("</ul>")


        _cb.AppendLine("Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)")
        _cb.AppendLine("  ctlInit()")
        _cb.AppendLine("If o" & _tbl & " is Nothing then")
        _cb.AppendLine("o" & _tbl & " = new Objects." & _tbl & "(currentuser,actualuser,pk,CDC)")
        _cb.AppendLine("End If")
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If Not cp.IsPrimaryKey Then
                If Not (cp.ColumnName = "user_fk" And cp.TableName = "persons") Then
                    Select Case cp.ColumnName
                        Case "creator_fk", "editor_fk", "created", "modified", "rowstatus"
                        Case Else
                            Select Case cp.DataType
                                Case "Integer"
                                    If cp.IsForeignKey Then
                                        _cb.AppendLine("Me." & cp.ColumnName & ".SelectedValue=o" & _tbl & "." & cp.ColumnName)
                                    ElseIf cp.IsForeignKeyControl Then
                                    Else
                                        _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                                        _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onFocus"", ""setNotice('Format: (n)n eg 4736')"")")
                                        _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onBlur"", ""clearNotice()"")")
                                    End If
                                Case "String"
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                                Case "Double"
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onFocus"", ""setNotice('Format: (n)n(.nn) eg 1234.56')"")")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onBlur"", ""clearNotice()"")")
                                Case "Char()"
                                    _cb.AppendLine("'Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                                Case "Byte()"
                                    _cb.AppendLine("'Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                                Case "Boolean"
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Checked=o" & _tbl & "." & cp.ColumnName)
                                Case "Datetime"
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName & ".ToString(""dd MMM yyyy"")")
                                    _cb.AppendLine("If Me." & cp.ColumnName & ".Text=""01 Jan 0001"" then Me." & cp.ColumnName & ".Text=""""")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onFocus"", ""setNotice(dateFormat)"")")
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Attributes.Add(""onBlur"", ""clearNotice()"")")
                                Case "Object"
                                    _cb.AppendLine("Me." & cp.ColumnName & ".Text=o" & _tbl & "." & cp.ColumnName)
                            End Select
                    End Select
                End If
            End If
        Next


        _cb.AppendLine("_CurrentPk=o" & _tbl & "." & _pk)
        _cb.AppendLine("Status=o" & _tbl & ".rowstatus")
        _cb.AppendLine("_read=True")
        _cb.AppendLine("End Sub")

        _cb.AppendLine("Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean")
        _cb.AppendLine("If o" & _tbl & " is Nothing then")
        _cb.AppendLine("o" & _tbl & " = new Objects." & _tbl & "(currentuser,actualuser,pk,CDC)")
        _cb.AppendLine("End If")
        _cb.AppendLine("Dim result as boolean=false")
        _cb.AppendLine("Try")
        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If Not cp.IsPrimaryKey Then
                If Not (cp.ColumnName = "user_fk" And cp.TableName = "persons") Then
                    Select Case cp.ColumnName
                        Case "creator_fk", "editor_fk", "created", "modified", "rowstatus", "invoice_fk"
                        Case Else
                            Select Case cp.DataType
                                Case "Integer"
                                    If cp.IsForeignKey Then
                                        _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".SelectedValue")
                                    ElseIf cp.IsForeignKeyControl Then
                                    Else
                                        _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                                    End If
                                Case "String"
                                    _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                                Case "Double"
                                    _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                                Case "Char()"
                                    _cb.AppendLine("'o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                                Case "Byte()"
                                    _cb.AppendLine("'o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                                Case "Boolean"
                                    _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Checked")
                                Case "Datetime"
                                    _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=CommonFN.CheckEmptyDate(Me." & cp.ColumnName & ".Text)")
                                Case "Object"
                                    _cb.AppendLine("o" & _tbl & "." & cp.ColumnName & "=Me." & cp.ColumnName & ".Text")
                            End Select
                    End Select
                End If
            End If
        Next

        _cb.AppendLine("result=o" & _tbl & ".Save()")
        _cb.AppendLine("if not result then throw o" & _tbl & ".LastError")
        _cb.AppendLine("_CurrentPk=o" & _tbl & "." & _pk)
        _cb.AppendLine("Catch Ex as Exception")
        _cb.AppendLine("result=False")
        _cb.AppendLine("LastError=Ex")
        _cb.AppendLine("End Try")
        _cb.AppendLine("Status=o" & _tbl & ".rowstatus")
        _cb.AppendLine("Return result")
        _cb.AppendLine("End Function")

        _cb.AppendLine("Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean")
        _cb.AppendLine("  If o" & _tbl & " is Nothing then")
        _cb.AppendLine("   o" & _tbl & " = new Objects." & _tbl & "(currentuser,actualuser,pk,CDC)")
        _cb.AppendLine("  End If")
        _cb.AppendLine("  o" & _tbl & ".rowstatus=0")
        _cb.AppendLine("  Return Save(currentuser,actualuser,pk)")
        _cb.AppendLine("End Function")

        _cb.AppendLine("Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean")
        _cb.AppendLine("  If o" & _tbl & " is Nothing then")
        _cb.AppendLine("   o" & _tbl & " = new Objects." & _tbl & "(currentuser,actualuser,pk,CDC)")
        _cb.AppendLine("  End If")
        _cb.AppendLine("  o" & _tbl & ".rowstatus=1")
        _cb.AppendLine("  Return Save(currentuser,actualuser,pk)")
        _cb.AppendLine("End Function")

        _cb.AppendLine("Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean")
        _cb.AppendLine("  If o" & _tbl & " is Nothing then")
        _cb.AppendLine("   o" & _tbl & " = new Objects." & _tbl & "(currentuser,pk,CDC)")
        _cb.AppendLine("  End If")
        _cb.AppendLine("  o" & _tbl & ".rowstatus=2")
        _cb.AppendLine("  Return Save(currentuser,actualuser,pk)")
        _cb.AppendLine("End Function")

        _cb.AppendLine("  Public Sub SetControlStatus()")
        _cb.AppendLine("  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)")
        _cb.AppendLine("  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)")


        For Each Me.dr In Table.Rows
            cp = New ColumnStructure(dr)
            If Not cp.IsPrimaryKey Then
                If Not (cp.ColumnName = "user_fk" And cp.TableName = "persons") Then
                    Select Case cp.ColumnName
                        Case "creator_fk", "editor_fk", "created", "modified", "rowstatus"
                        Case Else
                            _cb.AppendLine("  Me." & cp.ColumnName & ".Enabled=IIF(WL>=" & cp.ColumnSecurityLevel & ",True,False)")
                            _cb.AppendLine("  Me.blk" & cp.ColumnName & ".Visible=IIF(RL>=" & cp.ColumnSecurityLevel & ",True,False)")
                    End Select
                End If
            End If
        Next
        _cb.AppendLine("  End Sub")

        _cb.AppendLine("End Class")

        _WebControl = _wc.ToString
        _CodeBehind = _cb.ToString
    End Sub

End Class
