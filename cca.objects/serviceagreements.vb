Imports cca.common
Public Class serviceagreements
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _serviceagreement_pk as Integer

Dim _serviceagreement_pk_d as Boolean=False
Public Event serviceagreement_pkChanged()
Public ReadOnly Property serviceagreement_pk() as Integer
	Get
		Return _serviceagreement_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _serviceproduct_fk as Integer

Dim _serviceproduct_fk_d as Boolean=False
Public Event serviceproduct_fkChanged()
Public Property serviceproduct_fk() as Integer
	Get
		Return _serviceproduct_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _serviceproduct_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("serviceproduct_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Product"), CStr("Validation failed: Invalid Foreign Key"))
		If _serviceproduct_fk <> value then RaiseEvent serviceproduct_fkChanged
		End If
		_serviceproduct_fk = value
		If _ReadingData=True Then _d=False
		_serviceproduct_fk_d=_d
	End Set
End Property

Dim _date_from as Datetime

Dim _date_from_d as Boolean=False
Public Event date_fromChanged()
Public Property date_from() as Datetime
	Get
		Return _date_from
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _date_from <> value Then _d=True
		If not _ReadingData=True Then
		If _date_from <> value then RaiseEvent date_fromChanged
		End If
		_date_from = value
		If _ReadingData=True Then _d=False
		_date_from_d=_d
	End Set
End Property

Dim _date_to as Datetime

Dim _date_to_d as Boolean=False
Public Event date_toChanged()
Public Property date_to() as Datetime
	Get
		Return _date_to
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _date_to <> value Then _d=True
		If not _ReadingData=True Then
		If _date_to <> value then RaiseEvent date_toChanged
		End If
		_date_to = value
		If _ReadingData=True Then _d=False
		_date_to_d=_d
	End Set
End Property

Dim _otherinformation as String

Dim _otherinformation_d as Boolean=False
Public Event otherinformationChanged()
Public Property otherinformation() as String
	Get
		Return _otherinformation
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _otherinformation <> value Then _d=True
		If not _ReadingData=True Then
		If _otherinformation <> value then RaiseEvent otherinformationChanged
		End If
		_otherinformation = value
		If _ReadingData=True Then _d=False
		_otherinformation_d=_d
	End Set
End Property

Dim _rollingcontractclause as Boolean

Dim _rollingcontractclause_d as Boolean=False
Public Event rollingcontractclauseChanged()
Public Property rollingcontractclause() as Boolean
	Get
		Return _rollingcontractclause
	End Get
	Set (ByVal value As Boolean)
		Dim _d as boolean=false
		If _rollingcontractclause <> value Then _d=True
		If not _ReadingData=True Then
		If _rollingcontractclause <> value then RaiseEvent rollingcontractclauseChanged
		End If
		_rollingcontractclause = value
		If _ReadingData=True Then _d=False
		_rollingcontractclause_d=_d
	End Set
End Property

Dim _organisation_fk as Integer

Dim _organisation_fk_d as Boolean=False
Public Event organisation_fkChanged()
Public Property organisation_fk() as Integer
	Get
		Return _organisation_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _organisation_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("organisation_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Customer Name"), CStr("Validation failed: Invalid Foreign Key"))
		If _organisation_fk <> value then RaiseEvent organisation_fkChanged
		End If
		_organisation_fk = value
		If _ReadingData=True Then _d=False
		_organisation_fk_d=_d
	End Set
End Property

Dim _serviceagreementnumber as String

Dim _serviceagreementnumber_d as Boolean=False
Public Event serviceagreementnumberChanged()
Public Property serviceagreementnumber() as String
	Get
		Return _serviceagreementnumber
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _serviceagreementnumber <> value Then _d=True
		If not _ReadingData=True Then
		If _serviceagreementnumber <> value then RaiseEvent serviceagreementnumberChanged
		End If
		_serviceagreementnumber = value
		If _ReadingData=True Then _d=False
		_serviceagreementnumber_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _serviceagreement_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _serviceproduct_fk_d
		_IsDirty = _IsDirty OR _date_from_d
		_IsDirty = _IsDirty OR _date_to_d
		_IsDirty = _IsDirty OR _otherinformation_d
		_IsDirty = _IsDirty OR _rollingcontractclause_d
		_IsDirty = _IsDirty OR _organisation_fk_d
		_IsDirty = _IsDirty OR _serviceagreementnumber_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_serviceagreement_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_serviceproduct_fk=-1
	_organisation_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("serviceagreements",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_serviceagreements_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@serviceagreement_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_serviceagreement_pk=CommonFN.SafeRead(Row("serviceagreement_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		serviceproduct_fk=CommonFN.SafeRead(Row("serviceproduct_fk"))
		date_from=CommonFN.SafeRead(Row("date_from"))
		date_to=CommonFN.SafeRead(Row("date_to"))
		otherinformation=CommonFN.SafeRead(Row("otherinformation"))
		rollingcontractclause=CommonFN.SafeRead(Row("rollingcontractclause"))
		organisation_fk=CommonFN.SafeRead(Row("organisation_fk"))
		serviceagreementnumber=CommonFN.SafeRead(Row("serviceagreementnumber"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_serviceagreements_exists"
	PO = New SqlClient.SqlParameter("@serviceproduct_fk", serviceproduct_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@organisation_fk", organisation_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_serviceagreements_compare"
	PO = New SqlClient.SqlParameter("@serviceagreement_pk", serviceagreement_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@date_from", CommonFN.CheckEmptyDate(date_from.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@date_to", CommonFN.CheckEmptyDate(date_to.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@otherinformation", otherinformation)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@serviceagreementnumber", serviceagreementnumber)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and serviceagreement_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_serviceagreements_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@serviceagreement_pk", serviceagreement_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@serviceproduct_fk", serviceproduct_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@date_from", CommonFN.CheckEmptyDate(date_from.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@date_to", CommonFN.CheckEmptyDate(date_to.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@otherinformation", otherinformation)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@rollingcontractclause", rollingcontractclause)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@organisation_fk", organisation_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@serviceagreementnumber", serviceagreementnumber)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _serviceagreement_pk_d then CDC.AuditLog("serviceagreements",_returnPK,"serviceagreement_pk",_CurrentUser,modified,CStr(serviceagreement_pk)):_serviceagreement_pk_d=False
				If _creator_fk_d then CDC.AuditLog("serviceagreements",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("serviceagreements",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("serviceagreements",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("serviceagreements",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("serviceagreements",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _serviceproduct_fk_d then CDC.AuditLog("serviceagreements",_returnPK,"serviceproduct_fk",_CurrentUser,modified,CStr(serviceproduct_fk)):_serviceproduct_fk_d=False
				If _date_from_d then CDC.AuditLog("serviceagreements",_returnPK,"date_from",_CurrentUser,modified,CStr(date_from)):_date_from_d=False
				If _date_to_d then CDC.AuditLog("serviceagreements",_returnPK,"date_to",_CurrentUser,modified,CStr(date_to)):_date_to_d=False
				If _otherinformation_d then CDC.AuditLog("serviceagreements",_returnPK,"otherinformation",_CurrentUser,modified,CStr(otherinformation)):_otherinformation_d=False
				If _rollingcontractclause_d then CDC.AuditLog("serviceagreements",_returnPK,"rollingcontractclause",_CurrentUser,modified,CStr(rollingcontractclause)):_rollingcontractclause_d=False
				If _organisation_fk_d then CDC.AuditLog("serviceagreements",_returnPK,"organisation_fk",_CurrentUser,modified,CStr(organisation_fk)):_organisation_fk_d=False
				If _serviceagreementnumber_d then CDC.AuditLog("serviceagreements",_returnPK,"serviceagreementnumber",_CurrentUser,modified,CStr(serviceagreementnumber)):_serviceagreementnumber_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
