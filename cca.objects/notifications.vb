Imports cca.common
Public Class notifications
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _notification_pk as Integer

Dim _notification_pk_d as Boolean=False
Public Event notification_pkChanged()
Public ReadOnly Property notification_pk() as Integer
	Get
		Return _notification_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _title as String

Dim _title_d as Boolean=False
Public Event titleChanged()
Public Property title() as String
	Get
		Return _title
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _title <> value Then _d=True
		If not _ReadingData=True Then
		If _title <> value then RaiseEvent titleChanged
		End If
		_title = value
		If _ReadingData=True Then _d=False
		_title_d=_d
	End Set
End Property

Dim _message as String

Dim _message_d as Boolean=False
Public Event messageChanged()
Public Property message() as String
	Get
		Return _message
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _message <> value Then _d=True
		If not _ReadingData=True Then
		If _message <> value then RaiseEvent messageChanged
		End If
		_message = value
		If _ReadingData=True Then _d=False
		_message_d=_d
	End Set
End Property

Dim _priority as Integer

Dim _priority_d as Boolean=False
Public Event priorityChanged()
Public Property priority() as Integer
	Get
		Return _priority
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _priority <> value Then _d=True
		If not _ReadingData=True Then
		If _priority <> value then RaiseEvent priorityChanged
		End If
		_priority = value
		If _ReadingData=True Then _d=False
		_priority_d=_d
	End Set
End Property

Dim _approved as Boolean

Dim _approved_d as Boolean=False
Public Event approvedChanged()
Public Property approved() as Boolean
	Get
		Return _approved
	End Get
	Set (ByVal value As Boolean)
		Dim _d as boolean=false
		If _approved <> value Then _d=True
		If not _ReadingData=True Then
		If _approved <> value then RaiseEvent approvedChanged
		End If
		_approved = value
		If _ReadingData=True Then _d=False
		_approved_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _notification_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _title_d
		_IsDirty = _IsDirty OR _message_d
		_IsDirty = _IsDirty OR _priority_d
		_IsDirty = _IsDirty OR _approved_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_notification_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("notifications",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_notifications_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@notification_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_notification_pk=CommonFN.SafeRead(Row("notification_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		title=CommonFN.SafeRead(Row("title"))
		message=CommonFN.SafeRead(Row("message"))
		priority=CommonFN.SafeRead(Row("priority"))
		approved=CommonFN.SafeRead(Row("approved"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Return False
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_notifications_compare"
	PO = New SqlClient.SqlParameter("@notification_pk", notification_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@title", title)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and notification_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_notifications_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@notification_pk", notification_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@title", title)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@message", message)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@priority", priority)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@approved", approved)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _notification_pk_d then CDC.AuditLog("notifications",_returnPK,"notification_pk",_CurrentUser,modified,CStr(notification_pk)):_notification_pk_d=False
				If _creator_fk_d then CDC.AuditLog("notifications",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("notifications",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("notifications",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("notifications",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("notifications",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _title_d then CDC.AuditLog("notifications",_returnPK,"title",_CurrentUser,modified,CStr(title)):_title_d=False
				If _message_d then CDC.AuditLog("notifications",_returnPK,"message",_CurrentUser,modified,CStr(message)):_message_d=False
				If _priority_d then CDC.AuditLog("notifications",_returnPK,"priority",_CurrentUser,modified,CStr(priority)):_priority_d=False
				If _approved_d then CDC.AuditLog("notifications",_returnPK,"approved",_CurrentUser,modified,CStr(approved)):_approved_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
