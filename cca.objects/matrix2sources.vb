Imports cca.common
Public Class matrix2sources
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _matrixsource_pk as Integer

Dim _matrixsource_pk_d as Boolean=False
Public Event matrixsource_pkChanged()
Public ReadOnly Property matrixsource_pk() as Integer
	Get
		Return _matrixsource_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _matrixfile_fk as Integer

Dim _matrixfile_fk_d as Boolean=False
Public Event matrixfile_fkChanged()
Public Property matrixfile_fk() as Integer
	Get
		Return _matrixfile_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _matrixfile_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("matrixfile_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Matrix File"), CStr("Validation failed: Invalid Foreign Key"))
		If _matrixfile_fk <> value then RaiseEvent matrixfile_fkChanged
		End If
		_matrixfile_fk = value
		If _ReadingData=True Then _d=False
		_matrixfile_fk_d=_d
	End Set
End Property

Dim _supplier_fk as Integer

Dim _supplier_fk_d as Boolean=False
Public Event supplier_fkChanged()
Public Property supplier_fk() as Integer
	Get
		Return _supplier_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _supplier_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("supplier_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Supplier Name"), CStr("Validation failed: Invalid Foreign Key"))
		If _supplier_fk <> value then RaiseEvent supplier_fkChanged
		End If
		_supplier_fk = value
		If _ReadingData=True Then _d=False
		_supplier_fk_d=_d
	End Set
End Property

Dim _product as String

Dim _product_d as Boolean=False
Public Event productChanged()
Public Property product() as String
	Get
		Return _product
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _product <> value Then _d=True
		If not _ReadingData=True Then
		If _product <> value then RaiseEvent productChanged
		End If
		_product = value
		If _ReadingData=True Then _d=False
		_product_d=_d
	End Set
End Property

Dim _ldz_fk as Integer

Dim _ldz_fk_d as Boolean=False
Public Event ldz_fkChanged()
Public Property ldz_fk() as Integer
	Get
		Return _ldz_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _ldz_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("ldz_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("LDZ"), CStr("Validation failed: Required Field"))
		If _ldz_fk <> value then RaiseEvent ldz_fkChanged
		End If
		_ldz_fk = value
		If _ReadingData=True Then _d=False
		_ldz_fk_d=_d
	End Set
End Property

Dim _band_start as Integer

Dim _band_start_d as Boolean=False
Public Event band_startChanged()
Public Property band_start() as Integer
	Get
		Return _band_start
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _band_start <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Band Start"), CStr("Validation failed: Required Field"))
		If _band_start <> value then RaiseEvent band_startChanged
		End If
		_band_start = value
		If _ReadingData=True Then _d=False
		_band_start_d=_d
	End Set
End Property

Dim _band_end as Integer

Dim _band_end_d as Boolean=False
Public Event band_endChanged()
Public Property band_end() as Integer
	Get
		Return _band_end
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _band_end <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Band End"), CStr("Validation failed: Required Field"))
		If _band_end <> value then RaiseEvent band_endChanged
		End If
		_band_end = value
		If _ReadingData=True Then _d=False
		_band_end_d=_d
	End Set
End Property

Dim _validfrom as Datetime

Dim _validfrom_d as Boolean=False
Public Event validfromChanged()
Public Property validfrom() as Datetime
	Get
		Return _validfrom
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _validfrom <> value Then _d=True
		If not _ReadingData=True Then
		If _validfrom <> value then RaiseEvent validfromChanged
		End If
		_validfrom = value
		If _ReadingData=True Then _d=False
		_validfrom_d=_d
	End Set
End Property

Dim _validto as Datetime

Dim _validto_d as Boolean=False
Public Event validtoChanged()
Public Property validto() as Datetime
	Get
		Return _validto
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _validto <> value Then _d=True
		If not _ReadingData=True Then
		If _validto <> value then RaiseEvent validtoChanged
		End If
		_validto = value
		If _ReadingData=True Then _d=False
		_validto_d=_d
	End Set
End Property

Dim _standingcharge as Double

Dim _standingcharge_d as Boolean=False
Public Event standingchargeChanged()
Public Property standingcharge() as Double
	Get
		Return _standingcharge
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _standingcharge <> value Then _d=True
		If not _ReadingData=True Then
		If _standingcharge <> value then RaiseEvent standingchargeChanged
		End If
		_standingcharge = value
		If _ReadingData=True Then _d=False
		_standingcharge_d=_d
	End Set
End Property

Dim _rate as Double

Dim _rate_d as Boolean=False
Public Event rateChanged()
Public Property rate() as Double
	Get
		Return _rate
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _rate <> value Then _d=True
		If not _ReadingData=True Then
		If _rate <> value then RaiseEvent rateChanged
		End If
		_rate = value
		If _ReadingData=True Then _d=False
		_rate_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _matrixsource_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _matrixfile_fk_d
		_IsDirty = _IsDirty OR _supplier_fk_d
		_IsDirty = _IsDirty OR _product_d
		_IsDirty = _IsDirty OR _ldz_fk_d
		_IsDirty = _IsDirty OR _band_start_d
		_IsDirty = _IsDirty OR _band_end_d
		_IsDirty = _IsDirty OR _validfrom_d
		_IsDirty = _IsDirty OR _validto_d
		_IsDirty = _IsDirty OR _standingcharge_d
		_IsDirty = _IsDirty OR _rate_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_matrixsource_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_matrixfile_fk=-1
	_supplier_fk=-1
	_ldz_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("matrix2sources",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_matrix2sources_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@matrixsource_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_matrixsource_pk=CommonFN.SafeRead(Row("matrixsource_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		matrixfile_fk=CommonFN.SafeRead(Row("matrixfile_fk"))
		supplier_fk=CommonFN.SafeRead(Row("supplier_fk"))
		product=CommonFN.SafeRead(Row("product"))
		ldz_fk=CommonFN.SafeRead(Row("ldz_fk"))
		band_start=CommonFN.SafeRead(Row("band_start"))
		band_end=CommonFN.SafeRead(Row("band_end"))
		validfrom=CommonFN.SafeRead(Row("validfrom"))
		validto=CommonFN.SafeRead(Row("validto"))
		standingcharge=CommonFN.SafeRead(Row("standingcharge"))
		rate=CommonFN.SafeRead(Row("rate"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_matrix2sources_exists"
	PO = New SqlClient.SqlParameter("@matrixfile_fk", matrixfile_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@supplier_fk", supplier_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@ldz_fk", ldz_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_matrix2sources_compare"
	PO = New SqlClient.SqlParameter("@matrixsource_pk", matrixsource_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@product", product)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@validfrom", CommonFN.CheckEmptyDate(validfrom.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@validto", CommonFN.CheckEmptyDate(validto.ToString()))
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and matrixsource_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_matrix2sources_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@matrixsource_pk", matrixsource_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@matrixfile_fk", matrixfile_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@supplier_fk", supplier_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@product", product)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ldz_fk", ldz_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@band_start", band_start)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@band_end", band_end)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@validfrom", CommonFN.CheckEmptyDate(validfrom.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@validto", CommonFN.CheckEmptyDate(validto.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@standingcharge", standingcharge)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@rate", rate)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _matrixsource_pk_d then CDC.AuditLog("matrix2sources",_returnPK,"matrixsource_pk",_CurrentUser,modified,CStr(matrixsource_pk)):_matrixsource_pk_d=False
				If _creator_fk_d then CDC.AuditLog("matrix2sources",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("matrix2sources",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("matrix2sources",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("matrix2sources",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("matrix2sources",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _matrixfile_fk_d then CDC.AuditLog("matrix2sources",_returnPK,"matrixfile_fk",_CurrentUser,modified,CStr(matrixfile_fk)):_matrixfile_fk_d=False
				If _supplier_fk_d then CDC.AuditLog("matrix2sources",_returnPK,"supplier_fk",_CurrentUser,modified,CStr(supplier_fk)):_supplier_fk_d=False
				If _product_d then CDC.AuditLog("matrix2sources",_returnPK,"product",_CurrentUser,modified,CStr(product)):_product_d=False
				If _ldz_fk_d then CDC.AuditLog("matrix2sources",_returnPK,"ldz_fk",_CurrentUser,modified,CStr(ldz_fk)):_ldz_fk_d=False
				If _band_start_d then CDC.AuditLog("matrix2sources",_returnPK,"band_start",_CurrentUser,modified,CStr(band_start)):_band_start_d=False
				If _band_end_d then CDC.AuditLog("matrix2sources",_returnPK,"band_end",_CurrentUser,modified,CStr(band_end)):_band_end_d=False
				If _validfrom_d then CDC.AuditLog("matrix2sources",_returnPK,"validfrom",_CurrentUser,modified,CStr(validfrom)):_validfrom_d=False
				If _validto_d then CDC.AuditLog("matrix2sources",_returnPK,"validto",_CurrentUser,modified,CStr(validto)):_validto_d=False
				If _standingcharge_d then CDC.AuditLog("matrix2sources",_returnPK,"standingcharge",_CurrentUser,modified,CStr(standingcharge)):_standingcharge_d=False
				If _rate_d then CDC.AuditLog("matrix2sources",_returnPK,"rate",_CurrentUser,modified,CStr(rate)):_rate_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
