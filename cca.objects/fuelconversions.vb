Imports cca.common
Public Class fuelconversions
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _fuelconversion_pk as Integer

Dim _fuelconversion_pk_d as Boolean=False
Public Event fuelconversion_pkChanged()
Public ReadOnly Property fuelconversion_pk() as Integer
	Get
		Return _fuelconversion_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _sourcefuel_fk as Integer

Dim _sourcefuel_fk_d as Boolean=False
Public Event sourcefuel_fkChanged()
Public Property sourcefuel_fk() as Integer
	Get
		Return _sourcefuel_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _sourcefuel_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("sourcefuel_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("sourcefuel_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _sourcefuel_fk <> value then RaiseEvent sourcefuel_fkChanged
		End If
		_sourcefuel_fk = value
		If _ReadingData=True Then _d=False
		_sourcefuel_fk_d=_d
	End Set
End Property

Dim _sourceunit_fk as Integer

Dim _sourceunit_fk_d as Boolean=False
Public Event sourceunit_fkChanged()
Public Property sourceunit_fk() as Integer
	Get
		Return _sourceunit_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _sourceunit_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("sourceunit_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("sourceunit_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _sourceunit_fk <> value then RaiseEvent sourceunit_fkChanged
		End If
		_sourceunit_fk = value
		If _ReadingData=True Then _d=False
		_sourceunit_fk_d=_d
	End Set
End Property

Dim _targetfuel_fk as Integer

Dim _targetfuel_fk_d as Boolean=False
Public Event targetfuel_fkChanged()
Public Property targetfuel_fk() as Integer
	Get
		Return _targetfuel_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _targetfuel_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("targetfuel_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("targetfuel_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _targetfuel_fk <> value then RaiseEvent targetfuel_fkChanged
		End If
		_targetfuel_fk = value
		If _ReadingData=True Then _d=False
		_targetfuel_fk_d=_d
	End Set
End Property

Dim _targetunit_fk as Integer

Dim _targetunit_fk_d as Boolean=False
Public Event targetunit_fkChanged()
Public Property targetunit_fk() as Integer
	Get
		Return _targetunit_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _targetunit_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("targetunit_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("targetunit_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _targetunit_fk <> value then RaiseEvent targetunit_fkChanged
		End If
		_targetunit_fk = value
		If _ReadingData=True Then _d=False
		_targetunit_fk_d=_d
	End Set
End Property

Dim _validfrom as Datetime

Dim _validfrom_d as Boolean=False
Public Event validfromChanged()
Public Property validfrom() as Datetime
	Get
		Return _validfrom
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _validfrom <> value Then _d=True
		If not _ReadingData=True Then
		If _validfrom <> value then RaiseEvent validfromChanged
		End If
		_validfrom = value
		If _ReadingData=True Then _d=False
		_validfrom_d=_d
	End Set
End Property

Dim _validto as Datetime

Dim _validto_d as Boolean=False
Public Event validtoChanged()
Public Property validto() as Datetime
	Get
		Return _validto
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _validto <> value Then _d=True
		If not _ReadingData=True Then
		If _validto <> value then RaiseEvent validtoChanged
		End If
		_validto = value
		If _ReadingData=True Then _d=False
		_validto_d=_d
	End Set
End Property

Dim _conversionfactor as Double

Dim _conversionfactor_d as Boolean=False
Public Event conversionfactorChanged()
Public Property conversionfactor() as Double
	Get
		Return _conversionfactor
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _conversionfactor <> value Then _d=True
		If not _ReadingData=True Then
		If _conversionfactor <> value then RaiseEvent conversionfactorChanged
		End If
		_conversionfactor = value
		If _ReadingData=True Then _d=False
		_conversionfactor_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _fuelconversion_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _sourcefuel_fk_d
		_IsDirty = _IsDirty OR _sourceunit_fk_d
		_IsDirty = _IsDirty OR _targetfuel_fk_d
		_IsDirty = _IsDirty OR _targetunit_fk_d
		_IsDirty = _IsDirty OR _validfrom_d
		_IsDirty = _IsDirty OR _validto_d
		_IsDirty = _IsDirty OR _conversionfactor_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_fuelconversion_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_sourcefuel_fk=-1
	_sourceunit_fk=-1
	_targetfuel_fk=-1
	_targetunit_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("fuelconversions",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_fuelconversions_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@fuelconversion_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_fuelconversion_pk=CommonFN.SafeRead(Row("fuelconversion_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		sourcefuel_fk=CommonFN.SafeRead(Row("sourcefuel_fk"))
		sourceunit_fk=CommonFN.SafeRead(Row("sourceunit_fk"))
		targetfuel_fk=CommonFN.SafeRead(Row("targetfuel_fk"))
		targetunit_fk=CommonFN.SafeRead(Row("targetunit_fk"))
		validfrom=CommonFN.SafeRead(Row("validfrom"))
		validto=CommonFN.SafeRead(Row("validto"))
		conversionfactor=CommonFN.SafeRead(Row("conversionfactor"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_fuelconversions_exists"
	PO = New SqlClient.SqlParameter("@sourcefuel_fk", sourcefuel_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@sourceunit_fk", sourceunit_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@targetfuel_fk", targetfuel_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@targetunit_fk", targetunit_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique() as boolean
	Return True
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and fuelconversion_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_fuelconversions_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@fuelconversion_pk", fuelconversion_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@sourcefuel_fk", sourcefuel_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@sourceunit_fk", sourceunit_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetfuel_fk", targetfuel_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetunit_fk", targetunit_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@validfrom", CommonFN.CheckEmptyDate(validfrom.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@validto", CommonFN.CheckEmptyDate(validto.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@conversionfactor", conversionfactor)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _fuelconversion_pk_d then CDC.AuditLog("fuelconversions",_returnPK,"fuelconversion_pk",_CurrentUser,modified,CStr(fuelconversion_pk)):_fuelconversion_pk_d=False
				If _creator_fk_d then CDC.AuditLog("fuelconversions",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("fuelconversions",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("fuelconversions",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("fuelconversions",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("fuelconversions",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _sourcefuel_fk_d then CDC.AuditLog("fuelconversions",_returnPK,"sourcefuel_fk",_CurrentUser,modified,CStr(sourcefuel_fk)):_sourcefuel_fk_d=False
				If _sourceunit_fk_d then CDC.AuditLog("fuelconversions",_returnPK,"sourceunit_fk",_CurrentUser,modified,CStr(sourceunit_fk)):_sourceunit_fk_d=False
				If _targetfuel_fk_d then CDC.AuditLog("fuelconversions",_returnPK,"targetfuel_fk",_CurrentUser,modified,CStr(targetfuel_fk)):_targetfuel_fk_d=False
				If _targetunit_fk_d then CDC.AuditLog("fuelconversions",_returnPK,"targetunit_fk",_CurrentUser,modified,CStr(targetunit_fk)):_targetunit_fk_d=False
				If _validfrom_d then CDC.AuditLog("fuelconversions",_returnPK,"validfrom",_CurrentUser,modified,CStr(validfrom)):_validfrom_d=False
				If _validto_d then CDC.AuditLog("fuelconversions",_returnPK,"validto",_CurrentUser,modified,CStr(validto)):_validto_d=False
				If _conversionfactor_d then CDC.AuditLog("fuelconversions",_returnPK,"conversionfactor",_CurrentUser,modified,CStr(conversionfactor)):_conversionfactor_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
