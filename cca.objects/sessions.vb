Imports cca.common
Public Class sessions
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _session_pk as Integer

Dim _session_pk_d as Boolean=False
Public Event session_pkChanged()
Public ReadOnly Property session_pk() as Integer
	Get
		Return _session_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer

Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _sessionid as String

Dim _sessionid_d as Boolean=False
Public Event sessionidChanged()
Public Property sessionid() as String
	Get
		Return _sessionid
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _sessionid <> value Then _d=True
		If not _ReadingData=True Then
		If _sessionid <> value then RaiseEvent sessionidChanged
		End If
		_sessionid = value
		If _ReadingData=True Then _d=False
		_sessionid_d=_d
	End Set
End Property

Dim _ip as String

Dim _ip_d as Boolean=False
Public Event ipChanged()
Public Property ip() as String
	Get
		Return _ip
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _ip <> value Then _d=True
		If not _ReadingData=True Then
		If _ip <> value then RaiseEvent ipChanged
		End If
		_ip = value
		If _ReadingData=True Then _d=False
		_ip_d=_d
	End Set
End Property

Dim _currentuser as Integer

Dim _currentuser_d as Boolean=False
Public Event currentuserChanged()
Public Property currentuser() as Integer
	Get
		Return _currentuser
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _currentuser <> value Then _d=True
		If not _ReadingData=True Then
		If _currentuser <> value then RaiseEvent currentuserChanged
		End If
		_currentuser = value
		If _ReadingData=True Then _d=False
		_currentuser_d=_d
	End Set
End Property

Dim _actualuser as Integer

Dim _actualuser_d as Boolean=False
Public Event actualuserChanged()
Public Property actualuser() as Integer
	Get
		Return _actualuser
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _actualuser <> value Then _d=True
		If not _ReadingData=True Then
		If _actualuser <> value then RaiseEvent actualuserChanged
		End If
		_actualuser = value
		If _ReadingData=True Then _d=False
		_actualuser_d=_d
	End Set
End Property

Dim _targetuser as Integer

Dim _targetuser_d as Boolean=False
Public Event targetuserChanged()
Public Property targetuser() as Integer
	Get
		Return _targetuser
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _targetuser <> value Then _d=True
		If not _ReadingData=True Then
		If _targetuser <> value then RaiseEvent targetuserChanged
		End If
		_targetuser = value
		If _ReadingData=True Then _d=False
		_targetuser_d=_d
	End Set
End Property

Dim _mainid as Integer

Dim _mainid_d as Boolean=False
Public Event mainidChanged()
Public Property mainid() as Integer
	Get
		Return _mainid
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _mainid <> value Then _d=True
		If not _ReadingData=True Then
		If _mainid <> value then RaiseEvent mainidChanged
		End If
		_mainid = value
		If _ReadingData=True Then _d=False
		_mainid_d=_d
	End Set
End Property

Dim _subid as Integer

Dim _subid_d as Boolean=False
Public Event subidChanged()
Public Property subid() as Integer
	Get
		Return _subid
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _subid <> value Then _d=True
		If not _ReadingData=True Then
		If _subid <> value then RaiseEvent subidChanged
		End If
		_subid = value
		If _ReadingData=True Then _d=False
		_subid_d=_d
	End Set
End Property

Dim _linkid as Integer

Dim _linkid_d as Boolean=False
Public Event linkidChanged()
Public Property linkid() as Integer
	Get
		Return _linkid
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _linkid <> value Then _d=True
		If not _ReadingData=True Then
		If _linkid <> value then RaiseEvent linkidChanged
		End If
		_linkid = value
		If _ReadingData=True Then _d=False
		_linkid_d=_d
	End Set
End Property

Dim _PersonPageMode as Integer

Dim _PersonPageMode_d as Boolean=False
Public Event PersonPageModeChanged()
Public Property PersonPageMode() as Integer
	Get
		Return _PersonPageMode
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _PersonPageMode <> value Then _d=True
		If not _ReadingData=True Then
		If _PersonPageMode <> value then RaiseEvent PersonPageModeChanged
		End If
		_PersonPageMode = value
		If _ReadingData=True Then _d=False
		_PersonPageMode_d=_d
	End Set
End Property

Dim _AgreementPageMode as Integer

Dim _AgreementPageMode_d as Boolean=False
Public Event AgreementPageModeChanged()
Public Property AgreementPageMode() as Integer
	Get
		Return _AgreementPageMode
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _AgreementPageMode <> value Then _d=True
		If not _ReadingData=True Then
		If _AgreementPageMode <> value then RaiseEvent AgreementPageModeChanged
		End If
		_AgreementPageMode = value
		If _ReadingData=True Then _d=False
		_AgreementPageMode_d=_d
	End Set
End Property

Dim _redirected as Integer

Dim _redirected_d as Boolean=False
Public Event redirectedChanged()
Public Property redirected() as Integer
	Get
		Return _redirected
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _redirected <> value Then _d=True
		If not _ReadingData=True Then
		If _redirected <> value then RaiseEvent redirectedChanged
		End If
		_redirected = value
		If _ReadingData=True Then _d=False
		_redirected_d=_d
	End Set
End Property

Dim _noticecount as Integer

Dim _noticecount_d as Boolean=False
Public Event noticecountChanged()
Public Property noticecount() as Integer
	Get
		Return _noticecount
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _noticecount <> value Then _d=True
		If not _ReadingData=True Then
		If _noticecount <> value then RaiseEvent noticecountChanged
		End If
		_noticecount = value
		If _ReadingData=True Then _d=False
		_noticecount_d=_d
	End Set
End Property

Dim _targetAgreement as Integer

Dim _targetAgreement_d as Boolean=False
Public Event targetAgreementChanged()
Public Property targetAgreement() as Integer
	Get
		Return _targetAgreement
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _targetAgreement <> value Then _d=True
		If not _ReadingData=True Then
		If _targetAgreement <> value then RaiseEvent targetAgreementChanged
		End If
		_targetAgreement = value
		If _ReadingData=True Then _d=False
		_targetAgreement_d=_d
	End Set
End Property

Dim _targetAgreementSecondary as Integer

Dim _targetAgreementSecondary_d as Boolean=False
Public Event targetAgreementSecondaryChanged()
Public Property targetAgreementSecondary() as Integer
	Get
		Return _targetAgreementSecondary
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _targetAgreementSecondary <> value Then _d=True
		If not _ReadingData=True Then
		If _targetAgreementSecondary <> value then RaiseEvent targetAgreementSecondaryChanged
		End If
		_targetAgreementSecondary = value
		If _ReadingData=True Then _d=False
		_targetAgreementSecondary_d=_d
	End Set
End Property

Dim _targetDataEntry as Integer

Dim _targetDataEntry_d as Boolean=False
Public Event targetDataEntryChanged()
Public Property targetDataEntry() as Integer
	Get
		Return _targetDataEntry
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _targetDataEntry <> value Then _d=True
		If not _ReadingData=True Then
		If _targetDataEntry <> value then RaiseEvent targetDataEntryChanged
		End If
		_targetDataEntry = value
		If _ReadingData=True Then _d=False
		_targetDataEntry_d=_d
	End Set
End Property

Dim _DataEntryPageMode as Integer

Dim _DataEntryPageMode_d as Boolean=False
Public Event DataEntryPageModeChanged()
Public Property DataEntryPageMode() as Integer
	Get
		Return _DataEntryPageMode
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _DataEntryPageMode <> value Then _d=True
		If not _ReadingData=True Then
		If _DataEntryPageMode <> value then RaiseEvent DataEntryPageModeChanged
		End If
		_DataEntryPageMode = value
		If _ReadingData=True Then _d=False
		_DataEntryPageMode_d=_d
	End Set
End Property

Dim _matrix as Integer

Dim _matrix_d as Boolean=False
Public Event matrixChanged()
Public Property matrix() as Integer
	Get
		Return _matrix
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _matrix <> value Then _d=True
		If not _ReadingData=True Then
		If _matrix <> value then RaiseEvent matrixChanged
		End If
		_matrix = value
		If _ReadingData=True Then _d=False
		_matrix_d=_d
	End Set
End Property

Dim _matrixprice as Integer

Dim _matrixprice_d as Boolean=False
Public Event matrixpriceChanged()
Public Property matrixprice() as Integer
	Get
		Return _matrixprice
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _matrixprice <> value Then _d=True
		If not _ReadingData=True Then
		If _matrixprice <> value then RaiseEvent matrixpriceChanged
		End If
		_matrixprice = value
		If _ReadingData=True Then _d=False
		_matrixprice_d=_d
	End Set
End Property

Dim _matrixdetail as Integer

Dim _matrixdetail_d as Boolean=False
Public Event matrixdetailChanged()
Public Property matrixdetail() as Integer
	Get
		Return _matrixdetail
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _matrixdetail <> value Then _d=True
		If not _ReadingData=True Then
		If _matrixdetail <> value then RaiseEvent matrixdetailChanged
		End If
		_matrixdetail = value
		If _ReadingData=True Then _d=False
		_matrixdetail_d=_d
	End Set
End Property

Dim _MatrixPageMode as Integer

Dim _MatrixPageMode_d as Boolean=False
Public Event MatrixPageModeChanged()
Public Property MatrixPageMode() as Integer
	Get
		Return _MatrixPageMode
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _MatrixPageMode <> value Then _d=True
		If not _ReadingData=True Then
		If _MatrixPageMode <> value then RaiseEvent MatrixPageModeChanged
		End If
		_MatrixPageMode = value
		If _ReadingData=True Then _d=False
		_MatrixPageMode_d=_d
	End Set
End Property

Dim _targetBubble as Integer

Dim _targetBubble_d as Boolean=False
Public Event targetBubbleChanged()
Public Property targetBubble() as Integer
	Get
		Return _targetBubble
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _targetBubble <> value Then _d=True
		If not _ReadingData=True Then
		If _targetBubble <> value then RaiseEvent targetBubbleChanged
		End If
		_targetBubble = value
		If _ReadingData=True Then _d=False
		_targetBubble_d=_d
	End Set
End Property

Dim _targetBubbleSecondary as Integer

Dim _targetBubbleSecondary_d as Boolean=False
Public Event targetBubbleSecondaryChanged()
Public Property targetBubbleSecondary() as Integer
	Get
		Return _targetBubbleSecondary
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _targetBubbleSecondary <> value Then _d=True
		If not _ReadingData=True Then
		If _targetBubbleSecondary <> value then RaiseEvent targetBubbleSecondaryChanged
		End If
		_targetBubbleSecondary = value
		If _ReadingData=True Then _d=False
		_targetBubbleSecondary_d=_d
	End Set
End Property

Dim _BubblePageMode as Integer

Dim _BubblePageMode_d as Boolean=False
Public Event BubblePageModeChanged()
Public Property BubblePageMode() as Integer
	Get
		Return _BubblePageMode
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _BubblePageMode <> value Then _d=True
		If not _ReadingData=True Then
		If _BubblePageMode <> value then RaiseEvent BubblePageModeChanged
		End If
		_BubblePageMode = value
		If _ReadingData=True Then _d=False
		_BubblePageMode_d=_d
	End Set
End Property

Dim _CustomerPageMode as Integer

Dim _CustomerPageMode_d as Boolean=False
Public Event CustomerPageModeChanged()
Public Property CustomerPageMode() as Integer
	Get
		Return _CustomerPageMode
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _CustomerPageMode <> value Then _d=True
		If not _ReadingData=True Then
		If _CustomerPageMode <> value then RaiseEvent CustomerPageModeChanged
		End If
		_CustomerPageMode = value
		If _ReadingData=True Then _d=False
		_CustomerPageMode_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _session_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _sessionid_d
		_IsDirty = _IsDirty OR _ip_d
		_IsDirty = _IsDirty OR _currentuser_d
		_IsDirty = _IsDirty OR _actualuser_d
		_IsDirty = _IsDirty OR _targetuser_d
		_IsDirty = _IsDirty OR _mainid_d
		_IsDirty = _IsDirty OR _subid_d
		_IsDirty = _IsDirty OR _linkid_d
		_IsDirty = _IsDirty OR _PersonPageMode_d
		_IsDirty = _IsDirty OR _AgreementPageMode_d
		_IsDirty = _IsDirty OR _redirected_d
		_IsDirty = _IsDirty OR _noticecount_d
		_IsDirty = _IsDirty OR _targetAgreement_d
		_IsDirty = _IsDirty OR _targetAgreementSecondary_d
		_IsDirty = _IsDirty OR _targetDataEntry_d
		_IsDirty = _IsDirty OR _DataEntryPageMode_d
		_IsDirty = _IsDirty OR _matrix_d
		_IsDirty = _IsDirty OR _matrixprice_d
		_IsDirty = _IsDirty OR _matrixdetail_d
		_IsDirty = _IsDirty OR _MatrixPageMode_d
		_IsDirty = _IsDirty OR _targetBubble_d
		_IsDirty = _IsDirty OR _targetBubbleSecondary_d
		_IsDirty = _IsDirty OR _BubblePageMode_d
		_IsDirty = _IsDirty OR _CustomerPageMode_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Public Sub New()
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_session_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("sessions",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_sessions_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@session_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_session_pk=CommonFN.SafeRead(Row("session_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		sessionid=CommonFN.SafeRead(Row("sessionid"))
		ip=CommonFN.SafeRead(Row("ip"))
		currentuser=CommonFN.SafeRead(Row("currentuser"))
		actualuser=CommonFN.SafeRead(Row("actualuser"))
		targetuser=CommonFN.SafeRead(Row("targetuser"))
		mainid=CommonFN.SafeRead(Row("mainid"))
		subid=CommonFN.SafeRead(Row("subid"))
		linkid=CommonFN.SafeRead(Row("linkid"))
		PersonPageMode=CommonFN.SafeRead(Row("PersonPageMode"))
		AgreementPageMode=CommonFN.SafeRead(Row("AgreementPageMode"))
		redirected=CommonFN.SafeRead(Row("redirected"))
		noticecount=CommonFN.SafeRead(Row("noticecount"))
		targetAgreement=CommonFN.SafeRead(Row("targetAgreement"))
		targetAgreementSecondary=CommonFN.SafeRead(Row("targetAgreementSecondary"))
		targetDataEntry=CommonFN.SafeRead(Row("targetDataEntry"))
		DataEntryPageMode=CommonFN.SafeRead(Row("DataEntryPageMode"))
		matrix=CommonFN.SafeRead(Row("matrix"))
		matrixprice=CommonFN.SafeRead(Row("matrixprice"))
		matrixdetail=CommonFN.SafeRead(Row("matrixdetail"))
		MatrixPageMode=CommonFN.SafeRead(Row("MatrixPageMode"))
		targetBubble=CommonFN.SafeRead(Row("targetBubble"))
		targetBubbleSecondary=CommonFN.SafeRead(Row("targetBubbleSecondary"))
		BubblePageMode=CommonFN.SafeRead(Row("BubblePageMode"))
		CustomerPageMode=CommonFN.SafeRead(Row("CustomerPageMode"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Return False
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_sessions_compare"
	PO = New SqlClient.SqlParameter("@session_pk", session_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@sessionid", sessionid)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@ip", ip)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and session_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_sessions_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@session_pk", session_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@created", CommonFN.CheckEmptyDate(created.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@modified", CommonFN.CheckEmptyDate(modified.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@sessionid", sessionid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ip", ip)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@currentuser", currentuser)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@actualuser", actualuser)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetuser", targetuser)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@mainid", mainid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@subid", subid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@linkid", linkid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@PersonPageMode", PersonPageMode)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@AgreementPageMode", AgreementPageMode)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@redirected", redirected)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@noticecount", noticecount)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetAgreement", targetAgreement)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetAgreementSecondary", targetAgreementSecondary)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetDataEntry", targetDataEntry)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@DataEntryPageMode", DataEntryPageMode)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@matrix", matrix)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@matrixprice", matrixprice)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@matrixdetail", matrixdetail)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@MatrixPageMode", MatrixPageMode)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetBubble", targetBubble)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetBubbleSecondary", targetBubbleSecondary)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@BubblePageMode", BubblePageMode)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@CustomerPageMode", CustomerPageMode)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _session_pk_d then CDC.AuditLog("sessions",_returnPK,"session_pk",_CurrentUser,modified,CStr(session_pk)):_session_pk_d=False
				If _creator_fk_d then CDC.AuditLog("sessions",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("sessions",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("sessions",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("sessions",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("sessions",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _sessionid_d then CDC.AuditLog("sessions",_returnPK,"sessionid",_CurrentUser,modified,CStr(sessionid)):_sessionid_d=False
				If _ip_d then CDC.AuditLog("sessions",_returnPK,"ip",_CurrentUser,modified,CStr(ip)):_ip_d=False
				If _currentuser_d then CDC.AuditLog("sessions",_returnPK,"currentuser",_CurrentUser,modified,CStr(currentuser)):_currentuser_d=False
				If _actualuser_d then CDC.AuditLog("sessions",_returnPK,"actualuser",_CurrentUser,modified,CStr(actualuser)):_actualuser_d=False
				If _targetuser_d then CDC.AuditLog("sessions",_returnPK,"targetuser",_CurrentUser,modified,CStr(targetuser)):_targetuser_d=False
				If _mainid_d then CDC.AuditLog("sessions",_returnPK,"mainid",_CurrentUser,modified,CStr(mainid)):_mainid_d=False
				If _subid_d then CDC.AuditLog("sessions",_returnPK,"subid",_CurrentUser,modified,CStr(subid)):_subid_d=False
				If _linkid_d then CDC.AuditLog("sessions",_returnPK,"linkid",_CurrentUser,modified,CStr(linkid)):_linkid_d=False
				If _PersonPageMode_d then CDC.AuditLog("sessions",_returnPK,"PersonPageMode",_CurrentUser,modified,CStr(PersonPageMode)):_PersonPageMode_d=False
				If _AgreementPageMode_d then CDC.AuditLog("sessions",_returnPK,"AgreementPageMode",_CurrentUser,modified,CStr(AgreementPageMode)):_AgreementPageMode_d=False
				If _redirected_d then CDC.AuditLog("sessions",_returnPK,"redirected",_CurrentUser,modified,CStr(redirected)):_redirected_d=False
				If _noticecount_d then CDC.AuditLog("sessions",_returnPK,"noticecount",_CurrentUser,modified,CStr(noticecount)):_noticecount_d=False
				If _targetAgreement_d then CDC.AuditLog("sessions",_returnPK,"targetAgreement",_CurrentUser,modified,CStr(targetAgreement)):_targetAgreement_d=False
				If _targetAgreementSecondary_d then CDC.AuditLog("sessions",_returnPK,"targetAgreementSecondary",_CurrentUser,modified,CStr(targetAgreementSecondary)):_targetAgreementSecondary_d=False
				If _targetDataEntry_d then CDC.AuditLog("sessions",_returnPK,"targetDataEntry",_CurrentUser,modified,CStr(targetDataEntry)):_targetDataEntry_d=False
				If _DataEntryPageMode_d then CDC.AuditLog("sessions",_returnPK,"DataEntryPageMode",_CurrentUser,modified,CStr(DataEntryPageMode)):_DataEntryPageMode_d=False
				If _matrix_d then CDC.AuditLog("sessions",_returnPK,"matrix",_CurrentUser,modified,CStr(matrix)):_matrix_d=False
				If _matrixprice_d then CDC.AuditLog("sessions",_returnPK,"matrixprice",_CurrentUser,modified,CStr(matrixprice)):_matrixprice_d=False
				If _matrixdetail_d then CDC.AuditLog("sessions",_returnPK,"matrixdetail",_CurrentUser,modified,CStr(matrixdetail)):_matrixdetail_d=False
				If _MatrixPageMode_d then CDC.AuditLog("sessions",_returnPK,"MatrixPageMode",_CurrentUser,modified,CStr(MatrixPageMode)):_MatrixPageMode_d=False
				If _targetBubble_d then CDC.AuditLog("sessions",_returnPK,"targetBubble",_CurrentUser,modified,CStr(targetBubble)):_targetBubble_d=False
				If _targetBubbleSecondary_d then CDC.AuditLog("sessions",_returnPK,"targetBubbleSecondary",_CurrentUser,modified,CStr(targetBubbleSecondary)):_targetBubbleSecondary_d=False
				If _BubblePageMode_d then CDC.AuditLog("sessions",_returnPK,"BubblePageMode",_CurrentUser,modified,CStr(BubblePageMode)):_BubblePageMode_d=False
				If _CustomerPageMode_d then CDC.AuditLog("sessions",_returnPK,"CustomerPageMode",_CurrentUser,modified,CStr(CustomerPageMode)):_CustomerPageMode_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
