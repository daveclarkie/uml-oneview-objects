Imports cca.common
Public Class locations
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _location_pk as Integer

Dim _location_pk_d as Boolean=False
Public Event location_pkChanged()
Public ReadOnly Property location_pk() as Integer
	Get
		Return _location_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _buildingprefix as String

Dim _buildingprefix_d as Boolean=False
Public Event buildingprefixChanged()
Public Property buildingprefix() as String
	Get
		Return _buildingprefix
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _buildingprefix <> value Then _d=True
		If not _ReadingData=True Then
		If _buildingprefix <> value then RaiseEvent buildingprefixChanged
		End If
		_buildingprefix = value
		If _ReadingData=True Then _d=False
		_buildingprefix_d=_d
	End Set
End Property

Dim _streetnumber as String

Dim _streetnumber_d as Boolean=False
Public Event streetnumberChanged()
Public Property streetnumber() as String
	Get
		Return _streetnumber
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _streetnumber <> value Then _d=True
		If not _ReadingData=True Then
		If _streetnumber <> value then RaiseEvent streetnumberChanged
		End If
		_streetnumber = value
		If _ReadingData=True Then _d=False
		_streetnumber_d=_d
	End Set
End Property

Dim _street as String

Dim _street_d as Boolean=False
Public Event streetChanged()
Public Property street() as String
	Get
		Return _street
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _street <> value Then _d=True
		If not _ReadingData=True Then
		If _street <> value then RaiseEvent streetChanged
		End If
		_street = value
		If _ReadingData=True Then _d=False
		_street_d=_d
	End Set
End Property

Dim _district as String

Dim _district_d as Boolean=False
Public Event districtChanged()
Public Property district() as String
	Get
		Return _district
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _district <> value Then _d=True
		If not _ReadingData=True Then
		If _district <> value then RaiseEvent districtChanged
		End If
		_district = value
		If _ReadingData=True Then _d=False
		_district_d=_d
	End Set
End Property

Dim _town as String

Dim _town_d as Boolean=False
Public Event townChanged()
Public Property town() as String
	Get
		Return _town
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _town <> value Then _d=True
		If not _ReadingData=True Then
		If _town <> value then RaiseEvent townChanged
		End If
		_town = value
		If _ReadingData=True Then _d=False
		_town_d=_d
	End Set
End Property

Dim _county_fk as Integer

Dim _county_fk_d as Boolean=False
Public Event county_fkChanged()
Public Property county_fk() as Integer
	Get
		Return _county_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _county_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("county_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("County"), CStr("Validation failed: Invalid Foreign Key"))
		If _county_fk <> value then RaiseEvent county_fkChanged
		End If
		_county_fk = value
		If _ReadingData=True Then _d=False
		_county_fk_d=_d
	End Set
End Property

Dim _country_fk as Integer

Dim _country_fk_d as Boolean=False
Public Event country_fkChanged()
Public Property country_fk() as Integer
	Get
		Return _country_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _country_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("country_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Country"), CStr("Validation failed: Invalid Foreign Key"))
		If _country_fk <> value then RaiseEvent country_fkChanged
		End If
		_country_fk = value
		If _ReadingData=True Then _d=False
		_country_fk_d=_d
	End Set
End Property

Dim _postcode as String

Dim _postcode_d as Boolean=False
Public Event postcodeChanged()
Public Property postcode() as String
	Get
		Return _postcode
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _postcode <> value Then _d=True
		If not _ReadingData=True Then
		If _postcode <> value then RaiseEvent postcodeChanged
		End If
		_postcode = value
		If _ReadingData=True Then _d=False
		_postcode_d=_d
	End Set
End Property

Dim _longitude as Double

Dim _longitude_d as Boolean=False
Public Event longitudeChanged()
Public Property longitude() as Double
	Get
		Return _longitude
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _longitude <> value Then _d=True
		If not _ReadingData=True Then
		If _longitude <> value then RaiseEvent longitudeChanged
		End If
		_longitude = value
		If _ReadingData=True Then _d=False
		_longitude_d=_d
	End Set
End Property

Dim _latitude as Double

Dim _latitude_d as Boolean=False
Public Event latitudeChanged()
Public Property latitude() as Double
	Get
		Return _latitude
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _latitude <> value Then _d=True
		If not _ReadingData=True Then
		If _latitude <> value then RaiseEvent latitudeChanged
		End If
		_latitude = value
		If _ReadingData=True Then _d=False
		_latitude_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _location_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _buildingprefix_d
		_IsDirty = _IsDirty OR _streetnumber_d
		_IsDirty = _IsDirty OR _street_d
		_IsDirty = _IsDirty OR _district_d
		_IsDirty = _IsDirty OR _town_d
		_IsDirty = _IsDirty OR _county_fk_d
		_IsDirty = _IsDirty OR _country_fk_d
		_IsDirty = _IsDirty OR _postcode_d
		_IsDirty = _IsDirty OR _longitude_d
		_IsDirty = _IsDirty OR _latitude_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_location_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_county_fk=-1
	_country_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("locations",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_locations_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@location_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_location_pk=CommonFN.SafeRead(Row("location_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		buildingprefix=CommonFN.SafeRead(Row("buildingprefix"))
		streetnumber=CommonFN.SafeRead(Row("streetnumber"))
		street=CommonFN.SafeRead(Row("street"))
		district=CommonFN.SafeRead(Row("district"))
		town=CommonFN.SafeRead(Row("town"))
		county_fk=CommonFN.SafeRead(Row("county_fk"))
		country_fk=CommonFN.SafeRead(Row("country_fk"))
		postcode=CommonFN.SafeRead(Row("postcode"))
		longitude=CommonFN.SafeRead(Row("longitude"))
		latitude=CommonFN.SafeRead(Row("latitude"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_locations_exists"
	PO = New SqlClient.SqlParameter("@county_fk", county_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@country_fk", country_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_locations_compare"
	PO = New SqlClient.SqlParameter("@location_pk", location_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@buildingprefix", buildingprefix)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@streetnumber", streetnumber)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@street", street)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@district", district)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@town", town)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@postcode", postcode)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and location_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_locations_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@location_pk", location_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@buildingprefix", buildingprefix)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@streetnumber", streetnumber)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@street", street)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@district", district)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@town", town)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@county_fk", county_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@country_fk", country_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@postcode", postcode)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@longitude", longitude)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@latitude", latitude)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _location_pk_d then CDC.AuditLog("locations",_returnPK,"location_pk",_CurrentUser,modified,CStr(location_pk)):_location_pk_d=False
				If _creator_fk_d then CDC.AuditLog("locations",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("locations",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("locations",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("locations",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("locations",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _buildingprefix_d then CDC.AuditLog("locations",_returnPK,"buildingprefix",_CurrentUser,modified,CStr(buildingprefix)):_buildingprefix_d=False
				If _streetnumber_d then CDC.AuditLog("locations",_returnPK,"streetnumber",_CurrentUser,modified,CStr(streetnumber)):_streetnumber_d=False
				If _street_d then CDC.AuditLog("locations",_returnPK,"street",_CurrentUser,modified,CStr(street)):_street_d=False
				If _district_d then CDC.AuditLog("locations",_returnPK,"district",_CurrentUser,modified,CStr(district)):_district_d=False
				If _town_d then CDC.AuditLog("locations",_returnPK,"town",_CurrentUser,modified,CStr(town)):_town_d=False
				If _county_fk_d then CDC.AuditLog("locations",_returnPK,"county_fk",_CurrentUser,modified,CStr(county_fk)):_county_fk_d=False
				If _country_fk_d then CDC.AuditLog("locations",_returnPK,"country_fk",_CurrentUser,modified,CStr(country_fk)):_country_fk_d=False
				If _postcode_d then CDC.AuditLog("locations",_returnPK,"postcode",_CurrentUser,modified,CStr(postcode)):_postcode_d=False
				If _longitude_d then CDC.AuditLog("locations",_returnPK,"longitude",_CurrentUser,modified,CStr(longitude)):_longitude_d=False
				If _latitude_d then CDC.AuditLog("locations",_returnPK,"latitude",_CurrentUser,modified,CStr(latitude)):_latitude_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
