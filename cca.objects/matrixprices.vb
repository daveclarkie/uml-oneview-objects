Imports cca.common
Public Class matrixprices
    Dim _ReadingData As Boolean = False
    Dim _LastError As Exception = Nothing

    Public ReadOnly Property LastError() As Exception
        Get
            Return _LastError
        End Get
    End Property

    Dim _matrixprice_pk As Integer

    Dim _matrixprice_pk_d As Boolean = False
    Public Event matrixprice_pkChanged()
    Public ReadOnly Property matrixprice_pk() As Integer
        Get
            Return _matrixprice_pk
        End Get
    End Property

    Dim _creator_fk As Integer

    Dim _creator_fk_d As Boolean = False
    Public Event creator_fkChanged()
    Public Property creator_fk() As Integer
        Get
            Return _creator_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _creator_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("creator_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _creator_fk <> value Then RaiseEvent creator_fkChanged()
            End If
            _creator_fk = value
            If _ReadingData = True Then _d = False
            _creator_fk_d = _d
        End Set
    End Property

    Dim _editor_fk As Integer

    Dim _editor_fk_d As Boolean = False
    Public Event editor_fkChanged()
    Public Property editor_fk() As Integer
        Get
            Return _editor_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _editor_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("editor_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _editor_fk <> value Then RaiseEvent editor_fkChanged()
            End If
            _editor_fk = value
            If _ReadingData = True Then _d = False
            _editor_fk_d = _d
        End Set
    End Property

    Dim _created As Datetime

    Dim _created_d As Boolean = False
    Public Event createdChanged()
    Public Property created() As Datetime
        Get
            Return _created
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _created <> value Then _d = True
            If Not _ReadingData = True Then
                If _created <> value Then RaiseEvent createdChanged()
            End If
            _created = value
            If _ReadingData = True Then _d = False
            _created_d = _d
        End Set
    End Property

    Dim _modified As Datetime

    Dim _modified_d As Boolean = False
    Public Event modifiedChanged()
    Public Property modified() As Datetime
        Get
            Return _modified
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _modified <> value Then _d = True
            If Not _ReadingData = True Then
                If _modified <> value Then RaiseEvent modifiedChanged()
            End If
            _modified = value
            If _ReadingData = True Then _d = False
            _modified_d = _d
        End Set
    End Property

    Dim _rowstatus As Integer = 0
    Dim _rowstatus_d As Boolean = False
    Public Event rowstatusChanged()
    Public Property rowstatus() As Integer
        Get
            Return _rowstatus
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If _rowstatus <> value Then _d = True
            If Not _ReadingData = True Then
                If _rowstatus <> value Then RaiseEvent rowstatusChanged()
            End If
            _rowstatus = value
            If _ReadingData = True Then _d = False
            _rowstatus_d = _d
        End Set
    End Property

    Dim _matrixtype_fk As Integer

    Dim _matrixtype_fk_d As Boolean = False
    Public Event matrixtype_fkChanged()
    Public Property matrixtype_fk() As Integer
        Get
            Return _matrixtype_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _matrixtype_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("matrixtype_fk", value, CDC) Or value = -1 Then Throw New ArgumentOutOfRangeException(CStr("Matrix Type"), CStr("Validation failed: Required Field"))
                If _matrixtype_fk <> value Then RaiseEvent matrixtype_fkChanged()
            End If
            _matrixtype_fk = value
            If _ReadingData = True Then _d = False
            _matrixtype_fk_d = _d
        End Set
    End Property

    Dim _matrixdetail_fk As Integer

    Dim _matrixdetail_fk_d As Boolean = False
    Public Event matrixdetail_fkChanged()
    Public Property matrixdetail_fk() As Integer
        Get
            Return _matrixdetail_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _matrixdetail_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("matrixdetail_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("Detail"), CStr("Validation failed: Required Field"))
                If _matrixdetail_fk <> value Then RaiseEvent matrixdetail_fkChanged()
            End If
            _matrixdetail_fk = value
            If _ReadingData = True Then _d = False
            _matrixdetail_fk_d = _d
        End Set
    End Property

    Dim _matrix_fk As Integer

    Dim _matrix_fk_d As Boolean = False
    Public Event matrix_fkChanged()
    Public Property matrix_fk() As Integer
        Get
            Return _matrix_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _matrix_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("matrix_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("Matrix"), CStr("Validation failed: Invalid Foreign Key"))
                If _matrix_fk <> value Then RaiseEvent matrix_fkChanged()
            End If
            _matrix_fk = value
            If _ReadingData = True Then _d = False
            _matrix_fk_d = _d
        End Set
    End Property

    Dim _IsDirty As Boolean = False
    Public ReadOnly Property IsDirty() As Boolean
        Get
            _IsDirty = False
            _IsDirty = _IsDirty Or _matrixprice_pk_d
            _IsDirty = _IsDirty Or _creator_fk_d
            _IsDirty = _IsDirty Or _editor_fk_d
            _IsDirty = _IsDirty Or _created_d
            _IsDirty = _IsDirty Or _modified_d
            _IsDirty = _IsDirty Or _rowstatus_d
            _IsDirty = _IsDirty Or _matrixtype_fk_d
            _IsDirty = _IsDirty Or _matrixdetail_fk_d
            _IsDirty = _IsDirty Or _matrix_fk_d
            Return _IsDirty
        End Get
    End Property



    Dim CDC As DataCommon
    Dim Row As DataRow
    Dim _CurrentUser As Integer
    Dim _ActualUser As Integer

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        _ReadingData = True
        _matrixprice_pk = -1
        _creator_fk = CurrentUser
        _editor_fk = ActualUser
        Dim _dcreated As Datetime = Nothing : CDC.ReadScalarValue(_dcreated, New sqlclient.sqlCommand("select (getdate())"))
        created = _dcreated
        Dim _dmodified As Datetime = Nothing : CDC.ReadScalarValue(_dmodified, New sqlclient.sqlCommand("select (getdate())"))
        modified = _dmodified
        _matrixtype_fk = -1
        _matrixdetail_fk = -1
        _matrix_fk = -1
        _ReadingData = False
    End Sub

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, ByVal PK As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        Read(PK)
    End Sub

    Public Function Read(ByVal PK As Integer) As Boolean
        Dim _ReturnStatus As Boolean = True
        CDC.AccessLog("matrixprices", PK, _ActualUser)
        _ReadingData = True
        Try
            Dim CO As New SqlClient.SqlCommand("rkg_matrixprices_read")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.Add(New SqlClient.SqlParameter("@matrixprice_pk", PK))
            Row = CDC.ReadDataRow(CO)
            CO.Dispose()
            _matrixprice_pk = CommonFN.SafeRead(Row("matrixprice_pk"))
            creator_fk = CommonFN.SafeRead(Row("creator_fk"))
            editor_fk = CommonFN.SafeRead(Row("editor_fk"))
            created = CommonFN.SafeRead(Row("created"))
            modified = CommonFN.SafeRead(Row("modified"))
            rowstatus = CommonFN.SafeRead(Row("rowstatus"))
            matrixtype_fk = CommonFN.SafeRead(Row("matrixtype_fk"))
            matrixdetail_fk = CommonFN.SafeRead(Row("matrixdetail_fk"))
            matrix_fk = CommonFN.SafeRead(Row("matrix_fk"))
            Row = Nothing
        Catch Ex As Exception
            _ReturnStatus = False
            _LastError = Ex
        End Try
        _ReadingData = False
        Return _ReturnStatus
    End Function

    Dim _MatchedRecords As datatable
    Public ReadOnly Property MatchedRecords() As datatable
        Get
            If _MatchedRecords Is Nothing Then Exists()
            Return _MatchedRecords
        End Get
    End Property
    Public Function Exists() As Boolean
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_matrixprices_exists"
        PO = New SqlClient.SqlParameter("@matrixtype_fk", matrixtype_fk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@matrixdetail_fk", matrixdetail_fk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@matrix_fk", matrix_fk)
        CO.parameters.add(PO)
        Dim _returnPK As Integer = 0
        _MatchedRecords = CDC.ReadDataTable(CO)
        _returnPK = _MatchedRecords.rows.count
        If _returnPK > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Dim _SimilarRecords As datatable
    Public ReadOnly Property SimilarRecords() As datatable
        Get
            If _SimilarRecords Is Nothing Then isUnique()
            Return _SimilarRecords
        End Get
    End Property
    Public Function IsUnique() As Boolean
        Return True
    End Function

    Public Function Save() As Boolean
        Dim _ReturnStatus As Boolean = True
        If IsDirty Then
            If _CurrentUser <> creator_fk And matrixprice_pk < 1 Then creator_fk = _CurrentUser : created = now()
            If _ActualUser <> editor_fk Then editor_fk = _ActualUser
            modified = now()
            Try
                CDC.BeginTransaction()
                Dim PO As SqlClient.SqlParameter
                Dim CO As New SqlClient.SqlCommand()
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rkg_matrixprices_save"
                PO = Nothing
                PO = New SqlClient.SqlParameter("@matrixprice_pk", matrixprice_pk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tcreator_fk As Integer = Nothing
                If creator_fk = Nothing Then
                    CDC.ReadScalarValue(_tcreator_fk, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
                Else
                    PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _teditor_fk As Integer = Nothing
                If editor_fk = Nothing Then
                    CDC.ReadScalarValue(_teditor_fk, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
                Else
                    PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tcreated As Datetime = Nothing
                If created = Nothing Then
                    CDC.ReadScalarValue(_tcreated, New sqlclient.sqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@created", _tcreated)
                Else
                    PO = New SqlClient.SqlParameter("@created", created)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tmodified As Datetime = Nothing
                If modified = Nothing Then
                    CDC.ReadScalarValue(_tmodified, New sqlclient.sqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@modified", _tmodified)
                Else
                    PO = New SqlClient.SqlParameter("@modified", modified)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _trowstatus As Integer = Nothing
                If rowstatus = Nothing Then
                    CDC.ReadScalarValue(_trowstatus, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
                Else
                    PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@matrixtype_fk", matrixtype_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@matrixdetail_fk", matrixdetail_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@matrix_fk", matrix_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                Dim _returnPK As Integer = 0
                If CDC.ReadScalarValue(_returnPK, CO) Then
                    Try
                        If _matrixprice_pk_d Then CDC.AuditLog("matrixprices", _returnPK, "matrixprice_pk", _CurrentUser, modified, CStr(matrixprice_pk)) : _matrixprice_pk_d = False
                        If _creator_fk_d Then CDC.AuditLog("matrixprices", _returnPK, "creator_fk", _CurrentUser, modified, CStr(creator_fk)) : _creator_fk_d = False
                        If _editor_fk_d Then CDC.AuditLog("matrixprices", _returnPK, "editor_fk", _CurrentUser, modified, CStr(editor_fk)) : _editor_fk_d = False
                        If _created_d Then CDC.AuditLog("matrixprices", _returnPK, "created", _CurrentUser, modified, CStr(created)) : _created_d = False
                        If _modified_d Then CDC.AuditLog("matrixprices", _returnPK, "modified", _CurrentUser, modified, CStr(modified)) : _modified_d = False
                        If _rowstatus_d Then CDC.AuditLog("matrixprices", _returnPK, "rowstatus", _CurrentUser, modified, CStr(rowstatus)) : _rowstatus_d = False
                        If _matrixtype_fk_d Then CDC.AuditLog("matrixprices", _returnPK, "matrixtype_fk", _CurrentUser, modified, CStr(matrixtype_fk)) : _matrixtype_fk_d = False
                        If _matrixdetail_fk_d Then CDC.AuditLog("matrixprices", _returnPK, "matrixdetail_fk", _CurrentUser, modified, CStr(matrixdetail_fk)) : _matrixdetail_fk_d = False
                        If _matrix_fk_d Then CDC.AuditLog("matrixprices", _returnPK, "matrix_fk", _CurrentUser, modified, CStr(matrix_fk)) : _matrix_fk_d = False
                    Catch Ex2 As Exception
                        _ReturnStatus = False
                        _LastError = Ex2
                    End Try
                Else
                    _ReturnStatus = False
                End If
                If _ReturnStatus Then
                    CDC.CommitTransaction()
                    Read(_returnPK)
                Else
                    CDC.RollBackTransaction()
                End If
            Catch Ex As Exception
                _ReturnStatus = False
                _LastError = Ex
                CDC.RollBackTransaction()
            End Try
        Else
            _ReturnStatus = True
        End If
        Return _ReturnStatus
    End Function

    Public Function Enable() As Boolean
        rowstatus = 0
        Return Save
    End Function

    Public Function Disable() As Boolean
        rowstatus = 1
        Return Save
    End Function

    Public Function Delete() As Boolean
        rowstatus = 2
        Return Save
    End Function


End Class
