Imports cca.common
Public Class meterpointnotes
    Dim _ReadingData As Boolean = False
    Dim _LastError As Exception = Nothing

    Public ReadOnly Property LastError() As Exception
        Get
            Return _LastError
        End Get
    End Property

    Dim _meterpointnote_pk As Integer

    Dim _meterpointnote_pk_d As Boolean = False
    Public Event meterpointnote_pkChanged()
    Public ReadOnly Property meterpointnote_pk() As Integer
        Get
            Return _meterpointnote_pk
        End Get
    End Property

    Dim _creator_fk As Integer

    Dim _creator_fk_d As Boolean = False
    Public Event creator_fkChanged()
    Public Property creator_fk() As Integer
        Get
            Return _creator_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _creator_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("creator_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _creator_fk <> value Then RaiseEvent creator_fkChanged()
            End If
            _creator_fk = value
            If _ReadingData = True Then _d = False
            _creator_fk_d = _d
        End Set
    End Property

    Dim _editor_fk As Integer

    Dim _editor_fk_d As Boolean = False
    Public Event editor_fkChanged()
    Public Property editor_fk() As Integer
        Get
            Return _editor_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _editor_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("editor_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _editor_fk <> value Then RaiseEvent editor_fkChanged()
            End If
            _editor_fk = value
            If _ReadingData = True Then _d = False
            _editor_fk_d = _d
        End Set
    End Property

    Dim _created As Datetime

    Dim _created_d As Boolean = False
    Public Event createdChanged()
    Public Property created() As Datetime
        Get
            Return _created
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _created <> value Then _d = True
            If Not _ReadingData = True Then
                If _created <> value Then RaiseEvent createdChanged()
            End If
            _created = value
            If _ReadingData = True Then _d = False
            _created_d = _d
        End Set
    End Property

    Dim _modified As Datetime

    Dim _modified_d As Boolean = False
    Public Event modifiedChanged()
    Public Property modified() As Datetime
        Get
            Return _modified
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _modified <> value Then _d = True
            If Not _ReadingData = True Then
                If _modified <> value Then RaiseEvent modifiedChanged()
            End If
            _modified = value
            If _ReadingData = True Then _d = False
            _modified_d = _d
        End Set
    End Property

    Dim _rowstatus As Integer = 0
    Dim _rowstatus_d As Boolean = False
    Public Event rowstatusChanged()
    Public Property rowstatus() As Integer
        Get
            Return _rowstatus
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If _rowstatus <> value Then _d = True
            If Not _ReadingData = True Then
                If _rowstatus <> value Then RaiseEvent rowstatusChanged()
            End If
            _rowstatus = value
            If _ReadingData = True Then _d = False
            _rowstatus_d = _d
        End Set
    End Property

        Dim _meterpoint_fk As String

    Dim _meterpoint_fk_d As Boolean = False
    Public Event meterpoint_fkChanged()
    Public Property meterpoint_fk() As String
        Get
            Return _meterpoint_fk
        End Get
        Set(ByVal value As String)
            Dim _d As Boolean = False
            If _meterpoint_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If _meterpoint_fk <> value Then RaiseEvent meterpoint_fkChanged()
            End If
            _meterpoint_fk = value
            If _ReadingData = True Then _d = False
            _meterpoint_fk_d = _d
        End Set
    End Property


    Dim _note_fk As Integer

    Dim _note_fk_d As Boolean = False
    Public Event note_fkChanged()
    Public Property note_fk() As Integer
        Get
            Return _note_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _note_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("note_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("note_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _note_fk <> value Then RaiseEvent note_fkChanged()
            End If
            _note_fk = value
            If _ReadingData = True Then _d = False
            _note_fk_d = _d
        End Set
    End Property

    Dim _IsDirty As Boolean = False
    Public ReadOnly Property IsDirty() As Boolean
        Get
            _IsDirty = False
            _IsDirty = _IsDirty Or _meterpointnote_pk_d
            _IsDirty = _IsDirty Or _creator_fk_d
            _IsDirty = _IsDirty Or _editor_fk_d
            _IsDirty = _IsDirty Or _created_d
            _IsDirty = _IsDirty Or _modified_d
            _IsDirty = _IsDirty Or _rowstatus_d
            _IsDirty = _IsDirty Or _meterpoint_fk_d
            _IsDirty = _IsDirty Or _note_fk_d
            Return _IsDirty
        End Get
    End Property



    Dim CDC As DataCommon
    Dim Row As DataRow
    Dim _CurrentUser As Integer
    Dim _ActualUser As Integer

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        _ReadingData = True
        _meterpointnote_pk = -1
        _creator_fk = CurrentUser
        _editor_fk = ActualUser
        Dim _dcreated As Datetime = Nothing : CDC.ReadScalarValue(_dcreated, New sqlclient.sqlCommand("select (getdate())"))
        created = _dcreated
        Dim _dmodified As Datetime = Nothing : CDC.ReadScalarValue(_dmodified, New sqlclient.sqlCommand("select (getdate())"))
        modified = _dmodified
        _meterpoint_fk = -1
        _note_fk = -1
        _ReadingData = False
    End Sub

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, ByVal PK As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        Read(PK)
    End Sub

    Public Function Read(ByVal PK As Integer) As Boolean
        Dim _ReturnStatus As Boolean = True
        CDC.AccessLog("meterpointnotes", PK, _ActualUser)
        _ReadingData = True
        Try
            Dim CO As New SqlClient.SqlCommand("rkg_meterpointnotes_read")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.Add(New SqlClient.SqlParameter("@meterpointnote_pk", PK))
            Row = CDC.ReadDataRow(CO)
            CO.Dispose()
            _meterpointnote_pk = CommonFN.SafeRead(Row("meterpointnote_pk"))
            creator_fk = CommonFN.SafeRead(Row("creator_fk"))
            editor_fk = CommonFN.SafeRead(Row("editor_fk"))
            created = CommonFN.SafeRead(Row("created"))
            modified = CommonFN.SafeRead(Row("modified"))
            rowstatus = CommonFN.SafeRead(Row("rowstatus"))
            meterpoint_fk = CommonFN.SafeRead(Row("meterpoint_fk"))
            note_fk = CommonFN.SafeRead(Row("note_fk"))
            Row = Nothing
        Catch Ex As Exception
            _ReturnStatus = False
            _LastError = Ex
        End Try
        _ReadingData = False
        Return _ReturnStatus
    End Function

    Dim _MatchedRecords As datatable
    Public ReadOnly Property MatchedRecords() As datatable
        Get
            If _MatchedRecords Is Nothing Then Exists()
            Return _MatchedRecords
        End Get
    End Property
    Public Function Exists() As Boolean
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_meterpointnotes_exists"
        PO = New SqlClient.SqlParameter("@meterpoint_fk", meterpoint_fk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@note_fk", note_fk)
        CO.parameters.add(PO)
        Dim _returnPK As Integer = 0
        _MatchedRecords = CDC.ReadDataTable(CO)
        _returnPK = _MatchedRecords.rows.count
        If _returnPK > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Dim _SimilarRecords As datatable
    Public ReadOnly Property SimilarRecords() As datatable
        Get
            If _SimilarRecords Is Nothing Then isUnique()
            Return _SimilarRecords
        End Get
    End Property
    Public Function IsUnique(Optional ByVal minMatchCount As Integer = 0) As Boolean
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_meterpointnotes_compare"
        PO = New SqlClient.SqlParameter("@meterpointnote_pk", meterpointnote_pk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@meterpoint_fk", meterpoint_fk)
        CO.parameters.add(PO)
        If minMatchCount > 0 Then CO.Parameters.AddWithValue("@minMatchCount", minMatchCount)
        Dim _returnPK As Integer = 0
        _SimilarRecords = CDC.ReadDataTable(CO)
        _returnPK = _SimilarRecords.rows.count
        If _returnPK = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Save() As Boolean
        Dim _ReturnStatus As Boolean = True
        If IsDirty Then
            If _CurrentUser <> creator_fk And meterpointnote_pk < 1 Then creator_fk = _CurrentUser : created = now()
            If _ActualUser <> editor_fk Then editor_fk = _ActualUser
            modified = now()
            Try
                CDC.BeginTransaction()
                Dim PO As SqlClient.SqlParameter
                Dim CO As New SqlClient.SqlCommand()
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rkg_meterpointnotes_save"
                PO = Nothing
                PO = New SqlClient.SqlParameter("@meterpointnote_pk", meterpointnote_pk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tcreator_fk As Integer = Nothing
                If creator_fk = Nothing Then
                    CDC.ReadScalarValue(_tcreator_fk, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
                Else
                    PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _teditor_fk As Integer = Nothing
                If editor_fk = Nothing Then
                    CDC.ReadScalarValue(_teditor_fk, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
                Else
                    PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tcreated As Datetime = Nothing
                If created = Nothing Then
                    CDC.ReadScalarValue(_tcreated, New sqlclient.sqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@created", _tcreated)
                Else
                    PO = New SqlClient.SqlParameter("@created", created)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tmodified As Datetime = Nothing
                If modified = Nothing Then
                    CDC.ReadScalarValue(_tmodified, New sqlclient.sqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@modified", _tmodified)
                Else
                    PO = New SqlClient.SqlParameter("@modified", modified)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _trowstatus As Integer = Nothing
                If rowstatus = Nothing Then
                    CDC.ReadScalarValue(_trowstatus, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
                Else
                    PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@meterpoint_fk", meterpoint_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@note_fk", note_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                Dim _returnPK As Integer = 0
                If CDC.ReadScalarValue(_returnPK, CO) Then
                    Try
                        If _meterpointnote_pk_d Then CDC.AuditLog("meterpointnotes", _returnPK, "meterpointnote_pk", _CurrentUser, modified, CStr(meterpointnote_pk)) : _meterpointnote_pk_d = False
                        If _creator_fk_d Then CDC.AuditLog("meterpointnotes", _returnPK, "creator_fk", _CurrentUser, modified, CStr(creator_fk)) : _creator_fk_d = False
                        If _editor_fk_d Then CDC.AuditLog("meterpointnotes", _returnPK, "editor_fk", _CurrentUser, modified, CStr(editor_fk)) : _editor_fk_d = False
                        If _created_d Then CDC.AuditLog("meterpointnotes", _returnPK, "created", _CurrentUser, modified, CStr(created)) : _created_d = False
                        If _modified_d Then CDC.AuditLog("meterpointnotes", _returnPK, "modified", _CurrentUser, modified, CStr(modified)) : _modified_d = False
                        If _rowstatus_d Then CDC.AuditLog("meterpointnotes", _returnPK, "rowstatus", _CurrentUser, modified, CStr(rowstatus)) : _rowstatus_d = False
                        If _meterpoint_fk_d Then CDC.AuditLog("meterpointnotes", _returnPK, "meterpoint_fk", _CurrentUser, modified, CStr(meterpoint_fk)) : _meterpoint_fk_d = False
                        If _note_fk_d Then CDC.AuditLog("meterpointnotes", _returnPK, "note_fk", _CurrentUser, modified, CStr(note_fk)) : _note_fk_d = False
                    Catch Ex2 As Exception
                        _ReturnStatus = False
                        _LastError = Ex2
                    End Try
                Else
                    _ReturnStatus = False
                End If
                If _ReturnStatus Then
                    CDC.CommitTransaction()
                    Read(_returnPK)
                Else
                    CDC.RollBackTransaction()
                End If
            Catch Ex As Exception
                _ReturnStatus = False
                _LastError = Ex
                CDC.RollBackTransaction()
            End Try
        Else
            _ReturnStatus = True
        End If
        Return _ReturnStatus
    End Function

    Public Function Enable() As Boolean
        rowstatus = 0
        Return Save
    End Function

    Public Function Disable() As Boolean
        rowstatus = 1
        Return Save
    End Function

    Public Function Delete() As Boolean
        rowstatus = 2
        Return Save
    End Function


End Class
