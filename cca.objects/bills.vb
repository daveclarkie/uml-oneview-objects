Imports cca.common
Public Class bills
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _bill_pk as Integer

Dim _bill_pk_d as Boolean=False
Public Event bill_pkChanged()
Public ReadOnly Property bill_pk() as Integer
	Get
		Return _bill_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer

Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _fueltype as String

Dim _fueltype_d as Boolean=False
Public Event fueltypeChanged()
Public Property fueltype() as String
	Get
		Return _fueltype
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _fueltype <> value Then _d=True
		If not _ReadingData=True Then
		If _fueltype <> value then RaiseEvent fueltypeChanged
		End If
		_fueltype = value
		If _ReadingData=True Then _d=False
		_fueltype_d=_d
	End Set
End Property

Dim _site_fk as Integer

Dim _site_fk_d as Boolean=False
Public Event site_fkChanged()
Public Property site_fk() as Integer
	Get
		Return _site_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _site_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("site_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("site_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _site_fk <> value then RaiseEvent site_fkChanged
		End If
		_site_fk = value
		If _ReadingData=True Then _d=False
		_site_fk_d=_d
	End Set
End Property

Dim _billtype as String

Dim _billtype_d as Boolean=False
Public Event billtypeChanged()
Public Property billtype() as String
	Get
		Return _billtype
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _billtype <> value Then _d=True
		If not _ReadingData=True Then
		If _billtype <> value then RaiseEvent billtypeChanged
		End If
		_billtype = value
		If _ReadingData=True Then _d=False
		_billtype_d=_d
	End Set
End Property

Dim _billedfrom as Datetime

Dim _billedfrom_d as Boolean=False
Public Event billedfromChanged()
Public Property billedfrom() as Datetime
	Get
		Return _billedfrom
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _billedfrom <> value Then _d=True
		If not _ReadingData=True Then
		If _billedfrom <> value then RaiseEvent billedfromChanged
		End If
		_billedfrom = value
		If _ReadingData=True Then _d=False
		_billedfrom_d=_d
	End Set
End Property

Dim _billedto as Datetime

Dim _billedto_d as Boolean=False
Public Event billedtoChanged()
Public Property billedto() as Datetime
	Get
		Return _billedto
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _billedto <> value Then _d=True
		If not _ReadingData=True Then
		If _billedto <> value then RaiseEvent billedtoChanged
		End If
		_billedto = value
		If _ReadingData=True Then _d=False
		_billedto_d=_d
	End Set
End Property

Dim _addedby as String

Dim _addedby_d as Boolean=False
Public Event addedbyChanged()
Public Property addedby() as String
	Get
		Return _addedby
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _addedby <> value Then _d=True
		If not _ReadingData=True Then
		If _addedby <> value then RaiseEvent addedbyChanged
		End If
		_addedby = value
		If _ReadingData=True Then _d=False
		_addedby_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _bill_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _fueltype_d
		_IsDirty = _IsDirty OR _site_fk_d
		_IsDirty = _IsDirty OR _billtype_d
		_IsDirty = _IsDirty OR _billedfrom_d
		_IsDirty = _IsDirty OR _billedto_d
		_IsDirty = _IsDirty OR _addedby_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_bill_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	_site_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("bills",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_bills_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@bill_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_bill_pk=CommonFN.SafeRead(Row("bill_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		fueltype=CommonFN.SafeRead(Row("fueltype"))
		site_fk=CommonFN.SafeRead(Row("site_fk"))
		billtype=CommonFN.SafeRead(Row("billtype"))
		billedfrom=CommonFN.SafeRead(Row("billedfrom"))
		billedto=CommonFN.SafeRead(Row("billedto"))
		addedby=CommonFN.SafeRead(Row("addedby"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_bills_exists"
	PO = New SqlClient.SqlParameter("@site_fk", site_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_bills_compare"
	PO = New SqlClient.SqlParameter("@bill_pk", bill_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@fueltype", fueltype)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@billtype", billtype)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@billedfrom", CommonFN.CheckEmptyDate(billedfrom.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@billedto", CommonFN.CheckEmptyDate(billedto.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@addedby", addedby)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and bill_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_bills_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@bill_pk", bill_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@created", CommonFN.CheckEmptyDate(created.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@modified", CommonFN.CheckEmptyDate(modified.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@fueltype", fueltype)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@site_fk", site_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@billtype", billtype)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@billedfrom", CommonFN.CheckEmptyDate(billedfrom.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@billedto", CommonFN.CheckEmptyDate(billedto.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@addedby", addedby)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _bill_pk_d then CDC.AuditLog("bills",_returnPK,"bill_pk",_CurrentUser,modified,CStr(bill_pk)):_bill_pk_d=False
				If _creator_fk_d then CDC.AuditLog("bills",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("bills",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("bills",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("bills",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("bills",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _fueltype_d then CDC.AuditLog("bills",_returnPK,"fueltype",_CurrentUser,modified,CStr(fueltype)):_fueltype_d=False
				If _site_fk_d then CDC.AuditLog("bills",_returnPK,"site_fk",_CurrentUser,modified,CStr(site_fk)):_site_fk_d=False
				If _billtype_d then CDC.AuditLog("bills",_returnPK,"billtype",_CurrentUser,modified,CStr(billtype)):_billtype_d=False
				If _billedfrom_d then CDC.AuditLog("bills",_returnPK,"billedfrom",_CurrentUser,modified,CStr(billedfrom)):_billedfrom_d=False
				If _billedto_d then CDC.AuditLog("bills",_returnPK,"billedto",_CurrentUser,modified,CStr(billedto)):_billedto_d=False
				If _addedby_d then CDC.AuditLog("bills",_returnPK,"addedby",_CurrentUser,modified,CStr(addedby)):_addedby_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
