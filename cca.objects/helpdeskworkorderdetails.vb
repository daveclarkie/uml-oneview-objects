Imports cca.common
Public Class helpdeskworkorderdetails
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _helpdeskworkorderdetail_pk as Integer

Dim _helpdeskworkorderdetail_pk_d as Boolean=False
Public Event helpdeskworkorderdetail_pkChanged()
Public ReadOnly Property helpdeskworkorderdetail_pk() as Integer
	Get
		Return _helpdeskworkorderdetail_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _helpdesk_id as Integer

Dim _helpdesk_id_d as Boolean=False
Public Event helpdesk_idChanged()
Public Property helpdesk_id() as Integer
	Get
		Return _helpdesk_id
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _helpdesk_id <> value Then _d=True
		If not _ReadingData=True Then
		If _helpdesk_id <> value then RaiseEvent helpdesk_idChanged
		End If
		_helpdesk_id = value
		If _ReadingData=True Then _d=False
		_helpdesk_id_d=_d
	End Set
End Property

Dim _created_time as Datetime

Dim _created_time_d as Boolean=False
Public Event created_timeChanged()
Public Property created_time() as Datetime
	Get
		Return _created_time
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created_time <> value Then _d=True
		If not _ReadingData=True Then
		If _created_time <> value then RaiseEvent created_timeChanged
		End If
		_created_time = value
		If _ReadingData=True Then _d=False
		_created_time_d=_d
	End Set
End Property

Dim _requester as String

Dim _requester_d as Boolean=False
Public Event requesterChanged()
Public Property requester() as String
	Get
		Return _requester
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _requester <> value Then _d=True
		If not _ReadingData=True Then
		If _requester <> value then RaiseEvent requesterChanged
		End If
		_requester = value
		If _ReadingData=True Then _d=False
		_requester_d=_d
	End Set
End Property

Dim _requester_id as Integer

Dim _requester_id_d as Boolean=False
Public Event requester_idChanged()
Public Property requester_id() as Integer
	Get
		Return _requester_id
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _requester_id <> value Then _d=True
		If not _ReadingData=True Then
		If _requester_id <> value then RaiseEvent requester_idChanged
		End If
		_requester_id = value
		If _ReadingData=True Then _d=False
		_requester_id_d=_d
	End Set
End Property

Dim _technician as String

Dim _technician_d as Boolean=False
Public Event technicianChanged()
Public Property technician() as String
	Get
		Return _technician
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _technician <> value Then _d=True
		If not _ReadingData=True Then
		If _technician <> value then RaiseEvent technicianChanged
		End If
		_technician = value
		If _ReadingData=True Then _d=False
		_technician_d=_d
	End Set
End Property

Dim _technician_id as Integer

Dim _technician_id_d as Boolean=False
Public Event technician_idChanged()
Public Property technician_id() as Integer
	Get
		Return _technician_id
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _technician_id <> value Then _d=True
		If not _ReadingData=True Then
		If _technician_id <> value then RaiseEvent technician_idChanged
		End If
		_technician_id = value
		If _ReadingData=True Then _d=False
		_technician_id_d=_d
	End Set
End Property

Dim _client as String

Dim _client_d as Boolean=False
Public Event clientChanged()
Public Property client() as String
	Get
		Return _client
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _client <> value Then _d=True
		If not _ReadingData=True Then
		If _client <> value then RaiseEvent clientChanged
		End If
		_client = value
		If _ReadingData=True Then _d=False
		_client_d=_d
	End Set
End Property

Dim _tender_type as String

Dim _tender_type_d as Boolean=False
Public Event tender_typeChanged()
Public Property tender_type() as String
	Get
		Return _tender_type
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _tender_type <> value Then _d=True
		If not _ReadingData=True Then
		If _tender_type <> value then RaiseEvent tender_typeChanged
		End If
		_tender_type = value
		If _ReadingData=True Then _d=False
		_tender_type_d=_d
	End Set
End Property

Dim _country as String

Dim _country_d as Boolean=False
Public Event countryChanged()
Public Property country() as String
	Get
		Return _country
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _country <> value Then _d=True
		If not _ReadingData=True Then
		If _country <> value then RaiseEvent countryChanged
		End If
		_country = value
		If _ReadingData=True Then _d=False
		_country_d=_d
	End Set
End Property

Dim _contract_renewal as Datetime

Dim _contract_renewal_d as Boolean=False
Public Event contract_renewalChanged()
Public Property contract_renewal() as Datetime
	Get
		Return _contract_renewal
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _contract_renewal <> value Then _d=True
		If not _ReadingData=True Then
		If _contract_renewal <> value then RaiseEvent contract_renewalChanged
		End If
		_contract_renewal = value
		If _ReadingData=True Then _d=False
		_contract_renewal_d=_d
	End Set
End Property

Dim _tender_due as Datetime

Dim _tender_due_d as Boolean=False
Public Event tender_dueChanged()
Public Property tender_due() as Datetime
	Get
		Return _tender_due
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _tender_due <> value Then _d=True
		If not _ReadingData=True Then
		If _tender_due <> value then RaiseEvent tender_dueChanged
		End If
		_tender_due = value
		If _ReadingData=True Then _d=False
		_tender_due_d=_d
	End Set
End Property

Dim _status as String

Dim _status_d as Boolean=False
Public Event statusChanged()
Public Property status() as String
	Get
		Return _status
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _status <> value Then _d=True
		If not _ReadingData=True Then
		If _status <> value then RaiseEvent statusChanged
		End If
		_status = value
		If _ReadingData=True Then _d=False
		_status_d=_d
	End Set
End Property

Dim _commodity as String

Dim _commodity_d as Boolean=False
Public Event commodityChanged()
Public Property commodity() as String
	Get
		Return _commodity
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _commodity <> value Then _d=True
		If not _ReadingData=True Then
		If _commodity <> value then RaiseEvent commodityChanged
		End If
		_commodity = value
		If _ReadingData=True Then _d=False
		_commodity_d=_d
	End Set
End Property

Dim _completed_time as Datetime

Dim _completed_time_d as Boolean=False
Public Event completed_timeChanged()
Public Property completed_time() as Datetime
	Get
		Return _completed_time
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _completed_time <> value Then _d=True
		If not _ReadingData=True Then
		If _completed_time <> value then RaiseEvent completed_timeChanged
		End If
		_completed_time = value
		If _ReadingData=True Then _d=False
		_completed_time_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _helpdeskworkorderdetail_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _helpdesk_id_d
		_IsDirty = _IsDirty OR _created_time_d
		_IsDirty = _IsDirty OR _requester_d
		_IsDirty = _IsDirty OR _requester_id_d
		_IsDirty = _IsDirty OR _technician_d
		_IsDirty = _IsDirty OR _technician_id_d
		_IsDirty = _IsDirty OR _client_d
		_IsDirty = _IsDirty OR _tender_type_d
		_IsDirty = _IsDirty OR _country_d
		_IsDirty = _IsDirty OR _contract_renewal_d
		_IsDirty = _IsDirty OR _tender_due_d
		_IsDirty = _IsDirty OR _status_d
		_IsDirty = _IsDirty OR _commodity_d
		_IsDirty = _IsDirty OR _completed_time_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_helpdeskworkorderdetail_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("helpdeskworkorderdetails",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_helpdeskworkorderdetails_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@helpdeskworkorderdetail_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_helpdeskworkorderdetail_pk=CommonFN.SafeRead(Row("helpdeskworkorderdetail_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		helpdesk_id=CommonFN.SafeRead(Row("helpdesk_id"))
		created_time=CommonFN.SafeRead(Row("created_time"))
		requester=CommonFN.SafeRead(Row("requester"))
		requester_id=CommonFN.SafeRead(Row("requester_id"))
		technician=CommonFN.SafeRead(Row("technician"))
		technician_id=CommonFN.SafeRead(Row("technician_id"))
		client=CommonFN.SafeRead(Row("client"))
		tender_type=CommonFN.SafeRead(Row("tender_type"))
		country=CommonFN.SafeRead(Row("country"))
		contract_renewal=CommonFN.SafeRead(Row("contract_renewal"))
		tender_due=CommonFN.SafeRead(Row("tender_due"))
		status=CommonFN.SafeRead(Row("status"))
		commodity=CommonFN.SafeRead(Row("commodity"))
		completed_time=CommonFN.SafeRead(Row("completed_time"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Return False
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_helpdeskworkorderdetails_compare"
	PO = New SqlClient.SqlParameter("@helpdeskworkorderdetail_pk", helpdeskworkorderdetail_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@created_time", CommonFN.CheckEmptyDate(created_time.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@requester", requester)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@technician", technician)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@client", client)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@tender_type", tender_type)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@country", country)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@contract_renewal", CommonFN.CheckEmptyDate(contract_renewal.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@tender_due", CommonFN.CheckEmptyDate(tender_due.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@status", status)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@commodity", commodity)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@completed_time", CommonFN.CheckEmptyDate(completed_time.ToString()))
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and helpdeskworkorderdetail_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_helpdeskworkorderdetails_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@helpdeskworkorderdetail_pk", helpdeskworkorderdetail_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@helpdesk_id", helpdesk_id)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@created_time", CommonFN.CheckEmptyDate(created_time.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@requester", requester)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@requester_id", requester_id)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@technician", technician)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@technician_id", technician_id)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@client", client)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@tender_type", tender_type)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@country", country)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@contract_renewal", CommonFN.CheckEmptyDate(contract_renewal.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@tender_due", CommonFN.CheckEmptyDate(tender_due.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@status", status)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@commodity", commodity)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@completed_time", CommonFN.CheckEmptyDate(completed_time.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _helpdeskworkorderdetail_pk_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"helpdeskworkorderdetail_pk",_CurrentUser,modified,CStr(helpdeskworkorderdetail_pk)):_helpdeskworkorderdetail_pk_d=False
				If _creator_fk_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _helpdesk_id_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"helpdesk_id",_CurrentUser,modified,CStr(helpdesk_id)):_helpdesk_id_d=False
				If _created_time_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"created_time",_CurrentUser,modified,CStr(created_time)):_created_time_d=False
				If _requester_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"requester",_CurrentUser,modified,CStr(requester)):_requester_d=False
				If _requester_id_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"requester_id",_CurrentUser,modified,CStr(requester_id)):_requester_id_d=False
				If _technician_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"technician",_CurrentUser,modified,CStr(technician)):_technician_d=False
				If _technician_id_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"technician_id",_CurrentUser,modified,CStr(technician_id)):_technician_id_d=False
				If _client_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"client",_CurrentUser,modified,CStr(client)):_client_d=False
				If _tender_type_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"tender_type",_CurrentUser,modified,CStr(tender_type)):_tender_type_d=False
				If _country_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"country",_CurrentUser,modified,CStr(country)):_country_d=False
				If _contract_renewal_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"contract_renewal",_CurrentUser,modified,CStr(contract_renewal)):_contract_renewal_d=False
				If _tender_due_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"tender_due",_CurrentUser,modified,CStr(tender_due)):_tender_due_d=False
				If _status_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"status",_CurrentUser,modified,CStr(status)):_status_d=False
				If _commodity_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"commodity",_CurrentUser,modified,CStr(commodity)):_commodity_d=False
				If _completed_time_d then CDC.AuditLog("helpdeskworkorderdetails",_returnPK,"completed_time",_CurrentUser,modified,CStr(completed_time)):_completed_time_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
