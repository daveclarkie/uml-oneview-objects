Imports cca.common

Public Class dataconsumptionanalysis
    Dim _ReadingData As Boolean = False
    Dim _LastError As Exception = Nothing

    Public ReadOnly Property LastError() As Exception
        Get
            Return _LastError
        End Get
    End Property

    Dim _dataconsumptionanalysis_pk As Integer

    Dim _dataconsumptionanalysis_pk_d As Boolean = False
    Public Event dataconsumptionanalysis_pkChanged()
    Public ReadOnly Property dataconsumptionanalysis_pk() As Integer
        Get
            Return _dataconsumptionanalysis_pk
        End Get
    End Property

    Dim _creator_fk As Integer

    Dim _creator_fk_d As Boolean = False
    Public Event creator_fkChanged()
    Public Property creator_fk() As Integer
        Get
            Return _creator_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _creator_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("creator_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _creator_fk <> value Then RaiseEvent creator_fkChanged()
            End If
            _creator_fk = value
            If _ReadingData = True Then _d = False
            _creator_fk_d = _d
        End Set
    End Property

    Dim _editor_fk As Integer

    Dim _editor_fk_d As Boolean = False
    Public Event editor_fkChanged()
    Public Property editor_fk() As Integer
        Get
            Return _editor_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _editor_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("editor_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _editor_fk <> value Then RaiseEvent editor_fkChanged()
            End If
            _editor_fk = value
            If _ReadingData = True Then _d = False
            _editor_fk_d = _d
        End Set
    End Property

    Dim _created As DateTime

    Dim _created_d As Boolean = False
    Public Event createdChanged()
    Public Property created() As DateTime
        Get
            Return _created
        End Get
        Set(ByVal value As DateTime)
            Dim _d As Boolean = False
            If _created <> value Then _d = True
            If Not _ReadingData = True Then
                If _created <> value Then RaiseEvent createdChanged()
            End If
            _created = value
            If _ReadingData = True Then _d = False
            _created_d = _d
        End Set
    End Property

    Dim _modified As DateTime

    Dim _modified_d As Boolean = False
    Public Event modifiedChanged()
    Public Property modified() As DateTime
        Get
            Return _modified
        End Get
        Set(ByVal value As DateTime)
            Dim _d As Boolean = False
            If _modified <> value Then _d = True
            If Not _ReadingData = True Then
                If _modified <> value Then RaiseEvent modifiedChanged()
            End If
            _modified = value
            If _ReadingData = True Then _d = False
            _modified_d = _d
        End Set
    End Property

    Dim _rowstatus As Integer = 0
    Dim _rowstatus_d As Boolean = False
    Public Event rowstatusChanged()
    Public Property rowstatus() As Integer
        Get
            Return _rowstatus
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If _rowstatus <> value Then _d = True
            If Not _ReadingData = True Then
                If _rowstatus <> value Then RaiseEvent rowstatusChanged()
            End If
            _rowstatus = value
            If _ReadingData = True Then _d = False
            _rowstatus_d = _d
        End Set
    End Property

    Dim _meterpoint_fk As String

    Dim _meterpoint_fk_d As Boolean = False
    Public Event meterpoint_fkChanged()
    Public Property meterpoint_fk() As String
        Get
            Return _meterpoint_fk
        End Get
        Set(ByVal value As String)
            Dim _d As Boolean = False
            If _meterpoint_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If _meterpoint_fk <> value Then RaiseEvent meterpoint_fkChanged()
            End If
            _meterpoint_fk = value
            If _ReadingData = True Then _d = False
            _meterpoint_fk_d = _d
        End Set
    End Property

    Dim _agreement_fk As Integer

    Dim _agreement_fk_d As Boolean = False
    Public Event agreement_fkChanged()
    Public Property agreement_fk() As Integer
        Get
            Return _agreement_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _agreement_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("agreement_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("agreement_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _agreement_fk <> value Then RaiseEvent agreement_fkChanged()
            End If
            _agreement_fk = value
            If _ReadingData = True Then _d = False
            _agreement_fk_d = _d
        End Set
    End Property

    Dim _user_fk As Integer

    Dim _user_fk_d As Boolean = False
    Public Event user_fkChanged()
    Public Property user_fk() As Integer
        Get
            Return _user_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _user_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("user_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("user_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _user_fk <> value Then RaiseEvent user_fkChanged()
            End If
            _user_fk = value
            If _ReadingData = True Then _d = False
            _user_fk_d = _d
        End Set
    End Property

    Dim _IsDirty As Boolean = False
    Public ReadOnly Property IsDirty() As Boolean
        Get
            _IsDirty = False
            _IsDirty = _IsDirty Or _dataconsumptionanalysis_pk_d
            _IsDirty = _IsDirty Or _creator_fk_d
            _IsDirty = _IsDirty Or _editor_fk_d
            _IsDirty = _IsDirty Or _created_d
            _IsDirty = _IsDirty Or _modified_d
            _IsDirty = _IsDirty Or _rowstatus_d
            _IsDirty = _IsDirty Or _meterpoint_fk_d
            _IsDirty = _IsDirty Or _agreement_fk_d
            _IsDirty = _IsDirty Or _user_fk_d
            Return _IsDirty
        End Get
    End Property



    Dim CDC As DataCommon
    Dim Row As DataRow
    Dim _CurrentUser As Integer
    Dim _ActualUser As Integer

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        _ReadingData = True
        _dataconsumptionanalysis_pk = -1
        _creator_fk = CurrentUser
        _editor_fk = ActualUser
        Dim _dcreated As DateTime = Nothing : CDC.ReadScalarValue(_dcreated, New SqlClient.SqlCommand("select (getdate())"))
        created = _dcreated
        Dim _dmodified As DateTime = Nothing : CDC.ReadScalarValue(_dmodified, New SqlClient.SqlCommand("select (getdate())"))
        modified = _dmodified
        _meterpoint_fk = -1
        _agreement_fk = -1
        _user_fk = -1
        _ReadingData = False
    End Sub

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, ByVal PK As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        Read(PK)
    End Sub

    Public Function Read(ByVal PK As Integer) As Boolean
        Dim _ReturnStatus As Boolean = True
        CDC.AccessLog("dataconsumptionanalysis", PK, _ActualUser)
        _ReadingData = True
        Try
            Dim CO As New SqlClient.SqlCommand("rkg_dataconsumptionanalysis_read")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.Add(New SqlClient.SqlParameter("@dataconsumptionanalysis_pk", PK))
            Row = CDC.ReadDataRow(CO)
            CO.Dispose()
            _dataconsumptionanalysis_pk = CommonFN.SafeRead(Row("dataconsumptionanalysis_pk"))
            creator_fk = CommonFN.SafeRead(Row("creator_fk"))
            editor_fk = CommonFN.SafeRead(Row("editor_fk"))
            created = CommonFN.SafeRead(Row("created"))
            modified = CommonFN.SafeRead(Row("modified"))
            rowstatus = CommonFN.SafeRead(Row("rowstatus"))
            meterpoint_fk = CommonFN.SafeRead(Row("meterpoint_fk"))
            agreement_fk = CommonFN.SafeRead(Row("agreement_fk"))
            user_fk = CommonFN.SafeRead(Row("user_fk"))
            Row = Nothing
        Catch Ex As Exception
            _ReturnStatus = False
            _LastError = Ex
        End Try
        _ReadingData = False
        Return _ReturnStatus
    End Function

    Dim _MatchedRecords As DataTable
    Public ReadOnly Property MatchedRecords() As DataTable
        Get
            If _MatchedRecords Is Nothing Then Exists()
            Return _MatchedRecords
        End Get
    End Property
    Public Function Exists() As Boolean
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_dataconsumptionanalysis_exists"
        PO = New SqlClient.SqlParameter("@meterpoint_fk", meterpoint_fk)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@agreement_fk", agreement_fk)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@user_fk", user_fk)
        CO.Parameters.Add(PO)
        Dim _returnPK As Integer = 0
        _MatchedRecords = CDC.ReadDataTable(CO)
        _returnPK = _MatchedRecords.Rows.Count
        If _returnPK > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Dim _UserRecords As DataTable
    Public ReadOnly Property UserRecords() As DataTable
        Get
            If _UserRecords Is Nothing Then ByUser()
            Return _UserRecords
        End Get
    End Property
    Public Function ByUser(Optional ByVal user_pk As Integer = -1) As Boolean
        If user_pk = -1 Then user_pk = _user_fk
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_dataconsumptionanalysis_byuser"
        PO = New SqlClient.SqlParameter("@user_pk", user_pk)
        CO.Parameters.Add(PO)
        Dim _returnPK As Integer = 0
        _UserRecords = CDC.ReadDataTable(CO)
        _returnPK = _UserRecords.Rows.Count
        If _returnPK > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Dim _SimilarRecords As DataTable
    Public ReadOnly Property SimilarRecords() As DataTable
        Get
            If _SimilarRecords Is Nothing Then IsUnique()
            Return _SimilarRecords
        End Get
    End Property
    Public Function IsUnique(Optional ByVal minMatchCount As Integer = 0) As Boolean
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_dataconsumptionanalysis_compare"
        PO = New SqlClient.SqlParameter("@dataconsumptionanalysis_pk", dataconsumptionanalysis_pk)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@meterpoint_fk", meterpoint_fk)
        CO.Parameters.Add(PO)
        If minMatchCount > 0 Then CO.Parameters.AddWithValue("@minMatchCount", minMatchCount)
        Dim _returnPK As Integer = 0
        _SimilarRecords = CDC.ReadDataTable(CO)
        _returnPK = _SimilarRecords.Rows.Count
        If _returnPK = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Save() As Boolean
        Dim _ReturnStatus As Boolean = True
        If IsDirty Then
            If _CurrentUser <> creator_fk And dataconsumptionanalysis_pk < 1 Then creator_fk = _CurrentUser : created = Now()
            If _ActualUser <> editor_fk Then editor_fk = _ActualUser
            modified = Now()
            Try
                CDC.BeginTransaction()
                Dim PO As SqlClient.SqlParameter
                Dim CO As New SqlClient.SqlCommand()
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rkg_dataconsumptionanalysis_save"
                PO = Nothing
                PO = New SqlClient.SqlParameter("@dataconsumptionanalysis_pk", dataconsumptionanalysis_pk)
                If Not PO Is Nothing Then CO.Parameters.Add(PO)
                PO = Nothing
                Dim _tcreator_fk As Integer = Nothing
                If creator_fk = Nothing Then
                    CDC.ReadScalarValue(_tcreator_fk, New SqlClient.SqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
                Else
                    PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
                End If
                If Not PO Is Nothing Then CO.Parameters.Add(PO)
                PO = Nothing
                Dim _teditor_fk As Integer = Nothing
                If editor_fk = Nothing Then
                    CDC.ReadScalarValue(_teditor_fk, New SqlClient.SqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
                Else
                    PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
                End If
                If Not PO Is Nothing Then CO.Parameters.Add(PO)
                PO = Nothing
                Dim _tcreated As DateTime = Nothing
                If created = Nothing Then
                    CDC.ReadScalarValue(_tcreated, New SqlClient.SqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@created", _tcreated)
                Else
                    PO = New SqlClient.SqlParameter("@created", created)
                End If
                If Not PO Is Nothing Then CO.Parameters.Add(PO)
                PO = Nothing
                Dim _tmodified As DateTime = Nothing
                If modified = Nothing Then
                    CDC.ReadScalarValue(_tmodified, New SqlClient.SqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@modified", _tmodified)
                Else
                    PO = New SqlClient.SqlParameter("@modified", modified)
                End If
                If Not PO Is Nothing Then CO.Parameters.Add(PO)
                PO = Nothing
                Dim _trowstatus As Integer = Nothing
                If rowstatus = Nothing Then
                    CDC.ReadScalarValue(_trowstatus, New SqlClient.SqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
                Else
                    PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
                End If
                If Not PO Is Nothing Then CO.Parameters.Add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@meterpoint_fk", meterpoint_fk)
                If Not PO Is Nothing Then CO.Parameters.Add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@agreement_fk", agreement_fk)
                If Not PO Is Nothing Then CO.Parameters.Add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@user_fk", user_fk)
                If Not PO Is Nothing Then CO.Parameters.Add(PO)
                Dim _returnPK As Integer = 0
                DataCommon.PrintCommand(CO)
                If CDC.ReadScalarValue(_returnPK, CO) Then
                    Try
                        If _dataconsumptionanalysis_pk_d Then CDC.AuditLog("dataconsumptionanalysis", _returnPK, "dataconsumptionanalysis_pk", _CurrentUser, modified, CStr(dataconsumptionanalysis_pk)) : _dataconsumptionanalysis_pk_d = False
                        If _creator_fk_d Then CDC.AuditLog("dataconsumptionanalysis", _returnPK, "creator_fk", _CurrentUser, modified, CStr(creator_fk)) : _creator_fk_d = False
                        If _editor_fk_d Then CDC.AuditLog("dataconsumptionanalysis", _returnPK, "editor_fk", _CurrentUser, modified, CStr(editor_fk)) : _editor_fk_d = False
                        If _created_d Then CDC.AuditLog("dataconsumptionanalysis", _returnPK, "created", _CurrentUser, modified, CStr(created)) : _created_d = False
                        If _modified_d Then CDC.AuditLog("dataconsumptionanalysis", _returnPK, "modified", _CurrentUser, modified, CStr(modified)) : _modified_d = False
                        If _rowstatus_d Then CDC.AuditLog("dataconsumptionanalysis", _returnPK, "rowstatus", _CurrentUser, modified, CStr(rowstatus)) : _rowstatus_d = False
                        If _meterpoint_fk_d Then CDC.AuditLog("dataconsumptionanalysis", _returnPK, "meterpoint_fk", _CurrentUser, modified, CStr(meterpoint_fk)) : _meterpoint_fk_d = False
                        If _agreement_fk_d Then CDC.AuditLog("dataconsumptionanalysis", _returnPK, "agreement_fk", _CurrentUser, modified, CStr(agreement_fk)) : _agreement_fk_d = False
                        If _user_fk_d Then CDC.AuditLog("dataconsumptionanalysis", _returnPK, "user_fk", _CurrentUser, modified, CStr(user_fk)) : _user_fk_d = False
                    Catch Ex2 As Exception
                        _ReturnStatus = False
                        _LastError = Ex2
                    End Try
                Else
                    _ReturnStatus = False
                End If
                If _ReturnStatus Then
                    CDC.CommitTransaction()
                    Read(_returnPK)
                Else
                    CDC.RollbackTransaction()
                End If
            Catch Ex As Exception
                _ReturnStatus = False
                _LastError = Ex
                CDC.RollbackTransaction()
            End Try
        Else
            _ReturnStatus = True
        End If
        Return _ReturnStatus
    End Function

    Public Function Enable() As Boolean
        rowstatus = 0
        Return Save()
    End Function

    Public Function Disable() As Boolean
        rowstatus = 1
        Return Save()
    End Function

    Public Function Delete() As Boolean
        rowstatus = 2
        Return Save()
    End Function

   
End Class
