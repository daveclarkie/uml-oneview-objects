Imports cca.common
Public Class appointments
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _appointment_pk as Integer

Dim _appointment_pk_d as Boolean=False
Public Event appointment_pkChanged()
Public ReadOnly Property appointment_pk() as Integer
	Get
		Return _appointment_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _organisation_fk as Integer

Dim _organisation_fk_d as Boolean=False
Public Event organisation_fkChanged()
Public Property organisation_fk() as Integer
	Get
		Return _organisation_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _organisation_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("organisation_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Organisation"), CStr("Validation failed: Invalid Foreign Key"))
		If _organisation_fk <> value then RaiseEvent organisation_fkChanged
		End If
		_organisation_fk = value
		If _ReadingData=True Then _d=False
		_organisation_fk_d=_d
	End Set
End Property

Dim _site_fk as Integer

Dim _site_fk_d as Boolean=False
Public Event site_fkChanged()
Public Property site_fk() as Integer
	Get
		Return _site_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _site_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("site_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Site Name"), CStr("Validation failed: Invalid Foreign Key"))
		If _site_fk <> value then RaiseEvent site_fkChanged
		End If
		_site_fk = value
		If _ReadingData=True Then _d=False
		_site_fk_d=_d
	End Set
End Property

Dim _appointmenttype_fk as Integer

Dim _appointmenttype_fk_d as Boolean=False
Public Event appointmenttype_fkChanged()
Public Property appointmenttype_fk() as Integer
	Get
		Return _appointmenttype_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _appointmenttype_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("appointmenttype_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Appointment Type"), CStr("Validation failed: Invalid Foreign Key"))
		If _appointmenttype_fk <> value then RaiseEvent appointmenttype_fkChanged
		End If
		_appointmenttype_fk = value
		If _ReadingData=True Then _d=False
		_appointmenttype_fk_d=_d
	End Set
End Property

Dim _appointment as Datetime

Dim _appointment_d as Boolean=False
Public Event appointmentChanged()
Public Property appointment() as Datetime
	Get
		Return _appointment
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _appointment <> value Then _d=True
		If not _ReadingData=True Then
		If _appointment <> value then RaiseEvent appointmentChanged
		End If
		_appointment = value
		If _ReadingData=True Then _d=False
		_appointment_d=_d
	End Set
End Property

Dim _location_fk as Integer

Dim _location_fk_d as Boolean=False
Public Event location_fkChanged()
Public Property location_fk() as Integer
	Get
		Return _location_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _location_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("location_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Where"), CStr("Validation failed: Invalid Foreign Key"))
		If _location_fk <> value then RaiseEvent location_fkChanged
		End If
		_location_fk = value
		If _ReadingData=True Then _d=False
		_location_fk_d=_d
	End Set
End Property

Dim _appointmentstatus_fk as Integer

Dim _appointmentstatus_fk_d as Boolean=False
Public Event appointmentstatus_fkChanged()
Public Property appointmentstatus_fk() as Integer
	Get
		Return _appointmentstatus_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _appointmentstatus_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("appointmentstatus_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Status"), CStr("Validation failed: Invalid Foreign Key"))
		If _appointmentstatus_fk <> value then RaiseEvent appointmentstatus_fkChanged
		End If
		_appointmentstatus_fk = value
		If _ReadingData=True Then _d=False
		_appointmentstatus_fk_d=_d
	End Set
End Property

Dim _appointmentpurpose_fk as Integer

Dim _appointmentpurpose_fk_d as Boolean=False
Public Event appointmentpurpose_fkChanged()
Public Property appointmentpurpose_fk() as Integer
	Get
		Return _appointmentpurpose_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _appointmentpurpose_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("appointmentpurpose_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("appointmentpurpose_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _appointmentpurpose_fk <> value then RaiseEvent appointmentpurpose_fkChanged
		End If
		_appointmentpurpose_fk = value
		If _ReadingData=True Then _d=False
		_appointmentpurpose_fk_d=_d
	End Set
End Property

Dim _appointmentpurposeother as String

Dim _appointmentpurposeother_d as Boolean=False
Public Event appointmentpurposeotherChanged()
Public Property appointmentpurposeother() as String
	Get
		Return _appointmentpurposeother
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _appointmentpurposeother <> value Then _d=True
		If not _ReadingData=True Then
		If _appointmentpurposeother <> value then RaiseEvent appointmentpurposeotherChanged
		End If
		_appointmentpurposeother = value
		If _ReadingData=True Then _d=False
		_appointmentpurposeother_d=_d
	End Set
End Property

Dim _phone_fk as Integer

Dim _phone_fk_d as Boolean=False
Public Event phone_fkChanged()
Public Property phone_fk() as Integer
	Get
		Return _phone_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _phone_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("phone_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("phone_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _phone_fk <> value then RaiseEvent phone_fkChanged
		End If
		_phone_fk = value
		If _ReadingData=True Then _d=False
		_phone_fk_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _appointment_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _organisation_fk_d
		_IsDirty = _IsDirty OR _site_fk_d
		_IsDirty = _IsDirty OR _appointmenttype_fk_d
		_IsDirty = _IsDirty OR _appointment_d
		_IsDirty = _IsDirty OR _location_fk_d
		_IsDirty = _IsDirty OR _appointmentstatus_fk_d
		_IsDirty = _IsDirty OR _appointmentpurpose_fk_d
		_IsDirty = _IsDirty OR _appointmentpurposeother_d
		_IsDirty = _IsDirty OR _phone_fk_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_appointment_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_organisation_fk=-1
	_site_fk=-1
	_appointmenttype_fk=-1
	_location_fk=-1
	_appointmentstatus_fk=-1
	_appointmentpurpose_fk=-1
	_phone_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("appointments",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_appointments_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@appointment_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_appointment_pk=CommonFN.SafeRead(Row("appointment_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		organisation_fk=CommonFN.SafeRead(Row("organisation_fk"))
		site_fk=CommonFN.SafeRead(Row("site_fk"))
		appointmenttype_fk=CommonFN.SafeRead(Row("appointmenttype_fk"))
		appointment=CommonFN.SafeRead(Row("appointment"))
		location_fk=CommonFN.SafeRead(Row("location_fk"))
		appointmentstatus_fk=CommonFN.SafeRead(Row("appointmentstatus_fk"))
		appointmentpurpose_fk=CommonFN.SafeRead(Row("appointmentpurpose_fk"))
		appointmentpurposeother=CommonFN.SafeRead(Row("appointmentpurposeother"))
		phone_fk=CommonFN.SafeRead(Row("phone_fk"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_appointments_exists"
	PO = New SqlClient.SqlParameter("@organisation_fk", organisation_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@site_fk", site_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@appointmenttype_fk", appointmenttype_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@location_fk", location_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@appointmentstatus_fk", appointmentstatus_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@appointmentpurpose_fk", appointmentpurpose_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@phone_fk", phone_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_appointments_compare"
	PO = New SqlClient.SqlParameter("@appointment_pk", appointment_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@appointment", CommonFN.CheckEmptyDate(appointment.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@appointmentpurposeother", appointmentpurposeother)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and appointment_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_appointments_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@appointment_pk", appointment_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@organisation_fk", organisation_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@site_fk", site_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@appointmenttype_fk", appointmenttype_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@appointment", CommonFN.CheckEmptyDate(appointment.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@location_fk", location_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@appointmentstatus_fk", appointmentstatus_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@appointmentpurpose_fk", appointmentpurpose_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@appointmentpurposeother", appointmentpurposeother)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@phone_fk", phone_fk)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _appointment_pk_d then CDC.AuditLog("appointments",_returnPK,"appointment_pk",_CurrentUser,modified,CStr(appointment_pk)):_appointment_pk_d=False
				If _creator_fk_d then CDC.AuditLog("appointments",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("appointments",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("appointments",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("appointments",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("appointments",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _organisation_fk_d then CDC.AuditLog("appointments",_returnPK,"organisation_fk",_CurrentUser,modified,CStr(organisation_fk)):_organisation_fk_d=False
				If _site_fk_d then CDC.AuditLog("appointments",_returnPK,"site_fk",_CurrentUser,modified,CStr(site_fk)):_site_fk_d=False
				If _appointmenttype_fk_d then CDC.AuditLog("appointments",_returnPK,"appointmenttype_fk",_CurrentUser,modified,CStr(appointmenttype_fk)):_appointmenttype_fk_d=False
				If _appointment_d then CDC.AuditLog("appointments",_returnPK,"appointment",_CurrentUser,modified,CStr(appointment)):_appointment_d=False
				If _location_fk_d then CDC.AuditLog("appointments",_returnPK,"location_fk",_CurrentUser,modified,CStr(location_fk)):_location_fk_d=False
				If _appointmentstatus_fk_d then CDC.AuditLog("appointments",_returnPK,"appointmentstatus_fk",_CurrentUser,modified,CStr(appointmentstatus_fk)):_appointmentstatus_fk_d=False
				If _appointmentpurpose_fk_d then CDC.AuditLog("appointments",_returnPK,"appointmentpurpose_fk",_CurrentUser,modified,CStr(appointmentpurpose_fk)):_appointmentpurpose_fk_d=False
				If _appointmentpurposeother_d then CDC.AuditLog("appointments",_returnPK,"appointmentpurposeother",_CurrentUser,modified,CStr(appointmentpurposeother)):_appointmentpurposeother_d=False
				If _phone_fk_d then CDC.AuditLog("appointments",_returnPK,"phone_fk",_CurrentUser,modified,CStr(phone_fk)):_phone_fk_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
