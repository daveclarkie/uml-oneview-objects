Imports cca.common
Public Class reportlogs
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _reportlog_pk as Integer

Dim _reportlog_pk_d as Boolean=False
Public Event reportlog_pkChanged()
Public ReadOnly Property reportlog_pk() as Integer
	Get
		Return _reportlog_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _report_fk as Integer

Dim _report_fk_d as Boolean=False
Public Event report_fkChanged()
Public Property report_fk() as Integer
	Get
		Return _report_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _report_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("report_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("report_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _report_fk <> value then RaiseEvent report_fkChanged
		End If
		_report_fk = value
		If _ReadingData=True Then _d=False
		_report_fk_d=_d
	End Set
End Property

Dim _runfromurl as String

Dim _runfromurl_d as Boolean=False
Public Event runfromurlChanged()
Public Property runfromurl() as String
	Get
		Return _runfromurl
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _runfromurl <> value Then _d=True
		If not _ReadingData=True Then
		If _runfromurl <> value then RaiseEvent runfromurlChanged
		End If
		_runfromurl = value
		If _ReadingData=True Then _d=False
		_runfromurl_d=_d
	End Set
End Property

Dim _param1name as String

Dim _param1name_d as Boolean=False
Public Event param1nameChanged()
Public Property param1name() as String
	Get
		Return _param1name
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _param1name <> value Then _d=True
		If not _ReadingData=True Then
		If _param1name <> value then RaiseEvent param1nameChanged
		End If
		_param1name = value
		If _ReadingData=True Then _d=False
		_param1name_d=_d
	End Set
End Property

Dim _param1value as String

Dim _param1value_d as Boolean=False
Public Event param1valueChanged()
Public Property param1value() as String
	Get
		Return _param1value
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _param1value <> value Then _d=True
		If not _ReadingData=True Then
		If _param1value <> value then RaiseEvent param1valueChanged
		End If
		_param1value = value
		If _ReadingData=True Then _d=False
		_param1value_d=_d
	End Set
End Property

Dim _param2name as String

Dim _param2name_d as Boolean=False
Public Event param2nameChanged()
Public Property param2name() as String
	Get
		Return _param2name
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _param2name <> value Then _d=True
		If not _ReadingData=True Then
		If _param2name <> value then RaiseEvent param2nameChanged
		End If
		_param2name = value
		If _ReadingData=True Then _d=False
		_param2name_d=_d
	End Set
End Property

Dim _param2value as String

Dim _param2value_d as Boolean=False
Public Event param2valueChanged()
Public Property param2value() as String
	Get
		Return _param2value
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _param2value <> value Then _d=True
		If not _ReadingData=True Then
		If _param2value <> value then RaiseEvent param2valueChanged
		End If
		_param2value = value
		If _ReadingData=True Then _d=False
		_param2value_d=_d
	End Set
End Property

Dim _param3name as String

Dim _param3name_d as Boolean=False
Public Event param3nameChanged()
Public Property param3name() as String
	Get
		Return _param3name
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _param3name <> value Then _d=True
		If not _ReadingData=True Then
		If _param3name <> value then RaiseEvent param3nameChanged
		End If
		_param3name = value
		If _ReadingData=True Then _d=False
		_param3name_d=_d
	End Set
End Property

Dim _param3value as String

Dim _param3value_d as Boolean=False
Public Event param3valueChanged()
Public Property param3value() as String
	Get
		Return _param3value
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _param3value <> value Then _d=True
		If not _ReadingData=True Then
		If _param3value <> value then RaiseEvent param3valueChanged
		End If
		_param3value = value
		If _ReadingData=True Then _d=False
		_param3value_d=_d
	End Set
End Property

Dim _param4name as String

Dim _param4name_d as Boolean=False
Public Event param4nameChanged()
Public Property param4name() as String
	Get
		Return _param4name
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _param4name <> value Then _d=True
		If not _ReadingData=True Then
		If _param4name <> value then RaiseEvent param4nameChanged
		End If
		_param4name = value
		If _ReadingData=True Then _d=False
		_param4name_d=_d
	End Set
End Property

Dim _param4value as String

Dim _param4value_d as Boolean=False
Public Event param4valueChanged()
Public Property param4value() as String
	Get
		Return _param4value
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _param4value <> value Then _d=True
		If not _ReadingData=True Then
		If _param4value <> value then RaiseEvent param4valueChanged
		End If
		_param4value = value
		If _ReadingData=True Then _d=False
		_param4value_d=_d
	End Set
End Property

Dim _savedpath as String

Dim _savedpath_d as Boolean=False
Public Event savedpathChanged()
Public Property savedpath() as String
	Get
		Return _savedpath
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _savedpath <> value Then _d=True
		If not _ReadingData=True Then
		If _savedpath <> value then RaiseEvent savedpathChanged
		End If
		_savedpath = value
		If _ReadingData=True Then _d=False
		_savedpath_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _reportlog_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _report_fk_d
		_IsDirty = _IsDirty OR _runfromurl_d
		_IsDirty = _IsDirty OR _param1name_d
		_IsDirty = _IsDirty OR _param1value_d
		_IsDirty = _IsDirty OR _param2name_d
		_IsDirty = _IsDirty OR _param2value_d
		_IsDirty = _IsDirty OR _param3name_d
		_IsDirty = _IsDirty OR _param3value_d
		_IsDirty = _IsDirty OR _param4name_d
		_IsDirty = _IsDirty OR _param4value_d
		_IsDirty = _IsDirty OR _savedpath_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_reportlog_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_report_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("reportlogs",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_reportlogs_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@reportlog_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_reportlog_pk=CommonFN.SafeRead(Row("reportlog_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		report_fk=CommonFN.SafeRead(Row("report_fk"))
		runfromurl=CommonFN.SafeRead(Row("runfromurl"))
		param1name=CommonFN.SafeRead(Row("param1name"))
		param1value=CommonFN.SafeRead(Row("param1value"))
		param2name=CommonFN.SafeRead(Row("param2name"))
		param2value=CommonFN.SafeRead(Row("param2value"))
		param3name=CommonFN.SafeRead(Row("param3name"))
		param3value=CommonFN.SafeRead(Row("param3value"))
		param4name=CommonFN.SafeRead(Row("param4name"))
		param4value=CommonFN.SafeRead(Row("param4value"))
		savedpath=CommonFN.SafeRead(Row("savedpath"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_reportlogs_exists"
	PO = New SqlClient.SqlParameter("@report_fk", report_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_reportlogs_compare"
	PO = New SqlClient.SqlParameter("@reportlog_pk", reportlog_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@runfromurl", runfromurl)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@param1name", param1name)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@param1value", param1value)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@param2name", param2name)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@param2value", param2value)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@param3name", param3name)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@param3value", param3value)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@param4name", param4name)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@param4value", param4value)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@savedpath", savedpath)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and reportlog_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_reportlogs_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@reportlog_pk", reportlog_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@report_fk", report_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@runfromurl", runfromurl)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@param1name", param1name)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@param1value", param1value)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@param2name", param2name)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@param2value", param2value)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@param3name", param3name)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@param3value", param3value)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@param4name", param4name)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@param4value", param4value)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@savedpath", savedpath)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _reportlog_pk_d then CDC.AuditLog("reportlogs",_returnPK,"reportlog_pk",_CurrentUser,modified,CStr(reportlog_pk)):_reportlog_pk_d=False
				If _creator_fk_d then CDC.AuditLog("reportlogs",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("reportlogs",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("reportlogs",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("reportlogs",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("reportlogs",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _report_fk_d then CDC.AuditLog("reportlogs",_returnPK,"report_fk",_CurrentUser,modified,CStr(report_fk)):_report_fk_d=False
				If _runfromurl_d then CDC.AuditLog("reportlogs",_returnPK,"runfromurl",_CurrentUser,modified,CStr(runfromurl)):_runfromurl_d=False
				If _param1name_d then CDC.AuditLog("reportlogs",_returnPK,"param1name",_CurrentUser,modified,CStr(param1name)):_param1name_d=False
				If _param1value_d then CDC.AuditLog("reportlogs",_returnPK,"param1value",_CurrentUser,modified,CStr(param1value)):_param1value_d=False
				If _param2name_d then CDC.AuditLog("reportlogs",_returnPK,"param2name",_CurrentUser,modified,CStr(param2name)):_param2name_d=False
				If _param2value_d then CDC.AuditLog("reportlogs",_returnPK,"param2value",_CurrentUser,modified,CStr(param2value)):_param2value_d=False
				If _param3name_d then CDC.AuditLog("reportlogs",_returnPK,"param3name",_CurrentUser,modified,CStr(param3name)):_param3name_d=False
				If _param3value_d then CDC.AuditLog("reportlogs",_returnPK,"param3value",_CurrentUser,modified,CStr(param3value)):_param3value_d=False
				If _param4name_d then CDC.AuditLog("reportlogs",_returnPK,"param4name",_CurrentUser,modified,CStr(param4name)):_param4name_d=False
				If _param4value_d then CDC.AuditLog("reportlogs",_returnPK,"param4value",_CurrentUser,modified,CStr(param4value)):_param4value_d=False
				If _savedpath_d then CDC.AuditLog("reportlogs",_returnPK,"savedpath",_CurrentUser,modified,CStr(savedpath)):_savedpath_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
