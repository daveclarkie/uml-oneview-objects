Imports cca.common
Public Class months
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _month_pk as Integer

Dim _month_pk_d as Boolean=False
Public Event month_pkChanged()
Public ReadOnly Property month_pk() as Integer
	Get
		Return _month_pk
	End Get
End Property

Dim _monthname as String

Dim _monthname_d as Boolean=False
Public Event monthnameChanged()
Public Property monthname() as String
	Get
		Return _monthname
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _monthname <> value Then _d=True
		If not _ReadingData=True Then
		If _monthname <> value then RaiseEvent monthnameChanged
		End If
		_monthname = value
		If _ReadingData=True Then _d=False
		_monthname_d=_d
	End Set
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer

Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _month_pk_d
		_IsDirty = _IsDirty OR _monthname_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_month_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("months",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_months_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@month_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_month_pk=CommonFN.SafeRead(Row("month_pk"))
		monthname=CommonFN.SafeRead(Row("monthname"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Return False
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique() as boolean
	Return True
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and month_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_months_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@month_pk", month_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@monthname", monthname)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@created", CommonFN.CheckEmptyDate(created.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@modified", CommonFN.CheckEmptyDate(modified.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _month_pk_d then CDC.AuditLog("months",_returnPK,"month_pk",_CurrentUser,modified,CStr(month_pk)):_month_pk_d=False
				If _monthname_d then CDC.AuditLog("months",_returnPK,"monthname",_CurrentUser,modified,CStr(monthname)):_monthname_d=False
				If _creator_fk_d then CDC.AuditLog("months",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("months",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("months",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("months",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("months",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
