Imports cca.common
Public Class organisations
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _organisation_pk as Integer

Dim _organisation_pk_d as Boolean=False
Public Event organisation_pkChanged()
Public ReadOnly Property organisation_pk() as Integer
	Get
		Return _organisation_pk
	End Get
End Property

Dim _Custid as Integer

Dim _Custid_d as Boolean=False
Public Event CustidChanged()
Public Property Custid() as Integer
	Get
		Return _Custid
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _Custid <> value Then _d=True
		If not _ReadingData=True Then
		If _Custid <> value then RaiseEvent CustidChanged
		End If
		_Custid = value
		If _ReadingData=True Then _d=False
		_Custid_d=_d
	End Set
End Property

Dim _customername as String

Dim _customername_d as Boolean=False
Public Event customernameChanged()
Public Property customername() as String
	Get
		Return _customername
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _customername <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Organisation"), CStr("Validation failed: Required Field"))
		If _customername <> value then RaiseEvent customernameChanged
		End If
		_customername = value
		If _ReadingData=True Then _d=False
		_customername_d=_d
	End Set
End Property

Dim _address_1 as String

Dim _address_1_d as Boolean=False
Public Event address_1Changed()
Public Property address_1() as String
	Get
		Return _address_1
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _address_1 <> value Then _d=True
		If not _ReadingData=True Then
		If _address_1 <> value then RaiseEvent address_1Changed
		End If
		_address_1 = value
		If _ReadingData=True Then _d=False
		_address_1_d=_d
	End Set
End Property

Dim _address_2 as String

Dim _address_2_d as Boolean=False
Public Event address_2Changed()
Public Property address_2() as String
	Get
		Return _address_2
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _address_2 <> value Then _d=True
		If not _ReadingData=True Then
		If _address_2 <> value then RaiseEvent address_2Changed
		End If
		_address_2 = value
		If _ReadingData=True Then _d=False
		_address_2_d=_d
	End Set
End Property

Dim _address_3 as String

Dim _address_3_d as Boolean=False
Public Event address_3Changed()
Public Property address_3() as String
	Get
		Return _address_3
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _address_3 <> value Then _d=True
		If not _ReadingData=True Then
		If _address_3 <> value then RaiseEvent address_3Changed
		End If
		_address_3 = value
		If _ReadingData=True Then _d=False
		_address_3_d=_d
	End Set
End Property

Dim _Address_4 as String

Dim _Address_4_d as Boolean=False
Public Event Address_4Changed()
Public Property Address_4() as String
	Get
		Return _Address_4
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Address_4 <> value Then _d=True
		If not _ReadingData=True Then
		If _Address_4 <> value then RaiseEvent Address_4Changed
		End If
		_Address_4 = value
		If _ReadingData=True Then _d=False
		_Address_4_d=_d
	End Set
End Property

Dim _county_fk as Integer

Dim _county_fk_d as Boolean=False
Public Event county_fkChanged()
Public Property county_fk() as Integer
	Get
		Return _county_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _county_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("county_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("county_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _county_fk <> value then RaiseEvent county_fkChanged
		End If
		_county_fk = value
		If _ReadingData=True Then _d=False
		_county_fk_d=_d
	End Set
End Property

Dim _Pcode as String

Dim _Pcode_d as Boolean=False
Public Event PcodeChanged()
Public Property Pcode() as String
	Get
		Return _Pcode
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Pcode <> value Then _d=True
		If not _ReadingData=True Then
		If _Pcode <> value then RaiseEvent PcodeChanged
		End If
		_Pcode = value
		If _ReadingData=True Then _d=False
		_Pcode_d=_d
	End Set
End Property

Dim _Contact as String

Dim _Contact_d as Boolean=False
Public Event ContactChanged()
Public Property Contact() as String
	Get
		Return _Contact
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Contact <> value Then _d=True
		If not _ReadingData=True Then
		If _Contact <> value then RaiseEvent ContactChanged
		End If
		_Contact = value
		If _ReadingData=True Then _d=False
		_Contact_d=_d
	End Set
End Property

Dim _coregnum as String

Dim _coregnum_d as Boolean=False
Public Event coregnumChanged()
Public Property coregnum() as String
	Get
		Return _coregnum
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _coregnum <> value Then _d=True
		If not _ReadingData=True Then
		If _coregnum <> value then RaiseEvent coregnumChanged
		End If
		_coregnum = value
		If _ReadingData=True Then _d=False
		_coregnum_d=_d
	End Set
End Property

Dim _prodDecisionPoint as Integer

Dim _prodDecisionPoint_d as Boolean=False
Public Event prodDecisionPointChanged()
Public Property prodDecisionPoint() as Integer
	Get
		Return _prodDecisionPoint
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodDecisionPoint <> value Then _d=True
		If not _ReadingData=True Then
		If _prodDecisionPoint <> value then RaiseEvent prodDecisionPointChanged
		End If
		_prodDecisionPoint = value
		If _ReadingData=True Then _d=False
		_prodDecisionPoint_d=_d
	End Set
End Property

Dim _prodElecneg as Integer

Dim _prodElecneg_d as Boolean=False
Public Event prodElecnegChanged()
Public Property prodElecneg() as Integer
	Get
		Return _prodElecneg
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodElecneg <> value Then _d=True
		If not _ReadingData=True Then
		If _prodElecneg <> value then RaiseEvent prodElecnegChanged
		End If
		_prodElecneg = value
		If _ReadingData=True Then _d=False
		_prodElecneg_d=_d
	End Set
End Property

Dim _prodgasneg as Integer

Dim _prodgasneg_d as Boolean=False
Public Event prodgasnegChanged()
Public Property prodgasneg() as Integer
	Get
		Return _prodgasneg
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodgasneg <> value Then _d=True
		If not _ReadingData=True Then
		If _prodgasneg <> value then RaiseEvent prodgasnegChanged
		End If
		_prodgasneg = value
		If _ReadingData=True Then _d=False
		_prodgasneg_d=_d
	End Set
End Property

Dim _prodccl as Integer

Dim _prodccl_d as Boolean=False
Public Event prodcclChanged()
Public Property prodccl() as Integer
	Get
		Return _prodccl
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodccl <> value Then _d=True
		If not _ReadingData=True Then
		If _prodccl <> value then RaiseEvent prodcclChanged
		End If
		_prodccl = value
		If _ReadingData=True Then _d=False
		_prodccl_d=_d
	End Set
End Property

Dim _prodenergyaction as Integer

Dim _prodenergyaction_d as Boolean=False
Public Event prodenergyactionChanged()
Public Property prodenergyaction() as Integer
	Get
		Return _prodenergyaction
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodenergyaction <> value Then _d=True
		If not _ReadingData=True Then
		If _prodenergyaction <> value then RaiseEvent prodenergyactionChanged
		End If
		_prodenergyaction = value
		If _ReadingData=True Then _d=False
		_prodenergyaction_d=_d
	End Set
End Property

Dim _prodbillaudit as Integer

Dim _prodbillaudit_d as Boolean=False
Public Event prodbillauditChanged()
Public Property prodbillaudit() as Integer
	Get
		Return _prodbillaudit
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodbillaudit <> value Then _d=True
		If not _ReadingData=True Then
		If _prodbillaudit <> value then RaiseEvent prodbillauditChanged
		End If
		_prodbillaudit = value
		If _ReadingData=True Then _d=False
		_prodbillaudit_d=_d
	End Set
End Property

Dim _proddataserv as Integer

Dim _proddataserv_d as Boolean=False
Public Event proddataservChanged()
Public Property proddataserv() as Integer
	Get
		Return _proddataserv
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _proddataserv <> value Then _d=True
		If not _ReadingData=True Then
		If _proddataserv <> value then RaiseEvent proddataservChanged
		End If
		_proddataserv = value
		If _ReadingData=True Then _d=False
		_proddataserv_d=_d
	End Set
End Property

Dim _ProdWaterAudit as Integer

Dim _ProdWaterAudit_d as Boolean=False
Public Event ProdWaterAuditChanged()
Public Property ProdWaterAudit() as Integer
	Get
		Return _ProdWaterAudit
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _ProdWaterAudit <> value Then _d=True
		If not _ReadingData=True Then
		If _ProdWaterAudit <> value then RaiseEvent ProdWaterAuditChanged
		End If
		_ProdWaterAudit = value
		If _ReadingData=True Then _d=False
		_ProdWaterAudit_d=_d
	End Set
End Property

Dim _ProdWaterMgmt as Integer

Dim _ProdWaterMgmt_d as Boolean=False
Public Event ProdWaterMgmtChanged()
Public Property ProdWaterMgmt() as Integer
	Get
		Return _ProdWaterMgmt
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _ProdWaterMgmt <> value Then _d=True
		If not _ReadingData=True Then
		If _ProdWaterMgmt <> value then RaiseEvent ProdWaterMgmtChanged
		End If
		_ProdWaterMgmt = value
		If _ReadingData=True Then _d=False
		_ProdWaterMgmt_d=_d
	End Set
End Property

Dim _ProdCRCAttain as Integer

Dim _ProdCRCAttain_d as Boolean=False
Public Event ProdCRCAttainChanged()
Public Property ProdCRCAttain() as Integer
	Get
		Return _ProdCRCAttain
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _ProdCRCAttain <> value Then _d=True
		If not _ReadingData=True Then
		If _ProdCRCAttain <> value then RaiseEvent ProdCRCAttainChanged
		End If
		_ProdCRCAttain = value
		If _ReadingData=True Then _d=False
		_ProdCRCAttain_d=_d
	End Set
End Property

Dim _ProdCRCMaintain as Integer

Dim _ProdCRCMaintain_d as Boolean=False
Public Event ProdCRCMaintainChanged()
Public Property ProdCRCMaintain() as Integer
	Get
		Return _ProdCRCMaintain
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _ProdCRCMaintain <> value Then _d=True
		If not _ReadingData=True Then
		If _ProdCRCMaintain <> value then RaiseEvent ProdCRCMaintainChanged
		End If
		_ProdCRCMaintain = value
		If _ReadingData=True Then _d=False
		_ProdCRCMaintain_d=_d
	End Set
End Property

Dim _custorprospect as Integer

Dim _custorprospect_d as Boolean=False
Public Event custorprospectChanged()
Public Property custorprospect() as Integer
	Get
		Return _custorprospect
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _custorprospect <> value Then _d=True
		If not _ReadingData=True Then
		If _custorprospect <> value then RaiseEvent custorprospectChanged
		End If
		_custorprospect = value
		If _ReadingData=True Then _d=False
		_custorprospect_d=_d
	End Set
End Property

Dim _CCLexemption as Integer

Dim _CCLexemption_d as Boolean=False
Public Event CCLexemptionChanged()
Public Property CCLexemption() as Integer
	Get
		Return _CCLexemption
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _CCLexemption <> value Then _d=True
		If not _ReadingData=True Then
		If _CCLexemption <> value then RaiseEvent CCLexemptionChanged
		End If
		_CCLexemption = value
		If _ReadingData=True Then _d=False
		_CCLexemption_d=_d
	End Set
End Property

Dim _CCLexemptionfromdate as Datetime

Dim _CCLexemptionfromdate_d as Boolean=False
Public Event CCLexemptionfromdateChanged()
Public Property CCLexemptionfromdate() as Datetime
	Get
		Return _CCLexemptionfromdate
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _CCLexemptionfromdate <> value Then _d=True
		If not _ReadingData=True Then
		If _CCLexemptionfromdate <> value then RaiseEvent CCLexemptionfromdateChanged
		End If
		_CCLexemptionfromdate = value
		If _ReadingData=True Then _d=False
		_CCLexemptionfromdate_d=_d
	End Set
End Property

Dim _CCLcertapplied_activesupply as Integer

Dim _CCLcertapplied_activesupply_d as Boolean=False
Public Event CCLcertapplied_activesupplyChanged()
Public Property CCLcertapplied_activesupply() as Integer
	Get
		Return _CCLcertapplied_activesupply
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _CCLcertapplied_activesupply <> value Then _d=True
		If not _ReadingData=True Then
		If _CCLcertapplied_activesupply <> value then RaiseEvent CCLcertapplied_activesupplyChanged
		End If
		_CCLcertapplied_activesupply = value
		If _ReadingData=True Then _d=False
		_CCLcertapplied_activesupply_d=_d
	End Set
End Property

Dim _CCLcertapplied_pendingsupply as Integer

Dim _CCLcertapplied_pendingsupply_d as Boolean=False
Public Event CCLcertapplied_pendingsupplyChanged()
Public Property CCLcertapplied_pendingsupply() as Integer
	Get
		Return _CCLcertapplied_pendingsupply
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _CCLcertapplied_pendingsupply <> value Then _d=True
		If not _ReadingData=True Then
		If _CCLcertapplied_pendingsupply <> value then RaiseEvent CCLcertapplied_pendingsupplyChanged
		End If
		_CCLcertapplied_pendingsupply = value
		If _ReadingData=True Then _d=False
		_CCLcertapplied_pendingsupply_d=_d
	End Set
End Property

Dim _prodTelco as Integer

Dim _prodTelco_d as Boolean=False
Public Event prodTelcoChanged()
Public Property prodTelco() as Integer
	Get
		Return _prodTelco
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodTelco <> value Then _d=True
		If not _ReadingData=True Then
		If _prodTelco <> value then RaiseEvent prodTelcoChanged
		End If
		_prodTelco = value
		If _ReadingData=True Then _d=False
		_prodTelco_d=_d
	End Set
End Property

Dim _prodMT as Integer

Dim _prodMT_d as Boolean=False
Public Event prodMTChanged()
Public Property prodMT() as Integer
	Get
		Return _prodMT
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodMT <> value Then _d=True
		If not _ReadingData=True Then
		If _prodMT <> value then RaiseEvent prodMTChanged
		End If
		_prodMT = value
		If _ReadingData=True Then _d=False
		_prodMT_d=_d
	End Set
End Property

Dim _prodTRIAD as Integer

Dim _prodTRIAD_d as Boolean=False
Public Event prodTRIADChanged()
Public Property prodTRIAD() as Integer
	Get
		Return _prodTRIAD
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodTRIAD <> value Then _d=True
		If not _ReadingData=True Then
		If _prodTRIAD <> value then RaiseEvent prodTRIADChanged
		End If
		_prodTRIAD = value
		If _ReadingData=True Then _d=False
		_prodTRIAD_d=_d
	End Set
End Property

Dim _pricesensitivity as Integer

Dim _pricesensitivity_d as Boolean=False
Public Event pricesensitivityChanged()
Public Property pricesensitivity() as Integer
	Get
		Return _pricesensitivity
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _pricesensitivity <> value Then _d=True
		If not _ReadingData=True Then
		If _pricesensitivity <> value then RaiseEvent pricesensitivityChanged
		End If
		_pricesensitivity = value
		If _ReadingData=True Then _d=False
		_pricesensitivity_d=_d
	End Set
End Property

Dim _umlloyaltyvalue as Integer

Dim _umlloyaltyvalue_d as Boolean=False
Public Event umlloyaltyvalueChanged()
Public Property umlloyaltyvalue() as Integer
	Get
		Return _umlloyaltyvalue
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _umlloyaltyvalue <> value Then _d=True
		If not _ReadingData=True Then
		If _umlloyaltyvalue <> value then RaiseEvent umlloyaltyvalueChanged
		End If
		_umlloyaltyvalue = value
		If _ReadingData=True Then _d=False
		_umlloyaltyvalue_d=_d
	End Set
End Property

Dim _VATnum as String

Dim _VATnum_d as Boolean=False
Public Event VATnumChanged()
Public Property VATnum() as String
	Get
		Return _VATnum
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _VATnum <> value Then _d=True
		If not _ReadingData=True Then
		If _VATnum <> value then RaiseEvent VATnumChanged
		End If
		_VATnum = value
		If _ReadingData=True Then _d=False
		_VATnum_d=_d
	End Set
End Property

Dim _FKaccmgrid as Integer

Dim _FKaccmgrid_d as Boolean=False
Public Event FKaccmgridChanged()
Public Property FKaccmgrid() as Integer
	Get
		Return _FKaccmgrid
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _FKaccmgrid <> value Then _d=True
		If not _ReadingData=True Then
		If _FKaccmgrid <> value then RaiseEvent FKaccmgridChanged
		End If
		_FKaccmgrid = value
		If _ReadingData=True Then _d=False
		_FKaccmgrid_d=_d
	End Set
End Property

Dim _ProdBillCheck as Integer

Dim _ProdBillCheck_d as Boolean=False
Public Event ProdBillCheckChanged()
Public Property ProdBillCheck() as Integer
	Get
		Return _ProdBillCheck
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _ProdBillCheck <> value Then _d=True
		If not _ReadingData=True Then
		If _ProdBillCheck <> value then RaiseEvent ProdBillCheckChanged
		End If
		_ProdBillCheck = value
		If _ReadingData=True Then _d=False
		_ProdBillCheck_d=_d
	End Set
End Property

Dim _dtmCreated as Datetime

Dim _dtmCreated_d as Boolean=False
Public Event dtmCreatedChanged()
Public Property dtmCreated() as Datetime
	Get
		Return _dtmCreated
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _dtmCreated <> value Then _d=True
		If not _ReadingData=True Then
		If _dtmCreated <> value then RaiseEvent dtmCreatedChanged
		End If
		_dtmCreated = value
		If _ReadingData=True Then _d=False
		_dtmCreated_d=_d
	End Set
End Property

Dim _dtmModified as Datetime

Dim _dtmModified_d as Boolean=False
Public Event dtmModifiedChanged()
Public Property dtmModified() as Datetime
	Get
		Return _dtmModified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _dtmModified <> value Then _d=True
		If not _ReadingData=True Then
		If _dtmModified <> value then RaiseEvent dtmModifiedChanged
		End If
		_dtmModified = value
		If _ReadingData=True Then _d=False
		_dtmModified_d=_d
	End Set
End Property

Dim _intUserCreatedBy as Integer

Dim _intUserCreatedBy_d as Boolean=False
Public Event intUserCreatedByChanged()
Public Property intUserCreatedBy() as Integer
	Get
		Return _intUserCreatedBy
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intUserCreatedBy <> value Then _d=True
		If not _ReadingData=True Then
		If _intUserCreatedBy <> value then RaiseEvent intUserCreatedByChanged
		End If
		_intUserCreatedBy = value
		If _ReadingData=True Then _d=False
		_intUserCreatedBy_d=_d
	End Set
End Property

Dim _intUserModifiedBy as Integer

Dim _intUserModifiedBy_d as Boolean=False
Public Event intUserModifiedByChanged()
Public Property intUserModifiedBy() as Integer
	Get
		Return _intUserModifiedBy
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intUserModifiedBy <> value Then _d=True
		If not _ReadingData=True Then
		If _intUserModifiedBy <> value then RaiseEvent intUserModifiedByChanged
		End If
		_intUserModifiedBy = value
		If _ReadingData=True Then _d=False
		_intUserModifiedBy_d=_d
	End Set
End Property

Dim _intOptimaCustomerPK as Integer

Dim _intOptimaCustomerPK_d as Boolean=False
Public Event intOptimaCustomerPKChanged()
Public Property intOptimaCustomerPK() as Integer
	Get
		Return _intOptimaCustomerPK
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intOptimaCustomerPK <> value Then _d=True
		If not _ReadingData=True Then
		If _intOptimaCustomerPK <> value then RaiseEvent intOptimaCustomerPKChanged
		End If
		_intOptimaCustomerPK = value
		If _ReadingData=True Then _d=False
		_intOptimaCustomerPK_d=_d
	End Set
End Property

Dim _intDataServicesGroupSend as Integer

Dim _intDataServicesGroupSend_d as Boolean=False
Public Event intDataServicesGroupSendChanged()
Public Property intDataServicesGroupSend() as Integer
	Get
		Return _intDataServicesGroupSend
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intDataServicesGroupSend <> value Then _d=True
		If not _ReadingData=True Then
		If _intDataServicesGroupSend <> value then RaiseEvent intDataServicesGroupSendChanged
		End If
		_intDataServicesGroupSend = value
		If _ReadingData=True Then _d=False
		_intDataServicesGroupSend_d=_d
	End Set
End Property

Dim _bitDisabled as Boolean

Dim _bitDisabled_d as Boolean=False
Public Event bitDisabledChanged()
Public Property bitDisabled() as Boolean
	Get
		Return _bitDisabled
	End Get
	Set (ByVal value As Boolean)
		Dim _d as boolean=false
		If _bitDisabled <> value Then _d=True
		If not _ReadingData=True Then
		If _bitDisabled <> value then RaiseEvent bitDisabledChanged
		End If
		_bitDisabled = value
		If _ReadingData=True Then _d=False
		_bitDisabled_d=_d
	End Set
End Property

Dim _varCustomerNote as String

Dim _varCustomerNote_d as Boolean=False
Public Event varCustomerNoteChanged()
Public Property varCustomerNote() as String
	Get
		Return _varCustomerNote
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _varCustomerNote <> value Then _d=True
		If not _ReadingData=True Then
		If _varCustomerNote <> value then RaiseEvent varCustomerNoteChanged
		End If
		_varCustomerNote = value
		If _ReadingData=True Then _d=False
		_varCustomerNote_d=_d
	End Set
End Property

Dim _intMandCRef as Integer

Dim _intMandCRef_d as Boolean=False
Public Event intMandCRefChanged()
Public Property intMandCRef() as Integer
	Get
		Return _intMandCRef
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intMandCRef <> value Then _d=True
		If not _ReadingData=True Then
		If _intMandCRef <> value then RaiseEvent intMandCRefChanged
		End If
		_intMandCRef = value
		If _ReadingData=True Then _d=False
		_intMandCRef_d=_d
	End Set
End Property

Dim _intEnCoreRef as Integer

Dim _intEnCoreRef_d as Boolean=False
Public Event intEnCoreRefChanged()
Public Property intEnCoreRef() as Integer
	Get
		Return _intEnCoreRef
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intEnCoreRef <> value Then _d=True
		If not _ReadingData=True Then
		If _intEnCoreRef <> value then RaiseEvent intEnCoreRefChanged
		End If
		_intEnCoreRef = value
		If _ReadingData=True Then _d=False
		_intEnCoreRef_d=_d
	End Set
End Property

Dim _varAXRef as String

Dim _varAXRef_d as Boolean=False
Public Event varAXRefChanged()
Public Property varAXRef() as String
	Get
		Return _varAXRef
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _varAXRef <> value Then _d=True
		If not _ReadingData=True Then
		If _varAXRef <> value then RaiseEvent varAXRefChanged
		End If
		_varAXRef = value
		If _ReadingData=True Then _d=False
		_varAXRef_d=_d
	End Set
End Property

Dim _intEnterpriseManagerFK as Integer

Dim _intEnterpriseManagerFK_d as Boolean=False
Public Event intEnterpriseManagerFKChanged()
Public Property intEnterpriseManagerFK() as Integer
	Get
		Return _intEnterpriseManagerFK
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intEnterpriseManagerFK <> value Then _d=True
		If not _ReadingData=True Then
		If _intEnterpriseManagerFK <> value then RaiseEvent intEnterpriseManagerFKChanged
		End If
		_intEnterpriseManagerFK = value
		If _ReadingData=True Then _d=False
		_intEnterpriseManagerFK_d=_d
	End Set
End Property

Dim _country_fk as Integer

Dim _country_fk_d as Boolean=False
Public Event country_fkChanged()
Public Property country_fk() as Integer
	Get
		Return _country_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _country_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("country_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("country_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _country_fk <> value then RaiseEvent country_fkChanged
		End If
		_country_fk = value
		If _ReadingData=True Then _d=False
		_country_fk_d=_d
	End Set
End Property

Dim _intSubjectExpertFK as Integer

Dim _intSubjectExpertFK_d as Boolean=False
Public Event intSubjectExpertFKChanged()
Public Property intSubjectExpertFK() as Integer
	Get
		Return _intSubjectExpertFK
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intSubjectExpertFK <> value Then _d=True
		If not _ReadingData=True Then
		If _intSubjectExpertFK <> value then RaiseEvent intSubjectExpertFKChanged
		End If
		_intSubjectExpertFK = value
		If _ReadingData=True Then _d=False
		_intSubjectExpertFK_d=_d
	End Set
End Property

Dim _intSummitRef as Integer

Dim _intSummitRef_d as Boolean=False
Public Event intSummitRefChanged()
Public Property intSummitRef() as Integer
	Get
		Return _intSummitRef
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intSummitRef <> value Then _d=True
		If not _ReadingData=True Then
		If _intSummitRef <> value then RaiseEvent intSummitRefChanged
		End If
		_intSummitRef = value
		If _ReadingData=True Then _d=False
		_intSummitRef_d=_d
	End Set
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer

Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _organisation_pk_d
		_IsDirty = _IsDirty OR _Custid_d
		_IsDirty = _IsDirty OR _customername_d
		_IsDirty = _IsDirty OR _address_1_d
		_IsDirty = _IsDirty OR _address_2_d
		_IsDirty = _IsDirty OR _address_3_d
		_IsDirty = _IsDirty OR _Address_4_d
		_IsDirty = _IsDirty OR _county_fk_d
		_IsDirty = _IsDirty OR _Pcode_d
		_IsDirty = _IsDirty OR _Contact_d
		_IsDirty = _IsDirty OR _coregnum_d
		_IsDirty = _IsDirty OR _prodDecisionPoint_d
		_IsDirty = _IsDirty OR _prodElecneg_d
		_IsDirty = _IsDirty OR _prodgasneg_d
		_IsDirty = _IsDirty OR _prodccl_d
		_IsDirty = _IsDirty OR _prodenergyaction_d
		_IsDirty = _IsDirty OR _prodbillaudit_d
		_IsDirty = _IsDirty OR _proddataserv_d
		_IsDirty = _IsDirty OR _ProdWaterAudit_d
		_IsDirty = _IsDirty OR _ProdWaterMgmt_d
		_IsDirty = _IsDirty OR _ProdCRCAttain_d
		_IsDirty = _IsDirty OR _ProdCRCMaintain_d
		_IsDirty = _IsDirty OR _custorprospect_d
		_IsDirty = _IsDirty OR _CCLexemption_d
		_IsDirty = _IsDirty OR _CCLexemptionfromdate_d
		_IsDirty = _IsDirty OR _CCLcertapplied_activesupply_d
		_IsDirty = _IsDirty OR _CCLcertapplied_pendingsupply_d
		_IsDirty = _IsDirty OR _prodTelco_d
		_IsDirty = _IsDirty OR _prodMT_d
		_IsDirty = _IsDirty OR _prodTRIAD_d
		_IsDirty = _IsDirty OR _pricesensitivity_d
		_IsDirty = _IsDirty OR _umlloyaltyvalue_d
		_IsDirty = _IsDirty OR _VATnum_d
		_IsDirty = _IsDirty OR _FKaccmgrid_d
		_IsDirty = _IsDirty OR _ProdBillCheck_d
		_IsDirty = _IsDirty OR _dtmCreated_d
		_IsDirty = _IsDirty OR _dtmModified_d
		_IsDirty = _IsDirty OR _intUserCreatedBy_d
		_IsDirty = _IsDirty OR _intUserModifiedBy_d
		_IsDirty = _IsDirty OR _intOptimaCustomerPK_d
		_IsDirty = _IsDirty OR _intDataServicesGroupSend_d
		_IsDirty = _IsDirty OR _bitDisabled_d
		_IsDirty = _IsDirty OR _varCustomerNote_d
		_IsDirty = _IsDirty OR _intMandCRef_d
		_IsDirty = _IsDirty OR _intEnCoreRef_d
		_IsDirty = _IsDirty OR _varAXRef_d
		_IsDirty = _IsDirty OR _intEnterpriseManagerFK_d
		_IsDirty = _IsDirty OR _country_fk_d
		_IsDirty = _IsDirty OR _intSubjectExpertFK_d
		_IsDirty = _IsDirty OR _intSummitRef_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_organisation_pk=-1
	_county_fk=-1
	_country_fk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("organisations",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_organisations_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@organisation_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_organisation_pk=CommonFN.SafeRead(Row("organisation_pk"))
		Custid=CommonFN.SafeRead(Row("Custid"))
		customername=CommonFN.SafeRead(Row("customername"))
		address_1=CommonFN.SafeRead(Row("address_1"))
		address_2=CommonFN.SafeRead(Row("address_2"))
		address_3=CommonFN.SafeRead(Row("address_3"))
		Address_4=CommonFN.SafeRead(Row("Address_4"))
		county_fk=CommonFN.SafeRead(Row("county_fk"))
		Pcode=CommonFN.SafeRead(Row("Pcode"))
		Contact=CommonFN.SafeRead(Row("Contact"))
		coregnum=CommonFN.SafeRead(Row("coregnum"))
		prodDecisionPoint=CommonFN.SafeRead(Row("prodDecisionPoint"))
		prodElecneg=CommonFN.SafeRead(Row("prodElecneg"))
		prodgasneg=CommonFN.SafeRead(Row("prodgasneg"))
		prodccl=CommonFN.SafeRead(Row("prodccl"))
		prodenergyaction=CommonFN.SafeRead(Row("prodenergyaction"))
		prodbillaudit=CommonFN.SafeRead(Row("prodbillaudit"))
		proddataserv=CommonFN.SafeRead(Row("proddataserv"))
		ProdWaterAudit=CommonFN.SafeRead(Row("ProdWaterAudit"))
		ProdWaterMgmt=CommonFN.SafeRead(Row("ProdWaterMgmt"))
		ProdCRCAttain=CommonFN.SafeRead(Row("ProdCRCAttain"))
		ProdCRCMaintain=CommonFN.SafeRead(Row("ProdCRCMaintain"))
		custorprospect=CommonFN.SafeRead(Row("custorprospect"))
		CCLexemption=CommonFN.SafeRead(Row("CCLexemption"))
		CCLexemptionfromdate=CommonFN.SafeRead(Row("CCLexemptionfromdate"))
		CCLcertapplied_activesupply=CommonFN.SafeRead(Row("CCLcertapplied_activesupply"))
		CCLcertapplied_pendingsupply=CommonFN.SafeRead(Row("CCLcertapplied_pendingsupply"))
		prodTelco=CommonFN.SafeRead(Row("prodTelco"))
		prodMT=CommonFN.SafeRead(Row("prodMT"))
		prodTRIAD=CommonFN.SafeRead(Row("prodTRIAD"))
		pricesensitivity=CommonFN.SafeRead(Row("pricesensitivity"))
		umlloyaltyvalue=CommonFN.SafeRead(Row("umlloyaltyvalue"))
		VATnum=CommonFN.SafeRead(Row("VATnum"))
		FKaccmgrid=CommonFN.SafeRead(Row("FKaccmgrid"))
		ProdBillCheck=CommonFN.SafeRead(Row("ProdBillCheck"))
		dtmCreated=CommonFN.SafeRead(Row("dtmCreated"))
		dtmModified=CommonFN.SafeRead(Row("dtmModified"))
		intUserCreatedBy=CommonFN.SafeRead(Row("intUserCreatedBy"))
		intUserModifiedBy=CommonFN.SafeRead(Row("intUserModifiedBy"))
		intOptimaCustomerPK=CommonFN.SafeRead(Row("intOptimaCustomerPK"))
		intDataServicesGroupSend=CommonFN.SafeRead(Row("intDataServicesGroupSend"))
		bitDisabled=CommonFN.SafeRead(Row("bitDisabled"))
		varCustomerNote=CommonFN.SafeRead(Row("varCustomerNote"))
		intMandCRef=CommonFN.SafeRead(Row("intMandCRef"))
		intEnCoreRef=CommonFN.SafeRead(Row("intEnCoreRef"))
		varAXRef=CommonFN.SafeRead(Row("varAXRef"))
		intEnterpriseManagerFK=CommonFN.SafeRead(Row("intEnterpriseManagerFK"))
		country_fk=CommonFN.SafeRead(Row("country_fk"))
		intSubjectExpertFK=CommonFN.SafeRead(Row("intSubjectExpertFK"))
		intSummitRef=CommonFN.SafeRead(Row("intSummitRef"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_organisations_exists"
	PO = New SqlClient.SqlParameter("@county_fk", county_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@country_fk", country_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_organisations_compare"
	PO = New SqlClient.SqlParameter("@organisation_pk", organisation_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@CCLexemptionfromdate", CommonFN.CheckEmptyDate(CCLexemptionfromdate.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@dtmCreated", CommonFN.CheckEmptyDate(dtmCreated.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@dtmModified", CommonFN.CheckEmptyDate(dtmModified.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@varCustomerNote", varCustomerNote)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@varAXRef", varAXRef)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@created", CommonFN.CheckEmptyDate(created.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@modified", CommonFN.CheckEmptyDate(modified.ToString()))
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and organisation_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_organisations_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@organisation_pk", organisation_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Custid", Custid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@customername", customername)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@address_1", address_1)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@address_2", address_2)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@address_3", address_3)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Address_4", Address_4)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@county_fk", county_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Pcode", Pcode)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Contact", Contact)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@coregnum", coregnum)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodDecisionPoint", prodDecisionPoint)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodElecneg", prodElecneg)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodgasneg", prodgasneg)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodccl", prodccl)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodenergyaction", prodenergyaction)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodbillaudit", prodbillaudit)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@proddataserv", proddataserv)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ProdWaterAudit", ProdWaterAudit)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ProdWaterMgmt", ProdWaterMgmt)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ProdCRCAttain", ProdCRCAttain)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ProdCRCMaintain", ProdCRCMaintain)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@custorprospect", custorprospect)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@CCLexemption", CCLexemption)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@CCLexemptionfromdate", CommonFN.CheckEmptyDate(CCLexemptionfromdate.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@CCLcertapplied_activesupply", CCLcertapplied_activesupply)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@CCLcertapplied_pendingsupply", CCLcertapplied_pendingsupply)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodTelco", prodTelco)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodMT", prodMT)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodTRIAD", prodTRIAD)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@pricesensitivity", pricesensitivity)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@umlloyaltyvalue", umlloyaltyvalue)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@VATnum", VATnum)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@FKaccmgrid", FKaccmgrid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ProdBillCheck", ProdBillCheck)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@dtmCreated", CommonFN.CheckEmptyDate(dtmCreated.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@dtmModified", CommonFN.CheckEmptyDate(dtmModified.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intUserCreatedBy", intUserCreatedBy)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intUserModifiedBy", intUserModifiedBy)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intOptimaCustomerPK", intOptimaCustomerPK)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intDataServicesGroupSend", intDataServicesGroupSend)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@bitDisabled", bitDisabled)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@varCustomerNote", varCustomerNote)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intMandCRef", intMandCRef)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intEnCoreRef", intEnCoreRef)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@varAXRef", varAXRef)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intEnterpriseManagerFK", intEnterpriseManagerFK)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@country_fk", country_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intSubjectExpertFK", intSubjectExpertFK)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intSummitRef", intSummitRef)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@created", CommonFN.CheckEmptyDate(created.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@modified", CommonFN.CheckEmptyDate(modified.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _organisation_pk_d then CDC.AuditLog("organisations",_returnPK,"organisation_pk",_CurrentUser,modified,CStr(organisation_pk)):_organisation_pk_d=False
				If _Custid_d then CDC.AuditLog("organisations",_returnPK,"Custid",_CurrentUser,modified,CStr(Custid)):_Custid_d=False
				If _customername_d then CDC.AuditLog("organisations",_returnPK,"customername",_CurrentUser,modified,CStr(customername)):_customername_d=False
				If _address_1_d then CDC.AuditLog("organisations",_returnPK,"address_1",_CurrentUser,modified,CStr(address_1)):_address_1_d=False
				If _address_2_d then CDC.AuditLog("organisations",_returnPK,"address_2",_CurrentUser,modified,CStr(address_2)):_address_2_d=False
				If _address_3_d then CDC.AuditLog("organisations",_returnPK,"address_3",_CurrentUser,modified,CStr(address_3)):_address_3_d=False
				If _Address_4_d then CDC.AuditLog("organisations",_returnPK,"Address_4",_CurrentUser,modified,CStr(Address_4)):_Address_4_d=False
				If _county_fk_d then CDC.AuditLog("organisations",_returnPK,"county_fk",_CurrentUser,modified,CStr(county_fk)):_county_fk_d=False
				If _Pcode_d then CDC.AuditLog("organisations",_returnPK,"Pcode",_CurrentUser,modified,CStr(Pcode)):_Pcode_d=False
				If _Contact_d then CDC.AuditLog("organisations",_returnPK,"Contact",_CurrentUser,modified,CStr(Contact)):_Contact_d=False
				If _coregnum_d then CDC.AuditLog("organisations",_returnPK,"coregnum",_CurrentUser,modified,CStr(coregnum)):_coregnum_d=False
				If _prodDecisionPoint_d then CDC.AuditLog("organisations",_returnPK,"prodDecisionPoint",_CurrentUser,modified,CStr(prodDecisionPoint)):_prodDecisionPoint_d=False
				If _prodElecneg_d then CDC.AuditLog("organisations",_returnPK,"prodElecneg",_CurrentUser,modified,CStr(prodElecneg)):_prodElecneg_d=False
				If _prodgasneg_d then CDC.AuditLog("organisations",_returnPK,"prodgasneg",_CurrentUser,modified,CStr(prodgasneg)):_prodgasneg_d=False
				If _prodccl_d then CDC.AuditLog("organisations",_returnPK,"prodccl",_CurrentUser,modified,CStr(prodccl)):_prodccl_d=False
				If _prodenergyaction_d then CDC.AuditLog("organisations",_returnPK,"prodenergyaction",_CurrentUser,modified,CStr(prodenergyaction)):_prodenergyaction_d=False
				If _prodbillaudit_d then CDC.AuditLog("organisations",_returnPK,"prodbillaudit",_CurrentUser,modified,CStr(prodbillaudit)):_prodbillaudit_d=False
				If _proddataserv_d then CDC.AuditLog("organisations",_returnPK,"proddataserv",_CurrentUser,modified,CStr(proddataserv)):_proddataserv_d=False
				If _ProdWaterAudit_d then CDC.AuditLog("organisations",_returnPK,"ProdWaterAudit",_CurrentUser,modified,CStr(ProdWaterAudit)):_ProdWaterAudit_d=False
				If _ProdWaterMgmt_d then CDC.AuditLog("organisations",_returnPK,"ProdWaterMgmt",_CurrentUser,modified,CStr(ProdWaterMgmt)):_ProdWaterMgmt_d=False
				If _ProdCRCAttain_d then CDC.AuditLog("organisations",_returnPK,"ProdCRCAttain",_CurrentUser,modified,CStr(ProdCRCAttain)):_ProdCRCAttain_d=False
				If _ProdCRCMaintain_d then CDC.AuditLog("organisations",_returnPK,"ProdCRCMaintain",_CurrentUser,modified,CStr(ProdCRCMaintain)):_ProdCRCMaintain_d=False
				If _custorprospect_d then CDC.AuditLog("organisations",_returnPK,"custorprospect",_CurrentUser,modified,CStr(custorprospect)):_custorprospect_d=False
				If _CCLexemption_d then CDC.AuditLog("organisations",_returnPK,"CCLexemption",_CurrentUser,modified,CStr(CCLexemption)):_CCLexemption_d=False
				If _CCLexemptionfromdate_d then CDC.AuditLog("organisations",_returnPK,"CCLexemptionfromdate",_CurrentUser,modified,CStr(CCLexemptionfromdate)):_CCLexemptionfromdate_d=False
				If _CCLcertapplied_activesupply_d then CDC.AuditLog("organisations",_returnPK,"CCLcertapplied_activesupply",_CurrentUser,modified,CStr(CCLcertapplied_activesupply)):_CCLcertapplied_activesupply_d=False
				If _CCLcertapplied_pendingsupply_d then CDC.AuditLog("organisations",_returnPK,"CCLcertapplied_pendingsupply",_CurrentUser,modified,CStr(CCLcertapplied_pendingsupply)):_CCLcertapplied_pendingsupply_d=False
				If _prodTelco_d then CDC.AuditLog("organisations",_returnPK,"prodTelco",_CurrentUser,modified,CStr(prodTelco)):_prodTelco_d=False
				If _prodMT_d then CDC.AuditLog("organisations",_returnPK,"prodMT",_CurrentUser,modified,CStr(prodMT)):_prodMT_d=False
				If _prodTRIAD_d then CDC.AuditLog("organisations",_returnPK,"prodTRIAD",_CurrentUser,modified,CStr(prodTRIAD)):_prodTRIAD_d=False
				If _pricesensitivity_d then CDC.AuditLog("organisations",_returnPK,"pricesensitivity",_CurrentUser,modified,CStr(pricesensitivity)):_pricesensitivity_d=False
				If _umlloyaltyvalue_d then CDC.AuditLog("organisations",_returnPK,"umlloyaltyvalue",_CurrentUser,modified,CStr(umlloyaltyvalue)):_umlloyaltyvalue_d=False
				If _VATnum_d then CDC.AuditLog("organisations",_returnPK,"VATnum",_CurrentUser,modified,CStr(VATnum)):_VATnum_d=False
				If _FKaccmgrid_d then CDC.AuditLog("organisations",_returnPK,"FKaccmgrid",_CurrentUser,modified,CStr(FKaccmgrid)):_FKaccmgrid_d=False
				If _ProdBillCheck_d then CDC.AuditLog("organisations",_returnPK,"ProdBillCheck",_CurrentUser,modified,CStr(ProdBillCheck)):_ProdBillCheck_d=False
				If _dtmCreated_d then CDC.AuditLog("organisations",_returnPK,"dtmCreated",_CurrentUser,modified,CStr(dtmCreated)):_dtmCreated_d=False
				If _dtmModified_d then CDC.AuditLog("organisations",_returnPK,"dtmModified",_CurrentUser,modified,CStr(dtmModified)):_dtmModified_d=False
				If _intUserCreatedBy_d then CDC.AuditLog("organisations",_returnPK,"intUserCreatedBy",_CurrentUser,modified,CStr(intUserCreatedBy)):_intUserCreatedBy_d=False
				If _intUserModifiedBy_d then CDC.AuditLog("organisations",_returnPK,"intUserModifiedBy",_CurrentUser,modified,CStr(intUserModifiedBy)):_intUserModifiedBy_d=False
				If _intOptimaCustomerPK_d then CDC.AuditLog("organisations",_returnPK,"intOptimaCustomerPK",_CurrentUser,modified,CStr(intOptimaCustomerPK)):_intOptimaCustomerPK_d=False
				If _intDataServicesGroupSend_d then CDC.AuditLog("organisations",_returnPK,"intDataServicesGroupSend",_CurrentUser,modified,CStr(intDataServicesGroupSend)):_intDataServicesGroupSend_d=False
				If _bitDisabled_d then CDC.AuditLog("organisations",_returnPK,"bitDisabled",_CurrentUser,modified,CStr(bitDisabled)):_bitDisabled_d=False
				If _varCustomerNote_d then CDC.AuditLog("organisations",_returnPK,"varCustomerNote",_CurrentUser,modified,CStr(varCustomerNote)):_varCustomerNote_d=False
				If _intMandCRef_d then CDC.AuditLog("organisations",_returnPK,"intMandCRef",_CurrentUser,modified,CStr(intMandCRef)):_intMandCRef_d=False
				If _intEnCoreRef_d then CDC.AuditLog("organisations",_returnPK,"intEnCoreRef",_CurrentUser,modified,CStr(intEnCoreRef)):_intEnCoreRef_d=False
				If _varAXRef_d then CDC.AuditLog("organisations",_returnPK,"varAXRef",_CurrentUser,modified,CStr(varAXRef)):_varAXRef_d=False
				If _intEnterpriseManagerFK_d then CDC.AuditLog("organisations",_returnPK,"intEnterpriseManagerFK",_CurrentUser,modified,CStr(intEnterpriseManagerFK)):_intEnterpriseManagerFK_d=False
				If _country_fk_d then CDC.AuditLog("organisations",_returnPK,"country_fk",_CurrentUser,modified,CStr(country_fk)):_country_fk_d=False
				If _intSubjectExpertFK_d then CDC.AuditLog("organisations",_returnPK,"intSubjectExpertFK",_CurrentUser,modified,CStr(intSubjectExpertFK)):_intSubjectExpertFK_d=False
				If _intSummitRef_d then CDC.AuditLog("organisations",_returnPK,"intSummitRef",_CurrentUser,modified,CStr(intSummitRef)):_intSummitRef_d=False
				If _creator_fk_d then CDC.AuditLog("organisations",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("organisations",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("organisations",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("organisations",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("organisations",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
