Imports cca.common
Public Class survey2details
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _surveydetail_pk as Integer

Dim _surveydetail_pk_d as Boolean=False
Public Event surveydetail_pkChanged()
Public ReadOnly Property surveydetail_pk() as Integer
	Get
		Return _surveydetail_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _helpdesk_id as Integer

Dim _helpdesk_id_d as Boolean=False
Public Event helpdesk_idChanged()
Public Property helpdesk_id() as Integer
	Get
		Return _helpdesk_id
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _helpdesk_id <> value Then _d=True
		If not _ReadingData=True Then
		If _helpdesk_id <> value then RaiseEvent helpdesk_idChanged
		End If
		_helpdesk_id = value
		If _ReadingData=True Then _d=False
		_helpdesk_id_d=_d
	End Set
End Property

Dim _selecteduser_fk as Integer

Dim _selecteduser_fk_d as Boolean=False
Public Event selecteduser_fkChanged()
Public Property selecteduser_fk() as Integer
	Get
		Return _selecteduser_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _selecteduser_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("selecteduser_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("User"), CStr("Validation failed: Invalid Foreign Key"))
		If _selecteduser_fk <> value then RaiseEvent selecteduser_fkChanged
		End If
		_selecteduser_fk = value
		If _ReadingData=True Then _d=False
		_selecteduser_fk_d=_d
	End Set
End Property

Dim _question1_surveyanswerfivescale_fk as Integer

Dim _question1_surveyanswerfivescale_fk_d as Boolean=False
Public Event question1_surveyanswerfivescale_fkChanged()
Public Property question1_surveyanswerfivescale_fk() as Integer
	Get
		Return _question1_surveyanswerfivescale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question1_surveyanswerfivescale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question1_surveyanswerfivescale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Had a good knowledge of their customer"), CStr("Validation failed: Invalid Foreign Key"))
		If _question1_surveyanswerfivescale_fk <> value then RaiseEvent question1_surveyanswerfivescale_fkChanged
		End If
		_question1_surveyanswerfivescale_fk = value
		If _ReadingData=True Then _d=False
		_question1_surveyanswerfivescale_fk_d=_d
	End Set
End Property

Dim _question2_surveyanswerfivescale_fk as Integer

Dim _question2_surveyanswerfivescale_fk_d as Boolean=False
Public Event question2_surveyanswerfivescale_fkChanged()
Public Property question2_surveyanswerfivescale_fk() as Integer
	Get
		Return _question2_surveyanswerfivescale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question2_surveyanswerfivescale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question2_surveyanswerfivescale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Organised his/her activities and workload well"), CStr("Validation failed: Invalid Foreign Key"))
		If _question2_surveyanswerfivescale_fk <> value then RaiseEvent question2_surveyanswerfivescale_fkChanged
		End If
		_question2_surveyanswerfivescale_fk = value
		If _ReadingData=True Then _d=False
		_question2_surveyanswerfivescale_fk_d=_d
	End Set
End Property

Dim _question3_surveyanswerfivescale_fk as Integer

Dim _question3_surveyanswerfivescale_fk_d as Boolean=False
Public Event question3_surveyanswerfivescale_fkChanged()
Public Property question3_surveyanswerfivescale_fk() as Integer
	Get
		Return _question3_surveyanswerfivescale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question3_surveyanswerfivescale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question3_surveyanswerfivescale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Was proactive in responding to questions"), CStr("Validation failed: Invalid Foreign Key"))
		If _question3_surveyanswerfivescale_fk <> value then RaiseEvent question3_surveyanswerfivescale_fkChanged
		End If
		_question3_surveyanswerfivescale_fk = value
		If _ReadingData=True Then _d=False
		_question3_surveyanswerfivescale_fk_d=_d
	End Set
End Property

Dim _question4_surveyanswerfivescale_fk as Integer

Dim _question4_surveyanswerfivescale_fk_d as Boolean=False
Public Event question4_surveyanswerfivescale_fkChanged()
Public Property question4_surveyanswerfivescale_fk() as Integer
	Get
		Return _question4_surveyanswerfivescale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question4_surveyanswerfivescale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question4_surveyanswerfivescale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Pressed for appropriate closure from the customer"), CStr("Validation failed: Invalid Foreign Key"))
		If _question4_surveyanswerfivescale_fk <> value then RaiseEvent question4_surveyanswerfivescale_fkChanged
		End If
		_question4_surveyanswerfivescale_fk = value
		If _ReadingData=True Then _d=False
		_question4_surveyanswerfivescale_fk_d=_d
	End Set
End Property

Dim _question5_surveyanswerfivescale_fk as Integer

Dim _question5_surveyanswerfivescale_fk_d as Boolean=False
Public Event question5_surveyanswerfivescale_fkChanged()
Public Property question5_surveyanswerfivescale_fk() as Integer
	Get
		Return _question5_surveyanswerfivescale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question5_surveyanswerfivescale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question5_surveyanswerfivescale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Treated me with respect in their interactions with me"), CStr("Validation failed: Invalid Foreign Key"))
		If _question5_surveyanswerfivescale_fk <> value then RaiseEvent question5_surveyanswerfivescale_fkChanged
		End If
		_question5_surveyanswerfivescale_fk = value
		If _ReadingData=True Then _d=False
		_question5_surveyanswerfivescale_fk_d=_d
	End Set
End Property

Dim _comments as String

Dim _comments_d as Boolean=False
Public Event commentsChanged()
Public Property comments() as String
	Get
		Return _comments
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _comments <> value Then _d=True
		If not _ReadingData=True Then
		If _comments <> value then RaiseEvent commentsChanged
		End If
		_comments = value
		If _ReadingData=True Then _d=False
		_comments_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _surveydetail_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _helpdesk_id_d
		_IsDirty = _IsDirty OR _selecteduser_fk_d
		_IsDirty = _IsDirty OR _question1_surveyanswerfivescale_fk_d
		_IsDirty = _IsDirty OR _question2_surveyanswerfivescale_fk_d
		_IsDirty = _IsDirty OR _question3_surveyanswerfivescale_fk_d
		_IsDirty = _IsDirty OR _question4_surveyanswerfivescale_fk_d
		_IsDirty = _IsDirty OR _question5_surveyanswerfivescale_fk_d
		_IsDirty = _IsDirty OR _comments_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_surveydetail_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_selecteduser_fk=-1
	_question1_surveyanswerfivescale_fk=-1
	_question2_surveyanswerfivescale_fk=-1
	_question3_surveyanswerfivescale_fk=-1
	_question4_surveyanswerfivescale_fk=-1
	_question5_surveyanswerfivescale_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("survey2details",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_survey2details_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@surveydetail_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_surveydetail_pk=CommonFN.SafeRead(Row("surveydetail_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		helpdesk_id=CommonFN.SafeRead(Row("helpdesk_id"))
		selecteduser_fk=CommonFN.SafeRead(Row("selecteduser_fk"))
		question1_surveyanswerfivescale_fk=CommonFN.SafeRead(Row("question1_surveyanswerfivescale_fk"))
		question2_surveyanswerfivescale_fk=CommonFN.SafeRead(Row("question2_surveyanswerfivescale_fk"))
		question3_surveyanswerfivescale_fk=CommonFN.SafeRead(Row("question3_surveyanswerfivescale_fk"))
		question4_surveyanswerfivescale_fk=CommonFN.SafeRead(Row("question4_surveyanswerfivescale_fk"))
		question5_surveyanswerfivescale_fk=CommonFN.SafeRead(Row("question5_surveyanswerfivescale_fk"))
		comments=CommonFN.SafeRead(Row("comments"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_survey2details_exists"
	PO = New SqlClient.SqlParameter("@selecteduser_fk", selecteduser_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question1_surveyanswerfivescale_fk", question1_surveyanswerfivescale_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question2_surveyanswerfivescale_fk", question2_surveyanswerfivescale_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question3_surveyanswerfivescale_fk", question3_surveyanswerfivescale_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question4_surveyanswerfivescale_fk", question4_surveyanswerfivescale_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question5_surveyanswerfivescale_fk", question5_surveyanswerfivescale_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_survey2details_compare"
	PO = New SqlClient.SqlParameter("@surveydetail_pk", surveydetail_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@comments", comments)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and surveydetail_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_survey2details_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@surveydetail_pk", surveydetail_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@helpdesk_id", helpdesk_id)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@selecteduser_fk", selecteduser_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@question1_surveyanswerfivescale_fk", question1_surveyanswerfivescale_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@question2_surveyanswerfivescale_fk", question2_surveyanswerfivescale_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@question3_surveyanswerfivescale_fk", question3_surveyanswerfivescale_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@question4_surveyanswerfivescale_fk", question4_surveyanswerfivescale_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@question5_surveyanswerfivescale_fk", question5_surveyanswerfivescale_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@comments", comments)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _surveydetail_pk_d then CDC.AuditLog("survey2details",_returnPK,"surveydetail_pk",_CurrentUser,modified,CStr(surveydetail_pk)):_surveydetail_pk_d=False
				If _creator_fk_d then CDC.AuditLog("survey2details",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("survey2details",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("survey2details",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("survey2details",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("survey2details",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _helpdesk_id_d then CDC.AuditLog("survey2details",_returnPK,"helpdesk_id",_CurrentUser,modified,CStr(helpdesk_id)):_helpdesk_id_d=False
				If _selecteduser_fk_d then CDC.AuditLog("survey2details",_returnPK,"selecteduser_fk",_CurrentUser,modified,CStr(selecteduser_fk)):_selecteduser_fk_d=False
				If _question1_surveyanswerfivescale_fk_d then CDC.AuditLog("survey2details",_returnPK,"question1_surveyanswerfivescale_fk",_CurrentUser,modified,CStr(question1_surveyanswerfivescale_fk)):_question1_surveyanswerfivescale_fk_d=False
				If _question2_surveyanswerfivescale_fk_d then CDC.AuditLog("survey2details",_returnPK,"question2_surveyanswerfivescale_fk",_CurrentUser,modified,CStr(question2_surveyanswerfivescale_fk)):_question2_surveyanswerfivescale_fk_d=False
				If _question3_surveyanswerfivescale_fk_d then CDC.AuditLog("survey2details",_returnPK,"question3_surveyanswerfivescale_fk",_CurrentUser,modified,CStr(question3_surveyanswerfivescale_fk)):_question3_surveyanswerfivescale_fk_d=False
				If _question4_surveyanswerfivescale_fk_d then CDC.AuditLog("survey2details",_returnPK,"question4_surveyanswerfivescale_fk",_CurrentUser,modified,CStr(question4_surveyanswerfivescale_fk)):_question4_surveyanswerfivescale_fk_d=False
				If _question5_surveyanswerfivescale_fk_d then CDC.AuditLog("survey2details",_returnPK,"question5_surveyanswerfivescale_fk",_CurrentUser,modified,CStr(question5_surveyanswerfivescale_fk)):_question5_surveyanswerfivescale_fk_d=False
				If _comments_d then CDC.AuditLog("survey2details",_returnPK,"comments",_CurrentUser,modified,CStr(comments)):_comments_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
