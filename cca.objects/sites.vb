Imports cca.common
Public Class sites
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _site_pk as Integer

Dim _site_pk_d as Boolean=False
Public Event site_pkChanged()
Public ReadOnly Property site_pk() as Integer
	Get
		Return _site_pk
	End Get
End Property

Dim _organisation_fk as Integer

Dim _organisation_fk_d as Boolean=False
Public Event organisation_fkChanged()
Public Property organisation_fk() as Integer
	Get
		Return _organisation_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _organisation_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("organisation_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("organisation_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _organisation_fk <> value then RaiseEvent organisation_fkChanged
		End If
		_organisation_fk = value
		If _ReadingData=True Then _d=False
		_organisation_fk_d=_d
	End Set
End Property

Dim _sitename as String

Dim _sitename_d as Boolean=False
Public Event sitenameChanged()
Public Property sitename() as String
	Get
		Return _sitename
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _sitename <> value Then _d=True
		If not _ReadingData=True Then
		If _sitename <> value then RaiseEvent sitenameChanged
		End If
		_sitename = value
		If _ReadingData=True Then _d=False
		_sitename_d=_d
	End Set
End Property

Dim _deadoralive as Integer

Dim _deadoralive_d as Boolean=False
Public Event deadoraliveChanged()
Public Property deadoralive() as Integer
	Get
		Return _deadoralive
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _deadoralive <> value Then _d=True
		If not _ReadingData=True Then
		If _deadoralive <> value then RaiseEvent deadoraliveChanged
		End If
		_deadoralive = value
		If _ReadingData=True Then _d=False
		_deadoralive_d=_d
	End Set
End Property

Dim _ContactName as String

Dim _ContactName_d as Boolean=False
Public Event ContactNameChanged()
Public Property ContactName() as String
	Get
		Return _ContactName
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _ContactName <> value Then _d=True
		If not _ReadingData=True Then
		If _ContactName <> value then RaiseEvent ContactNameChanged
		End If
		_ContactName = value
		If _ReadingData=True Then _d=False
		_ContactName_d=_d
	End Set
End Property

Dim _Phone_1 as String

Dim _Phone_1_d as Boolean=False
Public Event Phone_1Changed()
Public Property Phone_1() as String
	Get
		Return _Phone_1
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Phone_1 <> value Then _d=True
		If not _ReadingData=True Then
		If _Phone_1 <> value then RaiseEvent Phone_1Changed
		End If
		_Phone_1 = value
		If _ReadingData=True Then _d=False
		_Phone_1_d=_d
	End Set
End Property

Dim _FaxNumber as String

Dim _FaxNumber_d as Boolean=False
Public Event FaxNumberChanged()
Public Property FaxNumber() as String
	Get
		Return _FaxNumber
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _FaxNumber <> value Then _d=True
		If not _ReadingData=True Then
		If _FaxNumber <> value then RaiseEvent FaxNumberChanged
		End If
		_FaxNumber = value
		If _ReadingData=True Then _d=False
		_FaxNumber_d=_d
	End Set
End Property

Dim _Accmgrid as Integer

Dim _Accmgrid_d as Boolean=False
Public Event AccmgridChanged()
Public Property Accmgrid() as Integer
	Get
		Return _Accmgrid
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _Accmgrid <> value Then _d=True
		If not _ReadingData=True Then
		If _Accmgrid <> value then RaiseEvent AccmgridChanged
		End If
		_Accmgrid = value
		If _ReadingData=True Then _d=False
		_Accmgrid_d=_d
	End Set
End Property

Dim _Address_1 as String

Dim _Address_1_d as Boolean=False
Public Event Address_1Changed()
Public Property Address_1() as String
	Get
		Return _Address_1
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Address_1 <> value Then _d=True
		If not _ReadingData=True Then
		If _Address_1 <> value then RaiseEvent Address_1Changed
		End If
		_Address_1 = value
		If _ReadingData=True Then _d=False
		_Address_1_d=_d
	End Set
End Property

Dim _Address_2 as String

Dim _Address_2_d as Boolean=False
Public Event Address_2Changed()
Public Property Address_2() as String
	Get
		Return _Address_2
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Address_2 <> value Then _d=True
		If not _ReadingData=True Then
		If _Address_2 <> value then RaiseEvent Address_2Changed
		End If
		_Address_2 = value
		If _ReadingData=True Then _d=False
		_Address_2_d=_d
	End Set
End Property

Dim _Address_3 as String

Dim _Address_3_d as Boolean=False
Public Event Address_3Changed()
Public Property Address_3() as String
	Get
		Return _Address_3
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Address_3 <> value Then _d=True
		If not _ReadingData=True Then
		If _Address_3 <> value then RaiseEvent Address_3Changed
		End If
		_Address_3 = value
		If _ReadingData=True Then _d=False
		_Address_3_d=_d
	End Set
End Property

Dim _Address_4 as String

Dim _Address_4_d as Boolean=False
Public Event Address_4Changed()
Public Property Address_4() as String
	Get
		Return _Address_4
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Address_4 <> value Then _d=True
		If not _ReadingData=True Then
		If _Address_4 <> value then RaiseEvent Address_4Changed
		End If
		_Address_4 = value
		If _ReadingData=True Then _d=False
		_Address_4_d=_d
	End Set
End Property

Dim _CountyID as Integer

Dim _CountyID_d as Boolean=False
Public Event CountyIDChanged()
Public Property CountyID() as Integer
	Get
		Return _CountyID
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _CountyID <> value Then _d=True
		If not _ReadingData=True Then
		If _CountyID <> value then RaiseEvent CountyIDChanged
		End If
		_CountyID = value
		If _ReadingData=True Then _d=False
		_CountyID_d=_d
	End Set
End Property

Dim _Pcode as String

Dim _Pcode_d as Boolean=False
Public Event PcodeChanged()
Public Property Pcode() as String
	Get
		Return _Pcode
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Pcode <> value Then _d=True
		If not _ReadingData=True Then
		If _Pcode <> value then RaiseEvent PcodeChanged
		End If
		_Pcode = value
		If _ReadingData=True Then _d=False
		_Pcode_d=_d
	End Set
End Property

Dim _Email as String

Dim _Email_d as Boolean=False
Public Event EmailChanged()
Public Property Email() as String
	Get
		Return _Email
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Email <> value Then _d=True
		If not _ReadingData=True Then
		If _Email <> value then RaiseEvent EmailChanged
		End If
		_Email = value
		If _ReadingData=True Then _d=False
		_Email_d=_d
	End Set
End Property

Dim _StatusID as Integer

Dim _StatusID_d as Boolean=False
Public Event StatusIDChanged()
Public Property StatusID() as Integer
	Get
		Return _StatusID
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _StatusID <> value Then _d=True
		If not _ReadingData=True Then
		If _StatusID <> value then RaiseEvent StatusIDChanged
		End If
		_StatusID = value
		If _ReadingData=True Then _d=False
		_StatusID_d=_d
	End Set
End Property

Dim _Customersiteref as String

Dim _Customersiteref_d as Boolean=False
Public Event CustomersiterefChanged()
Public Property Customersiteref() as String
	Get
		Return _Customersiteref
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Customersiteref <> value Then _d=True
		If not _ReadingData=True Then
		If _Customersiteref <> value then RaiseEvent CustomersiterefChanged
		End If
		_Customersiteref = value
		If _ReadingData=True Then _d=False
		_Customersiteref_d=_d
	End Set
End Property

Dim _Tenderstdt as Datetime

Dim _Tenderstdt_d as Boolean=False
Public Event TenderstdtChanged()
Public Property Tenderstdt() as Datetime
	Get
		Return _Tenderstdt
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _Tenderstdt <> value Then _d=True
		If not _ReadingData=True Then
		If _Tenderstdt <> value then RaiseEvent TenderstdtChanged
		End If
		_Tenderstdt = value
		If _ReadingData=True Then _d=False
		_Tenderstdt_d=_d
	End Set
End Property

Dim _Tenderlastdt as Datetime

Dim _Tenderlastdt_d as Boolean=False
Public Event TenderlastdtChanged()
Public Property Tenderlastdt() as Datetime
	Get
		Return _Tenderlastdt
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _Tenderlastdt <> value Then _d=True
		If not _ReadingData=True Then
		If _Tenderlastdt <> value then RaiseEvent TenderlastdtChanged
		End If
		_Tenderlastdt = value
		If _ReadingData=True Then _d=False
		_Tenderlastdt_d=_d
	End Set
End Property

Dim _tenderprtdate as Datetime

Dim _tenderprtdate_d as Boolean=False
Public Event tenderprtdateChanged()
Public Property tenderprtdate() as Datetime
	Get
		Return _tenderprtdate
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _tenderprtdate <> value Then _d=True
		If not _ReadingData=True Then
		If _tenderprtdate <> value then RaiseEvent tenderprtdateChanged
		End If
		_tenderprtdate = value
		If _ReadingData=True Then _d=False
		_tenderprtdate_d=_d
	End Set
End Property

Dim _tenderclsddate as Datetime

Dim _tenderclsddate_d as Boolean=False
Public Event tenderclsddateChanged()
Public Property tenderclsddate() as Datetime
	Get
		Return _tenderclsddate
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _tenderclsddate <> value Then _d=True
		If not _ReadingData=True Then
		If _tenderclsddate <> value then RaiseEvent tenderclsddateChanged
		End If
		_tenderclsddate = value
		If _ReadingData=True Then _d=False
		_tenderclsddate_d=_d
	End Set
End Property

Dim _Notes as String

Dim _Notes_d as Boolean=False
Public Event NotesChanged()
Public Property Notes() as String
	Get
		Return _Notes
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _Notes <> value Then _d=True
		If not _ReadingData=True Then
		If _Notes <> value then RaiseEvent NotesChanged
		End If
		_Notes = value
		If _ReadingData=True Then _d=False
		_Notes_d=_d
	End Set
End Property

Dim _Sameasbillingadress as Integer

Dim _Sameasbillingadress_d as Boolean=False
Public Event SameasbillingadressChanged()
Public Property Sameasbillingadress() as Integer
	Get
		Return _Sameasbillingadress
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _Sameasbillingadress <> value Then _d=True
		If not _ReadingData=True Then
		If _Sameasbillingadress <> value then RaiseEvent SameasbillingadressChanged
		End If
		_Sameasbillingadress = value
		If _ReadingData=True Then _d=False
		_Sameasbillingadress_d=_d
	End Set
End Property

Dim _tendbenchmark as String

Dim _tendbenchmark_d as Boolean=False
Public Event tendbenchmarkChanged()
Public Property tendbenchmark() as String
	Get
		Return _tendbenchmark
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _tendbenchmark <> value Then _d=True
		If not _ReadingData=True Then
		If _tendbenchmark <> value then RaiseEvent tendbenchmarkChanged
		End If
		_tendbenchmark = value
		If _ReadingData=True Then _d=False
		_tendbenchmark_d=_d
	End Set
End Property

Dim _typeofop as String

Dim _typeofop_d as Boolean=False
Public Event typeofopChanged()
Public Property typeofop() as String
	Get
		Return _typeofop
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _typeofop <> value Then _d=True
		If not _ReadingData=True Then
		If _typeofop <> value then RaiseEvent typeofopChanged
		End If
		_typeofop = value
		If _ReadingData=True Then _d=False
		_typeofop_d=_d
	End Set
End Property

Dim _voltageID as Integer

Dim _voltageID_d as Boolean=False
Public Event voltageIDChanged()
Public Property voltageID() as Integer
	Get
		Return _voltageID
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _voltageID <> value Then _d=True
		If not _ReadingData=True Then
		If _voltageID <> value then RaiseEvent voltageIDChanged
		End If
		_voltageID = value
		If _ReadingData=True Then _d=False
		_voltageID_d=_d
	End Set
End Property

Dim _MaxDemand as Integer

Dim _MaxDemand_d as Boolean=False
Public Event MaxDemandChanged()
Public Property MaxDemand() as Integer
	Get
		Return _MaxDemand
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _MaxDemand <> value Then _d=True
		If not _ReadingData=True Then
		If _MaxDemand <> value then RaiseEvent MaxDemandChanged
		End If
		_MaxDemand = value
		If _ReadingData=True Then _d=False
		_MaxDemand_d=_d
	End Set
End Property

Dim _prodelecneg as Integer

Dim _prodelecneg_d as Boolean=False
Public Event prodelecnegChanged()
Public Property prodelecneg() as Integer
	Get
		Return _prodelecneg
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodelecneg <> value Then _d=True
		If not _ReadingData=True Then
		If _prodelecneg <> value then RaiseEvent prodelecnegChanged
		End If
		_prodelecneg = value
		If _ReadingData=True Then _d=False
		_prodelecneg_d=_d
	End Set
End Property

Dim _prodgasneg as Integer

Dim _prodgasneg_d as Boolean=False
Public Event prodgasnegChanged()
Public Property prodgasneg() as Integer
	Get
		Return _prodgasneg
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodgasneg <> value Then _d=True
		If not _ReadingData=True Then
		If _prodgasneg <> value then RaiseEvent prodgasnegChanged
		End If
		_prodgasneg = value
		If _ReadingData=True Then _d=False
		_prodgasneg_d=_d
	End Set
End Property

Dim _produtilisys as Integer

Dim _produtilisys_d as Boolean=False
Public Event produtilisysChanged()
Public Property produtilisys() as Integer
	Get
		Return _produtilisys
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _produtilisys <> value Then _d=True
		If not _ReadingData=True Then
		If _produtilisys <> value then RaiseEvent produtilisysChanged
		End If
		_produtilisys = value
		If _ReadingData=True Then _d=False
		_produtilisys_d=_d
	End Set
End Property

Dim _prodCRC as Integer

Dim _prodCRC_d as Boolean=False
Public Event prodCRCChanged()
Public Property prodCRC() as Integer
	Get
		Return _prodCRC
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodCRC <> value Then _d=True
		If not _ReadingData=True Then
		If _prodCRC <> value then RaiseEvent prodCRCChanged
		End If
		_prodCRC = value
		If _ReadingData=True Then _d=False
		_prodCRC_d=_d
	End Set
End Property

Dim _prodWater as Integer

Dim _prodWater_d as Boolean=False
Public Event prodWaterChanged()
Public Property prodWater() as Integer
	Get
		Return _prodWater
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _prodWater <> value Then _d=True
		If not _ReadingData=True Then
		If _prodWater <> value then RaiseEvent prodWaterChanged
		End If
		_prodWater = value
		If _ReadingData=True Then _d=False
		_prodWater_d=_d
	End Set
End Property

Dim _federationid as Integer

Dim _federationid_d as Boolean=False
Public Event federationidChanged()
Public Property federationid() as Integer
	Get
		Return _federationid
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _federationid <> value Then _d=True
		If not _ReadingData=True Then
		If _federationid <> value then RaiseEvent federationidChanged
		End If
		_federationid = value
		If _ReadingData=True Then _d=False
		_federationid_d=_d
	End Set
End Property

Dim _fuelid as Integer

Dim _fuelid_d as Boolean=False
Public Event fuelidChanged()
Public Property fuelid() as Integer
	Get
		Return _fuelid
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _fuelid <> value Then _d=True
		If not _ReadingData=True Then
		If _fuelid <> value then RaiseEvent fuelidChanged
		End If
		_fuelid = value
		If _ReadingData=True Then _d=False
		_fuelid_d=_d
	End Set
End Property

Dim _CSC as Integer

Dim _CSC_d as Boolean=False
Public Event CSCChanged()
Public Property CSC() as Integer
	Get
		Return _CSC
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _CSC <> value Then _d=True
		If not _ReadingData=True Then
		If _CSC <> value then RaiseEvent CSCChanged
		End If
		_CSC = value
		If _ReadingData=True Then _d=False
		_CSC_d=_d
	End Set
End Property

Dim _CSCdate as Datetime

Dim _CSCdate_d as Boolean=False
Public Event CSCdateChanged()
Public Property CSCdate() as Datetime
	Get
		Return _CSCdate
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _CSCdate <> value Then _d=True
		If not _ReadingData=True Then
		If _CSCdate <> value then RaiseEvent CSCdateChanged
		End If
		_CSCdate = value
		If _ReadingData=True Then _d=False
		_CSCdate_d=_d
	End Set
End Property

Dim _MDdate as Datetime

Dim _MDdate_d as Boolean=False
Public Event MDdateChanged()
Public Property MDdate() as Datetime
	Get
		Return _MDdate
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _MDdate <> value Then _d=True
		If not _ReadingData=True Then
		If _MDdate <> value then RaiseEvent MDdateChanged
		End If
		_MDdate = value
		If _ReadingData=True Then _d=False
		_MDdate_d=_d
	End Set
End Property

Dim _FKsitegroupid as Integer

Dim _FKsitegroupid_d as Boolean=False
Public Event FKsitegroupidChanged()
Public Property FKsitegroupid() as Integer
	Get
		Return _FKsitegroupid
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _FKsitegroupid <> value Then _d=True
		If not _ReadingData=True Then
		If _FKsitegroupid <> value then RaiseEvent FKsitegroupidChanged
		End If
		_FKsitegroupid = value
		If _ReadingData=True Then _d=False
		_FKsitegroupid_d=_d
	End Set
End Property

Dim _Dservices_optout as Integer

Dim _Dservices_optout_d as Boolean=False
Public Event Dservices_optoutChanged()
Public Property Dservices_optout() as Integer
	Get
		Return _Dservices_optout
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _Dservices_optout <> value Then _d=True
		If not _ReadingData=True Then
		If _Dservices_optout <> value then RaiseEvent Dservices_optoutChanged
		End If
		_Dservices_optout = value
		If _ReadingData=True Then _d=False
		_Dservices_optout_d=_d
	End Set
End Property

Dim _Bill_check_optout as Integer

Dim _Bill_check_optout_d as Boolean=False
Public Event Bill_check_optoutChanged()
Public Property Bill_check_optout() as Integer
	Get
		Return _Bill_check_optout
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _Bill_check_optout <> value Then _d=True
		If not _ReadingData=True Then
		If _Bill_check_optout <> value then RaiseEvent Bill_check_optoutChanged
		End If
		_Bill_check_optout = value
		If _ReadingData=True Then _d=False
		_Bill_check_optout_d=_d
	End Set
End Property

Dim _CCL_mgmt as Integer

Dim _CCL_mgmt_d as Boolean=False
Public Event CCL_mgmtChanged()
Public Property CCL_mgmt() as Integer
	Get
		Return _CCL_mgmt
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _CCL_mgmt <> value Then _d=True
		If not _ReadingData=True Then
		If _CCL_mgmt <> value then RaiseEvent CCL_mgmtChanged
		End If
		_CCL_mgmt = value
		If _ReadingData=True Then _d=False
		_CCL_mgmt_d=_d
	End Set
End Property

Dim _varMarker as String

Dim _varMarker_d as Boolean=False
Public Event varMarkerChanged()
Public Property varMarker() as String
	Get
		Return _varMarker
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _varMarker <> value Then _d=True
		If not _ReadingData=True Then
		If _varMarker <> value then RaiseEvent varMarkerChanged
		End If
		_varMarker = value
		If _ReadingData=True Then _d=False
		_varMarker_d=_d
	End Set
End Property

Dim _Lat as Double

Dim _Lat_d as Boolean=False
Public Event LatChanged()
Public Property Lat() as Double
	Get
		Return _Lat
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _Lat <> value Then _d=True
		If not _ReadingData=True Then
		If _Lat <> value then RaiseEvent LatChanged
		End If
		_Lat = value
		If _ReadingData=True Then _d=False
		_Lat_d=_d
	End Set
End Property

Dim _varSiteFriendlyName as String

Dim _varSiteFriendlyName_d as Boolean=False
Public Event varSiteFriendlyNameChanged()
Public Property varSiteFriendlyName() as String
	Get
		Return _varSiteFriendlyName
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _varSiteFriendlyName <> value Then _d=True
		If not _ReadingData=True Then
		If _varSiteFriendlyName <> value then RaiseEvent varSiteFriendlyNameChanged
		End If
		_varSiteFriendlyName = value
		If _ReadingData=True Then _d=False
		_varSiteFriendlyName_d=_d
	End Set
End Property

Dim _intCountryFK as Integer

Dim _intCountryFK_d as Boolean=False
Public Event intCountryFKChanged()
Public Property intCountryFK() as Integer
	Get
		Return _intCountryFK
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _intCountryFK <> value Then _d=True
		If not _ReadingData=True Then
		If _intCountryFK <> value then RaiseEvent intCountryFKChanged
		End If
		_intCountryFK = value
		If _ReadingData=True Then _d=False
		_intCountryFK_d=_d
	End Set
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer

Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _site_pk_d
		_IsDirty = _IsDirty OR _organisation_fk_d
		_IsDirty = _IsDirty OR _sitename_d
		_IsDirty = _IsDirty OR _deadoralive_d
		_IsDirty = _IsDirty OR _ContactName_d
		_IsDirty = _IsDirty OR _Phone_1_d
		_IsDirty = _IsDirty OR _FaxNumber_d
		_IsDirty = _IsDirty OR _Accmgrid_d
		_IsDirty = _IsDirty OR _Address_1_d
		_IsDirty = _IsDirty OR _Address_2_d
		_IsDirty = _IsDirty OR _Address_3_d
		_IsDirty = _IsDirty OR _Address_4_d
		_IsDirty = _IsDirty OR _CountyID_d
		_IsDirty = _IsDirty OR _Pcode_d
		_IsDirty = _IsDirty OR _Email_d
		_IsDirty = _IsDirty OR _StatusID_d
		_IsDirty = _IsDirty OR _Customersiteref_d
		_IsDirty = _IsDirty OR _Tenderstdt_d
		_IsDirty = _IsDirty OR _Tenderlastdt_d
		_IsDirty = _IsDirty OR _tenderprtdate_d
		_IsDirty = _IsDirty OR _tenderclsddate_d
            _IsDirty = _IsDirty Or _Notes_d
		_IsDirty = _IsDirty OR _Sameasbillingadress_d
		_IsDirty = _IsDirty OR _tendbenchmark_d
		_IsDirty = _IsDirty OR _typeofop_d
		_IsDirty = _IsDirty OR _voltageID_d
		_IsDirty = _IsDirty OR _MaxDemand_d
		_IsDirty = _IsDirty OR _prodelecneg_d
		_IsDirty = _IsDirty OR _prodgasneg_d
		_IsDirty = _IsDirty OR _produtilisys_d
		_IsDirty = _IsDirty OR _prodCRC_d
		_IsDirty = _IsDirty OR _prodWater_d
		_IsDirty = _IsDirty OR _federationid_d
		_IsDirty = _IsDirty OR _fuelid_d
		_IsDirty = _IsDirty OR _CSC_d
		_IsDirty = _IsDirty OR _CSCdate_d
		_IsDirty = _IsDirty OR _MDdate_d
		_IsDirty = _IsDirty OR _FKsitegroupid_d
		_IsDirty = _IsDirty OR _Dservices_optout_d
		_IsDirty = _IsDirty OR _Bill_check_optout_d
		_IsDirty = _IsDirty OR _CCL_mgmt_d
		_IsDirty = _IsDirty OR _varMarker_d
		_IsDirty = _IsDirty OR _Lat_d
            _IsDirty = _IsDirty Or _varSiteFriendlyName_d
		_IsDirty = _IsDirty OR _intCountryFK_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_site_pk=-1
	_organisation_fk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("sites",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_sites_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@site_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_site_pk=CommonFN.SafeRead(Row("site_pk"))
		organisation_fk=CommonFN.SafeRead(Row("organisation_fk"))
		sitename=CommonFN.SafeRead(Row("sitename"))
		deadoralive=CommonFN.SafeRead(Row("deadoralive"))
		ContactName=CommonFN.SafeRead(Row("ContactName"))
		Phone_1=CommonFN.SafeRead(Row("Phone_1"))
		FaxNumber=CommonFN.SafeRead(Row("FaxNumber"))
		Accmgrid=CommonFN.SafeRead(Row("Accmgrid"))
		Address_1=CommonFN.SafeRead(Row("Address_1"))
		Address_2=CommonFN.SafeRead(Row("Address_2"))
		Address_3=CommonFN.SafeRead(Row("Address_3"))
		Address_4=CommonFN.SafeRead(Row("Address_4"))
		CountyID=CommonFN.SafeRead(Row("CountyID"))
		Pcode=CommonFN.SafeRead(Row("Pcode"))
		Email=CommonFN.SafeRead(Row("Email"))
		StatusID=CommonFN.SafeRead(Row("StatusID"))
		Customersiteref=CommonFN.SafeRead(Row("Customersiteref"))
		Tenderstdt=CommonFN.SafeRead(Row("Tenderstdt"))
		Tenderlastdt=CommonFN.SafeRead(Row("Tenderlastdt"))
		tenderprtdate=CommonFN.SafeRead(Row("tenderprtdate"))
		tenderclsddate=CommonFN.SafeRead(Row("tenderclsddate"))
            Notes = CommonFN.SafeRead(Row("Notes"))
		Sameasbillingadress=CommonFN.SafeRead(Row("Sameasbillingadress"))
		tendbenchmark=CommonFN.SafeRead(Row("tendbenchmark"))
		typeofop=CommonFN.SafeRead(Row("typeofop"))
		voltageID=CommonFN.SafeRead(Row("voltageID"))
		MaxDemand=CommonFN.SafeRead(Row("MaxDemand"))
		prodelecneg=CommonFN.SafeRead(Row("prodelecneg"))
		prodgasneg=CommonFN.SafeRead(Row("prodgasneg"))
		produtilisys=CommonFN.SafeRead(Row("produtilisys"))
		prodCRC=CommonFN.SafeRead(Row("prodCRC"))
		prodWater=CommonFN.SafeRead(Row("prodWater"))
		federationid=CommonFN.SafeRead(Row("federationid"))
		fuelid=CommonFN.SafeRead(Row("fuelid"))
		CSC=CommonFN.SafeRead(Row("CSC"))
		CSCdate=CommonFN.SafeRead(Row("CSCdate"))
		MDdate=CommonFN.SafeRead(Row("MDdate"))
		FKsitegroupid=CommonFN.SafeRead(Row("FKsitegroupid"))
		Dservices_optout=CommonFN.SafeRead(Row("Dservices_optout"))
		Bill_check_optout=CommonFN.SafeRead(Row("Bill_check_optout"))
		CCL_mgmt=CommonFN.SafeRead(Row("CCL_mgmt"))
		varMarker=CommonFN.SafeRead(Row("varMarker"))
            varSiteFriendlyName = CommonFN.SafeRead(Row("varSiteFriendlyName"))
		intCountryFK=CommonFN.SafeRead(Row("intCountryFK"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_sites_exists"
	PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_sites_compare"
	PO = New SqlClient.SqlParameter("@site_pk", site_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@Tenderstdt", CommonFN.CheckEmptyDate(Tenderstdt.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@Tenderlastdt", CommonFN.CheckEmptyDate(Tenderlastdt.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@tenderprtdate", CommonFN.CheckEmptyDate(tenderprtdate.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@tenderclsddate", CommonFN.CheckEmptyDate(tenderclsddate.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@CSCdate", CommonFN.CheckEmptyDate(CSCdate.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@MDdate", CommonFN.CheckEmptyDate(MDdate.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@varMarker", varMarker)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@varSiteFriendlyName", varSiteFriendlyName)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@created", CommonFN.CheckEmptyDate(created.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@modified", CommonFN.CheckEmptyDate(modified.ToString()))
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and site_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_sites_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@site_pk", site_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@organisation_fk", organisation_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@sitename", sitename)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@deadoralive", deadoralive)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ContactName", ContactName)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Phone_1", Phone_1)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@FaxNumber", FaxNumber)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Accmgrid", Accmgrid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Address_1", Address_1)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Address_2", Address_2)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Address_3", Address_3)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Address_4", Address_4)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@CountyID", CountyID)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Pcode", Pcode)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Email", Email)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@StatusID", StatusID)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Customersiteref", Customersiteref)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Tenderstdt", CommonFN.CheckEmptyDate(Tenderstdt.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Tenderlastdt", CommonFN.CheckEmptyDate(Tenderlastdt.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@tenderprtdate", CommonFN.CheckEmptyDate(tenderprtdate.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@tenderclsddate", CommonFN.CheckEmptyDate(tenderclsddate.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Notes", Notes)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Sameasbillingadress", Sameasbillingadress)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@tendbenchmark", tendbenchmark)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@typeofop", typeofop)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@voltageID", voltageID)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@MaxDemand", MaxDemand)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodelecneg", prodelecneg)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodgasneg", prodgasneg)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@produtilisys", produtilisys)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodCRC", prodCRC)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@prodWater", prodWater)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@federationid", federationid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@fuelid", fuelid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@CSC", CSC)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@CSCdate", CommonFN.CheckEmptyDate(CSCdate.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@MDdate", CommonFN.CheckEmptyDate(MDdate.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@FKsitegroupid", FKsitegroupid)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Dservices_optout", Dservices_optout)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Bill_check_optout", Bill_check_optout)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@CCL_mgmt", CCL_mgmt)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@varMarker", varMarker)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@Lat", Lat)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@varSiteFriendlyName", varSiteFriendlyName)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intCountryFK", intCountryFK)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@created", CommonFN.CheckEmptyDate(created.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@modified", CommonFN.CheckEmptyDate(modified.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _site_pk_d then CDC.AuditLog("sites",_returnPK,"site_pk",_CurrentUser,modified,CStr(site_pk)):_site_pk_d=False
				If _organisation_fk_d then CDC.AuditLog("sites",_returnPK,"organisation_fk",_CurrentUser,modified,CStr(organisation_fk)):_organisation_fk_d=False
				If _sitename_d then CDC.AuditLog("sites",_returnPK,"sitename",_CurrentUser,modified,CStr(sitename)):_sitename_d=False
				If _deadoralive_d then CDC.AuditLog("sites",_returnPK,"deadoralive",_CurrentUser,modified,CStr(deadoralive)):_deadoralive_d=False
				If _ContactName_d then CDC.AuditLog("sites",_returnPK,"ContactName",_CurrentUser,modified,CStr(ContactName)):_ContactName_d=False
				If _Phone_1_d then CDC.AuditLog("sites",_returnPK,"Phone_1",_CurrentUser,modified,CStr(Phone_1)):_Phone_1_d=False
				If _FaxNumber_d then CDC.AuditLog("sites",_returnPK,"FaxNumber",_CurrentUser,modified,CStr(FaxNumber)):_FaxNumber_d=False
				If _Accmgrid_d then CDC.AuditLog("sites",_returnPK,"Accmgrid",_CurrentUser,modified,CStr(Accmgrid)):_Accmgrid_d=False
				If _Address_1_d then CDC.AuditLog("sites",_returnPK,"Address_1",_CurrentUser,modified,CStr(Address_1)):_Address_1_d=False
				If _Address_2_d then CDC.AuditLog("sites",_returnPK,"Address_2",_CurrentUser,modified,CStr(Address_2)):_Address_2_d=False
				If _Address_3_d then CDC.AuditLog("sites",_returnPK,"Address_3",_CurrentUser,modified,CStr(Address_3)):_Address_3_d=False
				If _Address_4_d then CDC.AuditLog("sites",_returnPK,"Address_4",_CurrentUser,modified,CStr(Address_4)):_Address_4_d=False
				If _CountyID_d then CDC.AuditLog("sites",_returnPK,"CountyID",_CurrentUser,modified,CStr(CountyID)):_CountyID_d=False
				If _Pcode_d then CDC.AuditLog("sites",_returnPK,"Pcode",_CurrentUser,modified,CStr(Pcode)):_Pcode_d=False
				If _Email_d then CDC.AuditLog("sites",_returnPK,"Email",_CurrentUser,modified,CStr(Email)):_Email_d=False
				If _StatusID_d then CDC.AuditLog("sites",_returnPK,"StatusID",_CurrentUser,modified,CStr(StatusID)):_StatusID_d=False
				If _Customersiteref_d then CDC.AuditLog("sites",_returnPK,"Customersiteref",_CurrentUser,modified,CStr(Customersiteref)):_Customersiteref_d=False
				If _Tenderstdt_d then CDC.AuditLog("sites",_returnPK,"Tenderstdt",_CurrentUser,modified,CStr(Tenderstdt)):_Tenderstdt_d=False
				If _Tenderlastdt_d then CDC.AuditLog("sites",_returnPK,"Tenderlastdt",_CurrentUser,modified,CStr(Tenderlastdt)):_Tenderlastdt_d=False
				If _tenderprtdate_d then CDC.AuditLog("sites",_returnPK,"tenderprtdate",_CurrentUser,modified,CStr(tenderprtdate)):_tenderprtdate_d=False
				If _tenderclsddate_d then CDC.AuditLog("sites",_returnPK,"tenderclsddate",_CurrentUser,modified,CStr(tenderclsddate)):_tenderclsddate_d=False
                        If _Notes_d Then CDC.AuditLog("sites", _returnPK, "Notes", _CurrentUser, modified, CStr(Notes)) : _Notes_d = False
				If _Sameasbillingadress_d then CDC.AuditLog("sites",_returnPK,"Sameasbillingadress",_CurrentUser,modified,CStr(Sameasbillingadress)):_Sameasbillingadress_d=False
				If _tendbenchmark_d then CDC.AuditLog("sites",_returnPK,"tendbenchmark",_CurrentUser,modified,CStr(tendbenchmark)):_tendbenchmark_d=False
				If _typeofop_d then CDC.AuditLog("sites",_returnPK,"typeofop",_CurrentUser,modified,CStr(typeofop)):_typeofop_d=False
				If _voltageID_d then CDC.AuditLog("sites",_returnPK,"voltageID",_CurrentUser,modified,CStr(voltageID)):_voltageID_d=False
				If _MaxDemand_d then CDC.AuditLog("sites",_returnPK,"MaxDemand",_CurrentUser,modified,CStr(MaxDemand)):_MaxDemand_d=False
				If _prodelecneg_d then CDC.AuditLog("sites",_returnPK,"prodelecneg",_CurrentUser,modified,CStr(prodelecneg)):_prodelecneg_d=False
				If _prodgasneg_d then CDC.AuditLog("sites",_returnPK,"prodgasneg",_CurrentUser,modified,CStr(prodgasneg)):_prodgasneg_d=False
				If _produtilisys_d then CDC.AuditLog("sites",_returnPK,"produtilisys",_CurrentUser,modified,CStr(produtilisys)):_produtilisys_d=False
				If _prodCRC_d then CDC.AuditLog("sites",_returnPK,"prodCRC",_CurrentUser,modified,CStr(prodCRC)):_prodCRC_d=False
				If _prodWater_d then CDC.AuditLog("sites",_returnPK,"prodWater",_CurrentUser,modified,CStr(prodWater)):_prodWater_d=False
				If _federationid_d then CDC.AuditLog("sites",_returnPK,"federationid",_CurrentUser,modified,CStr(federationid)):_federationid_d=False
				If _fuelid_d then CDC.AuditLog("sites",_returnPK,"fuelid",_CurrentUser,modified,CStr(fuelid)):_fuelid_d=False
				If _CSC_d then CDC.AuditLog("sites",_returnPK,"CSC",_CurrentUser,modified,CStr(CSC)):_CSC_d=False
				If _CSCdate_d then CDC.AuditLog("sites",_returnPK,"CSCdate",_CurrentUser,modified,CStr(CSCdate)):_CSCdate_d=False
				If _MDdate_d then CDC.AuditLog("sites",_returnPK,"MDdate",_CurrentUser,modified,CStr(MDdate)):_MDdate_d=False
				If _FKsitegroupid_d then CDC.AuditLog("sites",_returnPK,"FKsitegroupid",_CurrentUser,modified,CStr(FKsitegroupid)):_FKsitegroupid_d=False
				If _Dservices_optout_d then CDC.AuditLog("sites",_returnPK,"Dservices_optout",_CurrentUser,modified,CStr(Dservices_optout)):_Dservices_optout_d=False
				If _Bill_check_optout_d then CDC.AuditLog("sites",_returnPK,"Bill_check_optout",_CurrentUser,modified,CStr(Bill_check_optout)):_Bill_check_optout_d=False
				If _CCL_mgmt_d then CDC.AuditLog("sites",_returnPK,"CCL_mgmt",_CurrentUser,modified,CStr(CCL_mgmt)):_CCL_mgmt_d=False
				If _varMarker_d then CDC.AuditLog("sites",_returnPK,"varMarker",_CurrentUser,modified,CStr(varMarker)):_varMarker_d=False
				If _Lat_d then CDC.AuditLog("sites",_returnPK,"Lat",_CurrentUser,modified,CStr(Lat)):_Lat_d=False
                        If _varSiteFriendlyName_d Then CDC.AuditLog("sites", _returnPK, "varSiteFriendlyName", _CurrentUser, modified, CStr(varSiteFriendlyName)) : _varSiteFriendlyName_d = False
				If _intCountryFK_d then CDC.AuditLog("sites",_returnPK,"intCountryFK",_CurrentUser,modified,CStr(intCountryFK)):_intCountryFK_d=False
				If _creator_fk_d then CDC.AuditLog("sites",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("sites",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("sites",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("sites",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("sites",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
