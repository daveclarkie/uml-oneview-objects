Imports cca.common
Public Class matrix1details
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _matrixdetail_pk as Integer

Dim _matrixdetail_pk_d as Boolean=False
Public Event matrixdetail_pkChanged()
Public ReadOnly Property matrixdetail_pk() as Integer
	Get
		Return _matrixdetail_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _profile_fk as Integer

Dim _profile_fk_d as Boolean=False
Public Event profile_fkChanged()
Public Property profile_fk() as Integer
	Get
		Return _profile_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _profile_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("profile_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Profile Class"), CStr("Validation failed: Required Field"))
		If _profile_fk <> value then RaiseEvent profile_fkChanged
		End If
		_profile_fk = value
		If _ReadingData=True Then _d=False
		_profile_fk_d=_d
	End Set
End Property

Dim _metertimeswitchcode as String

Dim _metertimeswitchcode_d as Boolean=False
Public Event metertimeswitchcodeChanged()
Public Property metertimeswitchcode() as String
	Get
		Return _metertimeswitchcode
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _metertimeswitchcode <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("MTC"), CStr("Validation failed: Required Field"))
		If _metertimeswitchcode <> value then RaiseEvent metertimeswitchcodeChanged
		End If
		_metertimeswitchcode = value
		If _ReadingData=True Then _d=False
		_metertimeswitchcode_d=_d
	End Set
End Property

Dim _linelossfactor as String

Dim _linelossfactor_d as Boolean=False
Public Event linelossfactorChanged()
Public Property linelossfactor() as String
	Get
		Return _linelossfactor
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _linelossfactor <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("LLF"), CStr("Validation failed: Required Field"))
		If _linelossfactor <> value then RaiseEvent linelossfactorChanged
		End If
		_linelossfactor = value
		If _ReadingData=True Then _d=False
		_linelossfactor_d=_d
	End Set
End Property

Dim _distribution_fk as Integer

Dim _distribution_fk_d as Boolean=False
Public Event distribution_fkChanged()
Public Property distribution_fk() as Integer
	Get
		Return _distribution_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _distribution_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("distribution_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Distribution"), CStr("Validation failed: Required Field"))
		If _distribution_fk <> value then RaiseEvent distribution_fkChanged
		End If
		_distribution_fk = value
		If _ReadingData=True Then _d=False
		_distribution_fk_d=_d
	End Set
End Property

Dim _uniqueidentifier as String

Dim _uniqueidentifier_d as Boolean=False
Public Event uniqueidentifierChanged()
Public Property uniqueidentifier() as String
	Get
		Return _uniqueidentifier
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _uniqueidentifier <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Unique Identifier"), CStr("Validation failed: Required Field"))
		If _uniqueidentifier <> value then RaiseEvent uniqueidentifierChanged
		End If
		_uniqueidentifier = value
		If _ReadingData=True Then _d=False
		_uniqueidentifier_d=_d
	End Set
End Property

Dim _checkdigit as String

Dim _checkdigit_d as Boolean=False
Public Event checkdigitChanged()
Public Property checkdigit() as String
	Get
		Return _checkdigit
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _checkdigit <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Check Digit"), CStr("Validation failed: Required Field"))
		If _checkdigit <> value then RaiseEvent checkdigitChanged
		End If
		_checkdigit = value
		If _ReadingData=True Then _d=False
		_checkdigit_d=_d
	End Set
End Property

Dim _consumptionsingle as Integer

Dim _consumptionsingle_d as Boolean=False
Public Event consumptionsingleChanged()
Public Property consumptionsingle() as Integer
	Get
		Return _consumptionsingle
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _consumptionsingle <> value Then _d=True
		If not _ReadingData=True Then
		If _consumptionsingle <> value then RaiseEvent consumptionsingleChanged
		End If
		_consumptionsingle = value
		If _ReadingData=True Then _d=False
		_consumptionsingle_d=_d
	End Set
End Property

Dim _consumptionday as Integer

Dim _consumptionday_d as Boolean=False
Public Event consumptiondayChanged()
Public Property consumptionday() as Integer
	Get
		Return _consumptionday
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _consumptionday <> value Then _d=True
		If not _ReadingData=True Then
		If _consumptionday <> value then RaiseEvent consumptiondayChanged
		End If
		_consumptionday = value
		If _ReadingData=True Then _d=False
		_consumptionday_d=_d
	End Set
End Property

Dim _consumptionnight as Integer

Dim _consumptionnight_d as Boolean=False
Public Event consumptionnightChanged()
Public Property consumptionnight() as Integer
	Get
		Return _consumptionnight
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _consumptionnight <> value Then _d=True
		If not _ReadingData=True Then
		If _consumptionnight <> value then RaiseEvent consumptionnightChanged
		End If
		_consumptionnight = value
		If _ReadingData=True Then _d=False
		_consumptionnight_d=_d
	End Set
End Property

Dim _consumptionother as Integer

Dim _consumptionother_d as Boolean=False
Public Event consumptionotherChanged()
Public Property consumptionother() as Integer
	Get
		Return _consumptionother
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _consumptionother <> value Then _d=True
		If not _ReadingData=True Then
		If _consumptionother <> value then RaiseEvent consumptionotherChanged
		End If
		_consumptionother = value
		If _ReadingData=True Then _d=False
		_consumptionother_d=_d
	End Set
End Property

Dim _current_standingcharge2 as Double

Dim _current_standingcharge2_d as Boolean=False
Public Event current_standingcharge2Changed()
Public Property current_standingcharge2() as Double
	Get
		Return _current_standingcharge2
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _current_standingcharge2 <> value Then _d=True
		If not _ReadingData=True Then
		If _current_standingcharge2 <> value then RaiseEvent current_standingcharge2Changed
		End If
		_current_standingcharge2 = value
		If _ReadingData=True Then _d=False
		_current_standingcharge2_d=_d
	End Set
End Property

Dim _current_ratesingle as Double

Dim _current_ratesingle_d as Boolean=False
Public Event current_ratesingleChanged()
Public Property current_ratesingle() as Double
	Get
		Return _current_ratesingle
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _current_ratesingle <> value Then _d=True
		If not _ReadingData=True Then
		If _current_ratesingle <> value then RaiseEvent current_ratesingleChanged
		End If
		_current_ratesingle = value
		If _ReadingData=True Then _d=False
		_current_ratesingle_d=_d
	End Set
End Property

Dim _current_rateday as Double

Dim _current_rateday_d as Boolean=False
Public Event current_ratedayChanged()
Public Property current_rateday() as Double
	Get
		Return _current_rateday
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _current_rateday <> value Then _d=True
		If not _ReadingData=True Then
		If _current_rateday <> value then RaiseEvent current_ratedayChanged
		End If
		_current_rateday = value
		If _ReadingData=True Then _d=False
		_current_rateday_d=_d
	End Set
End Property

Dim _current_ratenight as Double

Dim _current_ratenight_d as Boolean=False
Public Event current_ratenightChanged()
Public Property current_ratenight() as Double
	Get
		Return _current_ratenight
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _current_ratenight <> value Then _d=True
		If not _ReadingData=True Then
		If _current_ratenight <> value then RaiseEvent current_ratenightChanged
		End If
		_current_ratenight = value
		If _ReadingData=True Then _d=False
		_current_ratenight_d=_d
	End Set
End Property

Dim _current_rateother as Double

Dim _current_rateother_d as Boolean=False
Public Event current_rateotherChanged()
Public Property current_rateother() as Double
	Get
		Return _current_rateother
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _current_rateother <> value Then _d=True
		If not _ReadingData=True Then
		If _current_rateother <> value then RaiseEvent current_rateotherChanged
		End If
		_current_rateother = value
		If _ReadingData=True Then _d=False
		_current_rateother_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _matrixdetail_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _profile_fk_d
		_IsDirty = _IsDirty OR _metertimeswitchcode_d
		_IsDirty = _IsDirty OR _linelossfactor_d
		_IsDirty = _IsDirty OR _distribution_fk_d
		_IsDirty = _IsDirty OR _uniqueidentifier_d
		_IsDirty = _IsDirty OR _checkdigit_d
		_IsDirty = _IsDirty OR _consumptionsingle_d
		_IsDirty = _IsDirty OR _consumptionday_d
		_IsDirty = _IsDirty OR _consumptionnight_d
		_IsDirty = _IsDirty OR _consumptionother_d
		_IsDirty = _IsDirty OR _current_standingcharge2_d
		_IsDirty = _IsDirty OR _current_ratesingle_d
		_IsDirty = _IsDirty OR _current_rateday_d
		_IsDirty = _IsDirty OR _current_ratenight_d
		_IsDirty = _IsDirty OR _current_rateother_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_matrixdetail_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_profile_fk=-1
	_distribution_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("matrix1details",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_matrix1details_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@matrixdetail_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_matrixdetail_pk=CommonFN.SafeRead(Row("matrixdetail_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		profile_fk=CommonFN.SafeRead(Row("profile_fk"))
		metertimeswitchcode=CommonFN.SafeRead(Row("metertimeswitchcode"))
		linelossfactor=CommonFN.SafeRead(Row("linelossfactor"))
		distribution_fk=CommonFN.SafeRead(Row("distribution_fk"))
		uniqueidentifier=CommonFN.SafeRead(Row("uniqueidentifier"))
		checkdigit=CommonFN.SafeRead(Row("checkdigit"))
		consumptionsingle=CommonFN.SafeRead(Row("consumptionsingle"))
		consumptionday=CommonFN.SafeRead(Row("consumptionday"))
		consumptionnight=CommonFN.SafeRead(Row("consumptionnight"))
		consumptionother=CommonFN.SafeRead(Row("consumptionother"))
		current_standingcharge2=CommonFN.SafeRead(Row("current_standingcharge2"))
		current_ratesingle=CommonFN.SafeRead(Row("current_ratesingle"))
		current_rateday=CommonFN.SafeRead(Row("current_rateday"))
		current_ratenight=CommonFN.SafeRead(Row("current_ratenight"))
		current_rateother=CommonFN.SafeRead(Row("current_rateother"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_matrix1details_exists"
	PO = New SqlClient.SqlParameter("@profile_fk", profile_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@distribution_fk", distribution_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_matrix1details_compare"
	PO = New SqlClient.SqlParameter("@matrixdetail_pk", matrixdetail_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@metertimeswitchcode", metertimeswitchcode)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@linelossfactor", linelossfactor)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@uniqueidentifier", uniqueidentifier)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@checkdigit", checkdigit)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and matrixdetail_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_matrix1details_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@matrixdetail_pk", matrixdetail_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@profile_fk", profile_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@metertimeswitchcode", metertimeswitchcode)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@linelossfactor", linelossfactor)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@distribution_fk", distribution_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@uniqueidentifier", uniqueidentifier)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@checkdigit", checkdigit)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@consumptionsingle", consumptionsingle)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@consumptionday", consumptionday)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@consumptionnight", consumptionnight)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@consumptionother", consumptionother)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@current_standingcharge2", current_standingcharge2)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@current_ratesingle", current_ratesingle)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@current_rateday", current_rateday)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@current_ratenight", current_ratenight)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@current_rateother", current_rateother)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _matrixdetail_pk_d then CDC.AuditLog("matrix1details",_returnPK,"matrixdetail_pk",_CurrentUser,modified,CStr(matrixdetail_pk)):_matrixdetail_pk_d=False
				If _creator_fk_d then CDC.AuditLog("matrix1details",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("matrix1details",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("matrix1details",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("matrix1details",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("matrix1details",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _profile_fk_d then CDC.AuditLog("matrix1details",_returnPK,"profile_fk",_CurrentUser,modified,CStr(profile_fk)):_profile_fk_d=False
				If _metertimeswitchcode_d then CDC.AuditLog("matrix1details",_returnPK,"metertimeswitchcode",_CurrentUser,modified,CStr(metertimeswitchcode)):_metertimeswitchcode_d=False
				If _linelossfactor_d then CDC.AuditLog("matrix1details",_returnPK,"linelossfactor",_CurrentUser,modified,CStr(linelossfactor)):_linelossfactor_d=False
				If _distribution_fk_d then CDC.AuditLog("matrix1details",_returnPK,"distribution_fk",_CurrentUser,modified,CStr(distribution_fk)):_distribution_fk_d=False
				If _uniqueidentifier_d then CDC.AuditLog("matrix1details",_returnPK,"uniqueidentifier",_CurrentUser,modified,CStr(uniqueidentifier)):_uniqueidentifier_d=False
				If _checkdigit_d then CDC.AuditLog("matrix1details",_returnPK,"checkdigit",_CurrentUser,modified,CStr(checkdigit)):_checkdigit_d=False
				If _consumptionsingle_d then CDC.AuditLog("matrix1details",_returnPK,"consumptionsingle",_CurrentUser,modified,CStr(consumptionsingle)):_consumptionsingle_d=False
				If _consumptionday_d then CDC.AuditLog("matrix1details",_returnPK,"consumptionday",_CurrentUser,modified,CStr(consumptionday)):_consumptionday_d=False
				If _consumptionnight_d then CDC.AuditLog("matrix1details",_returnPK,"consumptionnight",_CurrentUser,modified,CStr(consumptionnight)):_consumptionnight_d=False
				If _consumptionother_d then CDC.AuditLog("matrix1details",_returnPK,"consumptionother",_CurrentUser,modified,CStr(consumptionother)):_consumptionother_d=False
				If _current_standingcharge2_d then CDC.AuditLog("matrix1details",_returnPK,"current_standingcharge2",_CurrentUser,modified,CStr(current_standingcharge2)):_current_standingcharge2_d=False
				If _current_ratesingle_d then CDC.AuditLog("matrix1details",_returnPK,"current_ratesingle",_CurrentUser,modified,CStr(current_ratesingle)):_current_ratesingle_d=False
				If _current_rateday_d then CDC.AuditLog("matrix1details",_returnPK,"current_rateday",_CurrentUser,modified,CStr(current_rateday)):_current_rateday_d=False
				If _current_ratenight_d then CDC.AuditLog("matrix1details",_returnPK,"current_ratenight",_CurrentUser,modified,CStr(current_ratenight)):_current_ratenight_d=False
				If _current_rateother_d then CDC.AuditLog("matrix1details",_returnPK,"current_rateother",_CurrentUser,modified,CStr(current_rateother)):_current_rateother_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
