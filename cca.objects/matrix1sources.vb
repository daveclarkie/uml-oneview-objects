Imports cca.common
Public Class matrix1sources
    Dim _ReadingData As Boolean = False
    Dim _LastError As Exception = Nothing

    Public ReadOnly Property LastError() As Exception
        Get
            Return _LastError
        End Get
    End Property

    Dim _matrixsource_pk As Integer

    Dim _matrixsource_pk_d As Boolean = False
    Public Event matrixsource_pkChanged()
    Public ReadOnly Property matrixsource_pk() As Integer
        Get
            Return _matrixsource_pk
        End Get
    End Property

    Dim _creator_fk As Integer

    Dim _creator_fk_d As Boolean = False
    Public Event creator_fkChanged()
    Public Property creator_fk() As Integer
        Get
            Return _creator_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _creator_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("creator_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _creator_fk <> value Then RaiseEvent creator_fkChanged()
            End If
            _creator_fk = value
            If _ReadingData = True Then _d = False
            _creator_fk_d = _d
        End Set
    End Property

    Dim _editor_fk As Integer

    Dim _editor_fk_d As Boolean = False
    Public Event editor_fkChanged()
    Public Property editor_fk() As Integer
        Get
            Return _editor_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _editor_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("editor_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _editor_fk <> value Then RaiseEvent editor_fkChanged()
            End If
            _editor_fk = value
            If _ReadingData = True Then _d = False
            _editor_fk_d = _d
        End Set
    End Property

    Dim _created As Datetime

    Dim _created_d As Boolean = False
    Public Event createdChanged()
    Public Property created() As Datetime
        Get
            Return _created
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _created <> value Then _d = True
            If Not _ReadingData = True Then
                If _created <> value Then RaiseEvent createdChanged()
            End If
            _created = value
            If _ReadingData = True Then _d = False
            _created_d = _d
        End Set
    End Property

    Dim _modified As Datetime

    Dim _modified_d As Boolean = False
    Public Event modifiedChanged()
    Public Property modified() As Datetime
        Get
            Return _modified
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _modified <> value Then _d = True
            If Not _ReadingData = True Then
                If _modified <> value Then RaiseEvent modifiedChanged()
            End If
            _modified = value
            If _ReadingData = True Then _d = False
            _modified_d = _d
        End Set
    End Property

    Dim _rowstatus As Integer = 0
    Dim _rowstatus_d As Boolean = False
    Public Event rowstatusChanged()
    Public Property rowstatus() As Integer
        Get
            Return _rowstatus
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If _rowstatus <> value Then _d = True
            If Not _ReadingData = True Then
                If _rowstatus <> value Then RaiseEvent rowstatusChanged()
            End If
            _rowstatus = value
            If _ReadingData = True Then _d = False
            _rowstatus_d = _d
        End Set
    End Property

    Dim _matrixfile_fk As Integer

    Dim _matrixfile_fk_d As Boolean = False
    Public Event matrixfile_fkChanged()
    Public Property matrixfile_fk() As Integer
        Get
            Return _matrixfile_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _matrixfile_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("matrixfile_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("Matrix File"), CStr("Validation failed: Invalid Foreign Key"))
                If _matrixfile_fk <> value Then RaiseEvent matrixfile_fkChanged()
            End If
            _matrixfile_fk = value
            If _ReadingData = True Then _d = False
            _matrixfile_fk_d = _d
        End Set
    End Property

    Dim _supplier_fk As Integer

    Dim _supplier_fk_d As Boolean = False
    Public Event supplier_fkChanged()
    Public Property supplier_fk() As Integer
        Get
            Return _supplier_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _supplier_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("supplier_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("Supplier Name"), CStr("Validation failed: Invalid Foreign Key"))
                If _supplier_fk <> value Then RaiseEvent supplier_fkChanged()
            End If
            _supplier_fk = value
            If _ReadingData = True Then _d = False
            _supplier_fk_d = _d
        End Set
    End Property

    Dim _productname As String

    Dim _productname_d As Boolean = False
    Public Event productnameChanged()
    Public Property productname() As String
        Get
            Return _productname
        End Get
        Set(ByVal value As String)
            Dim _d As Boolean = False
            If _productname <> value Then _d = True
            If Not _ReadingData = True Then
                If _productname <> value Then RaiseEvent productnameChanged()
            End If
            _productname = value
            If _ReadingData = True Then _d = False
            _productname_d = _d
        End Set
    End Property

    Dim _distribution_fk As Integer

    Dim _distribution_fk_d As Boolean = False
    Public Event distribution_fkChanged()
    Public Property distribution_fk() As Integer
        Get
            Return _distribution_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _distribution_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("distribution_fk", value, CDC) Or value = -1 Then Throw New ArgumentOutOfRangeException(CStr("Distribution"), CStr("Validation failed: Required Field"))
                If _distribution_fk <> value Then RaiseEvent distribution_fkChanged()
            End If
            _distribution_fk = value
            If _ReadingData = True Then _d = False
            _distribution_fk_d = _d
        End Set
    End Property

    Dim _profile_fk As Integer

    Dim _profile_fk_d As Boolean = False
    Public Event profile_fkChanged()
    Public Property profile_fk() As Integer
        Get
            Return _profile_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _profile_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("profile_fk", value, CDC) Or value = -1 Then Throw New ArgumentOutOfRangeException(CStr("Profile Class"), CStr("Validation failed: Required Field"))
                If _profile_fk <> value Then RaiseEvent profile_fkChanged()
            End If
            _profile_fk = value
            If _ReadingData = True Then _d = False
            _profile_fk_d = _d
        End Set
    End Property

    Dim _contractlength As Integer

    Dim _contractlength_d As Boolean = False
    Public Event contractlengthChanged()
    Public Property contractlength() As Integer
        Get
            Return _contractlength
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If _contractlength <> value Then _d = True
            If Not _ReadingData = True Then
                If _contractlength <> value Then RaiseEvent contractlengthChanged()
            End If
            _contractlength = value
            If _ReadingData = True Then _d = False
            _contractlength_d = _d
        End Set
    End Property

    Dim _validfrom As Datetime

    Dim _validfrom_d As Boolean = False
    Public Event validfromChanged()
    Public Property validfrom() As Datetime
        Get
            Return _validfrom
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _validfrom <> value Then _d = True
            If Not _ReadingData = True Then
                If _validfrom <> value Then RaiseEvent validfromChanged()
            End If
            _validfrom = value
            If _ReadingData = True Then _d = False
            _validfrom_d = _d
        End Set
    End Property

    Dim _validto As Datetime

    Dim _validto_d As Boolean = False
    Public Event validtoChanged()
    Public Property validto() As Datetime
        Get
            Return _validto
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _validto <> value Then _d = True
            If Not _ReadingData = True Then
                If _validto <> value Then RaiseEvent validtoChanged()
            End If
            _validto = value
            If _ReadingData = True Then _d = False
            _validto_d = _d
        End Set
    End Property

    Dim _standingcharge As Double

    Dim _standingcharge_d As Boolean = False
    Public Event standingchargeChanged()
    Public Property standingcharge() As Double
        Get
            Return _standingcharge
        End Get
        Set(ByVal value As Double)
            Dim _d As Boolean = False
            If _standingcharge <> value Then _d = True
            If Not _ReadingData = True Then
                If _standingcharge <> value Then RaiseEvent standingchargeChanged()
            End If
            _standingcharge = value
            If _ReadingData = True Then _d = False
            _standingcharge_d = _d
        End Set
    End Property

    Dim _rate_single As Double

    Dim _rate_single_d As Boolean = False
    Public Event rate_singleChanged()
    Public Property rate_single() As Double
        Get
            Return _rate_single
        End Get
        Set(ByVal value As Double)
            Dim _d As Boolean = False
            If _rate_single <> value Then _d = True
            If Not _ReadingData = True Then
                If _rate_single <> value Then RaiseEvent rate_singleChanged()
            End If
            _rate_single = value
            If _ReadingData = True Then _d = False
            _rate_single_d = _d
        End Set
    End Property

    Dim _rate_day As Double

    Dim _rate_day_d As Boolean = False
    Public Event rate_dayChanged()
    Public Property rate_day() As Double
        Get
            Return _rate_day
        End Get
        Set(ByVal value As Double)
            Dim _d As Boolean = False
            If _rate_day <> value Then _d = True
            If Not _ReadingData = True Then
                If _rate_day <> value Then RaiseEvent rate_dayChanged()
            End If
            _rate_day = value
            If _ReadingData = True Then _d = False
            _rate_day_d = _d
        End Set
    End Property

    Dim _rate_night As Double

    Dim _rate_night_d As Boolean = False
    Public Event rate_nightChanged()
    Public Property rate_night() As Double
        Get
            Return _rate_night
        End Get
        Set(ByVal value As Double)
            Dim _d As Boolean = False
            If _rate_night <> value Then _d = True
            If Not _ReadingData = True Then
                If _rate_night <> value Then RaiseEvent rate_nightChanged()
            End If
            _rate_night = value
            If _ReadingData = True Then _d = False
            _rate_night_d = _d
        End Set
    End Property

    Dim _rate_other As Double

    Dim _rate_other_d As Boolean = False
    Public Event rate_otherChanged()
    Public Property rate_other() As Double
        Get
            Return _rate_other
        End Get
        Set(ByVal value As Double)
            Dim _d As Boolean = False
            If _rate_other <> value Then _d = True
            If Not _ReadingData = True Then
                If _rate_other <> value Then RaiseEvent rate_otherChanged()
            End If
            _rate_other = value
            If _ReadingData = True Then _d = False
            _rate_other_d = _d
        End Set
    End Property

    Dim _IsDirty As Boolean = False
    Public ReadOnly Property IsDirty() As Boolean
        Get
            _IsDirty = False
            _IsDirty = _IsDirty Or _matrixsource_pk_d
            _IsDirty = _IsDirty Or _creator_fk_d
            _IsDirty = _IsDirty Or _editor_fk_d
            _IsDirty = _IsDirty Or _created_d
            _IsDirty = _IsDirty Or _modified_d
            _IsDirty = _IsDirty Or _rowstatus_d
            _IsDirty = _IsDirty Or _matrixfile_fk_d
            _IsDirty = _IsDirty Or _supplier_fk_d
            _IsDirty = _IsDirty Or _productname_d
            _IsDirty = _IsDirty Or _distribution_fk_d
            _IsDirty = _IsDirty Or _profile_fk_d
            _IsDirty = _IsDirty Or _contractlength_d
            _IsDirty = _IsDirty Or _validfrom_d
            _IsDirty = _IsDirty Or _validto_d
            _IsDirty = _IsDirty Or _standingcharge_d
            _IsDirty = _IsDirty Or _rate_single_d
            _IsDirty = _IsDirty Or _rate_day_d
            _IsDirty = _IsDirty Or _rate_night_d
            _IsDirty = _IsDirty Or _rate_other_d
            Return _IsDirty
        End Get
    End Property



    Dim CDC As DataCommon
    Dim Row As DataRow
    Dim _CurrentUser As Integer
    Dim _ActualUser As Integer

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        _ReadingData = True
        _matrixsource_pk = -1
        _creator_fk = CurrentUser
        _editor_fk = ActualUser
        Dim _dcreated As Datetime = Nothing : CDC.ReadScalarValue(_dcreated, New sqlclient.sqlCommand("select (getdate())"))
        created = _dcreated
        Dim _dmodified As Datetime = Nothing : CDC.ReadScalarValue(_dmodified, New sqlclient.sqlCommand("select (getdate())"))
        modified = _dmodified
        _matrixfile_fk = -1
        _supplier_fk = -1
        _distribution_fk = -1
        _profile_fk = -1
        _ReadingData = False
    End Sub

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, ByVal PK As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        Read(PK)
    End Sub

    Public Function Read(ByVal PK As Integer) As Boolean
        Dim _ReturnStatus As Boolean = True
        CDC.AccessLog("matrix1sources", PK, _ActualUser)
        _ReadingData = True
        Try
            Dim CO As New SqlClient.SqlCommand("rkg_matrix1sources_read")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.Add(New SqlClient.SqlParameter("@matrixsource_pk", PK))
            Row = CDC.ReadDataRow(CO)
            CO.Dispose()
            _matrixsource_pk = CommonFN.SafeRead(Row("matrixsource_pk"))
            creator_fk = CommonFN.SafeRead(Row("creator_fk"))
            editor_fk = CommonFN.SafeRead(Row("editor_fk"))
            created = CommonFN.SafeRead(Row("created"))
            modified = CommonFN.SafeRead(Row("modified"))
            rowstatus = CommonFN.SafeRead(Row("rowstatus"))
            matrixfile_fk = CommonFN.SafeRead(Row("matrixfile_fk"))
            supplier_fk = CommonFN.SafeRead(Row("supplier_fk"))
            productname = CommonFN.SafeRead(Row("productname"))
            distribution_fk = CommonFN.SafeRead(Row("distribution_fk"))
            profile_fk = CommonFN.SafeRead(Row("profile_fk"))
            contractlength = CommonFN.SafeRead(Row("contractlength"))
            validfrom = CommonFN.SafeRead(Row("validfrom"))
            validto = CommonFN.SafeRead(Row("validto"))
            standingcharge = CommonFN.SafeRead(Row("standingcharge"))
            rate_single = CommonFN.SafeRead(Row("rate_single"))
            rate_day = CommonFN.SafeRead(Row("rate_day"))
            rate_night = CommonFN.SafeRead(Row("rate_night"))
            rate_other = CommonFN.SafeRead(Row("rate_other"))
            Row = Nothing
        Catch Ex As Exception
            _ReturnStatus = False
            _LastError = Ex
        End Try
        _ReadingData = False
        Return _ReturnStatus
    End Function

    Dim _MatchedRecords As datatable
    Public ReadOnly Property MatchedRecords() As datatable
        Get
            If _MatchedRecords Is Nothing Then Exists()
            Return _MatchedRecords
        End Get
    End Property
    Public Function Exists() As Boolean
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_matrix1sources_exists"
        PO = New SqlClient.SqlParameter("@supplier_fk", supplier_fk)
        CO.Parameters.Add(PO)

        PO = New SqlClient.SqlParameter("@productname", productname)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@distribution_fk", distribution_fk)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@profile_fk", profile_fk)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@contractlength", contractlength)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@validfrom", validfrom)
        CO.Parameters.Add(PO)
        PO = New SqlClient.SqlParameter("@validto", validto)
        CO.Parameters.Add(PO)

        Dim _returnPK As Integer = 0
        _MatchedRecords = CDC.ReadDataTable(CO)
        _returnPK = _MatchedRecords.rows.count
        If _returnPK > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Dim _SimilarRecords As datatable
    Public ReadOnly Property SimilarRecords() As datatable
        Get
            If _SimilarRecords Is Nothing Then isUnique()
            Return _SimilarRecords
        End Get
    End Property
    Public Function IsUnique(Optional ByVal minMatchCount As Integer = 0) As Boolean
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_matrix1sources_compare"
        PO = New SqlClient.SqlParameter("@matrixsource_pk", matrixsource_pk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@productname", productname)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@validfrom", CommonFN.CheckEmptyDate(validfrom.ToString()))
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@validto", CommonFN.CheckEmptyDate(validto.ToString()))
        CO.parameters.add(PO)
        If minMatchCount > 0 Then CO.Parameters.AddWithValue("@minMatchCount", minMatchCount)
        Dim _returnPK As Integer = 0
        _SimilarRecords = CDC.ReadDataTable(CO)
        _returnPK = _SimilarRecords.rows.count
        If _returnPK = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Save() As Boolean
        Dim _ReturnStatus As Boolean = True
        If IsDirty Then
            If _CurrentUser <> creator_fk And matrixsource_pk < 1 Then creator_fk = _CurrentUser : created = now()
            If _ActualUser <> editor_fk Then editor_fk = _ActualUser
            modified = now()
            Try
                CDC.BeginTransaction()
                Dim PO As SqlClient.SqlParameter
                Dim CO As New SqlClient.SqlCommand()
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rkg_matrix1sources_save"
                PO = Nothing
                PO = New SqlClient.SqlParameter("@matrixsource_pk", matrixsource_pk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tcreator_fk As Integer = Nothing
                If creator_fk = Nothing Then
                    CDC.ReadScalarValue(_tcreator_fk, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
                Else
                    PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _teditor_fk As Integer = Nothing
                If editor_fk = Nothing Then
                    CDC.ReadScalarValue(_teditor_fk, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
                Else
                    PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tcreated As Datetime = Nothing
                If created = Nothing Then
                    CDC.ReadScalarValue(_tcreated, New sqlclient.sqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@created", _tcreated)
                Else
                    PO = New SqlClient.SqlParameter("@created", created)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tmodified As Datetime = Nothing
                If modified = Nothing Then
                    CDC.ReadScalarValue(_tmodified, New sqlclient.sqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@modified", _tmodified)
                Else
                    PO = New SqlClient.SqlParameter("@modified", modified)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _trowstatus As Integer = Nothing
                If rowstatus = Nothing Then
                    CDC.ReadScalarValue(_trowstatus, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
                Else
                    PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@matrixfile_fk", matrixfile_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@supplier_fk", supplier_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@productname", productname)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@distribution_fk", distribution_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@profile_fk", profile_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@contractlength", contractlength)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@validfrom", CommonFN.CheckEmptyDate(validfrom.ToString()))
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@validto", CommonFN.CheckEmptyDate(validto.ToString()))
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@standingcharge", standingcharge)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@rate_single", rate_single)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@rate_day", rate_day)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@rate_night", rate_night)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@rate_other", rate_other)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                Dim _returnPK As Integer = 0
                If CDC.ReadScalarValue(_returnPK, CO) Then
                    Try
                        If _matrixsource_pk_d Then CDC.AuditLog("matrix1sources", _returnPK, "matrixsource_pk", _CurrentUser, modified, CStr(matrixsource_pk)) : _matrixsource_pk_d = False
                        If _creator_fk_d Then CDC.AuditLog("matrix1sources", _returnPK, "creator_fk", _CurrentUser, modified, CStr(creator_fk)) : _creator_fk_d = False
                        If _editor_fk_d Then CDC.AuditLog("matrix1sources", _returnPK, "editor_fk", _CurrentUser, modified, CStr(editor_fk)) : _editor_fk_d = False
                        If _created_d Then CDC.AuditLog("matrix1sources", _returnPK, "created", _CurrentUser, modified, CStr(created)) : _created_d = False
                        If _modified_d Then CDC.AuditLog("matrix1sources", _returnPK, "modified", _CurrentUser, modified, CStr(modified)) : _modified_d = False
                        If _rowstatus_d Then CDC.AuditLog("matrix1sources", _returnPK, "rowstatus", _CurrentUser, modified, CStr(rowstatus)) : _rowstatus_d = False
                        If _matrixfile_fk_d Then CDC.AuditLog("matrix1sources", _returnPK, "matrixfile_fk", _CurrentUser, modified, CStr(matrixfile_fk)) : _matrixfile_fk_d = False
                        If _supplier_fk_d Then CDC.AuditLog("matrix1sources", _returnPK, "supplier_fk", _CurrentUser, modified, CStr(supplier_fk)) : _supplier_fk_d = False
                        If _productname_d Then CDC.AuditLog("matrix1sources", _returnPK, "productname", _CurrentUser, modified, CStr(productname)) : _productname_d = False
                        If _distribution_fk_d Then CDC.AuditLog("matrix1sources", _returnPK, "distribution_fk", _CurrentUser, modified, CStr(distribution_fk)) : _distribution_fk_d = False
                        If _profile_fk_d Then CDC.AuditLog("matrix1sources", _returnPK, "profile_fk", _CurrentUser, modified, CStr(profile_fk)) : _profile_fk_d = False
                        If _contractlength_d Then CDC.AuditLog("matrix1sources", _returnPK, "contractlength", _CurrentUser, modified, CStr(contractlength)) : _contractlength_d = False
                        If _validfrom_d Then CDC.AuditLog("matrix1sources", _returnPK, "validfrom", _CurrentUser, modified, CStr(validfrom)) : _validfrom_d = False
                        If _validto_d Then CDC.AuditLog("matrix1sources", _returnPK, "validto", _CurrentUser, modified, CStr(validto)) : _validto_d = False
                        If _standingcharge_d Then CDC.AuditLog("matrix1sources", _returnPK, "standingcharge", _CurrentUser, modified, CStr(standingcharge)) : _standingcharge_d = False
                        If _rate_single_d Then CDC.AuditLog("matrix1sources", _returnPK, "rate_single", _CurrentUser, modified, CStr(rate_single)) : _rate_single_d = False
                        If _rate_day_d Then CDC.AuditLog("matrix1sources", _returnPK, "rate_day", _CurrentUser, modified, CStr(rate_day)) : _rate_day_d = False
                        If _rate_night_d Then CDC.AuditLog("matrix1sources", _returnPK, "rate_night", _CurrentUser, modified, CStr(rate_night)) : _rate_night_d = False
                        If _rate_other_d Then CDC.AuditLog("matrix1sources", _returnPK, "rate_other", _CurrentUser, modified, CStr(rate_other)) : _rate_other_d = False
                    Catch Ex2 As Exception
                        _ReturnStatus = False
                        _LastError = Ex2
                    End Try
                Else
                    _ReturnStatus = False
                End If
                If _ReturnStatus Then
                    CDC.CommitTransaction()
                    Read(_returnPK)
                Else
                    CDC.RollBackTransaction()
                End If
            Catch Ex As Exception
                _ReturnStatus = False
                _LastError = Ex
                CDC.RollBackTransaction()
            End Try
        Else
            _ReturnStatus = True
        End If
        Return _ReturnStatus
    End Function

    Public Function Enable() As Boolean
        rowstatus = 0
        Return Save
    End Function

    Public Function Disable() As Boolean
        rowstatus = 1
        Return Save
    End Function

    Public Function Delete() As Boolean
        rowstatus = 2
        Return Save
    End Function


End Class
