Imports cca.common
Public Class meterpoints
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _meterpoint_pk as String

Dim _meterpoint_pk_d as Boolean=False
Public Event meterpoint_pkChanged()
Public ReadOnly Property meterpoint_pk() as String
	Get
		Return _meterpoint_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer

Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _meter_fk as Integer

Dim _meter_fk_d as Boolean=False
Public Event meter_fkChanged()
Public Property meter_fk() as Integer
	Get
		Return _meter_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _meter_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("meter_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("meter_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _meter_fk <> value then RaiseEvent meter_fkChanged
		End If
		_meter_fk = value
		If _ReadingData=True Then _d=False
		_meter_fk_d=_d
	End Set
End Property

Dim _tablename as String

Dim _tablename_d as Boolean=False
Public Event tablenameChanged()
Public Property tablename() as String
	Get
		Return _tablename
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _tablename <> value Then _d=True
		If not _ReadingData=True Then
		If _tablename <> value then RaiseEvent tablenameChanged
		End If
		_tablename = value
		If _ReadingData=True Then _d=False
		_tablename_d=_d
	End Set
End Property

Dim _meter as String

Dim _meter_d as Boolean=False
Public Event meterChanged()
Public Property meter() as String
	Get
		Return _meter
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _meter <> value Then _d=True
		If not _ReadingData=True Then
		If _meter <> value then RaiseEvent meterChanged
		End If
		_meter = value
		If _ReadingData=True Then _d=False
		_meter_d=_d
	End Set
End Property

Dim _channel as Integer

Dim _channel_d as Boolean=False
Public Event channelChanged()
Public Property channel() as Integer
	Get
		Return _channel
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _channel <> value Then _d=True
		If not _ReadingData=True Then
		If _channel <> value then RaiseEvent channelChanged
		End If
		_channel = value
		If _ReadingData=True Then _d=False
		_channel_d=_d
	End Set
End Property

Dim _fuel_fk as Integer

Dim _fuel_fk_d as Boolean=False
Public Event fuel_fkChanged()
Public Property fuel_fk() as Integer
	Get
		Return _fuel_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _fuel_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("fuel_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("fuel_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _fuel_fk <> value then RaiseEvent fuel_fkChanged
		End If
		_fuel_fk = value
		If _ReadingData=True Then _d=False
		_fuel_fk_d=_d
	End Set
End Property

Dim _unit_fk as Integer

Dim _unit_fk_d as Boolean=False
Public Event unit_fkChanged()
Public Property unit_fk() as Integer
	Get
		Return _unit_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _unit_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("unit_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("unit_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _unit_fk <> value then RaiseEvent unit_fkChanged
		End If
		_unit_fk = value
		If _ReadingData=True Then _d=False
		_unit_fk_d=_d
	End Set
End Property

Dim _umlsite_fk as Integer

Dim _umlsite_fk_d as Boolean=False
Public Event umlsite_fkChanged()
Public Property umlsite_fk() as Integer
	Get
		Return _umlsite_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _umlsite_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("umlsite_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("umlsite_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _umlsite_fk <> value then RaiseEvent umlsite_fkChanged
		End If
		_umlsite_fk = value
		If _ReadingData=True Then _d=False
		_umlsite_fk_d=_d
	End Set
End Property

Dim _sitename as String

Dim _sitename_d as Boolean=False
Public Event sitenameChanged()
Public Property sitename() as String
	Get
		Return _sitename
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _sitename <> value Then _d=True
		If not _ReadingData=True Then
		If _sitename <> value then RaiseEvent sitenameChanged
		End If
		_sitename = value
		If _ReadingData=True Then _d=False
		_sitename_d=_d
	End Set
End Property

Dim _displayname as String

Dim _displayname_d as Boolean=False
Public Event displaynameChanged()
Public Property displayname() as String
	Get
		Return _displayname
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _displayname <> value Then _d=True
		If not _ReadingData=True Then
		If _displayname <> value then RaiseEvent displaynameChanged
		End If
		_displayname = value
		If _ReadingData=True Then _d=False
		_displayname_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _meterpoint_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _meter_fk_d
		_IsDirty = _IsDirty OR _tablename_d
		_IsDirty = _IsDirty OR _meter_d
		_IsDirty = _IsDirty OR _channel_d
		_IsDirty = _IsDirty OR _fuel_fk_d
		_IsDirty = _IsDirty OR _unit_fk_d
		_IsDirty = _IsDirty OR _umlsite_fk_d
		_IsDirty = _IsDirty OR _sitename_d
		_IsDirty = _IsDirty OR _displayname_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_meterpoint_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	_meter_fk=-1
	_fuel_fk=-1
	_unit_fk=-1
	_umlsite_fk=-1
	_ReadingData=False
End Sub

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, ByVal PK As String, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        Read(PK)
    End Sub

    Public Function Read(ByVal PK As String) As Boolean
        Dim _ReturnStatus As Boolean = True
        'CDC.AccessLog("meterpoints", PK, _ActualUser)
        _ReadingData = True
        Try
            Dim CO As New SqlClient.SqlCommand("rkg_meterpoints_read")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.Add(New SqlClient.SqlParameter("@meterpoint_pk", PK))
            Row = CDC.ReadDataRow(CO)
            CO.Dispose()
            _meterpoint_pk = CommonFN.SafeRead(Row("meterpoint_pk"))
            creator_fk = CommonFN.SafeRead(Row("creator_fk"))
            editor_fk = CommonFN.SafeRead(Row("editor_fk"))
            created = CommonFN.SafeRead(Row("created"))
            modified = CommonFN.SafeRead(Row("modified"))
            rowstatus = CommonFN.SafeRead(Row("rowstatus"))
            meter_fk = CommonFN.SafeRead(Row("meter_fk"))
            tablename = CommonFN.SafeRead(Row("tablename"))
            meter = CommonFN.SafeRead(Row("meter"))
            channel = CommonFN.SafeRead(Row("channel"))
            fuel_fk = CommonFN.SafeRead(Row("fuel_fk"))
            unit_fk = CommonFN.SafeRead(Row("unit_fk"))
            umlsite_fk = CommonFN.SafeRead(Row("umlsite_fk"))
            sitename = CommonFN.SafeRead(Row("sitename"))
            displayname = CommonFN.SafeRead(Row("displayname"))
            Row = Nothing
        Catch Ex As Exception
            _ReturnStatus = False
            _LastError = Ex
        End Try
        _ReadingData = False
        Return _ReturnStatus
    End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_meterpoints_exists"
	PO = New SqlClient.SqlParameter("@meter_fk", meter_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@fuel_fk", fuel_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@unit_fk", unit_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@umlsite_fk", umlsite_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_meterpoints_compare"
	PO = New SqlClient.SqlParameter("@meterpoint_pk", meterpoint_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@tablename", tablename)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and meterpoint_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_meterpoints_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@meterpoint_pk", meterpoint_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@created", CommonFN.CheckEmptyDate(created.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@modified", CommonFN.CheckEmptyDate(modified.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@meter_fk", meter_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@tablename", tablename)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@meter", meter)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@channel", channel)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@fuel_fk", fuel_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@unit_fk", unit_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@umlsite_fk", umlsite_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@sitename", sitename)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@displayname", displayname)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _meterpoint_pk_d then CDC.AuditLog("meterpoints",_returnPK,"meterpoint_pk",_CurrentUser,modified,CStr(meterpoint_pk)):_meterpoint_pk_d=False
				If _creator_fk_d then CDC.AuditLog("meterpoints",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("meterpoints",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("meterpoints",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("meterpoints",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("meterpoints",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _meter_fk_d then CDC.AuditLog("meterpoints",_returnPK,"meter_fk",_CurrentUser,modified,CStr(meter_fk)):_meter_fk_d=False
				If _tablename_d then CDC.AuditLog("meterpoints",_returnPK,"tablename",_CurrentUser,modified,CStr(tablename)):_tablename_d=False
				If _meter_d then CDC.AuditLog("meterpoints",_returnPK,"meter",_CurrentUser,modified,CStr(meter)):_meter_d=False
				If _channel_d then CDC.AuditLog("meterpoints",_returnPK,"channel",_CurrentUser,modified,CStr(channel)):_channel_d=False
				If _fuel_fk_d then CDC.AuditLog("meterpoints",_returnPK,"fuel_fk",_CurrentUser,modified,CStr(fuel_fk)):_fuel_fk_d=False
				If _unit_fk_d then CDC.AuditLog("meterpoints",_returnPK,"unit_fk",_CurrentUser,modified,CStr(unit_fk)):_unit_fk_d=False
				If _umlsite_fk_d then CDC.AuditLog("meterpoints",_returnPK,"umlsite_fk",_CurrentUser,modified,CStr(umlsite_fk)):_umlsite_fk_d=False
				If _sitename_d then CDC.AuditLog("meterpoints",_returnPK,"sitename",_CurrentUser,modified,CStr(sitename)):_sitename_d=False
				If _displayname_d then CDC.AuditLog("meterpoints",_returnPK,"displayname",_CurrentUser,modified,CStr(displayname)):_displayname_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
