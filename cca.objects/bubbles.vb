Imports cca.common
Public Class bubbles
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _bubble_pk as Integer

Dim _bubble_pk_d as Boolean=False
Public Event bubble_pkChanged()
Public ReadOnly Property bubble_pk() as Integer
	Get
		Return _bubble_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _organisation_fk as Integer

Dim _organisation_fk_d as Boolean=False
Public Event organisation_fkChanged()
Public Property organisation_fk() as Integer
	Get
		Return _organisation_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _organisation_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("organisation_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Organisation"), CStr("Validation failed: Required Field"))
		If _organisation_fk <> value then RaiseEvent organisation_fkChanged
		End If
		_organisation_fk = value
		If _ReadingData=True Then _d=False
		_organisation_fk_d=_d
	End Set
End Property

Dim _bubblename as String

Dim _bubblename_d as Boolean=False
Public Event bubblenameChanged()
Public Property bubblename() as String
	Get
		Return _bubblename
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _bubblename <> value Then _d=True
		If not _ReadingData=True Then
		If _bubblename <> value then RaiseEvent bubblenameChanged
		End If
		_bubblename = value
		If _ReadingData=True Then _d=False
		_bubblename_d=_d
	End Set
End Property

Dim _federation_fk as Integer

Dim _federation_fk_d as Boolean=False
Public Event federation_fkChanged()
Public Property federation_fk() as Integer
	Get
		Return _federation_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _federation_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("federation_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Federation"), CStr("Validation failed: Required Field"))
		If _federation_fk <> value then RaiseEvent federation_fkChanged
		End If
		_federation_fk = value
		If _ReadingData=True Then _d=False
		_federation_fk_d=_d
	End Set
End Property

Dim _targetunitidentifier as String

Dim _targetunitidentifier_d as Boolean=False
Public Event targetunitidentifierChanged()
Public Property targetunitidentifier() as String
	Get
		Return _targetunitidentifier
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _targetunitidentifier <> value Then _d=True
		If not _ReadingData=True Then
		If _targetunitidentifier <> value then RaiseEvent targetunitidentifierChanged
		End If
		_targetunitidentifier = value
		If _ReadingData=True Then _d=False
		_targetunitidentifier_d=_d
	End Set
End Property

Dim _agreementtype_fk as Integer

Dim _agreementtype_fk_d as Boolean=False
Public Event agreementtype_fkChanged()
Public Property agreementtype_fk() as Integer
	Get
		Return _agreementtype_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _agreementtype_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("agreementtype_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Bubble Type"), CStr("Validation failed: Required Field"))
		If _agreementtype_fk <> value then RaiseEvent agreementtype_fkChanged
		End If
		_agreementtype_fk = value
		If _ReadingData=True Then _d=False
		_agreementtype_fk_d=_d
	End Set
End Property

Dim _agreementmeasure_fk as Integer

Dim _agreementmeasure_fk_d as Boolean=False
Public Event agreementmeasure_fkChanged()
Public Property agreementmeasure_fk() as Integer
	Get
		Return _agreementmeasure_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _agreementmeasure_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("agreementmeasure_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Bubble Measure"), CStr("Validation failed: Required Field"))
		If _agreementmeasure_fk <> value then RaiseEvent agreementmeasure_fkChanged
		End If
		_agreementmeasure_fk = value
		If _ReadingData=True Then _d=False
		_agreementmeasure_fk_d=_d
	End Set
End Property

Dim _bubble_fk as Integer = 0
Dim _bubble_fk_d as Boolean=False
Public Event bubble_fkChanged()
Public Property bubble_fk() as Integer
	Get
		Return _bubble_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _bubble_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("bubble_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Original Agreement"), CStr("Validation failed: Invalid Foreign Key"))
		If _bubble_fk <> value then RaiseEvent bubble_fkChanged
		End If
		_bubble_fk = value
		If _ReadingData=True Then _d=False
		_bubble_fk_d=_d
	End Set
End Property

Dim _includecrcsaving as Boolean

Dim _includecrcsaving_d as Boolean=False
Public Event includecrcsavingChanged()
Public Property includecrcsaving() as Boolean
	Get
		Return _includecrcsaving
	End Get
	Set (ByVal value As Boolean)
		Dim _d as boolean=false
		If _includecrcsaving <> value Then _d=True
		If not _ReadingData=True Then
		If _includecrcsaving <> value then RaiseEvent includecrcsavingChanged
		End If
		_includecrcsaving = value
		If _ReadingData=True Then _d=False
		_includecrcsaving_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _bubble_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _organisation_fk_d
		_IsDirty = _IsDirty OR _bubblename_d
		_IsDirty = _IsDirty OR _federation_fk_d
		_IsDirty = _IsDirty OR _targetunitidentifier_d
		_IsDirty = _IsDirty OR _agreementtype_fk_d
		_IsDirty = _IsDirty OR _agreementmeasure_fk_d
		_IsDirty = _IsDirty OR _bubble_fk_d
		_IsDirty = _IsDirty OR _includecrcsaving_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_bubble_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_organisation_fk=-1
	_federation_fk=-1
	_agreementtype_fk=-1
	_agreementmeasure_fk=-1
	_bubble_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("bubbles",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_bubbles_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@bubble_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_bubble_pk=CommonFN.SafeRead(Row("bubble_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		organisation_fk=CommonFN.SafeRead(Row("organisation_fk"))
		bubblename=CommonFN.SafeRead(Row("bubblename"))
		federation_fk=CommonFN.SafeRead(Row("federation_fk"))
		targetunitidentifier=CommonFN.SafeRead(Row("targetunitidentifier"))
		agreementtype_fk=CommonFN.SafeRead(Row("agreementtype_fk"))
		agreementmeasure_fk=CommonFN.SafeRead(Row("agreementmeasure_fk"))
		bubble_fk=CommonFN.SafeRead(Row("bubble_fk"))
		includecrcsaving=CommonFN.SafeRead(Row("includecrcsaving"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_bubbles_exists"
	PO = New SqlClient.SqlParameter("@organisation_fk", organisation_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@federation_fk", federation_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@agreementtype_fk", agreementtype_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@agreementmeasure_fk", agreementmeasure_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@bubble_fk", bubble_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_bubbles_compare"
	PO = New SqlClient.SqlParameter("@bubble_pk", bubble_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@bubblename", bubblename)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@targetunitidentifier", targetunitidentifier)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and bubble_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_bubbles_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@bubble_pk", bubble_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@organisation_fk", organisation_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@bubblename", bubblename)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@federation_fk", federation_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetunitidentifier", targetunitidentifier)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@agreementtype_fk", agreementtype_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@agreementmeasure_fk", agreementmeasure_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tbubble_fk as Integer  = Nothing
		If bubble_fk= nothing  then
			CDC.ReadScalarValue(_tbubble_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@bubble_fk", _tbubble_fk)
		Else
			PO = New SqlClient.SqlParameter("@bubble_fk", bubble_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@includecrcsaving", includecrcsaving)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _bubble_pk_d then CDC.AuditLog("bubbles",_returnPK,"bubble_pk",_CurrentUser,modified,CStr(bubble_pk)):_bubble_pk_d=False
				If _creator_fk_d then CDC.AuditLog("bubbles",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("bubbles",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("bubbles",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("bubbles",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("bubbles",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _organisation_fk_d then CDC.AuditLog("bubbles",_returnPK,"organisation_fk",_CurrentUser,modified,CStr(organisation_fk)):_organisation_fk_d=False
				If _bubblename_d then CDC.AuditLog("bubbles",_returnPK,"bubblename",_CurrentUser,modified,CStr(bubblename)):_bubblename_d=False
				If _federation_fk_d then CDC.AuditLog("bubbles",_returnPK,"federation_fk",_CurrentUser,modified,CStr(federation_fk)):_federation_fk_d=False
				If _targetunitidentifier_d then CDC.AuditLog("bubbles",_returnPK,"targetunitidentifier",_CurrentUser,modified,CStr(targetunitidentifier)):_targetunitidentifier_d=False
				If _agreementtype_fk_d then CDC.AuditLog("bubbles",_returnPK,"agreementtype_fk",_CurrentUser,modified,CStr(agreementtype_fk)):_agreementtype_fk_d=False
				If _agreementmeasure_fk_d then CDC.AuditLog("bubbles",_returnPK,"agreementmeasure_fk",_CurrentUser,modified,CStr(agreementmeasure_fk)):_agreementmeasure_fk_d=False
				If _bubble_fk_d then CDC.AuditLog("bubbles",_returnPK,"bubble_fk",_CurrentUser,modified,CStr(bubble_fk)):_bubble_fk_d=False
				If _includecrcsaving_d then CDC.AuditLog("bubbles",_returnPK,"includecrcsaving",_CurrentUser,modified,CStr(includecrcsaving)):_includecrcsaving_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
