Imports cca.common
Public Class reportautomations
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _reportautomation_pk as Integer

Dim _reportautomation_pk_d as Boolean=False
Public Event reportautomation_pkChanged()
Public ReadOnly Property reportautomation_pk() as Integer
	Get
		Return _reportautomation_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _report_fk as Integer

Dim _report_fk_d as Boolean=False
Public Event report_fkChanged()
Public Property report_fk() as Integer
	Get
		Return _report_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _report_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("report_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("report_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _report_fk <> value then RaiseEvent report_fkChanged
		End If
		_report_fk = value
		If _ReadingData=True Then _d=False
		_report_fk_d=_d
	End Set
End Property

Dim _user_fk as Integer

Dim _user_fk_d as Boolean=False
Public Event user_fkChanged()
Public Property user_fk() as Integer
	Get
		Return _user_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _user_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("user_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("user_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _user_fk <> value then RaiseEvent user_fkChanged
		End If
		_user_fk = value
		If _ReadingData=True Then _d=False
		_user_fk_d=_d
	End Set
End Property

Dim _email_fk as Integer

Dim _email_fk_d as Boolean=False
Public Event email_fkChanged()
Public Property email_fk() as Integer
	Get
		Return _email_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _email_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("email_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("email_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _email_fk <> value then RaiseEvent email_fkChanged
		End If
		_email_fk = value
		If _ReadingData=True Then _d=False
		_email_fk_d=_d
	End Set
End Property

Dim _frequency_fk as Integer

Dim _frequency_fk_d as Boolean=False
Public Event frequency_fkChanged()
Public Property frequency_fk() as Integer
	Get
		Return _frequency_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _frequency_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("frequency_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("frequency_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _frequency_fk <> value then RaiseEvent frequency_fkChanged
		End If
		_frequency_fk = value
		If _ReadingData=True Then _d=False
		_frequency_fk_d=_d
	End Set
End Property

Dim _startdate as Datetime

Dim _startdate_d as Boolean=False
Public Event startdateChanged()
Public Property startdate() as Datetime
	Get
		Return _startdate
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _startdate <> value Then _d=True
		If not _ReadingData=True Then
		If _startdate <> value then RaiseEvent startdateChanged
		End If
		_startdate = value
		If _ReadingData=True Then _d=False
		_startdate_d=_d
	End Set
End Property

Dim _reportformat_fk as Integer

Dim _reportformat_fk_d as Boolean=False
Public Event reportformat_fkChanged()
Public Property reportformat_fk() as Integer
	Get
		Return _reportformat_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _reportformat_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("reportformat_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("reportformat_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _reportformat_fk <> value then RaiseEvent reportformat_fkChanged
		End If
		_reportformat_fk = value
		If _ReadingData=True Then _d=False
		_reportformat_fk_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _reportautomation_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _report_fk_d
		_IsDirty = _IsDirty OR _user_fk_d
		_IsDirty = _IsDirty OR _email_fk_d
		_IsDirty = _IsDirty OR _frequency_fk_d
		_IsDirty = _IsDirty OR _startdate_d
		_IsDirty = _IsDirty OR _reportformat_fk_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_reportautomation_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_report_fk=-1
	_user_fk=-1
	_email_fk=-1
	_frequency_fk=-1
	_reportformat_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("reportautomations",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_reportautomations_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@reportautomation_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_reportautomation_pk=CommonFN.SafeRead(Row("reportautomation_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		report_fk=CommonFN.SafeRead(Row("report_fk"))
		user_fk=CommonFN.SafeRead(Row("user_fk"))
		email_fk=CommonFN.SafeRead(Row("email_fk"))
		frequency_fk=CommonFN.SafeRead(Row("frequency_fk"))
		startdate=CommonFN.SafeRead(Row("startdate"))
		reportformat_fk=CommonFN.SafeRead(Row("reportformat_fk"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_reportautomations_exists"
	PO = New SqlClient.SqlParameter("@report_fk", report_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@user_fk", user_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@email_fk", email_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@frequency_fk", frequency_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@reportformat_fk", reportformat_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function

Dim _UserRecords as datatable
Public ReadOnly Property UserRecords() as datatable
	Get
		if _UserRecords is nothing then ByUser()
		return _UserRecords
	End Get
End Property
Public Function ByUser(optional byval user_pk as integer=-1) as boolean
	If user_pk=-1 then user_pk=_user_fk
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_reportautomations_byuser"
	PO = New SqlClient.SqlParameter("@user_pk", user_pk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_UserRecords=CDC.ReadDataTable(CO)
	_returnPK=_UserRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function

Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique() as boolean
	Return True
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and reportautomation_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_reportautomations_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@reportautomation_pk", reportautomation_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@report_fk", report_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@user_fk", user_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@email_fk", email_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@frequency_fk", frequency_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@startdate", CommonFN.CheckEmptyDate(startdate.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@reportformat_fk", reportformat_fk)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _reportautomation_pk_d then CDC.AuditLog("reportautomations",_returnPK,"reportautomation_pk",_CurrentUser,modified,CStr(reportautomation_pk)):_reportautomation_pk_d=False
				If _creator_fk_d then CDC.AuditLog("reportautomations",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("reportautomations",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("reportautomations",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("reportautomations",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("reportautomations",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _report_fk_d then CDC.AuditLog("reportautomations",_returnPK,"report_fk",_CurrentUser,modified,CStr(report_fk)):_report_fk_d=False
				If _user_fk_d then CDC.AuditLog("reportautomations",_returnPK,"user_fk",_CurrentUser,modified,CStr(user_fk)):_user_fk_d=False
				If _email_fk_d then CDC.AuditLog("reportautomations",_returnPK,"email_fk",_CurrentUser,modified,CStr(email_fk)):_email_fk_d=False
				If _frequency_fk_d then CDC.AuditLog("reportautomations",_returnPK,"frequency_fk",_CurrentUser,modified,CStr(frequency_fk)):_frequency_fk_d=False
				If _startdate_d then CDC.AuditLog("reportautomations",_returnPK,"startdate",_CurrentUser,modified,CStr(startdate)):_startdate_d=False
				If _reportformat_fk_d then CDC.AuditLog("reportautomations",_returnPK,"reportformat_fk",_CurrentUser,modified,CStr(reportformat_fk)):_reportformat_fk_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
