Imports cca.common
Public Class survey3details
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _surveydetail_pk as Integer

Dim _surveydetail_pk_d as Boolean=False
Public Event surveydetail_pkChanged()
Public ReadOnly Property surveydetail_pk() as Integer
	Get
		Return _surveydetail_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _selecteduser_fk as Integer

Dim _selecteduser_fk_d as Boolean=False
Public Event selecteduser_fkChanged()
Public Property selecteduser_fk() as Integer
	Get
		Return _selecteduser_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _selecteduser_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("selecteduser_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("selecteduser_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _selecteduser_fk <> value then RaiseEvent selecteduser_fkChanged
		End If
		_selecteduser_fk = value
		If _ReadingData=True Then _d=False
		_selecteduser_fk_d=_d
	End Set
End Property

Dim _supplier_fk as Integer

Dim _supplier_fk_d as Boolean=False
Public Event supplier_fkChanged()
Public Property supplier_fk() as Integer
	Get
		Return _supplier_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _supplier_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("supplier_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("supplier_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _supplier_fk <> value then RaiseEvent supplier_fkChanged
		End If
		_supplier_fk = value
		If _ReadingData=True Then _d=False
		_supplier_fk_d=_d
	End Set
End Property

Dim _survey_fk as Integer

Dim _survey_fk_d as Boolean=False
Public Event survey_fkChanged()
Public Property survey_fk() as Integer
	Get
		Return _survey_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _survey_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("survey_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("survey_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _survey_fk <> value then RaiseEvent survey_fkChanged
		End If
		_survey_fk = value
		If _ReadingData=True Then _d=False
		_survey_fk_d=_d
	End Set
End Property

Dim _question1_surveyanswerfrequencyfivescale_fk as Integer = -1
Dim _question1_surveyanswerfrequencyfivescale_fk_d as Boolean=False
Public Event question1_surveyanswerfrequencyfivescale_fkChanged()
Public Property question1_surveyanswerfrequencyfivescale_fk() as Integer
	Get
		Return _question1_surveyanswerfrequencyfivescale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question1_surveyanswerfrequencyfivescale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question1_surveyanswerfrequencyfivescale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("question1_surveyanswerfrequencyfivescale_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _question1_surveyanswerfrequencyfivescale_fk <> value then RaiseEvent question1_surveyanswerfrequencyfivescale_fkChanged
		End If
		_question1_surveyanswerfrequencyfivescale_fk = value
		If _ReadingData=True Then _d=False
		_question1_surveyanswerfrequencyfivescale_fk_d=_d
	End Set
End Property

Dim _question2_surveyanswerfrequencyfivescale_fk as Integer = -1
Dim _question2_surveyanswerfrequencyfivescale_fk_d as Boolean=False
Public Event question2_surveyanswerfrequencyfivescale_fkChanged()
Public Property question2_surveyanswerfrequencyfivescale_fk() as Integer
	Get
		Return _question2_surveyanswerfrequencyfivescale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question2_surveyanswerfrequencyfivescale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question2_surveyanswerfrequencyfivescale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("question2_surveyanswerfrequencyfivescale_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _question2_surveyanswerfrequencyfivescale_fk <> value then RaiseEvent question2_surveyanswerfrequencyfivescale_fkChanged
		End If
		_question2_surveyanswerfrequencyfivescale_fk = value
		If _ReadingData=True Then _d=False
		_question2_surveyanswerfrequencyfivescale_fk_d=_d
	End Set
End Property

Dim _question3_surveyanswersixscale_fk as Integer = -1
Dim _question3_surveyanswersixscale_fk_d as Boolean=False
Public Event question3_surveyanswersixscale_fkChanged()
Public Property question3_surveyanswersixscale_fk() as Integer
	Get
		Return _question3_surveyanswersixscale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question3_surveyanswersixscale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question3_surveyanswersixscale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("question3_surveyanswersixscale_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _question3_surveyanswersixscale_fk <> value then RaiseEvent question3_surveyanswersixscale_fkChanged
		End If
		_question3_surveyanswersixscale_fk = value
		If _ReadingData=True Then _d=False
		_question3_surveyanswersixscale_fk_d=_d
	End Set
End Property

Dim _question4_surveyanswersixscale_fk as Integer = -1
Dim _question4_surveyanswersixscale_fk_d as Boolean=False
Public Event question4_surveyanswersixscale_fkChanged()
Public Property question4_surveyanswersixscale_fk() as Integer
	Get
		Return _question4_surveyanswersixscale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question4_surveyanswersixscale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question4_surveyanswersixscale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("question4_surveyanswersixscale_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _question4_surveyanswersixscale_fk <> value then RaiseEvent question4_surveyanswersixscale_fkChanged
		End If
		_question4_surveyanswersixscale_fk = value
		If _ReadingData=True Then _d=False
		_question4_surveyanswersixscale_fk_d=_d
	End Set
End Property

Dim _question5_surveyanswerfrequencyfivescale_fk as Integer = -1
Dim _question5_surveyanswerfrequencyfivescale_fk_d as Boolean=False
Public Event question5_surveyanswerfrequencyfivescale_fkChanged()
Public Property question5_surveyanswerfrequencyfivescale_fk() as Integer
	Get
		Return _question5_surveyanswerfrequencyfivescale_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _question5_surveyanswerfrequencyfivescale_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("question5_surveyanswerfrequencyfivescale_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("question5_surveyanswerfrequencyfivescale_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _question5_surveyanswerfrequencyfivescale_fk <> value then RaiseEvent question5_surveyanswerfrequencyfivescale_fkChanged
		End If
		_question5_surveyanswerfrequencyfivescale_fk = value
		If _ReadingData=True Then _d=False
		_question5_surveyanswerfrequencyfivescale_fk_d=_d
	End Set
End Property

Dim _summary1_surveytimetocomplete_fk as Integer = -1
Dim _summary1_surveytimetocomplete_fk_d as Boolean=False
Public Event summary1_surveytimetocomplete_fkChanged()
Public Property summary1_surveytimetocomplete_fk() as Integer
	Get
		Return _summary1_surveytimetocomplete_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _summary1_surveytimetocomplete_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("summary1_surveytimetocomplete_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("summary1_surveytimetocomplete_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _summary1_surveytimetocomplete_fk <> value then RaiseEvent summary1_surveytimetocomplete_fkChanged
		End If
		_summary1_surveytimetocomplete_fk = value
		If _ReadingData=True Then _d=False
		_summary1_surveytimetocomplete_fk_d=_d
	End Set
End Property

Dim _summary2_surveyeaseofcomplete_fk as Integer = -1
Dim _summary2_surveyeaseofcomplete_fk_d as Boolean=False
Public Event summary2_surveyeaseofcomplete_fkChanged()
Public Property summary2_surveyeaseofcomplete_fk() as Integer
	Get
		Return _summary2_surveyeaseofcomplete_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _summary2_surveyeaseofcomplete_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("summary2_surveyeaseofcomplete_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("summary2_surveyeaseofcomplete_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _summary2_surveyeaseofcomplete_fk <> value then RaiseEvent summary2_surveyeaseofcomplete_fkChanged
		End If
		_summary2_surveyeaseofcomplete_fk = value
		If _ReadingData=True Then _d=False
		_summary2_surveyeaseofcomplete_fk_d=_d
	End Set
End Property

Dim _summary3_otherinformation as String

Dim _summary3_otherinformation_d as Boolean=False
Public Event summary3_otherinformationChanged()
Public Property summary3_otherinformation() as String
	Get
		Return _summary3_otherinformation
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _summary3_otherinformation <> value Then _d=True
		If not _ReadingData=True Then
		If _summary3_otherinformation <> value then RaiseEvent summary3_otherinformationChanged
		End If
		_summary3_otherinformation = value
		If _ReadingData=True Then _d=False
		_summary3_otherinformation_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _surveydetail_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _selecteduser_fk_d
		_IsDirty = _IsDirty OR _supplier_fk_d
		_IsDirty = _IsDirty OR _survey_fk_d
		_IsDirty = _IsDirty OR _question1_surveyanswerfrequencyfivescale_fk_d
		_IsDirty = _IsDirty OR _question2_surveyanswerfrequencyfivescale_fk_d
		_IsDirty = _IsDirty OR _question3_surveyanswersixscale_fk_d
		_IsDirty = _IsDirty OR _question4_surveyanswersixscale_fk_d
		_IsDirty = _IsDirty OR _question5_surveyanswerfrequencyfivescale_fk_d
		_IsDirty = _IsDirty OR _summary1_surveytimetocomplete_fk_d
		_IsDirty = _IsDirty OR _summary2_surveyeaseofcomplete_fk_d
		_IsDirty = _IsDirty OR _summary3_otherinformation_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_surveydetail_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_selecteduser_fk=-1
	_supplier_fk=-1
	_survey_fk=-1
	Dim _dquestion1_surveyanswerfrequencyfivescale_fk as Integer = Nothing: CDC.ReadScalarValue(_dquestion1_surveyanswerfrequencyfivescale_fk, new sqlclient.sqlCommand("select -1"))
	_question1_surveyanswerfrequencyfivescale_fk= _dquestion1_surveyanswerfrequencyfivescale_fk
	Dim _dquestion2_surveyanswerfrequencyfivescale_fk as Integer = Nothing: CDC.ReadScalarValue(_dquestion2_surveyanswerfrequencyfivescale_fk, new sqlclient.sqlCommand("select -1"))
	_question2_surveyanswerfrequencyfivescale_fk= _dquestion2_surveyanswerfrequencyfivescale_fk
	Dim _dquestion3_surveyanswersixscale_fk as Integer = Nothing: CDC.ReadScalarValue(_dquestion3_surveyanswersixscale_fk, new sqlclient.sqlCommand("select -1"))
	_question3_surveyanswersixscale_fk= _dquestion3_surveyanswersixscale_fk
	Dim _dquestion4_surveyanswersixscale_fk as Integer = Nothing: CDC.ReadScalarValue(_dquestion4_surveyanswersixscale_fk, new sqlclient.sqlCommand("select -1"))
	_question4_surveyanswersixscale_fk= _dquestion4_surveyanswersixscale_fk
	Dim _dquestion5_surveyanswerfrequencyfivescale_fk as Integer = Nothing: CDC.ReadScalarValue(_dquestion5_surveyanswerfrequencyfivescale_fk, new sqlclient.sqlCommand("select -1"))
	_question5_surveyanswerfrequencyfivescale_fk= _dquestion5_surveyanswerfrequencyfivescale_fk
	Dim _dsummary1_surveytimetocomplete_fk as Integer = Nothing: CDC.ReadScalarValue(_dsummary1_surveytimetocomplete_fk, new sqlclient.sqlCommand("select -1"))
	_summary1_surveytimetocomplete_fk= _dsummary1_surveytimetocomplete_fk
	Dim _dsummary2_surveyeaseofcomplete_fk as Integer = Nothing: CDC.ReadScalarValue(_dsummary2_surveyeaseofcomplete_fk, new sqlclient.sqlCommand("select -1"))
	_summary2_surveyeaseofcomplete_fk= _dsummary2_surveyeaseofcomplete_fk
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("survey3details",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_survey3details_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@surveydetail_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_surveydetail_pk=CommonFN.SafeRead(Row("surveydetail_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		selecteduser_fk=CommonFN.SafeRead(Row("selecteduser_fk"))
		supplier_fk=CommonFN.SafeRead(Row("supplier_fk"))
		survey_fk=CommonFN.SafeRead(Row("survey_fk"))
		question1_surveyanswerfrequencyfivescale_fk=CommonFN.SafeRead(Row("question1_surveyanswerfrequencyfivescale_fk"))
		question2_surveyanswerfrequencyfivescale_fk=CommonFN.SafeRead(Row("question2_surveyanswerfrequencyfivescale_fk"))
		question3_surveyanswersixscale_fk=CommonFN.SafeRead(Row("question3_surveyanswersixscale_fk"))
		question4_surveyanswersixscale_fk=CommonFN.SafeRead(Row("question4_surveyanswersixscale_fk"))
		question5_surveyanswerfrequencyfivescale_fk=CommonFN.SafeRead(Row("question5_surveyanswerfrequencyfivescale_fk"))
		summary1_surveytimetocomplete_fk=CommonFN.SafeRead(Row("summary1_surveytimetocomplete_fk"))
		summary2_surveyeaseofcomplete_fk=CommonFN.SafeRead(Row("summary2_surveyeaseofcomplete_fk"))
		summary3_otherinformation=CommonFN.SafeRead(Row("summary3_otherinformation"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_survey3details_exists"
	PO = New SqlClient.SqlParameter("@selecteduser_fk", selecteduser_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@supplier_fk", supplier_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@survey_fk", survey_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question1_surveyanswerfrequencyfivescale_fk", question1_surveyanswerfrequencyfivescale_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question2_surveyanswerfrequencyfivescale_fk", question2_surveyanswerfrequencyfivescale_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question3_surveyanswersixscale_fk", question3_surveyanswersixscale_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question4_surveyanswersixscale_fk", question4_surveyanswersixscale_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@question5_surveyanswerfrequencyfivescale_fk", question5_surveyanswerfrequencyfivescale_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@summary1_surveytimetocomplete_fk", summary1_surveytimetocomplete_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@summary2_surveyeaseofcomplete_fk", summary2_surveyeaseofcomplete_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_survey3details_compare"
	PO = New SqlClient.SqlParameter("@surveydetail_pk", surveydetail_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@summary3_otherinformation", summary3_otherinformation)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and surveydetail_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_survey3details_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@surveydetail_pk", surveydetail_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@selecteduser_fk", selecteduser_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@supplier_fk", supplier_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@survey_fk", survey_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tquestion1_surveyanswerfrequencyfivescale_fk as Integer  = Nothing
		If question1_surveyanswerfrequencyfivescale_fk= nothing  then
			CDC.ReadScalarValue(_tquestion1_surveyanswerfrequencyfivescale_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@question1_surveyanswerfrequencyfivescale_fk", _tquestion1_surveyanswerfrequencyfivescale_fk)
		Else
			PO = New SqlClient.SqlParameter("@question1_surveyanswerfrequencyfivescale_fk", question1_surveyanswerfrequencyfivescale_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tquestion2_surveyanswerfrequencyfivescale_fk as Integer  = Nothing
		If question2_surveyanswerfrequencyfivescale_fk= nothing  then
			CDC.ReadScalarValue(_tquestion2_surveyanswerfrequencyfivescale_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@question2_surveyanswerfrequencyfivescale_fk", _tquestion2_surveyanswerfrequencyfivescale_fk)
		Else
			PO = New SqlClient.SqlParameter("@question2_surveyanswerfrequencyfivescale_fk", question2_surveyanswerfrequencyfivescale_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tquestion3_surveyanswersixscale_fk as Integer  = Nothing
		If question3_surveyanswersixscale_fk= nothing  then
			CDC.ReadScalarValue(_tquestion3_surveyanswersixscale_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@question3_surveyanswersixscale_fk", _tquestion3_surveyanswersixscale_fk)
		Else
			PO = New SqlClient.SqlParameter("@question3_surveyanswersixscale_fk", question3_surveyanswersixscale_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tquestion4_surveyanswersixscale_fk as Integer  = Nothing
		If question4_surveyanswersixscale_fk= nothing  then
			CDC.ReadScalarValue(_tquestion4_surveyanswersixscale_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@question4_surveyanswersixscale_fk", _tquestion4_surveyanswersixscale_fk)
		Else
			PO = New SqlClient.SqlParameter("@question4_surveyanswersixscale_fk", question4_surveyanswersixscale_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tquestion5_surveyanswerfrequencyfivescale_fk as Integer  = Nothing
		If question5_surveyanswerfrequencyfivescale_fk= nothing  then
			CDC.ReadScalarValue(_tquestion5_surveyanswerfrequencyfivescale_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@question5_surveyanswerfrequencyfivescale_fk", _tquestion5_surveyanswerfrequencyfivescale_fk)
		Else
			PO = New SqlClient.SqlParameter("@question5_surveyanswerfrequencyfivescale_fk", question5_surveyanswerfrequencyfivescale_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tsummary1_surveytimetocomplete_fk as Integer  = Nothing
		If summary1_surveytimetocomplete_fk= nothing  then
			CDC.ReadScalarValue(_tsummary1_surveytimetocomplete_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@summary1_surveytimetocomplete_fk", _tsummary1_surveytimetocomplete_fk)
		Else
			PO = New SqlClient.SqlParameter("@summary1_surveytimetocomplete_fk", summary1_surveytimetocomplete_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tsummary2_surveyeaseofcomplete_fk as Integer  = Nothing
		If summary2_surveyeaseofcomplete_fk= nothing  then
			CDC.ReadScalarValue(_tsummary2_surveyeaseofcomplete_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@summary2_surveyeaseofcomplete_fk", _tsummary2_surveyeaseofcomplete_fk)
		Else
			PO = New SqlClient.SqlParameter("@summary2_surveyeaseofcomplete_fk", summary2_surveyeaseofcomplete_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@summary3_otherinformation", summary3_otherinformation)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _surveydetail_pk_d then CDC.AuditLog("survey3details",_returnPK,"surveydetail_pk",_CurrentUser,modified,CStr(surveydetail_pk)):_surveydetail_pk_d=False
				If _creator_fk_d then CDC.AuditLog("survey3details",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("survey3details",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("survey3details",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("survey3details",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("survey3details",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _selecteduser_fk_d then CDC.AuditLog("survey3details",_returnPK,"selecteduser_fk",_CurrentUser,modified,CStr(selecteduser_fk)):_selecteduser_fk_d=False
				If _supplier_fk_d then CDC.AuditLog("survey3details",_returnPK,"supplier_fk",_CurrentUser,modified,CStr(supplier_fk)):_supplier_fk_d=False
				If _survey_fk_d then CDC.AuditLog("survey3details",_returnPK,"survey_fk",_CurrentUser,modified,CStr(survey_fk)):_survey_fk_d=False
				If _question1_surveyanswerfrequencyfivescale_fk_d then CDC.AuditLog("survey3details",_returnPK,"question1_surveyanswerfrequencyfivescale_fk",_CurrentUser,modified,CStr(question1_surveyanswerfrequencyfivescale_fk)):_question1_surveyanswerfrequencyfivescale_fk_d=False
				If _question2_surveyanswerfrequencyfivescale_fk_d then CDC.AuditLog("survey3details",_returnPK,"question2_surveyanswerfrequencyfivescale_fk",_CurrentUser,modified,CStr(question2_surveyanswerfrequencyfivescale_fk)):_question2_surveyanswerfrequencyfivescale_fk_d=False
				If _question3_surveyanswersixscale_fk_d then CDC.AuditLog("survey3details",_returnPK,"question3_surveyanswersixscale_fk",_CurrentUser,modified,CStr(question3_surveyanswersixscale_fk)):_question3_surveyanswersixscale_fk_d=False
				If _question4_surveyanswersixscale_fk_d then CDC.AuditLog("survey3details",_returnPK,"question4_surveyanswersixscale_fk",_CurrentUser,modified,CStr(question4_surveyanswersixscale_fk)):_question4_surveyanswersixscale_fk_d=False
				If _question5_surveyanswerfrequencyfivescale_fk_d then CDC.AuditLog("survey3details",_returnPK,"question5_surveyanswerfrequencyfivescale_fk",_CurrentUser,modified,CStr(question5_surveyanswerfrequencyfivescale_fk)):_question5_surveyanswerfrequencyfivescale_fk_d=False
				If _summary1_surveytimetocomplete_fk_d then CDC.AuditLog("survey3details",_returnPK,"summary1_surveytimetocomplete_fk",_CurrentUser,modified,CStr(summary1_surveytimetocomplete_fk)):_summary1_surveytimetocomplete_fk_d=False
				If _summary2_surveyeaseofcomplete_fk_d then CDC.AuditLog("survey3details",_returnPK,"summary2_surveyeaseofcomplete_fk",_CurrentUser,modified,CStr(summary2_surveyeaseofcomplete_fk)):_summary2_surveyeaseofcomplete_fk_d=False
				If _summary3_otherinformation_d then CDC.AuditLog("survey3details",_returnPK,"summary3_otherinformation",_CurrentUser,modified,CStr(summary3_otherinformation)):_summary3_otherinformation_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
