Imports cca.common
Public Class agreements
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _agreement_pk as Integer

Dim _agreement_pk_d as Boolean=False
Public Event agreement_pkChanged()
Public ReadOnly Property agreement_pk() as Integer
	Get
		Return _agreement_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _organisation_fk as Integer

Dim _organisation_fk_d as Boolean=False
Public Event organisation_fkChanged()
Public Property organisation_fk() as Integer
	Get
		Return _organisation_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _organisation_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("organisation_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Organisation"), CStr("Validation failed: Required Field"))
		If _organisation_fk <> value then RaiseEvent organisation_fkChanged
		End If
		_organisation_fk = value
		If _ReadingData=True Then _d=False
		_organisation_fk_d=_d
	End Set
End Property

Dim _agreementname as String

Dim _agreementname_d as Boolean=False
Public Event agreementnameChanged()
Public Property agreementname() as String
	Get
		Return _agreementname
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _agreementname <> value Then _d=True
		If not _ReadingData=True Then
		If _agreementname <> value then RaiseEvent agreementnameChanged
		End If
		_agreementname = value
		If _ReadingData=True Then _d=False
		_agreementname_d=_d
	End Set
End Property

Dim _federation_fk as Integer

Dim _federation_fk_d as Boolean=False
Public Event federation_fkChanged()
Public Property federation_fk() as Integer
	Get
		Return _federation_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _federation_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("federation_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Federation"), CStr("Validation failed: Required Field"))
		If _federation_fk <> value then RaiseEvent federation_fkChanged
		End If
		_federation_fk = value
		If _ReadingData=True Then _d=False
		_federation_fk_d=_d
	End Set
End Property

Dim _agreementnumber as String

Dim _agreementnumber_d as Boolean=False
Public Event agreementnumberChanged()
Public Property agreementnumber() as String
	Get
		Return _agreementnumber
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _agreementnumber <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Agreement Number"), CStr("Validation failed: Required Field"))
		If _agreementnumber <> value then RaiseEvent agreementnumberChanged
		End If
		_agreementnumber = value
		If _ReadingData=True Then _d=False
		_agreementnumber_d=_d
	End Set
End Property

Dim _facilitynumber as String

Dim _facilitynumber_d as Boolean=False
Public Event facilitynumberChanged()
Public Property facilitynumber() as String
	Get
		Return _facilitynumber
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _facilitynumber <> value Then _d=True
		If not _ReadingData=True Then
		If _facilitynumber <> value then RaiseEvent facilitynumberChanged
		End If
		_facilitynumber = value
		If _ReadingData=True Then _d=False
		_facilitynumber_d=_d
	End Set
End Property

Dim _targetunitidentifier as String

Dim _targetunitidentifier_d as Boolean=False
Public Event targetunitidentifierChanged()
Public Property targetunitidentifier() as String
	Get
		Return _targetunitidentifier
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _targetunitidentifier <> value Then _d=True
		If not _ReadingData=True Then
		If _targetunitidentifier <> value then RaiseEvent targetunitidentifierChanged
		End If
		_targetunitidentifier = value
		If _ReadingData=True Then _d=False
		_targetunitidentifier_d=_d
	End Set
End Property

Dim _baseyearstart as Datetime

Dim _baseyearstart_d as Boolean=False
Public Event baseyearstartChanged()
Public Property baseyearstart() as Datetime
	Get
		Return _baseyearstart
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _baseyearstart <> value Then _d=True
		If not _ReadingData=True Then
		If _baseyearstart <> value then RaiseEvent baseyearstartChanged
		End If
		_baseyearstart = value
		If _ReadingData=True Then _d=False
		_baseyearstart_d=_d
	End Set
End Property

Dim _baseyearend as Datetime

Dim _baseyearend_d as Boolean=False
Public Event baseyearendChanged()
Public Property baseyearend() as Datetime
	Get
		Return _baseyearend
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _baseyearend <> value Then _d=True
		If not _ReadingData=True Then
		If _baseyearend <> value then RaiseEvent baseyearendChanged
		End If
		_baseyearend = value
		If _ReadingData=True Then _d=False
		_baseyearend_d=_d
	End Set
End Property

Dim _agreementtype_fk as Integer

Dim _agreementtype_fk_d as Boolean=False
Public Event agreementtype_fkChanged()
Public Property agreementtype_fk() as Integer
	Get
		Return _agreementtype_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _agreementtype_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("agreementtype_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Agreement Type"), CStr("Validation failed: Required Field"))
		If _agreementtype_fk <> value then RaiseEvent agreementtype_fkChanged
		End If
		_agreementtype_fk = value
		If _ReadingData=True Then _d=False
		_agreementtype_fk_d=_d
	End Set
End Property

Dim _agreementmeasure_fk as Integer

Dim _agreementmeasure_fk_d as Boolean=False
Public Event agreementmeasure_fkChanged()
Public Property agreementmeasure_fk() as Integer
	Get
		Return _agreementmeasure_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _agreementmeasure_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("agreementmeasure_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Agreement Measure"), CStr("Validation failed: Required Field"))
		If _agreementmeasure_fk <> value then RaiseEvent agreementmeasure_fkChanged
		End If
		_agreementmeasure_fk = value
		If _ReadingData=True Then _d=False
		_agreementmeasure_fk_d=_d
	End Set
End Property

Dim _agreement_fk as Integer = 0
Dim _agreement_fk_d as Boolean=False
Public Event agreement_fkChanged()
Public Property agreement_fk() as Integer
	Get
		Return _agreement_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _agreement_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("agreement_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Original Agreement"), CStr("Validation failed: Invalid Foreign Key"))
		If _agreement_fk <> value then RaiseEvent agreement_fkChanged
		End If
		_agreement_fk = value
		If _ReadingData=True Then _d=False
		_agreement_fk_d=_d
	End Set
End Property

Dim _submeterinstalled as Boolean = False
Dim _submeterinstalled_d as Boolean=False
Public Event submeterinstalledChanged()
Public Property submeterinstalled() as Boolean
	Get
		Return _submeterinstalled
	End Get
	Set (ByVal value As Boolean)
		Dim _d as boolean=false
		If _submeterinstalled <> value Then _d=True
		If not _ReadingData=True Then
		If _submeterinstalled <> value then RaiseEvent submeterinstalledChanged
		End If
		_submeterinstalled = value
		If _ReadingData=True Then _d=False
		_submeterinstalled_d=_d
	End Set
End Property

Dim _notes as String

Dim _notes_d as Boolean=False
Public Event notesChanged()
Public Property notes() as String
	Get
		Return _notes
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _notes <> value Then _d=True
		If not _ReadingData=True Then
		If _notes <> value then RaiseEvent notesChanged
		End If
		_notes = value
		If _ReadingData=True Then _d=False
		_notes_d=_d
	End Set
End Property

Dim _agreementstart as Datetime

Dim _agreementstart_d as Boolean=False
Public Event agreementstartChanged()
Public Property agreementstart() as Datetime
	Get
		Return _agreementstart
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _agreementstart <> value Then _d=True
		If not _ReadingData=True Then
		If _agreementstart <> value then RaiseEvent agreementstartChanged
		End If
		_agreementstart = value
		If _ReadingData=True Then _d=False
		_agreementstart_d=_d
	End Set
End Property

Dim _includecrcsaving as Boolean = False
Dim _includecrcsaving_d as Boolean=False
Public Event includecrcsavingChanged()
Public Property includecrcsaving() as Boolean
	Get
		Return _includecrcsaving
	End Get
	Set (ByVal value As Boolean)
		Dim _d as boolean=false
		If _includecrcsaving <> value Then _d=True
		If not _ReadingData=True Then
		If _includecrcsaving <> value then RaiseEvent includecrcsavingChanged
		End If
		_includecrcsaving = value
		If _ReadingData=True Then _d=False
		_includecrcsaving_d=_d
	End Set
End Property

Dim _technicaluser_fk as Integer = -1
Dim _technicaluser_fk_d as Boolean=False
Public Event technicaluser_fkChanged()
Public Property technicaluser_fk() as Integer
	Get
		Return _technicaluser_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _technicaluser_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("technicaluser_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Technical Analyst"), CStr("Validation failed: Invalid Foreign Key"))
		If _technicaluser_fk <> value then RaiseEvent technicaluser_fkChanged
		End If
		_technicaluser_fk = value
		If _ReadingData=True Then _d=False
		_technicaluser_fk_d=_d
	End Set
End Property

Dim _reportinguser_fk as Integer = -1
Dim _reportinguser_fk_d as Boolean=False
Public Event reportinguser_fkChanged()
Public Property reportinguser_fk() as Integer
	Get
		Return _reportinguser_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _reportinguser_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("reportinguser_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Reporting Analyst"), CStr("Validation failed: Invalid Foreign Key"))
		If _reportinguser_fk <> value then RaiseEvent reportinguser_fkChanged
		End If
		_reportinguser_fk = value
		If _ReadingData=True Then _d=False
		_reportinguser_fk_d=_d
	End Set
End Property

Dim _accountmanageruser_fk as Integer = -1
Dim _accountmanageruser_fk_d as Boolean=False
Public Event accountmanageruser_fkChanged()
Public Property accountmanageruser_fk() as Integer
	Get
		Return _accountmanageruser_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _accountmanageruser_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("accountmanageruser_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("Account Manager"), CStr("Validation failed: Invalid Foreign Key"))
		If _accountmanageruser_fk <> value then RaiseEvent accountmanageruser_fkChanged
		End If
		_accountmanageruser_fk = value
		If _ReadingData=True Then _d=False
		_accountmanageruser_fk_d=_d
	End Set
End Property

Dim _agreementstage_fk as Integer

Dim _agreementstage_fk_d as Boolean=False
Public Event agreementstage_fkChanged()
Public Property agreementstage_fk() as Integer
	Get
		Return _agreementstage_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _agreementstage_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("agreementstage_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("agreementstage_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _agreementstage_fk <> value then RaiseEvent agreementstage_fkChanged
		End If
		_agreementstage_fk = value
		If _ReadingData=True Then _d=False
		_agreementstage_fk_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _agreement_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _organisation_fk_d
		_IsDirty = _IsDirty OR _agreementname_d
		_IsDirty = _IsDirty OR _federation_fk_d
		_IsDirty = _IsDirty OR _agreementnumber_d
		_IsDirty = _IsDirty OR _facilitynumber_d
		_IsDirty = _IsDirty OR _targetunitidentifier_d
		_IsDirty = _IsDirty OR _baseyearstart_d
		_IsDirty = _IsDirty OR _baseyearend_d
		_IsDirty = _IsDirty OR _agreementtype_fk_d
		_IsDirty = _IsDirty OR _agreementmeasure_fk_d
		_IsDirty = _IsDirty OR _agreement_fk_d
		_IsDirty = _IsDirty OR _submeterinstalled_d
		_IsDirty = _IsDirty OR _notes_d
		_IsDirty = _IsDirty OR _agreementstart_d
		_IsDirty = _IsDirty OR _includecrcsaving_d
		_IsDirty = _IsDirty OR _technicaluser_fk_d
		_IsDirty = _IsDirty OR _reportinguser_fk_d
		_IsDirty = _IsDirty OR _accountmanageruser_fk_d
		_IsDirty = _IsDirty OR _agreementstage_fk_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_agreement_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_organisation_fk=-1
	_federation_fk=-1
	_agreementtype_fk=-1
	_agreementmeasure_fk=-1
	_agreement_fk=-1
	Dim _dtechnicaluser_fk as Integer = Nothing: CDC.ReadScalarValue(_dtechnicaluser_fk, new sqlclient.sqlCommand("select -1"))
	_technicaluser_fk= _dtechnicaluser_fk
	Dim _dreportinguser_fk as Integer = Nothing: CDC.ReadScalarValue(_dreportinguser_fk, new sqlclient.sqlCommand("select -1"))
	_reportinguser_fk= _dreportinguser_fk
	Dim _daccountmanageruser_fk as Integer = Nothing: CDC.ReadScalarValue(_daccountmanageruser_fk, new sqlclient.sqlCommand("select -1"))
	_accountmanageruser_fk= _daccountmanageruser_fk
	_agreementstage_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("agreements",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_agreements_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@agreement_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_agreement_pk=CommonFN.SafeRead(Row("agreement_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		organisation_fk=CommonFN.SafeRead(Row("organisation_fk"))
		agreementname=CommonFN.SafeRead(Row("agreementname"))
		federation_fk=CommonFN.SafeRead(Row("federation_fk"))
		agreementnumber=CommonFN.SafeRead(Row("agreementnumber"))
		facilitynumber=CommonFN.SafeRead(Row("facilitynumber"))
		targetunitidentifier=CommonFN.SafeRead(Row("targetunitidentifier"))
		baseyearstart=CommonFN.SafeRead(Row("baseyearstart"))
		baseyearend=CommonFN.SafeRead(Row("baseyearend"))
		agreementtype_fk=CommonFN.SafeRead(Row("agreementtype_fk"))
		agreementmeasure_fk=CommonFN.SafeRead(Row("agreementmeasure_fk"))
		agreement_fk=CommonFN.SafeRead(Row("agreement_fk"))
		submeterinstalled=CommonFN.SafeRead(Row("submeterinstalled"))
		notes=CommonFN.SafeRead(Row("notes"))
		agreementstart=CommonFN.SafeRead(Row("agreementstart"))
		includecrcsaving=CommonFN.SafeRead(Row("includecrcsaving"))
		technicaluser_fk=CommonFN.SafeRead(Row("technicaluser_fk"))
		reportinguser_fk=CommonFN.SafeRead(Row("reportinguser_fk"))
		accountmanageruser_fk=CommonFN.SafeRead(Row("accountmanageruser_fk"))
		agreementstage_fk=CommonFN.SafeRead(Row("agreementstage_fk"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_agreements_exists"
	PO = New SqlClient.SqlParameter("@organisation_fk", organisation_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@federation_fk", federation_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@agreementtype_fk", agreementtype_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@agreementmeasure_fk", agreementmeasure_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@agreement_fk", agreement_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@technicaluser_fk", technicaluser_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@reportinguser_fk", reportinguser_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@accountmanageruser_fk", accountmanageruser_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@agreementstage_fk", agreementstage_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_agreements_compare"
	PO = New SqlClient.SqlParameter("@agreement_pk", agreement_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@agreementname", agreementname)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@agreementnumber", agreementnumber)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@facilitynumber", facilitynumber)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@targetunitidentifier", targetunitidentifier)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@baseyearstart", CommonFN.CheckEmptyDate(baseyearstart.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@baseyearend", CommonFN.CheckEmptyDate(baseyearend.ToString()))
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@notes", notes)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@agreementstart", CommonFN.CheckEmptyDate(agreementstart.ToString()))
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and agreement_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_agreements_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@agreement_pk", agreement_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@organisation_fk", organisation_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@agreementname", agreementname)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@federation_fk", federation_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@agreementnumber", agreementnumber)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@facilitynumber", facilitynumber)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@targetunitidentifier", targetunitidentifier)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@baseyearstart", CommonFN.CheckEmptyDate(baseyearstart.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@baseyearend", CommonFN.CheckEmptyDate(baseyearend.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@agreementtype_fk", agreementtype_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@agreementmeasure_fk", agreementmeasure_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tagreement_fk as Integer  = Nothing
		If agreement_fk= nothing  then
			CDC.ReadScalarValue(_tagreement_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@agreement_fk", _tagreement_fk)
		Else
			PO = New SqlClient.SqlParameter("@agreement_fk", agreement_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tsubmeterinstalled as Boolean  = Nothing
		If submeterinstalled= nothing  then
			CDC.ReadScalarValue(_tsubmeterinstalled, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@submeterinstalled", _tsubmeterinstalled)
		Else
			PO = New SqlClient.SqlParameter("@submeterinstalled", submeterinstalled)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@notes", notes)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@agreementstart", CommonFN.CheckEmptyDate(agreementstart.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tincludecrcsaving as Boolean  = Nothing
		If includecrcsaving= nothing  then
			CDC.ReadScalarValue(_tincludecrcsaving, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@includecrcsaving", _tincludecrcsaving)
		Else
			PO = New SqlClient.SqlParameter("@includecrcsaving", includecrcsaving)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _ttechnicaluser_fk as Integer  = Nothing
		If technicaluser_fk= nothing  then
			CDC.ReadScalarValue(_ttechnicaluser_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@technicaluser_fk", _ttechnicaluser_fk)
		Else
			PO = New SqlClient.SqlParameter("@technicaluser_fk", technicaluser_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _treportinguser_fk as Integer  = Nothing
		If reportinguser_fk= nothing  then
			CDC.ReadScalarValue(_treportinguser_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@reportinguser_fk", _treportinguser_fk)
		Else
			PO = New SqlClient.SqlParameter("@reportinguser_fk", reportinguser_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _taccountmanageruser_fk as Integer  = Nothing
		If accountmanageruser_fk= nothing  then
			CDC.ReadScalarValue(_taccountmanageruser_fk, new sqlclient.sqlCommand("select -1"))
			PO = New SqlClient.SqlParameter("@accountmanageruser_fk", _taccountmanageruser_fk)
		Else
			PO = New SqlClient.SqlParameter("@accountmanageruser_fk", accountmanageruser_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@agreementstage_fk", agreementstage_fk)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _agreement_pk_d then CDC.AuditLog("agreements",_returnPK,"agreement_pk",_CurrentUser,modified,CStr(agreement_pk)):_agreement_pk_d=False
				If _creator_fk_d then CDC.AuditLog("agreements",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("agreements",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("agreements",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("agreements",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("agreements",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _organisation_fk_d then CDC.AuditLog("agreements",_returnPK,"organisation_fk",_CurrentUser,modified,CStr(organisation_fk)):_organisation_fk_d=False
				If _agreementname_d then CDC.AuditLog("agreements",_returnPK,"agreementname",_CurrentUser,modified,CStr(agreementname)):_agreementname_d=False
				If _federation_fk_d then CDC.AuditLog("agreements",_returnPK,"federation_fk",_CurrentUser,modified,CStr(federation_fk)):_federation_fk_d=False
				If _agreementnumber_d then CDC.AuditLog("agreements",_returnPK,"agreementnumber",_CurrentUser,modified,CStr(agreementnumber)):_agreementnumber_d=False
				If _facilitynumber_d then CDC.AuditLog("agreements",_returnPK,"facilitynumber",_CurrentUser,modified,CStr(facilitynumber)):_facilitynumber_d=False
				If _targetunitidentifier_d then CDC.AuditLog("agreements",_returnPK,"targetunitidentifier",_CurrentUser,modified,CStr(targetunitidentifier)):_targetunitidentifier_d=False
				If _baseyearstart_d then CDC.AuditLog("agreements",_returnPK,"baseyearstart",_CurrentUser,modified,CStr(baseyearstart)):_baseyearstart_d=False
				If _baseyearend_d then CDC.AuditLog("agreements",_returnPK,"baseyearend",_CurrentUser,modified,CStr(baseyearend)):_baseyearend_d=False
				If _agreementtype_fk_d then CDC.AuditLog("agreements",_returnPK,"agreementtype_fk",_CurrentUser,modified,CStr(agreementtype_fk)):_agreementtype_fk_d=False
				If _agreementmeasure_fk_d then CDC.AuditLog("agreements",_returnPK,"agreementmeasure_fk",_CurrentUser,modified,CStr(agreementmeasure_fk)):_agreementmeasure_fk_d=False
				If _agreement_fk_d then CDC.AuditLog("agreements",_returnPK,"agreement_fk",_CurrentUser,modified,CStr(agreement_fk)):_agreement_fk_d=False
				If _submeterinstalled_d then CDC.AuditLog("agreements",_returnPK,"submeterinstalled",_CurrentUser,modified,CStr(submeterinstalled)):_submeterinstalled_d=False
				If _notes_d then CDC.AuditLog("agreements",_returnPK,"notes",_CurrentUser,modified,CStr(notes)):_notes_d=False
				If _agreementstart_d then CDC.AuditLog("agreements",_returnPK,"agreementstart",_CurrentUser,modified,CStr(agreementstart)):_agreementstart_d=False
				If _includecrcsaving_d then CDC.AuditLog("agreements",_returnPK,"includecrcsaving",_CurrentUser,modified,CStr(includecrcsaving)):_includecrcsaving_d=False
				If _technicaluser_fk_d then CDC.AuditLog("agreements",_returnPK,"technicaluser_fk",_CurrentUser,modified,CStr(technicaluser_fk)):_technicaluser_fk_d=False
				If _reportinguser_fk_d then CDC.AuditLog("agreements",_returnPK,"reportinguser_fk",_CurrentUser,modified,CStr(reportinguser_fk)):_reportinguser_fk_d=False
				If _accountmanageruser_fk_d then CDC.AuditLog("agreements",_returnPK,"accountmanageruser_fk",_CurrentUser,modified,CStr(accountmanageruser_fk)):_accountmanageruser_fk_d=False
				If _agreementstage_fk_d then CDC.AuditLog("agreements",_returnPK,"agreementstage_fk",_CurrentUser,modified,CStr(agreementstage_fk)):_agreementstage_fk_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
