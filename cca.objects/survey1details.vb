Imports cca.common
Public Class survey1details
    Dim _ReadingData As Boolean = False
    Dim _LastError As Exception = Nothing

    Public ReadOnly Property LastError() As Exception
        Get
            Return _LastError
        End Get
    End Property

    Dim _surveydetail_pk As Integer

    Dim _surveydetail_pk_d As Boolean = False
    Public Event surveydetail_pkChanged()
    Public ReadOnly Property surveydetail_pk() As Integer
        Get
            Return _surveydetail_pk
        End Get
    End Property

    Dim _creator_fk As Integer

    Dim _creator_fk_d As Boolean = False
    Public Event creator_fkChanged()
    Public Property creator_fk() As Integer
        Get
            Return _creator_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _creator_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("creator_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _creator_fk <> value Then RaiseEvent creator_fkChanged()
            End If
            _creator_fk = value
            If _ReadingData = True Then _d = False
            _creator_fk_d = _d
        End Set
    End Property

    Dim _editor_fk As Integer

    Dim _editor_fk_d As Boolean = False
    Public Event editor_fkChanged()
    Public Property editor_fk() As Integer
        Get
            Return _editor_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _editor_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("editor_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
                If _editor_fk <> value Then RaiseEvent editor_fkChanged()
            End If
            _editor_fk = value
            If _ReadingData = True Then _d = False
            _editor_fk_d = _d
        End Set
    End Property

    Dim _created As Datetime

    Dim _created_d As Boolean = False
    Public Event createdChanged()
    Public Property created() As Datetime
        Get
            Return _created
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _created <> value Then _d = True
            If Not _ReadingData = True Then
                If _created <> value Then RaiseEvent createdChanged()
            End If
            _created = value
            If _ReadingData = True Then _d = False
            _created_d = _d
        End Set
    End Property

    Dim _modified As Datetime

    Dim _modified_d As Boolean = False
    Public Event modifiedChanged()
    Public Property modified() As Datetime
        Get
            Return _modified
        End Get
        Set(ByVal value As Datetime)
            Dim _d As Boolean = False
            If _modified <> value Then _d = True
            If Not _ReadingData = True Then
                If _modified <> value Then RaiseEvent modifiedChanged()
            End If
            _modified = value
            If _ReadingData = True Then _d = False
            _modified_d = _d
        End Set
    End Property

    Dim _rowstatus As Integer = 0
    Dim _rowstatus_d As Boolean = False
    Public Event rowstatusChanged()
    Public Property rowstatus() As Integer
        Get
            Return _rowstatus
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If _rowstatus <> value Then _d = True
            If Not _ReadingData = True Then
                If _rowstatus <> value Then RaiseEvent rowstatusChanged()
            End If
            _rowstatus = value
            If _ReadingData = True Then _d = False
            _rowstatus_d = _d
        End Set
    End Property

    Dim _helpdesk_id As Integer

    Dim _helpdesk_id_d As Boolean = False
    Public Event helpdesk_idChanged()
    Public Property helpdesk_id() As Integer
        Get
            Return _helpdesk_id
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If _helpdesk_id <> value Then _d = True
            If Not _ReadingData = True Then
                If _helpdesk_id <> value Then RaiseEvent helpdesk_idChanged()
            End If
            _helpdesk_id = value
            If _ReadingData = True Then _d = False
            _helpdesk_id_d = _d
        End Set
    End Property

    Dim _selecteduser_fk As Integer

    Dim _selecteduser_fk_d As Boolean = False
    Public Event selecteduser_fkChanged()
    Public Property selecteduser_fk() As Integer
        Get
            Return _selecteduser_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _selecteduser_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("selecteduser_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("User"), CStr("Validation failed: Invalid Foreign Key"))
                If _selecteduser_fk <> value Then RaiseEvent selecteduser_fkChanged()
            End If
            _selecteduser_fk = value
            If _ReadingData = True Then _d = False
            _selecteduser_fk_d = _d
        End Set
    End Property

    Dim _question1_surveyanswerfivescale_fk As Integer

    Dim _question1_surveyanswerfivescale_fk_d As Boolean = False
    Public Event question1_surveyanswerfivescale_fkChanged()
    Public Property question1_surveyanswerfivescale_fk() As Integer
        Get
            Return _question1_surveyanswerfivescale_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _question1_surveyanswerfivescale_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("question1_surveyanswerfivescale_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("With the accuracy of the SSRs I received"), CStr("Validation failed: Invalid Foreign Key"))
                If _question1_surveyanswerfivescale_fk <> value Then RaiseEvent question1_surveyanswerfivescale_fkChanged()
            End If
            _question1_surveyanswerfivescale_fk = value
            If _ReadingData = True Then _d = False
            _question1_surveyanswerfivescale_fk_d = _d
        End Set
    End Property

    Dim _question2_surveyanswerfivescale_fk As Integer

    Dim _question2_surveyanswerfivescale_fk_d As Boolean = False
    Public Event question2_surveyanswerfivescale_fkChanged()
    Public Property question2_surveyanswerfivescale_fk() As Integer
        Get
            Return _question2_surveyanswerfivescale_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _question2_surveyanswerfivescale_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("question2_surveyanswerfivescale_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("With the quality (format/style) of the SSRs I received"), CStr("Validation failed: Invalid Foreign Key"))
                If _question2_surveyanswerfivescale_fk <> value Then RaiseEvent question2_surveyanswerfivescale_fkChanged()
            End If
            _question2_surveyanswerfivescale_fk = value
            If _ReadingData = True Then _d = False
            _question2_surveyanswerfivescale_fk_d = _d
        End Set
    End Property

    Dim _question3_surveyanswerfivescale_fk As Integer

    Dim _question3_surveyanswerfivescale_fk_d As Boolean = False
    Public Event question3_surveyanswerfivescale_fkChanged()
    Public Property question3_surveyanswerfivescale_fk() As Integer
        Get
            Return _question3_surveyanswerfivescale_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _question3_surveyanswerfivescale_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("question3_surveyanswerfivescale_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("With the direction/recommendation that the Sourcing team provided"), CStr("Validation failed: Invalid Foreign Key"))
                If _question3_surveyanswerfivescale_fk <> value Then RaiseEvent question3_surveyanswerfivescale_fkChanged()
            End If
            _question3_surveyanswerfivescale_fk = value
            If _ReadingData = True Then _d = False
            _question3_surveyanswerfivescale_fk_d = _d
        End Set
    End Property

    Dim _question4_surveyanswerfivescale_fk As Integer

    Dim _question4_surveyanswerfivescale_fk_d As Boolean = False
    Public Event question4_surveyanswerfivescale_fkChanged()
    Public Property question4_surveyanswerfivescale_fk() As Integer
        Get
            Return _question4_surveyanswerfivescale_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _question4_surveyanswerfivescale_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("question4_surveyanswerfivescale_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("With the speed of response from the Sourcing team"), CStr("Validation failed: Invalid Foreign Key"))
                If _question4_surveyanswerfivescale_fk <> value Then RaiseEvent question4_surveyanswerfivescale_fkChanged()
            End If
            _question4_surveyanswerfivescale_fk = value
            If _ReadingData = True Then _d = False
            _question4_surveyanswerfivescale_fk_d = _d
        End Set
    End Property

    Dim _question5_surveyanswerfivescale_fk As Integer

    Dim _question5_surveyanswerfivescale_fk_d As Boolean = False
    Public Event question5_surveyanswerfivescale_fkChanged()
    Public Property question5_surveyanswerfivescale_fk() As Integer
        Get
            Return _question5_surveyanswerfivescale_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _question5_surveyanswerfivescale_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("question5_surveyanswerfivescale_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("That the Sourcing team conducted themselves professionally"), CStr("Validation failed: Invalid Foreign Key"))
                If _question5_surveyanswerfivescale_fk <> value Then RaiseEvent question5_surveyanswerfivescale_fkChanged()
            End If
            _question5_surveyanswerfivescale_fk = value
            If _ReadingData = True Then _d = False
            _question5_surveyanswerfivescale_fk_d = _d
        End Set
    End Property

    Dim _comments As String

    Dim _comments_d As Boolean = False
    Public Event commentsChanged()
    Public Property comments() As String
        Get
            Return _comments
        End Get
        Set(ByVal value As String)
            Dim _d As Boolean = False
            If _comments <> value Then _d = True
            If Not _ReadingData = True Then
                If _comments <> value Then RaiseEvent commentsChanged()
            End If
            _comments = value
            If _ReadingData = True Then _d = False
            _comments_d = _d
        End Set
    End Property

    Dim _question6_surveyanswerfivescale_fk As Integer

    Dim _question6_surveyanswerfivescale_fk_d As Boolean = False
    Public Event question6_surveyanswerfivescale_fkChanged()
    Public Property question6_surveyanswerfivescale_fk() As Integer
        Get
            Return _question6_surveyanswerfivescale_fk
        End Get
        Set(ByVal value As Integer)
            Dim _d As Boolean = False
            If value = 0 Or value = Nothing Then value = -1
            If _question6_surveyanswerfivescale_fk <> value Then _d = True
            If Not _ReadingData = True Then
                If Not CommonFN.IsValidPk("question6_surveyanswerfivescale_fk", value, CDC) Then Throw New ArgumentOutOfRangeException(CStr("The Client was satisfied with the overall quality of the Sourcing service"), CStr("Validation failed: Invalid Foreign Key"))
                If _question6_surveyanswerfivescale_fk <> value Then RaiseEvent question6_surveyanswerfivescale_fkChanged()
            End If
            _question6_surveyanswerfivescale_fk = value
            If _ReadingData = True Then _d = False
            _question6_surveyanswerfivescale_fk_d = _d
        End Set
    End Property

    Dim _comments2 As String

    Dim _comments2_d As Boolean = False
    Public Event comments2Changed()
    Public Property comments2() As String
        Get
            Return _comments2
        End Get
        Set(ByVal value As String)
            Dim _d As Boolean = False
            If _comments2 <> value Then _d = True
            If Not _ReadingData = True Then
                If _comments2 <> value Then RaiseEvent comments2Changed()
            End If
            _comments2 = value
            If _ReadingData = True Then _d = False
            _comments2_d = _d
        End Set
    End Property

    Dim _IsDirty As Boolean = False
    Public ReadOnly Property IsDirty() As Boolean
        Get
            _IsDirty = False
            _IsDirty = _IsDirty Or _surveydetail_pk_d
            _IsDirty = _IsDirty Or _creator_fk_d
            _IsDirty = _IsDirty Or _editor_fk_d
            _IsDirty = _IsDirty Or _created_d
            _IsDirty = _IsDirty Or _modified_d
            _IsDirty = _IsDirty Or _rowstatus_d
            _IsDirty = _IsDirty Or _helpdesk_id_d
            _IsDirty = _IsDirty Or _selecteduser_fk_d
            _IsDirty = _IsDirty Or _question1_surveyanswerfivescale_fk_d
            _IsDirty = _IsDirty Or _question2_surveyanswerfivescale_fk_d
            _IsDirty = _IsDirty Or _question3_surveyanswerfivescale_fk_d
            _IsDirty = _IsDirty Or _question4_surveyanswerfivescale_fk_d
            _IsDirty = _IsDirty Or _question5_surveyanswerfivescale_fk_d
            _IsDirty = _IsDirty Or _comments_d
            _IsDirty = _IsDirty Or _question6_surveyanswerfivescale_fk_d
            _IsDirty = _IsDirty Or _comments2_d
            Return _IsDirty
        End Get
    End Property



    Dim CDC As DataCommon
    Dim Row As DataRow
    Dim _CurrentUser As Integer
    Dim _ActualUser As Integer

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        _ReadingData = True
        _surveydetail_pk = -1
        _creator_fk = CurrentUser
        _editor_fk = ActualUser
        Dim _dcreated As Datetime = Nothing : CDC.ReadScalarValue(_dcreated, New sqlclient.sqlCommand("select (getdate())"))
        created = _dcreated
        Dim _dmodified As Datetime = Nothing : CDC.ReadScalarValue(_dmodified, New sqlclient.sqlCommand("select (getdate())"))
        modified = _dmodified
        _selecteduser_fk = -1
        _question1_surveyanswerfivescale_fk = -1
        _question2_surveyanswerfivescale_fk = -1
        _question3_surveyanswerfivescale_fk = -1
        _question4_surveyanswerfivescale_fk = -1
        _question5_surveyanswerfivescale_fk = -1
        _question6_surveyanswerfivescale_fk = -1
        _ReadingData = False
    End Sub

    Public Sub New(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, ByVal PK As Integer, Optional ByRef DataClass As DataCommon = Nothing)
        CDC = DataClass
        _CurrentUser = CurrentUser
        _ActualUser = ActualUser
        Read(PK)
    End Sub

    Public Function Read(ByVal PK As Integer) As Boolean
        Dim _ReturnStatus As Boolean = True
        CDC.AccessLog("survey1details", PK, _ActualUser)
        _ReadingData = True
        Try
            Dim CO As New SqlClient.SqlCommand("rkg_survey1details_read")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.Add(New SqlClient.SqlParameter("@surveydetail_pk", PK))
            Row = CDC.ReadDataRow(CO)
            CO.Dispose()
            _surveydetail_pk = CommonFN.SafeRead(Row("surveydetail_pk"))
            creator_fk = CommonFN.SafeRead(Row("creator_fk"))
            editor_fk = CommonFN.SafeRead(Row("editor_fk"))
            created = CommonFN.SafeRead(Row("created"))
            modified = CommonFN.SafeRead(Row("modified"))
            rowstatus = CommonFN.SafeRead(Row("rowstatus"))
            helpdesk_id = CommonFN.SafeRead(Row("helpdesk_id"))
            selecteduser_fk = CommonFN.SafeRead(Row("selecteduser_fk"))
            question1_surveyanswerfivescale_fk = CommonFN.SafeRead(Row("question1_surveyanswerfivescale_fk"))
            question2_surveyanswerfivescale_fk = CommonFN.SafeRead(Row("question2_surveyanswerfivescale_fk"))
            question3_surveyanswerfivescale_fk = CommonFN.SafeRead(Row("question3_surveyanswerfivescale_fk"))
            question4_surveyanswerfivescale_fk = CommonFN.SafeRead(Row("question4_surveyanswerfivescale_fk"))
            question5_surveyanswerfivescale_fk = CommonFN.SafeRead(Row("question5_surveyanswerfivescale_fk"))
            comments = CommonFN.SafeRead(Row("comments"))
            question6_surveyanswerfivescale_fk = CommonFN.SafeRead(Row("question6_surveyanswerfivescale_fk"))
            comments2 = CommonFN.SafeRead(Row("comments2"))
            Row = Nothing
        Catch Ex As Exception
            _ReturnStatus = False
            _LastError = Ex
        End Try
        _ReadingData = False
        Return _ReturnStatus
    End Function

    Dim _MatchedRecords As datatable
    Public ReadOnly Property MatchedRecords() As datatable
        Get
            If _MatchedRecords Is Nothing Then Exists()
            Return _MatchedRecords
        End Get
    End Property
    Public Function Exists() As Boolean
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_survey1details_exists"
        PO = New SqlClient.SqlParameter("@selecteduser_fk", selecteduser_fk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@question1_surveyanswerfivescale_fk", question1_surveyanswerfivescale_fk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@question2_surveyanswerfivescale_fk", question2_surveyanswerfivescale_fk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@question3_surveyanswerfivescale_fk", question3_surveyanswerfivescale_fk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@question4_surveyanswerfivescale_fk", question4_surveyanswerfivescale_fk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@question5_surveyanswerfivescale_fk", question5_surveyanswerfivescale_fk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@question6_surveyanswerfivescale_fk", question6_surveyanswerfivescale_fk)
        CO.parameters.add(PO)
        Dim _returnPK As Integer = 0
        _MatchedRecords = CDC.ReadDataTable(CO)
        _returnPK = _MatchedRecords.rows.count
        If _returnPK > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Dim _SimilarRecords As datatable
    Public ReadOnly Property SimilarRecords() As datatable
        Get
            If _SimilarRecords Is Nothing Then isUnique()
            Return _SimilarRecords
        End Get
    End Property
    Public Function IsUnique(Optional ByVal minMatchCount As Integer = 0) As Boolean
        Dim PO As New SqlClient.SqlParameter()
        Dim CO As New SqlClient.SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_survey1details_compare"
        PO = New SqlClient.SqlParameter("@surveydetail_pk", surveydetail_pk)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@comments", comments)
        CO.parameters.add(PO)
        PO = New SqlClient.SqlParameter("@comments2", comments2)
        CO.parameters.add(PO)
        If minMatchCount > 0 Then CO.Parameters.AddWithValue("@minMatchCount", minMatchCount)
        Dim _returnPK As Integer = 0
        _SimilarRecords = CDC.ReadDataTable(CO)
        _returnPK = _SimilarRecords.rows.count
        If _returnPK = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Save() As Boolean
        Dim _ReturnStatus As Boolean = True
        If IsDirty Then
            If _CurrentUser <> creator_fk And surveydetail_pk < 1 Then creator_fk = _CurrentUser : created = now()
            If _ActualUser <> editor_fk Then editor_fk = _ActualUser
            modified = now()
            Try
                CDC.BeginTransaction()
                Dim PO As SqlClient.SqlParameter
                Dim CO As New SqlClient.SqlCommand()
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rkg_survey1details_save"
                PO = Nothing
                PO = New SqlClient.SqlParameter("@surveydetail_pk", surveydetail_pk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tcreator_fk As Integer = Nothing
                If creator_fk = Nothing Then
                    CDC.ReadScalarValue(_tcreator_fk, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
                Else
                    PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _teditor_fk As Integer = Nothing
                If editor_fk = Nothing Then
                    CDC.ReadScalarValue(_teditor_fk, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
                Else
                    PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tcreated As Datetime = Nothing
                If created = Nothing Then
                    CDC.ReadScalarValue(_tcreated, New sqlclient.sqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@created", _tcreated)
                Else
                    PO = New SqlClient.SqlParameter("@created", created)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _tmodified As Datetime = Nothing
                If modified = Nothing Then
                    CDC.ReadScalarValue(_tmodified, New sqlclient.sqlCommand("select (getdate())"))
                    PO = New SqlClient.SqlParameter("@modified", _tmodified)
                Else
                    PO = New SqlClient.SqlParameter("@modified", modified)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                Dim _trowstatus As Integer = Nothing
                If rowstatus = Nothing Then
                    CDC.ReadScalarValue(_trowstatus, New sqlclient.sqlCommand("select 0"))
                    PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
                Else
                    PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
                End If
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@helpdesk_id", helpdesk_id)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@selecteduser_fk", selecteduser_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@question1_surveyanswerfivescale_fk", question1_surveyanswerfivescale_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@question2_surveyanswerfivescale_fk", question2_surveyanswerfivescale_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@question3_surveyanswerfivescale_fk", question3_surveyanswerfivescale_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@question4_surveyanswerfivescale_fk", question4_surveyanswerfivescale_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@question5_surveyanswerfivescale_fk", question5_surveyanswerfivescale_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@comments", comments)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@question6_surveyanswerfivescale_fk", question6_surveyanswerfivescale_fk)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                PO = Nothing
                PO = New SqlClient.SqlParameter("@comments2", comments2)
                If Not PO Is Nothing Then CO.parameters.add(PO)
                Dim _returnPK As Integer = 0
                If CDC.ReadScalarValue(_returnPK, CO) Then
                    Try
                        If _surveydetail_pk_d Then CDC.AuditLog("survey1details", _returnPK, "surveydetail_pk", _CurrentUser, modified, CStr(surveydetail_pk)) : _surveydetail_pk_d = False
                        If _creator_fk_d Then CDC.AuditLog("survey1details", _returnPK, "creator_fk", _CurrentUser, modified, CStr(creator_fk)) : _creator_fk_d = False
                        If _editor_fk_d Then CDC.AuditLog("survey1details", _returnPK, "editor_fk", _CurrentUser, modified, CStr(editor_fk)) : _editor_fk_d = False
                        If _created_d Then CDC.AuditLog("survey1details", _returnPK, "created", _CurrentUser, modified, CStr(created)) : _created_d = False
                        If _modified_d Then CDC.AuditLog("survey1details", _returnPK, "modified", _CurrentUser, modified, CStr(modified)) : _modified_d = False
                        If _rowstatus_d Then CDC.AuditLog("survey1details", _returnPK, "rowstatus", _CurrentUser, modified, CStr(rowstatus)) : _rowstatus_d = False
                        If _helpdesk_id_d Then CDC.AuditLog("survey1details", _returnPK, "helpdesk_id", _CurrentUser, modified, CStr(helpdesk_id)) : _helpdesk_id_d = False
                        If _selecteduser_fk_d Then CDC.AuditLog("survey1details", _returnPK, "selecteduser_fk", _CurrentUser, modified, CStr(selecteduser_fk)) : _selecteduser_fk_d = False
                        If _question1_surveyanswerfivescale_fk_d Then CDC.AuditLog("survey1details", _returnPK, "question1_surveyanswerfivescale_fk", _CurrentUser, modified, CStr(question1_surveyanswerfivescale_fk)) : _question1_surveyanswerfivescale_fk_d = False
                        If _question2_surveyanswerfivescale_fk_d Then CDC.AuditLog("survey1details", _returnPK, "question2_surveyanswerfivescale_fk", _CurrentUser, modified, CStr(question2_surveyanswerfivescale_fk)) : _question2_surveyanswerfivescale_fk_d = False
                        If _question3_surveyanswerfivescale_fk_d Then CDC.AuditLog("survey1details", _returnPK, "question3_surveyanswerfivescale_fk", _CurrentUser, modified, CStr(question3_surveyanswerfivescale_fk)) : _question3_surveyanswerfivescale_fk_d = False
                        If _question4_surveyanswerfivescale_fk_d Then CDC.AuditLog("survey1details", _returnPK, "question4_surveyanswerfivescale_fk", _CurrentUser, modified, CStr(question4_surveyanswerfivescale_fk)) : _question4_surveyanswerfivescale_fk_d = False
                        If _question5_surveyanswerfivescale_fk_d Then CDC.AuditLog("survey1details", _returnPK, "question5_surveyanswerfivescale_fk", _CurrentUser, modified, CStr(question5_surveyanswerfivescale_fk)) : _question5_surveyanswerfivescale_fk_d = False
                        If _comments_d Then CDC.AuditLog("survey1details", _returnPK, "comments", _CurrentUser, modified, CStr(comments)) : _comments_d = False
                        If _question6_surveyanswerfivescale_fk_d Then CDC.AuditLog("survey1details", _returnPK, "question6_surveyanswerfivescale_fk", _CurrentUser, modified, CStr(question6_surveyanswerfivescale_fk)) : _question6_surveyanswerfivescale_fk_d = False
                        If _comments2_d Then CDC.AuditLog("survey1details", _returnPK, "comments2", _CurrentUser, modified, CStr(comments2)) : _comments2_d = False
                    Catch Ex2 As Exception
                        _ReturnStatus = False
                        _LastError = Ex2
                    End Try
                Else
                    _ReturnStatus = False
                End If
                If _ReturnStatus Then
                    CDC.CommitTransaction()
                    Read(_returnPK)
                Else
                    CDC.RollBackTransaction()
                End If
            Catch Ex As Exception
                _ReturnStatus = False
                _LastError = Ex
                CDC.RollBackTransaction()
            End Try
        Else
            _ReturnStatus = True
        End If
        Return _ReturnStatus
    End Function

    Public Function Enable() As Boolean
        rowstatus = 0
        Return Save
    End Function

    Public Function Disable() As Boolean
        rowstatus = 1
        Return Save
    End Function

    Public Function Delete() As Boolean
        rowstatus = 2
        Return Save
    End Function


End Class
