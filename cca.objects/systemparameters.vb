Imports cca.common
Public Class systemparameters
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _systemparameter_pk as Integer

Dim _systemparameter_pk_d as Boolean=False
Public Event systemparameter_pkChanged()
Public ReadOnly Property systemparameter_pk() as Integer
	Get
		Return _systemparameter_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _customerlocation as String

Dim _customerlocation_d as Boolean=False
Public Event customerlocationChanged()
Public Property customerlocation() as String
	Get
		Return _customerlocation
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _customerlocation <> value Then _d=True
		If not _ReadingData=True Then
		If _customerlocation <> value then RaiseEvent customerlocationChanged
		End If
		_customerlocation = value
		If _ReadingData=True Then _d=False
		_customerlocation_d=_d
	End Set
End Property

Dim _ssrsdomain as String

Dim _ssrsdomain_d as Boolean=False
Public Event ssrsdomainChanged()
Public Property ssrsdomain() as String
	Get
		Return _ssrsdomain
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _ssrsdomain <> value Then _d=True
		If not _ReadingData=True Then
		If _ssrsdomain <> value then RaiseEvent ssrsdomainChanged
		End If
		_ssrsdomain = value
		If _ReadingData=True Then _d=False
		_ssrsdomain_d=_d
	End Set
End Property

Dim _ssrsusername as String

Dim _ssrsusername_d as Boolean=False
Public Event ssrsusernameChanged()
Public Property ssrsusername() as String
	Get
		Return _ssrsusername
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _ssrsusername <> value Then _d=True
		If not _ReadingData=True Then
		If _ssrsusername <> value then RaiseEvent ssrsusernameChanged
		End If
		_ssrsusername = value
		If _ReadingData=True Then _d=False
		_ssrsusername_d=_d
	End Set
End Property

Dim _ssrspassword as String

Dim _ssrspassword_d as Boolean=False
Public Event ssrspasswordChanged()
Public Property ssrspassword() as String
	Get
		Return _ssrspassword
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _ssrspassword <> value Then _d=True
		If not _ReadingData=True Then
		If _ssrspassword <> value then RaiseEvent ssrspasswordChanged
		End If
		_ssrspassword = value
		If _ReadingData=True Then _d=False
		_ssrspassword_d=_d
	End Set
End Property

Dim _ssrsurl as String

Dim _ssrsurl_d as Boolean=False
Public Event ssrsurlChanged()
Public Property ssrsurl() as String
	Get
		Return _ssrsurl
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _ssrsurl <> value Then _d=True
		If not _ReadingData=True Then
		If _ssrsurl <> value then RaiseEvent ssrsurlChanged
		End If
		_ssrsurl = value
		If _ReadingData=True Then _d=False
		_ssrsurl_d=_d
	End Set
End Property

Dim _importfolder as String

Dim _importfolder_d as Boolean=False
Public Event importfolderChanged()
Public Property importfolder() as String
	Get
		Return _importfolder
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _importfolder <> value Then _d=True
		If not _ReadingData=True Then
		If _importfolder <> value then RaiseEvent importfolderChanged
		End If
		_importfolder = value
		If _ReadingData=True Then _d=False
		_importfolder_d=_d
	End Set
End Property

Dim _matriximportfolder as String

Dim _matriximportfolder_d as Boolean=False
Public Event matriximportfolderChanged()
Public Property matriximportfolder() as String
	Get
		Return _matriximportfolder
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _matriximportfolder <> value Then _d=True
		If not _ReadingData=True Then
		If _matriximportfolder <> value then RaiseEvent matriximportfolderChanged
		End If
		_matriximportfolder = value
		If _ReadingData=True Then _d=False
		_matriximportfolder_d=_d
	End Set
End Property

Dim _datalocation as String

Dim _datalocation_d as Boolean=False
Public Event datalocationChanged()
Public Property datalocation() as String
	Get
		Return _datalocation
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _datalocation <> value Then _d=True
		If not _ReadingData=True Then
		If _datalocation <> value then RaiseEvent datalocationChanged
		End If
		_datalocation = value
		If _ReadingData=True Then _d=False
		_datalocation_d=_d
	End Set
End Property

Dim _smtpserver as String

Dim _smtpserver_d as Boolean=False
Public Event smtpserverChanged()
Public Property smtpserver() as String
	Get
		Return _smtpserver
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _smtpserver <> value Then _d=True
		If not _ReadingData=True Then
		If _smtpserver <> value then RaiseEvent smtpserverChanged
		End If
		_smtpserver = value
		If _ReadingData=True Then _d=False
		_smtpserver_d=_d
	End Set
End Property

Dim _helpdeskurl as String

Dim _helpdeskurl_d as Boolean=False
Public Event helpdeskurlChanged()
Public Property helpdeskurl() as String
	Get
		Return _helpdeskurl
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _helpdeskurl <> value Then _d=True
		If not _ReadingData=True Then
		If _helpdeskurl <> value then RaiseEvent helpdeskurlChanged
		End If
		_helpdeskurl = value
		If _ReadingData=True Then _d=False
		_helpdeskurl_d=_d
	End Set
End Property

Dim _applicationdirectory as String

Dim _applicationdirectory_d as Boolean=False
Public Event applicationdirectoryChanged()
Public Property applicationdirectory() as String
	Get
		Return _applicationdirectory
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _applicationdirectory <> value Then _d=True
		If not _ReadingData=True Then
		If _applicationdirectory <> value then RaiseEvent applicationdirectoryChanged
		End If
		_applicationdirectory = value
		If _ReadingData=True Then _d=False
		_applicationdirectory_d=_d
	End Set
End Property

Dim _intervaldatadirectory as String

Dim _intervaldatadirectory_d as Boolean=False
Public Event intervaldatadirectoryChanged()
Public Property intervaldatadirectory() as String
	Get
		Return _intervaldatadirectory
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _intervaldatadirectory <> value Then _d=True
		If not _ReadingData=True Then
		If _intervaldatadirectory <> value then RaiseEvent intervaldatadirectoryChanged
		End If
		_intervaldatadirectory = value
		If _ReadingData=True Then _d=False
		_intervaldatadirectory_d=_d
	End Set
End Property

Dim _tyrrelldatadirectory as String

Dim _tyrrelldatadirectory_d as Boolean=False
Public Event tyrrelldatadirectoryChanged()
Public Property tyrrelldatadirectory() as String
	Get
		Return _tyrrelldatadirectory
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _tyrrelldatadirectory <> value Then _d=True
		If not _ReadingData=True Then
		If _tyrrelldatadirectory <> value then RaiseEvent tyrrelldatadirectoryChanged
		End If
		_tyrrelldatadirectory = value
		If _ReadingData=True Then _d=False
		_tyrrelldatadirectory_d=_d
	End Set
End Property

Dim _helpdeskRESTAPIurl as String

Dim _helpdeskRESTAPIurl_d as Boolean=False
Public Event helpdeskRESTAPIurlChanged()
Public Property helpdeskRESTAPIurl() as String
	Get
		Return _helpdeskRESTAPIurl
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _helpdeskRESTAPIurl <> value Then _d=True
		If not _ReadingData=True Then
		If _helpdeskRESTAPIurl <> value then RaiseEvent helpdeskRESTAPIurlChanged
		End If
		_helpdeskRESTAPIurl = value
		If _ReadingData=True Then _d=False
		_helpdeskRESTAPIurl_d=_d
	End Set
End Property

Dim _helpdeskRESTAPIkey as String

Dim _helpdeskRESTAPIkey_d as Boolean=False
Public Event helpdeskRESTAPIkeyChanged()
Public Property helpdeskRESTAPIkey() as String
	Get
		Return _helpdeskRESTAPIkey
	End Get
	Set (ByVal value As String)
		Dim _d as boolean=false
		If _helpdeskRESTAPIkey <> value Then _d=True
		If not _ReadingData=True Then
		If _helpdeskRESTAPIkey <> value then RaiseEvent helpdeskRESTAPIkeyChanged
		End If
		_helpdeskRESTAPIkey = value
		If _ReadingData=True Then _d=False
		_helpdeskRESTAPIkey_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _systemparameter_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _customerlocation_d
		_IsDirty = _IsDirty OR _ssrsdomain_d
		_IsDirty = _IsDirty OR _ssrsusername_d
		_IsDirty = _IsDirty OR _ssrspassword_d
		_IsDirty = _IsDirty OR _ssrsurl_d
		_IsDirty = _IsDirty OR _importfolder_d
		_IsDirty = _IsDirty OR _matriximportfolder_d
		_IsDirty = _IsDirty OR _datalocation_d
		_IsDirty = _IsDirty OR _smtpserver_d
		_IsDirty = _IsDirty OR _helpdeskurl_d
		_IsDirty = _IsDirty OR _applicationdirectory_d
		_IsDirty = _IsDirty OR _intervaldatadirectory_d
		_IsDirty = _IsDirty OR _tyrrelldatadirectory_d
		_IsDirty = _IsDirty OR _helpdeskRESTAPIurl_d
		_IsDirty = _IsDirty OR _helpdeskRESTAPIkey_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_systemparameter_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("systemparameters",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_systemparameters_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@systemparameter_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_systemparameter_pk=CommonFN.SafeRead(Row("systemparameter_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		customerlocation=CommonFN.SafeRead(Row("customerlocation"))
		ssrsdomain=CommonFN.SafeRead(Row("ssrsdomain"))
		ssrsusername=CommonFN.SafeRead(Row("ssrsusername"))
		ssrspassword=CommonFN.SafeRead(Row("ssrspassword"))
		ssrsurl=CommonFN.SafeRead(Row("ssrsurl"))
		importfolder=CommonFN.SafeRead(Row("importfolder"))
		matriximportfolder=CommonFN.SafeRead(Row("matriximportfolder"))
		datalocation=CommonFN.SafeRead(Row("datalocation"))
		smtpserver=CommonFN.SafeRead(Row("smtpserver"))
		helpdeskurl=CommonFN.SafeRead(Row("helpdeskurl"))
		applicationdirectory=CommonFN.SafeRead(Row("applicationdirectory"))
		intervaldatadirectory=CommonFN.SafeRead(Row("intervaldatadirectory"))
		tyrrelldatadirectory=CommonFN.SafeRead(Row("tyrrelldatadirectory"))
		helpdeskRESTAPIurl=CommonFN.SafeRead(Row("helpdeskRESTAPIurl"))
		helpdeskRESTAPIkey=CommonFN.SafeRead(Row("helpdeskRESTAPIkey"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Return False
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique(optional byval minMatchCount as integer=0) as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_systemparameters_compare"
	PO = New SqlClient.SqlParameter("@systemparameter_pk", systemparameter_pk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@customerlocation", customerlocation)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@ssrsdomain", ssrsdomain)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@ssrsusername", ssrsusername)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@ssrspassword", ssrspassword)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@ssrsurl", ssrsurl)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@importfolder", importfolder)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@matriximportfolder", matriximportfolder)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@datalocation", datalocation)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@smtpserver", smtpserver)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@helpdeskurl", helpdeskurl)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@applicationdirectory", applicationdirectory)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@intervaldatadirectory", intervaldatadirectory)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@tyrrelldatadirectory", tyrrelldatadirectory)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@helpdeskRESTAPIurl", helpdeskRESTAPIurl)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@helpdeskRESTAPIkey", helpdeskRESTAPIkey)
	CO.parameters.add(PO)
	If minMatchCount>0 then CO.Parameters.AddWithValue("@minMatchCount",minMatchCount)
	Dim _returnPK As Integer = 0
	_SimilarRecords=CDC.ReadDataTable(CO)
	_returnPK=_SimilarRecords.rows.count
	If _returnPK = 0 Then
		Return True
	Else
		Return False
	End If
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and systemparameter_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_systemparameters_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@systemparameter_pk", systemparameter_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@customerlocation", customerlocation)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ssrsdomain", ssrsdomain)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ssrsusername", ssrsusername)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ssrspassword", ssrspassword)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@ssrsurl", ssrsurl)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@importfolder", importfolder)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@matriximportfolder", matriximportfolder)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@datalocation", datalocation)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@smtpserver", smtpserver)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@helpdeskurl", helpdeskurl)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@applicationdirectory", applicationdirectory)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@intervaldatadirectory", intervaldatadirectory)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@tyrrelldatadirectory", tyrrelldatadirectory)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@helpdeskRESTAPIurl", helpdeskRESTAPIurl)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@helpdeskRESTAPIkey", helpdeskRESTAPIkey)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _systemparameter_pk_d then CDC.AuditLog("systemparameters",_returnPK,"systemparameter_pk",_CurrentUser,modified,CStr(systemparameter_pk)):_systemparameter_pk_d=False
				If _creator_fk_d then CDC.AuditLog("systemparameters",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("systemparameters",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("systemparameters",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("systemparameters",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("systemparameters",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _customerlocation_d then CDC.AuditLog("systemparameters",_returnPK,"customerlocation",_CurrentUser,modified,CStr(customerlocation)):_customerlocation_d=False
				If _ssrsdomain_d then CDC.AuditLog("systemparameters",_returnPK,"ssrsdomain",_CurrentUser,modified,CStr(ssrsdomain)):_ssrsdomain_d=False
				If _ssrsusername_d then CDC.AuditLog("systemparameters",_returnPK,"ssrsusername",_CurrentUser,modified,CStr(ssrsusername)):_ssrsusername_d=False
				If _ssrspassword_d then CDC.AuditLog("systemparameters",_returnPK,"ssrspassword",_CurrentUser,modified,CStr(ssrspassword)):_ssrspassword_d=False
				If _ssrsurl_d then CDC.AuditLog("systemparameters",_returnPK,"ssrsurl",_CurrentUser,modified,CStr(ssrsurl)):_ssrsurl_d=False
				If _importfolder_d then CDC.AuditLog("systemparameters",_returnPK,"importfolder",_CurrentUser,modified,CStr(importfolder)):_importfolder_d=False
				If _matriximportfolder_d then CDC.AuditLog("systemparameters",_returnPK,"matriximportfolder",_CurrentUser,modified,CStr(matriximportfolder)):_matriximportfolder_d=False
				If _datalocation_d then CDC.AuditLog("systemparameters",_returnPK,"datalocation",_CurrentUser,modified,CStr(datalocation)):_datalocation_d=False
				If _smtpserver_d then CDC.AuditLog("systemparameters",_returnPK,"smtpserver",_CurrentUser,modified,CStr(smtpserver)):_smtpserver_d=False
				If _helpdeskurl_d then CDC.AuditLog("systemparameters",_returnPK,"helpdeskurl",_CurrentUser,modified,CStr(helpdeskurl)):_helpdeskurl_d=False
				If _applicationdirectory_d then CDC.AuditLog("systemparameters",_returnPK,"applicationdirectory",_CurrentUser,modified,CStr(applicationdirectory)):_applicationdirectory_d=False
				If _intervaldatadirectory_d then CDC.AuditLog("systemparameters",_returnPK,"intervaldatadirectory",_CurrentUser,modified,CStr(intervaldatadirectory)):_intervaldatadirectory_d=False
				If _tyrrelldatadirectory_d then CDC.AuditLog("systemparameters",_returnPK,"tyrrelldatadirectory",_CurrentUser,modified,CStr(tyrrelldatadirectory)):_tyrrelldatadirectory_d=False
				If _helpdeskRESTAPIurl_d then CDC.AuditLog("systemparameters",_returnPK,"helpdeskRESTAPIurl",_CurrentUser,modified,CStr(helpdeskRESTAPIurl)):_helpdeskRESTAPIurl_d=False
				If _helpdeskRESTAPIkey_d then CDC.AuditLog("systemparameters",_returnPK,"helpdeskRESTAPIkey",_CurrentUser,modified,CStr(helpdeskRESTAPIkey)):_helpdeskRESTAPIkey_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
