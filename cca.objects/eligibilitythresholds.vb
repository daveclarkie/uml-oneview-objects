Imports cca.common
Public Class eligibilitythresholds
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _eligibilitythreshold_pk as Integer

Dim _eligibilitythreshold_pk_d as Boolean=False
Public Event eligibilitythreshold_pkChanged()
Public ReadOnly Property eligibilitythreshold_pk() as Integer
	Get
		Return _eligibilitythreshold_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _threshold as Double

Dim _threshold_d as Boolean=False
Public Event thresholdChanged()
Public Property threshold() as Double
	Get
		Return _threshold
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _threshold <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Threshold"), CStr("Validation failed: Required Field"))
		If _threshold <> value then RaiseEvent thresholdChanged
		End If
		_threshold = value
		If _ReadingData=True Then _d=False
		_threshold_d=_d
	End Set
End Property

Dim _correctionvalue as Double

Dim _correctionvalue_d as Boolean=False
Public Event correctionvalueChanged()
Public Property correctionvalue() as Double
	Get
		Return _correctionvalue
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _correctionvalue <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Correction Value"), CStr("Validation failed: Required Field"))
		If _correctionvalue <> value then RaiseEvent correctionvalueChanged
		End If
		_correctionvalue = value
		If _ReadingData=True Then _d=False
		_correctionvalue_d=_d
	End Set
End Property

Dim _submissionfrom as Datetime

Dim _submissionfrom_d as Boolean=False
Public Event submissionfromChanged()
Public Property submissionfrom() as Datetime
	Get
		Return _submissionfrom
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _submissionfrom <> value Then _d=True
		If not _ReadingData=True Then
		If value = #12:00:00 AM# Then Throw New ArgumentOutOfRangeException(CStr("Submission From"), CStr("Validation failed: Required Field"))
		If _submissionfrom <> value then RaiseEvent submissionfromChanged
		End If
		_submissionfrom = value
		If _ReadingData=True Then _d=False
		_submissionfrom_d=_d
	End Set
End Property

Dim _submissionto as Datetime

Dim _submissionto_d as Boolean=False
Public Event submissiontoChanged()
Public Property submissionto() as Datetime
	Get
		Return _submissionto
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _submissionto <> value Then _d=True
		If not _ReadingData=True Then
		If value = #12:00:00 AM# Then Throw New ArgumentOutOfRangeException(CStr("Submission To"), CStr("Validation failed: Required Field"))
		If _submissionto <> value then RaiseEvent submissiontoChanged
		End If
		_submissionto = value
		If _ReadingData=True Then _d=False
		_submissionto_d=_d
	End Set
End Property

Dim _reportfrom as Datetime

Dim _reportfrom_d as Boolean=False
Public Event reportfromChanged()
Public Property reportfrom() as Datetime
	Get
		Return _reportfrom
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _reportfrom <> value Then _d=True
		If not _ReadingData=True Then
		If value = #12:00:00 AM# Then Throw New ArgumentOutOfRangeException(CStr("Reporting From"), CStr("Validation failed: Required Field"))
		If _reportfrom <> value then RaiseEvent reportfromChanged
		End If
		_reportfrom = value
		If _ReadingData=True Then _d=False
		_reportfrom_d=_d
	End Set
End Property

Dim _reportto as Datetime

Dim _reportto_d as Boolean=False
Public Event reporttoChanged()
Public Property reportto() as Datetime
	Get
		Return _reportto
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _reportto <> value Then _d=True
		If not _ReadingData=True Then
		If value = #12:00:00 AM# Then Throw New ArgumentOutOfRangeException(CStr("Reporting To"), CStr("Validation failed: Required Field"))
		If _reportto <> value then RaiseEvent reporttoChanged
		End If
		_reportto = value
		If _ReadingData=True Then _d=False
		_reportto_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _eligibilitythreshold_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _threshold_d
		_IsDirty = _IsDirty OR _correctionvalue_d
		_IsDirty = _IsDirty OR _submissionfrom_d
		_IsDirty = _IsDirty OR _submissionto_d
		_IsDirty = _IsDirty OR _reportfrom_d
		_IsDirty = _IsDirty OR _reportto_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_eligibilitythreshold_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("eligibilitythresholds",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_eligibilitythresholds_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@eligibilitythreshold_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_eligibilitythreshold_pk=CommonFN.SafeRead(Row("eligibilitythreshold_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		threshold=CommonFN.SafeRead(Row("threshold"))
		correctionvalue=CommonFN.SafeRead(Row("correctionvalue"))
		submissionfrom=CommonFN.SafeRead(Row("submissionfrom"))
		submissionto=CommonFN.SafeRead(Row("submissionto"))
		reportfrom=CommonFN.SafeRead(Row("reportfrom"))
		reportto=CommonFN.SafeRead(Row("reportto"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Return False
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique() as boolean
	Return True
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and eligibilitythreshold_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_eligibilitythresholds_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@eligibilitythreshold_pk", eligibilitythreshold_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@threshold", threshold)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@correctionvalue", correctionvalue)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@submissionfrom", CommonFN.CheckEmptyDate(submissionfrom.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@submissionto", CommonFN.CheckEmptyDate(submissionto.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@reportfrom", CommonFN.CheckEmptyDate(reportfrom.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@reportto", CommonFN.CheckEmptyDate(reportto.ToString()))
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _eligibilitythreshold_pk_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"eligibilitythreshold_pk",_CurrentUser,modified,CStr(eligibilitythreshold_pk)):_eligibilitythreshold_pk_d=False
				If _creator_fk_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _threshold_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"threshold",_CurrentUser,modified,CStr(threshold)):_threshold_d=False
				If _correctionvalue_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"correctionvalue",_CurrentUser,modified,CStr(correctionvalue)):_correctionvalue_d=False
				If _submissionfrom_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"submissionfrom",_CurrentUser,modified,CStr(submissionfrom)):_submissionfrom_d=False
				If _submissionto_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"submissionto",_CurrentUser,modified,CStr(submissionto)):_submissionto_d=False
				If _reportfrom_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"reportfrom",_CurrentUser,modified,CStr(reportfrom)):_reportfrom_d=False
				If _reportto_d then CDC.AuditLog("eligibilitythresholds",_returnPK,"reportto",_CurrentUser,modified,CStr(reportto)):_reportto_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
