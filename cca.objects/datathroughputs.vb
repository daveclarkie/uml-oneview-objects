Imports cca.common
Public Class datathroughputs
Dim _ReadingData as Boolean=False
Dim _LastError as Exception=Nothing

Public ReadOnly Property LastError() as Exception
	Get
		Return _LastError
	End Get
End Property

Dim _datathroughput_pk as Integer

Dim _datathroughput_pk_d as Boolean=False
Public Event datathroughput_pkChanged()
Public ReadOnly Property datathroughput_pk() as Integer
	Get
		Return _datathroughput_pk
	End Get
End Property

Dim _creator_fk as Integer

Dim _creator_fk_d as Boolean=False
Public Event creator_fkChanged()
Public Property creator_fk() as Integer
	Get
		Return _creator_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _creator_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("creator_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("creator_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _creator_fk <> value then RaiseEvent creator_fkChanged
		End If
		_creator_fk = value
		If _ReadingData=True Then _d=False
		_creator_fk_d=_d
	End Set
End Property

Dim _editor_fk as Integer

Dim _editor_fk_d as Boolean=False
Public Event editor_fkChanged()
Public Property editor_fk() as Integer
	Get
		Return _editor_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _editor_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("editor_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("editor_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _editor_fk <> value then RaiseEvent editor_fkChanged
		End If
		_editor_fk = value
		If _ReadingData=True Then _d=False
		_editor_fk_d=_d
	End Set
End Property

Dim _created as Datetime

Dim _created_d as Boolean=False
Public Event createdChanged()
Public Property created() as Datetime
	Get
		Return _created
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _created <> value Then _d=True
		If not _ReadingData=True Then
		If _created <> value then RaiseEvent createdChanged
		End If
		_created = value
		If _ReadingData=True Then _d=False
		_created_d=_d
	End Set
End Property

Dim _modified as Datetime

Dim _modified_d as Boolean=False
Public Event modifiedChanged()
Public Property modified() as Datetime
	Get
		Return _modified
	End Get
	Set (ByVal value As Datetime)
		Dim _d as boolean=false
		If _modified <> value Then _d=True
		If not _ReadingData=True Then
		If _modified <> value then RaiseEvent modifiedChanged
		End If
		_modified = value
		If _ReadingData=True Then _d=False
		_modified_d=_d
	End Set
End Property

Dim _rowstatus as Integer = 0
Dim _rowstatus_d as Boolean=False
Public Event rowstatusChanged()
Public Property rowstatus() as Integer
	Get
		Return _rowstatus
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If _rowstatus <> value Then _d=True
		If not _ReadingData=True Then
		If _rowstatus <> value then RaiseEvent rowstatusChanged
		End If
		_rowstatus = value
		If _ReadingData=True Then _d=False
		_rowstatus_d=_d
	End Set
End Property

Dim _throughput_fk as Integer

Dim _throughput_fk_d as Boolean=False
Public Event throughput_fkChanged()
Public Property throughput_fk() as Integer
	Get
		Return _throughput_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _throughput_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("throughput_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Throughput"), CStr("Validation failed: Required Field"))
		If _throughput_fk <> value then RaiseEvent throughput_fkChanged
		End If
		_throughput_fk = value
		If _ReadingData=True Then _d=False
		_throughput_fk_d=_d
	End Set
End Property

Dim _month_fk as Integer

Dim _month_fk_d as Boolean=False
Public Event month_fkChanged()
Public Property month_fk() as Integer
	Get
		Return _month_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _month_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("month_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("month_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _month_fk <> value then RaiseEvent month_fkChanged
		End If
		_month_fk = value
		If _ReadingData=True Then _d=False
		_month_fk_d=_d
	End Set
End Property

Dim _year_fk as Integer

Dim _year_fk_d as Boolean=False
Public Event year_fkChanged()
Public Property year_fk() as Integer
	Get
		Return _year_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _year_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("year_fk",value,CDC) or value=-1 Then Throw New ArgumentOutOfRangeException(CStr("Year"), CStr("Validation failed: Required Field"))
		If _year_fk <> value then RaiseEvent year_fkChanged
		End If
		_year_fk = value
		If _ReadingData=True Then _d=False
		_year_fk_d=_d
	End Set
End Property

Dim _totalthroughput as Double

Dim _totalthroughput_d as Boolean=False
Public Event totalthroughputChanged()
Public Property totalthroughput() as Double
	Get
		Return _totalthroughput
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _totalthroughput <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Total Throughput"), CStr("Validation failed: Required Field"))
		If _totalthroughput <> value then RaiseEvent totalthroughputChanged
		End If
		_totalthroughput = value
		If _ReadingData=True Then _d=False
		_totalthroughput_d=_d
	End Set
End Property

Dim _eligiblethroughput as Double

Dim _eligiblethroughput_d as Boolean=False
Public Event eligiblethroughputChanged()
Public Property eligiblethroughput() as Double
	Get
		Return _eligiblethroughput
	End Get
	Set (ByVal value As Double)
		Dim _d as boolean=false
		If _eligiblethroughput <> value Then _d=True
		If not _ReadingData=True Then
		If CStr(value).Length = 0 Then Throw New ArgumentOutOfRangeException(CStr("Eligible Throughput"), CStr("Validation failed: Required Field"))
		If _eligiblethroughput <> value then RaiseEvent eligiblethroughputChanged
		End If
		_eligiblethroughput = value
		If _ReadingData=True Then _d=False
		_eligiblethroughput_d=_d
	End Set
End Property

Dim _dataimportmethod_fk as Integer

Dim _dataimportmethod_fk_d as Boolean=False
Public Event dataimportmethod_fkChanged()
Public Property dataimportmethod_fk() as Integer
	Get
		Return _dataimportmethod_fk
	End Get
	Set (ByVal value As Integer)
		Dim _d as boolean=false
		If value=0 or value=nothing Then value=-1
		If _dataimportmethod_fk <> value Then _d=True
		If not _ReadingData=True Then
		If Not CommonFN.IsValidPk("dataimportmethod_fk",value,CDC) Then Throw New ArgumentOutOfRangeException(CStr("dataimportmethod_fk"), CStr("Validation failed: Invalid Foreign Key"))
		If _dataimportmethod_fk <> value then RaiseEvent dataimportmethod_fkChanged
		End If
		_dataimportmethod_fk = value
		If _ReadingData=True Then _d=False
		_dataimportmethod_fk_d=_d
	End Set
End Property

Dim _IsDirty as Boolean=False
Public ReadOnly Property IsDirty() as Boolean
	Get
		_IsDirty=False
		_IsDirty = _IsDirty OR _datathroughput_pk_d
		_IsDirty = _IsDirty OR _creator_fk_d
		_IsDirty = _IsDirty OR _editor_fk_d
		_IsDirty = _IsDirty OR _created_d
		_IsDirty = _IsDirty OR _modified_d
		_IsDirty = _IsDirty OR _rowstatus_d
		_IsDirty = _IsDirty OR _throughput_fk_d
		_IsDirty = _IsDirty OR _month_fk_d
		_IsDirty = _IsDirty OR _year_fk_d
		_IsDirty = _IsDirty OR _totalthroughput_d
		_IsDirty = _IsDirty OR _eligiblethroughput_d
		_IsDirty = _IsDirty OR _dataimportmethod_fk_d
		Return _IsDirty
	End Get
End Property



Dim CDC as DataCommon
Dim Row as DataRow
Dim _CurrentUser as Integer
Dim _ActualUser as Integer

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	_ReadingData=True
	_datathroughput_pk=-1
	_creator_fk=CurrentUser
	_editor_fk=ActualUser
	Dim _dcreated as Datetime = Nothing: CDC.ReadScalarValue(_dcreated, new sqlclient.sqlCommand("select (getdate())"))
	created= _dcreated
	Dim _dmodified as Datetime = Nothing: CDC.ReadScalarValue(_dmodified, new sqlclient.sqlCommand("select (getdate())"))
	modified= _dmodified
	_throughput_fk=-1
	_month_fk=-1
	_year_fk=-1
	_dataimportmethod_fk=-1
	_ReadingData=False
End Sub

Public Sub New(byval CurrentUser as Integer, byval ActualUser as integer, byval PK as Integer, Optional ByRef DataClass As DataCommon = Nothing)
		CDC = DataClass
	_CurrentUser=CurrentUser
	_ActualUser=ActualUser
	Read(PK)
End Sub

Public Function Read(byval PK as Integer) as boolean
	Dim _ReturnStatus as boolean=True
	CDC.AccessLog("datathroughputs",PK,_ActualUser)
	_ReadingData=True
	Try
		Dim CO As New SqlClient.SqlCommand("rkg_datathroughputs_read")
		CO.CommandType = CommandType.StoredProcedure
		CO.Parameters.Add(New SqlClient.SqlParameter("@datathroughput_pk", PK))
		Row=CDC.ReadDataRow(CO)
		CO.Dispose
		_datathroughput_pk=CommonFN.SafeRead(Row("datathroughput_pk"))
		creator_fk=CommonFN.SafeRead(Row("creator_fk"))
		editor_fk=CommonFN.SafeRead(Row("editor_fk"))
		created=CommonFN.SafeRead(Row("created"))
		modified=CommonFN.SafeRead(Row("modified"))
		rowstatus=CommonFN.SafeRead(Row("rowstatus"))
		throughput_fk=CommonFN.SafeRead(Row("throughput_fk"))
		month_fk=CommonFN.SafeRead(Row("month_fk"))
		year_fk=CommonFN.SafeRead(Row("year_fk"))
		totalthroughput=CommonFN.SafeRead(Row("totalthroughput"))
		eligiblethroughput=CommonFN.SafeRead(Row("eligiblethroughput"))
		dataimportmethod_fk=CommonFN.SafeRead(Row("dataimportmethod_fk"))
		Row=Nothing
	Catch Ex as Exception
	_ReturnStatus=False
	_LastError=Ex
	End Try
	_ReadingData=False
	Return _ReturnStatus
End Function

Dim _MatchedRecords as datatable
Public ReadOnly Property MatchedRecords() as datatable
	Get
		if _MatchedRecords is nothing then Exists
		return _MatchedRecords
	End Get
End Property
Public Function Exists() as boolean
	Dim PO As New SqlClient.SqlParameter()
	Dim CO As New SqlClient.SqlCommand()
	CO.CommandType = CommandType.StoredProcedure
	CO.CommandText = "rkg_datathroughputs_exists"
	PO = New SqlClient.SqlParameter("@throughput_fk", throughput_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@month_fk", month_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@year_fk", year_fk)
	CO.parameters.add(PO)
	PO = New SqlClient.SqlParameter("@dataimportmethod_fk", dataimportmethod_fk)
	CO.parameters.add(PO)
	Dim _returnPK As Integer = 0
	_MatchedRecords=CDC.ReadDataTable(CO)
	_returnPK=_MatchedRecords.rows.count
	If _returnPK > 0 Then
		Return True
	Else
		Return False
	End If
End Function


Dim _SimilarRecords as datatable
Public ReadOnly Property SimilarRecords() as datatable
	Get
		if _SimilarRecords is nothing then isUnique
		return _SimilarRecords
	End Get
End Property
Public Function IsUnique() as boolean
	Return True
End Function

Public Function Save() as boolean
	Dim _ReturnStatus as boolean=True
	If IsDirty then
		If _CurrentUser<>creator_fk and datathroughput_pk<1 then creator_fk=_CurrentUser : created=now()
		If _ActualUser<>editor_fk then editor_fk=_ActualUser
		modified=now()
		Try
			CDC.BeginTransaction
		Dim PO As SqlClient.SqlParameter
		Dim CO As New SqlClient.SqlCommand()
		CO.CommandType = CommandType.StoredProcedure
		CO.CommandText = "rkg_datathroughputs_save"
		PO=Nothing
		PO = New SqlClient.SqlParameter("@datathroughput_pk", datathroughput_pk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreator_fk as Integer  = Nothing
		If creator_fk= nothing  then
			CDC.ReadScalarValue(_tcreator_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@creator_fk", _tcreator_fk)
		Else
			PO = New SqlClient.SqlParameter("@creator_fk", creator_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _teditor_fk as Integer  = Nothing
		If editor_fk= nothing  then
			CDC.ReadScalarValue(_teditor_fk, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@editor_fk", _teditor_fk)
		Else
			PO = New SqlClient.SqlParameter("@editor_fk", editor_fk)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tcreated as Datetime  = Nothing
		If created= nothing  then
			CDC.ReadScalarValue(_tcreated, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@created", _tcreated)
		Else
			PO = New SqlClient.SqlParameter("@created", created)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _tmodified as Datetime  = Nothing
		If modified= nothing  then
			CDC.ReadScalarValue(_tmodified, new sqlclient.sqlCommand("select (getdate())"))
			PO = New SqlClient.SqlParameter("@modified", _tmodified)
		Else
			PO = New SqlClient.SqlParameter("@modified", modified)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		Dim _trowstatus as Integer  = Nothing
		If rowstatus= nothing  then
			CDC.ReadScalarValue(_trowstatus, new sqlclient.sqlCommand("select 0"))
			PO = New SqlClient.SqlParameter("@rowstatus", _trowstatus)
		Else
			PO = New SqlClient.SqlParameter("@rowstatus", rowstatus)
		End If
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@throughput_fk", throughput_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@month_fk", month_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@year_fk", year_fk)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@totalthroughput", totalthroughput)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@eligiblethroughput", eligiblethroughput)
		If not PO is nothing then CO.parameters.add(PO)
		PO=Nothing
		PO = New SqlClient.SqlParameter("@dataimportmethod_fk", dataimportmethod_fk)
		If not PO is nothing then CO.parameters.add(PO)
		Dim _returnPK As Integer = 0
		If CDC.ReadScalarValue(_returnPK, CO) Then
			Try
				If _datathroughput_pk_d then CDC.AuditLog("datathroughputs",_returnPK,"datathroughput_pk",_CurrentUser,modified,CStr(datathroughput_pk)):_datathroughput_pk_d=False
				If _creator_fk_d then CDC.AuditLog("datathroughputs",_returnPK,"creator_fk",_CurrentUser,modified,CStr(creator_fk)):_creator_fk_d=False
				If _editor_fk_d then CDC.AuditLog("datathroughputs",_returnPK,"editor_fk",_CurrentUser,modified,CStr(editor_fk)):_editor_fk_d=False
				If _created_d then CDC.AuditLog("datathroughputs",_returnPK,"created",_CurrentUser,modified,CStr(created)):_created_d=False
				If _modified_d then CDC.AuditLog("datathroughputs",_returnPK,"modified",_CurrentUser,modified,CStr(modified)):_modified_d=False
				If _rowstatus_d then CDC.AuditLog("datathroughputs",_returnPK,"rowstatus",_CurrentUser,modified,CStr(rowstatus)):_rowstatus_d=False
				If _throughput_fk_d then CDC.AuditLog("datathroughputs",_returnPK,"throughput_fk",_CurrentUser,modified,CStr(throughput_fk)):_throughput_fk_d=False
				If _month_fk_d then CDC.AuditLog("datathroughputs",_returnPK,"month_fk",_CurrentUser,modified,CStr(month_fk)):_month_fk_d=False
				If _year_fk_d then CDC.AuditLog("datathroughputs",_returnPK,"year_fk",_CurrentUser,modified,CStr(year_fk)):_year_fk_d=False
				If _totalthroughput_d then CDC.AuditLog("datathroughputs",_returnPK,"totalthroughput",_CurrentUser,modified,CStr(totalthroughput)):_totalthroughput_d=False
				If _eligiblethroughput_d then CDC.AuditLog("datathroughputs",_returnPK,"eligiblethroughput",_CurrentUser,modified,CStr(eligiblethroughput)):_eligiblethroughput_d=False
				If _dataimportmethod_fk_d then CDC.AuditLog("datathroughputs",_returnPK,"dataimportmethod_fk",_CurrentUser,modified,CStr(dataimportmethod_fk)):_dataimportmethod_fk_d=False
			Catch Ex2 as Exception
				_ReturnStatus=False
				_LastError=Ex2
			End Try
		Else
			_ReturnStatus=False
		End If
		If _ReturnStatus Then
			CDC.CommitTransaction
			Read(_returnPK)
		Else
			CDC.RollBackTransaction
		End If
		Catch Ex as Exception
			_ReturnStatus=False
			_LastError=Ex
			CDC.RollBackTransaction
		End Try
	Else
		_ReturnStatus=True
	End If
	Return _ReturnStatus
End Function

Public Function Enable() as boolean
	rowstatus = 0
	Return Save
End Function

Public Function Disable() as boolean
	rowstatus = 1
	Return Save
End Function

Public Function Delete() as boolean
	rowstatus = 2
	Return Save
End Function


End Class
